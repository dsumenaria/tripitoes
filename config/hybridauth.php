<?php
use Cake\Core\Configure;

return [
    'HybridAuth' => [
        'providers' => [
            'Google' => [
                'enabled' => true,
                'keys' => [
                    'id' => '861794848024-jcp49gjafo9hu0poqvevpse47594mmku.apps.googleusercontent.com',
                    'secret' => 'rTVmwQ1oFi-6n1gOQtn_7RSc'
                ],
                'approval_prompt' => 'force' // Force required here
            ],
            'Facebook' => [
                'enabled' => true,
                'keys' => [
                    'id' => '287740132050348',
                    'secret' => '07402a5d7e52e2835c296c06d3f771c7'
                ]
            ],
            'LinkedIn' => [
                'enabled' => true,
                'keys' => [
                    'id' => '81hcd9jf57sord',
                    'secret' => '51jxBrLxvBzhNvId'
                ]
            ]
        ],
        'debug_mode' => Configure::read('debug'),
        'debug_file' => LOGS . 'hybridauth.log',
    ]
];
?>