<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * TripitoesUsers Controller
 *
 * @property \App\Model\Table\TripitoesUsersTable $TripitoesUsers
 *
 * @method \App\Model\Entity\TripitoesUser[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class TripitoesUsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
		 

 
        parent::beforeFilter($event);
		//phpinfo(); exit;
        $this->Auth->allow(['login','index','logout']);
		//$this->Security->config('unlockedActions', 'login');
    }

    public function login() {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $redirect = $this->request->session()->read('redirect');
                return $this->redirect($redirect);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }
    public function logout() {
        return $this->redirect($this->Auth->logout());
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
		  $url =  Router::url( $this->here, true );
		   str_replace("http://","https://",$url);
		//echo"<pre>";
		//echo $this->Html->url(null, true);
		//print_r($this);
		//exit; 
        $tripitoesUsers = $this->paginate($this->TripitoesUsers);

        $this->set(compact('tripitoesUsers'));
    }

    /**
     * View method
     *
     * @param string|null $id Tripitoes User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tripitoesUser = $this->TripitoesUsers->get($id, [
            'contain' => ['Facebooks', 'Instagrams', 'Googles', 'TripitoesHotelCarts', 'TripitoesHotelLikes', 'TripitoesHotelReports', 'TripitoesHotelViews', 'TripitoesPackageCarts', 'TripitoesPackageLikes', 'TripitoesPackageReports', 'TripitoesPackageViews', 'TripitoesTaxiCarts', 'TripitoesTaxiLikes', 'TripitoesTaxiReports', 'TripitoesTaxiViews']
        ]);

        $this->set('tripitoesUser', $tripitoesUser);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		
        $tripitoesUser = $this->TripitoesUsers->newEntity();
        if ($this->request->is('post')) {
            $tripitoesUser = $this->TripitoesUsers->patchEntity($tripitoesUser, $this->request->getData());
            if ($this->TripitoesUsers->save($tripitoesUser)) {
                $this->Flash->success(__('The tripitoes user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tripitoes user could not be saved. Please, try again.'));
        }
        $facebooks = $this->TripitoesUsers->Facebooks->find('list', ['limit' => 200]);
        $instagrams = $this->TripitoesUsers->Instagrams->find('list', ['limit' => 200]);
        $googles = $this->TripitoesUsers->Googles->find('list', ['limit' => 200]);
        $this->set(compact('tripitoesUser', 'facebooks', 'instagrams', 'googles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Tripitoes User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tripitoesUser = $this->TripitoesUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tripitoesUser = $this->TripitoesUsers->patchEntity($tripitoesUser, $this->request->getData());
            if ($this->TripitoesUsers->save($tripitoesUser)) {
                $this->Flash->success(__('The tripitoes user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The tripitoes user could not be saved. Please, try again.'));
        }
        $facebooks = $this->TripitoesUsers->Facebooks->find('list', ['limit' => 200]);
        $instagrams = $this->TripitoesUsers->Instagrams->find('list', ['limit' => 200]);
        $googles = $this->TripitoesUsers->Googles->find('list', ['limit' => 200]);
        $this->set(compact('tripitoesUser', 'facebooks', 'instagrams', 'googles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Tripitoes User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tripitoesUser = $this->TripitoesUsers->get($id);
        if ($this->TripitoesUsers->delete($tripitoesUser)) {
            $this->Flash->success(__('The tripitoes user has been deleted.'));
        } else {
            $this->Flash->error(__('The tripitoes user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function submitRating()
    {
        $userid=$this->request->query('userid');
        $review=$this->request->query('review');
        $reviewConmments=$this->request->query('reviewConmments');
        $type=$this->request->query('type');
        $tripitoes_user_id = $this->Auth->User('id');

        $count=$this->TripitoesUsers->TripitoesRatings->find()->Where(['user_id'=>$userid,'type'=>$type,'tripitoes_user_id'=>$tripitoes_user_id])->count();
        if($count==0){
            $TripitoesRatings=$this->TripitoesUsers->TripitoesRatings;
            $query = $TripitoesRatings->query();
            $query->insert(['user_id', 'tripitoes_user_id','comments','rating','type'])
                ->values([
                    'user_id' => $userid,
                    'tripitoes_user_id' => $tripitoes_user_id,
                    'rating' => $review,
                    'type' => $type,
                    'comments' => $reviewConmments
                ])
                ->execute();
        }
        else{
            $Exist=$this->TripitoesUsers->TripitoesRatings->find()->Where(['user_id'=>$userid,'type'=>$type,'tripitoes_user_id'=>$tripitoes_user_id])->first();
             $update_id=$Exist['id'];

            $TripitoesRatings=$this->TripitoesUsers->TripitoesRatings;
            $query = $TripitoesRatings->query();
            $query->update();
            $query->set([
                        'user_id' => $userid,
                        'tripitoes_user_id' => $tripitoes_user_id,
                        'rating' => $review,
                        'type' => $type,
                        'comments' => $reviewConmments
                    ])
                ->where(['id'=>$update_id])
                ->execute();
        }

        //-- For OVER ALL    
        $FetRating=$this->TripitoesUsers->TripitoesRatings->find()->Where(['user_id'=>$userid,'type'=>$type])->toArray();

        $overallrating=$this->TripitoesRating->MakeRating($FetRating);

        $Users=$this->TripitoesUsers->Users;
        $UsersQuery = $Users->query();
        $UsersQuery->update();
        if($type=='Package'){
            $UsersQuery->set(['trip_package_rating' => $overallrating]);
        }
        if($type=='Hotel'){
            $UsersQuery->set(['trip_hotel_rating' => $overallrating]);
        }
        if($type=='Transport'){
            $UsersQuery->set(['trip_transport_rating' => $overallrating]);
        }
            
        $UsersQuery->where(['id'=>$userid])
            ->execute();
 
        echo "success";
        exit;
    }
}
