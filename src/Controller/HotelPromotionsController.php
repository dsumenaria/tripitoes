<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * HotelPromotions Controller
 *
 * @property \App\Model\Table\HotelPromotionsTable $HotelPromotions
 *
 * @method \App\Model\Entity\HotelPromotion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HotelPromotionsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['hotelList','filterData','citylist','citylistpopup','details','saveCall','saveShare']);
    }
    public function hotelList()
    {
        $this->viewBuilder()->setLayout('detail_layout');
        $category = @$this->request->query('category'); 
        $startPrice = @$this->request->query('startPrice'); 
        $endPrice = @$this->request->query('endPrice');  
        $saved = @$this->request->query('saved'); 
        $hotel_destination = @$this->request->query('hotel_destination'); 

        $condition=array();
         
        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->HotelPromotions->TripitoesHotelCarts->find()->where(['TripitoesHotelCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['hotel_promotion_id'];
            }
             
        }

        if(($startPrice>0) && !empty($endPrice)){
            $condition['HotelPromotions.trip_price >='] = $startPrice;
            $condition['HotelPromotions.trip_price <='] = $endPrice;
        }
         
        $orArray=array(); 
        $Filtertype='';
        $Filtervalue='';
        $FilterCountry='';
        if(!empty($hotel_destination))
        {
            $cityStateCountryArray=explode(',',$hotel_destination);
            $Filtervalue=$cityStateCountryArray[0];
            $Filtertype=$cityStateCountryArray[1];
            if($Filtertype=='City'){
                $Pcity = $this->HotelPromotions->Cities
                    ->find()->select(['state_id','name'])->where(['Cities.id' =>$Filtervalue])->first(); 
                $FilterCountry=$Pcity['state_id'];
                
                $orArray['OR'][]['HotelPromotions.city_id']=$Filtervalue;
            }
             
            if($Filtertype=='State'){
                $Fcountry = $this->HotelPromotions->Cities->States
                ->find()->select(['country_id','state_name'])->where(['States.id' => $Filtervalue])->first();
                $FilterCountry=$Fcountry['country_id']; 
                $state_name=$Fcountry['state_name'];

                $PFcity = $this->HotelPromotions->Cities
                    ->find()->select(['id'])->where(['Cities.state_id' =>$Filtervalue]);
                if(!empty($PFcity))
                {   $data_arr=array();
                    foreach($PFcity as $data)
                    {
                        $data_arr[] = $data->id;
                    }
                    $orArray['OR'][]['HotelPromotions.city_id IN']=$data_arr;
                } 
            }
        }         
 
        $limit=10;
        $page = $this->request->query('page'); 
        if(!$page){
            $page=1;
        }
        // QUERY

        $hotelPromotions=$this->HotelPromotions->find();
        $hotelPromotions->contain([
            'HotelCategories','HotelCities'=>['States'],
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','web_url','address'])
                ->contain(['Cities','HotelState','Countries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Hotel']);
                }]);
            }
        ]);
        if($category){
            $hotelPromotions->where(['HotelPromotions.hotel_category_id IN' => $category]);
        }
        else { 
            //$hotelPromotions->where(['HotelPromotions.position !='=>11]);
        }
        if(!empty($CartsId)){
            $hotelPromotions->where(['HotelPromotions.id IN' => $CartsId]);
        }
        if(!empty($condition)){
            $hotelPromotions->where($condition);
        }
        $hotelPromotions->where(['HotelPromotions.is_deleted' =>0,'HotelPromotions.share'=>1,'HotelPromotions.visible_date >=' =>date('Y-m-d')]);
        if(!empty($orArray)){
            $hotelPromotions->where($orArray);
        }
        $total_counts=$hotelPromotions->count();  
        $hotelPromotions->limit($limit)
            ->page($page)
            ->group(['HotelPromotions.id']) 
			->order(['HotelPromotions.trip_total_like' => 'DESC'])
            ->order(['Users.isVerified' =>'DESC','HotelPromotions.position' =>'ASC','HotelPromotions.id' =>'DESC'])
            ->autoFields(true); 
        $packageCategories=$this->HotelPromotions->HotelCategories->find()->where(['HotelCategories.is_deleted'=>0])->order(['HotelCategories.name'=>'ASC']);

        $socialController='HotelPromotions';
        $socialAction='hotelList';

        $countrylist=$this->HotelPromotions->States->find()->where(['States.is_deleted'=>0,'States.country_id'=>101]);
        //$FilterCountry='';
        $this->set(compact('hotelPromotions','packageCategories','total_counts','countrylist','category','socialController','socialAction','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved'));
        $this->set('_serialize', ['hotelPromotions','packageCategories','total_counts','countrylist','category','socialController','socialAction','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved']);
    } 

    public function filterData($category_id=null)
    {
        $myJSON=$this->request->query('myJSON');
        $dataArray = json_decode($myJSON, true);
        $country = $dataArray[0]['country'];  
        $city = $dataArray[0]['city'];    
        $category = $dataArray[0]['category'];
        $sellerRate = $dataArray[0]['sellerRate'];

        $duration = $dataArray[0]['duration'];   
        $hotelclass = $dataArray[0]['hotelclass'];  
        $VarifiedSeller = $dataArray[0]['VarifiedSeller'];  
        $startPricem = $dataArray[0]['startPricem'];  
        $startPrice = $dataArray[0]['startPrice'];  
        $endPricem = $dataArray[0]['endPricem']; 
        $endPrice = $dataArray[0]['endPrice']; 
        $sort = $dataArray[0]['sort']; 
        $SortByMobile = @$dataArray[0]['SortByMobile'];  
        $page_no = @$dataArray[0]['page_no']; 
        $SellerCity = @$dataArray[0]['SellerCity'];
        $SellerCityMobile = @$dataArray[0]['SellerCityMobile'];
        $SellerCityMobile2 = @$dataArray[0]['SellerCityMobile2'];
        $saved = @$dataArray[0]['saved'];
        $sellercitySearch='';
        //pr($dataArray);
        if(!empty($SellerCity)){
            $sellercitySearch= $SellerCity;
        } 
        if(!empty($SellerCityMobile)){
            $sellercitySearch= $SellerCityMobile;
        } 
        if(!empty($SellerCityMobile2)){
            $sellercitySearch= $SellerCityMobile2;
        }

        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->HotelPromotions->TripitoesHotelCarts->find()->where(['TripitoesHotelCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['hotel_promotion_id'];
            }
        }
        $dasktopsearch = @ltrim($dataArray[0]['dasktopsearch']); 
        $mobileSearchBox = @ltrim($dataArray[0]['mobileSearchBox']);
        $search_bar='';
        if(!empty($dasktopsearch)){
            $search_bar= $dasktopsearch;
        } 
        if(!empty($mobileSearchBox)){
            $search_bar= $mobileSearchBox;
        }

        $condition=array();
        $FindInState=array();
        $city_get='';
        $data_arr = [];
        $data_arr_title = [];
        $datacitywise = [];
        if($search_bar){
            $search_bar_title = null;
            
            $data_arr_title = [];
                
                $search_bar_title = $this->HotelPromotions->find()
                    ->select(['id'])
                    ->where(['hotel_name Like' =>'%'.$search_bar.'%'])
                    ->toArray();
                if(!empty($search_bar_title)) 
                {
                    foreach($search_bar_title as $data_bar)
                    {
                        $data_arr_title[] = $data_bar->id;
                    }                   
                }
                
                $search_bar_state = $this->HotelPromotions->Cities->States->find()
                ->select(['id'])
                ->where(['States.state_name Like' =>'%'.$search_bar.'%'])
                ->toArray();
                 
                 $data_arrcitye=[];
                if(!empty($search_bar_state)) 
                {
                    foreach($search_bar_state as $data_bar)
                    {
                        $data_arrcitye[] = $data_bar->id;
                    }
                    $search_bar_states = $this->HotelPromotions->Cities->find()
                    ->select(['id'])
                    ->where(['Cities.state_id IN' =>$data_arrcitye])
                    ->toArray(); 
                    foreach($search_bar_states as $data_bars)
                    {
                        $datacitywise[] = $data_bars->id;
                    }
                    if(!empty($datacitywise)){
                        $search_bar_D = $this->HotelPromotions->find()
                        ->select(['id'])
                        ->where(['HotelPromotions.city_id IN' =>$datacitywise])
                        ->toArray();
                        if(!empty($search_bar_D)) 
                        {
                            foreach($search_bar_D as $data_bar)
                            {
                                $data_arr_title[] = $data_bar->id;
                            }                   
                        }
                    }
                }
                $search_bar_city = $this->HotelPromotions->Cities->find()
                ->select(['id'])
                ->where(['Cities.name Like' =>'%'.$search_bar.'%'])
                ->toArray();
                $datacitywise=array(); 
                foreach($search_bar_city as $citywst)
                {
                    $datacitywise[] = $citywst->id;
                }
                if(!empty($datacitywise)){
                    $search_bar_D = $this->HotelPromotions->find()
                    ->select(['id'])
                    ->where(['HotelPromotions.city_id IN' =>$datacitywise])
                    ->toArray();
                    if(!empty($search_bar_D)) 
                    {
                        foreach($search_bar_D as $data_bar)
                        {
                            $data_arr_title[] = $data_bar->id;
                        }                   
                    }
                }
            if(!empty($data_arr_title)){
                $data_arr= ['HotelPromotions.id IN' =>$data_arr_title];
            }else{
                 $data_arr= ['HotelPromotions.id IN' =>''];
            }
                       
        } 
          
        $OrRating=array();
        if(!empty($sellerRate)){
            $fiveRange=array();
            $fourRange=array();
            $threeRange=array();
            $twoRange=array();
            $oneRange=array(); 
            $OrRating['OR']=array();;
            foreach ($sellerRate as $dataFetch) {
                if($dataFetch==5){
                    $OrRating['OR'][]['Users.trip_hotel_rating']='5';
                }
                if($dataFetch==4){
                    $OrRating['OR'][]=array('Users.trip_hotel_rating <=' => '4.9' , 'Users.trip_hotel_rating >=' => '4.0');
                }
                if($dataFetch==3){
                   $OrRating['OR'][]=array('Users.trip_hotel_rating <=' => '3.9' , 'Users.trip_hotel_rating >=' => '3.0');
                     
                }
                if($dataFetch==2){
                    $OrRating['OR'][]=array('Users.trip_hotel_rating <=' => '2.9' , 'Users.trip_hotel_rating >=' => '2.0');
                 }
                if($dataFetch==1){
                    $OrRating['OR'][]=array('Users.trip_hotel_rating <=' => '1.9' , 'Users.trip_hotel_rating >=' => '1.0');
                 }
            }
        }
        if(!empty($sellercitySearch)){
            $city_get=$sellercitySearch;
        } 
         
        if(!empty($hotelclass)){ 
            $condition['HotelPromotions.hotel_rating IN'] = $hotelclass;
        } 

        if(!empty($VarifiedSeller)){
            $condition['Users.isVerified'] = $VarifiedSeller;
        }

        if(!empty($startPrice) && !empty($endPrice)){
            $condition['HotelPromotions.trip_price >='] = $startPrice;
            $condition['HotelPromotions.trip_price <='] = $endPrice;
        }
        if(!empty($startPricem) && !empty($endPricem)){
            $condition['HotelPromotions.trip_price >='] = $startPricem;
            $condition['HotelPromotions.trip_price <='] = $endPricem;
        }

        //-- OTher Table FIlter
        $cityFilter=null;
        if(!empty($country))
        { 
            $states=$this->HotelPromotions->Cities->find()->select(['id','name'])->where(['Cities.state_id IN'=>$country]);
            
            $cityArray=array();
            foreach ($states as $values) {
               $cityArray[]=$values->id;
            }
            $cityFilter['OR'][]['HotelPromotions.city_id IN']=$cityArray;
        }
         
        if(!empty($city))
        { 
            $cities=$this->HotelPromotions->Cities->find()->select(['name','id'])->where(['Cities.id IN'=>$city]);
            $Ddate=array();
            foreach ($cities as $value) {
               $cityFilter['OR'][]['HotelPromotions.city_id IN']=$city;
            }
        }
        $categoryFilter=null;
        if(!empty($category))
        {
            if (in_array("999", $category))
            {   
                if(sizeof($category)>1){ 
                    $categoryFilter = ['HotelPromotions.hotel_category_id IN'=>$category];
                }
                else{  
                     $categoryFilter = '';
                } 
                
            }
            else if (in_array("0", $category)){
                 $categoryFilter = ['HotelPromotions.position !='=>11];
            }
            else{
                 $categoryFilter = ['HotelPromotions.hotel_category_id IN'=>$category];
            }
        }
        else {
             $categoryFilter = '';//['HotelPromotions.position !='=>11];
        }
        //pr($categoryFilter);
        //-- Sorting
        $where_short=[];
        if(!empty($sort)){
            if($sort=='like_H2L'){
                $where_short = ['HotelPromotions.trip_total_like' => 'DESC'];
            }
            if($sort=='view_H2L'){
                $where_short = ['HotelPromotions.trip_views' => 'DESC']; 
            }
            if($sort=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC']; 
            }
            if($sort=='price_L2H'){
                $where_short = ['HotelPromotions.trip_price' => 'ASC'];
            }
        }
        if(!empty($SortByMobile)){
            if($SortByMobile=='like_H2L'){
                $where_short = ['HotelPromotions.trip_total_like' => 'DESC'];
            }
            if($SortByMobile=='view_H2L'){
                $where_short = ['HotelPromotions.trip_views' => 'DESC']; 
            }
            if($SortByMobile=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC'];
            }
            if($SortByMobile=='price_L2H'){
                $where_short = ['HotelPromotions.trip_price' => 'ASC'];
            }
        }
        //-- Shoting

        // QUERY
        $limit=10;
        if(empty($page_no)){
           $page_no=1; 
        }
 
        $hotelPromotions=$this->HotelPromotions->find();
        $hotelPromotions->contain([
            'HotelCategories','HotelCities'=>['States'],
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','city_id','state_id','web_url','address'])
                ->contain(['Cities','HotelState','Countries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Hotel']);
                }]);
            }
        ]); 
        if($categoryFilter){
            $hotelPromotions->where($categoryFilter);
        } 
        if($cityFilter){
            $hotelPromotions->where($cityFilter);
        } 
          
        if(!empty($CartsId)){
            $hotelPromotions->where(['HotelPromotions.id IN' => $CartsId]);
        }  
        $hotelPromotions->where([            
            'HotelPromotions.is_deleted' =>0,
            'HotelPromotions.share'=>1,
            'HotelPromotions.visible_date >=' =>date('Y-m-d')
        ]);
        if(!empty($condition)){
            $hotelPromotions->where($condition);
        }
        if(!empty($OrRating)){
            $hotelPromotions->where($OrRating);  
        }
         
        if(!empty($data_arr)){
            $hotelPromotions->where($data_arr);  
        }

        if(!empty($city_get)){
           $hotelPromotions->where(['Users.city_id' =>$city_get]);
        }
        if(!empty($FindInState)){
           $hotelPromotions->where(['Users.state_id IN' =>$FindInState]);
        } 
 
        $hotelPromotions->group(['HotelPromotions.id'])
            ->order($where_short)
            ->order(['Users.isVerified' =>'DESC','HotelPromotions.position' =>'ASC','HotelPromotions.id' =>'DESC'])
            ->autoFields(true); 
        $total_counts=$hotelPromotions->count();
        $hotelPromotions->limit($limit)->page($page_no) ;
        $this->set(compact('hotelPromotions','total_counts'));   
    }

    public function citylist()
    {
        if ($this->request->is(['ajax']))
        {
           $countries=$this->request->query('countries');
           if(!empty($countries)){     
               $countryList=explode(',',$countries);
                
               $cities=$this->HotelPromotions->Cities->find()->where(['Cities.state_id IN' => $countryList]);
               echo '<ul id="list-2">';
               foreach ($cities as $value) {
                   echo "<li class='city_list FiterData' type='city' value='".$value->id."'>".$value->name."</li>";
               }
               echo"</ul>";
           }
        }
        exit;
    }
   
    public function citylistpopup()
    {
        if ($this->request->is(['ajax']))
        {
           $countries=$this->request->query('countries'); 
           if(!empty($countries)){ 
               $countryList=explode(',',$countries);
                
               $cities=$this->HotelPromotions->Cities->find()->where(['Cities.state_id IN' => $countryList]);
               echo '<ul id="list-m-2">';
               foreach ($cities as $value) {
                   echo "<li class='city_list' type='city' value='".$value->id."'>".$value->name."</li>";
               }
               echo"</ul>";
           }
        }
        exit;
    }

    public function details($id = null)
    {
        $this->viewBuilder()->setLayout('detail_layout');
        //-- Query
        $hotelPromotionsData=$this->HotelPromotions->find()->contain([
            'HotelCategories','HotelCities'=>['States'],
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','web_url','address','company_img_1_pic','company_img_2_pic'])
                ->contain(['Cities','HotelState','Countries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Hotel'])->contain(['TripitoesUsers'=>['SocialProfiles']]);
                }]);
            }
        ]);
        $hotelPromotionsData->where([            
            'HotelPromotions.id' =>$id,
            'HotelPromotions.is_deleted' =>0,
            'HotelPromotions.share'=>1,
            'HotelPromotions.visible_date >=' =>date('Y-m-d')
        ]);
        $Existornot=$hotelPromotionsData->count();
        if($Existornot==0){
            return $this->redirect(['action' => 'hotelList']);
        }
        $hotelPromotionsData= $hotelPromotionsData->first();

        $Existcount=$hotelPromotionsData->trip_views;
        $hotelPromotionsData->trip_views=$Existcount+1;
        $this->HotelPromotions->save($hotelPromotionsData);
        //--- Features Hotel 
        $where_short=['Users.isVerified' =>'DESC','HotelPromotions.position' =>'ASC','HotelPromotions.id' =>'DESC'];
        $hotelPromotions=$this->HotelPromotions->find();
        $hotelPromotions->select(['id','trip_total_like'])
        ->contain(['HotelCategories','Cities'=>['States'],'Users'=>function($q){
                    return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings']);;
        }])
        ->where(['HotelPromotions.visible_date >=' =>date('Y-m-d')])
        ->where(['HotelPromotions.position !=' =>'11'])
        ->where(['HotelPromotions.is_deleted' =>0,'HotelPromotions.share'=>1,'HotelPromotions.visible_date >=' =>date('Y-m-d')])
        ->group(['HotelPromotions.id']) 
        ->order($where_short)
        ->autoFields(true);


        $socialController='HotelPromotions';
        $socialAction='details/'.$id; 
 
        $this->set('hotelPromotions', $hotelPromotions);
        $this->set('hotelPromotionsData', $hotelPromotionsData);
        $this->set('socialController', $socialController);
        $this->set('socialAction', $socialAction);
    }
    

    public function hotelLike($hotel_promotion_id,$tripitoes_user_id)
    {
        $tripitoesHotelLikes=$this->HotelPromotions->TripitoesHotelLikes;
        $query = $tripitoesHotelLikes->query();
        $query->insert(['hotel_promotion_id', 'tripitoes_user_id'])
            ->values([
                'hotel_promotion_id' => $hotel_promotion_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $hotelPromotions = $this->HotelPromotions->get($hotel_promotion_id);
        $hotelPromotions->trip_total_like+=1;
        $hotelPromotions = $this->HotelPromotions->save($hotelPromotions);
        echo $hotelPromotions->trip_total_like;
        exit;
    }

    public function hotelSave($hotel_promotion_id,$tripitoes_user_id)
    {
        $tripitoesHotelCarts=$this->HotelPromotions->TripitoesHotelCarts;
        $query = $tripitoesHotelCarts->query();
        $query->insert(['hotel_promotion_id', 'tripitoes_user_id'])
            ->values([
                'hotel_promotion_id' => $hotel_promotion_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $tripitoesUsers = $this->HotelPromotions->TripitoesUsers->get($tripitoes_user_id);
        $tripitoesUsers->total_saved+=1;
        $tripitoesUsers = $this->HotelPromotions->TripitoesUsers->save($tripitoesUsers);
        echo $tripitoesUsers->total_saved;
        exit;
    }

    public function hotelReport()
    {
        $hotelid=$this->request->query('hotelid');
        $report=$this->request->query('report');
        $tripitoes_user_id = $this->Auth->User('id');

        $tripitoesHotelReports=$this->HotelPromotions->TripitoesHotelReports;
        $query = $tripitoesHotelReports->query();
        $query->insert(['hotel_promotion_id', 'tripitoes_user_id','reason'])
            ->values([
                'hotel_promotion_id' => $hotelid,
                'tripitoes_user_id' => $tripitoes_user_id,
                'reason' => $report
            ])
            ->execute();
        echo "success";
        exit;
    }
    public function saveCall($id=null){
        $hotelPromotions = $this->HotelPromotions->get($id);
        $hotelPromotions->trip_calls+=1;
        $hotelPromotions = $this->HotelPromotions->save($hotelPromotions);
        exit;
    }
    public function saveShare($id=null){
        $hotelPromotions = $this->HotelPromotions->get($id);
        $hotelPromotions->trip_shares+=1;
        $hotelPromotions = $this->HotelPromotions->save($hotelPromotions);
        exit;
    }

}
