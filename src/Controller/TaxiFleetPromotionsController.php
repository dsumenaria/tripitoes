<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
/**
 * TaxiFleetPromotions Controller
 *
 * @property \App\Model\Table\TaxiFleetPromotionsTable $TaxiFleetPromotions
 *
 * @method \App\Model\Entity\TaxiFleetPromotion[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TaxiFleetPromotionsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['transportList','filterData','details','saveCall','saveShare']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function transportList($category=null)
    {
        $this->viewBuilder()->setLayout('detail_layout');
        $category = @$this->request->query('category'); 
        $startPrice = @$this->request->query('startPrice'); 
        $endPrice = @$this->request->query('endPrice');
        $pickup_city = @$this->request->query('pickup_city'); 
        $saved = @$this->request->query('saved'); 
        $destination_city = @$this->request->query('destination_city'); 

        $condition=array();
         
        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->TaxiFleetPromotions->TripitoesTaxiCarts->find()->where(['TripitoesTaxiCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['taxi_fleet_promotion_id'];
            }
             
        } 
        //pr($condition);
        $data_arr=array(); 
        $CompanyCity=array(); 
        $Filtertype='';
        $Filtervalue='';
        $FilterCountry='';
        if(!empty($pickup_city)){
            $CompanyCity['Users.city_id'] =$pickup_city;
        }  

        if(!empty($destination_city)){
            $Filtervalue=$destination_city;
            $Filtertype='City';
            if($Filtertype=='City'){
                $Pcity = $this->TaxiFleetPromotions->TaxiFleetPromotionCities->Cities
                    ->find()->select(['state_id'])->where(['Cities.id' =>$Filtervalue])->first(); 
                $FilterCountry=$Pcity['state_id'];
                $search_bar_city_data = $this->TaxiFleetPromotions->TaxiFleetPromotionCities->find()
                ->select(['taxi_fleet_promotion_id'])->where(['TaxiFleetPromotionCities.city_id' =>$Filtervalue])->toArray();
                
                if(!empty($search_bar_city_data))
                {
                    foreach($search_bar_city_data as $data)
                    {
                        $data_arr[] = $data->taxi_fleet_promotion_id;
                    }
                }
            }
            if(!empty($data_arr)){
                $condition['TaxiFleetPromotions.id IN'] =$data_arr;
            }
        }
         
        $limit=10;
        $page = $this->request->query('page'); 
        if(!$page){
            $page=1;
        }
        // QUERY
 
        $taxiFleetPromotions=$this->TaxiFleetPromotions->find();
        $taxiFleetPromotions->contain([
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','web_url','address','city_id','state_id'])
                ->contain(['Cities','States','Countries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Transport'])->contain(['TripitoesUsers'=>['SocialProfiles']]);
                }]);
            }
            ,'TaxiFleetPromotionCities'=>['Cities'=>['States']]
            ,'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']
            ,'TaxiFleetPromotionStates'=>['States']
        ]);
        if($category){
            $taxiFleetPromotions->matching('TaxiFleetPromotionRows', function ($q) use($category) {
                return $q->where(['taxi_fleet_car_bus_id'=>$category]);
            });
        }
        else{
            //$taxiFleetPromotions->where(['TaxiFleetPromotions.position !=' =>11]);
        }

        if($CompanyCity){
            $taxiFleetPromotions->where(['Users.city_id' =>$pickup_city]);
        }

        if(!empty($CartsId)){
            $taxiFleetPromotions->where(['TaxiFleetPromotions.id IN' => $CartsId]);
        }
        $taxiFleetPromotions->where(['TaxiFleetPromotions.is_deleted' =>0,'TaxiFleetPromotions.share'=>1,'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d'),$condition]);

        $total_counts=$taxiFleetPromotions->count();
        $taxiFleetPromotions->limit($limit)
        ->page($page)
        ->group(['TaxiFleetPromotions.id']) 
        ->order(['TaxiFleetPromotions.trip_total_like' => 'DESC'])
        ->order(['Users.isVerified' =>'DESC','TaxiFleetPromotions.position' =>'ASC','TaxiFleetPromotions.id' =>'DESC'])
        ->autoFields(true);
             
        $packageCategories=$this->TaxiFleetPromotions->TaxiFleetPromotionRows->TaxiFleetCarBuses->find()->where(['TaxiFleetCarBuses.is_deleted'=>0])->order(['TaxiFleetCarBuses.name'=>'ASC']);

        $socialController='TaxiFleetPromotions';
        $socialAction='transportList';
        $countrylist=$this->TaxiFleetPromotions->TaxiFleetPromotionStates->States->find()->where(['States.is_deleted'=>0,'country_id'=>101]);
        $this->set(compact('taxiFleetPromotions','packageCategories','total_counts','countrylist','category','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved','socialController','socialAction'));
        $this->set('_serialize', ['taxiFleetPromotions','packageCategories','total_counts','countrylist','category','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved','socialController','socialAction']);
    } 
    
    public function filterData($category_id=null)
    {
        $myJSON=$this->request->query('myJSON');
        $dataArray = json_decode($myJSON, true);
        $country = $dataArray[0]['country'];  
        $city = $dataArray[0]['city'];    
        $category = $dataArray[0]['category'];
        $sellerRate = $dataArray[0]['sellerRate'];

        $duration = $dataArray[0]['duration'];   
        $hotelclass = $dataArray[0]['hotelclass'];  
        $VarifiedSeller = $dataArray[0]['VarifiedSeller'];  
        $startPricem = $dataArray[0]['startPricem'];  
        $startPrice = $dataArray[0]['startPrice'];  
        $endPricem = $dataArray[0]['endPricem']; 
        $endPrice = $dataArray[0]['endPrice'];   
        $sort = $dataArray[0]['sort'];  
        $SortByMobile = @$dataArray[0]['SortByMobile'];  
        $page_no = @$dataArray[0]['page_no'];  
        $saved = @$dataArray[0]['saved'];
        //pr($dataArray);
        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->TaxiFleetPromotions->TripitoesTaxiCarts->find()->where(['TripitoesTaxiCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['taxi_fleet_promotion_id'];
            }
             
        }
        $dasktopsearch = @ltrim($dataArray[0]['dasktopsearch']); 
        $mobileSearchBox = @ltrim($dataArray[0]['mobileSearchBox']);
        $search_bar='';
        if(!empty($dasktopsearch)){
            $search_bar= $dasktopsearch;
        } 
        if(!empty($mobileSearchBox)){
            $search_bar= $mobileSearchBox;
        }
        $condition=array();
        $FindInState=array();
        $city_get='';
        $search_bar_title = null;
        if($search_bar){
            $data_arr = [];
            $data_arr_title = [];
                
            // $search_bar_title = $this->TaxiFleetPromotions->find()
            //     ->select(['id'])
            //     ->where(['title Like' =>'%'.$search_bar.'%'])
            //     ->toArray();
            //     pr($search_bar_title->toArray()); 
            // if(!empty($search_bar_title)) 
            // {
            //     foreach($search_bar_title as $data_bar)
            //     {
            //         $data_arr_title[] = $data_bar->id;
            //     }                   
            // }
            
            $search_bar_state = $this->TaxiFleetPromotions->TaxiFleetPromotionStates->States
            ->find()->select(['id'])->where(['States.state_name Like' =>'%'.$search_bar.'%']);
            
            if(!empty($search_bar_state)) 
            {
                $search_bar_cty = $this->TaxiFleetPromotions->TaxiFleetPromotionStates
                ->find()->select(['id','taxi_fleet_promotion_id'])->where(['TaxiFleetPromotionStates.state_id IN' =>$search_bar_state]);
                
                if(!empty($search_bar_cty)) 
                {
                    foreach($search_bar_cty as $data)
                    {
                        $data_arr[] = $data->taxi_fleet_promotion_id;
                    }
                }
            }
            
            $search_bar_city = $this->TaxiFleetPromotions->TaxiFleetPromotionCities->Cities
            ->find()->select(['id'])->where(['Cities.name Like' =>'%'.$search_bar.'%']);

            if(!empty($search_bar_city)) 
            { 

                $search_bar_city_data = $this->TaxiFleetPromotions->TaxiFleetPromotionCities->find()
                ->select(['taxi_fleet_promotion_id'])->where(['TaxiFleetPromotionCities.city_id IN' =>$search_bar_city])->toArray();

                if(!empty($search_bar_city_data))
                {
                    foreach($search_bar_city_data as $data)
                    {
                        $data_arr[] = $data->taxi_fleet_promotion_id;
                    }
                }
            }

            $search_bar_citys = $this->TaxiFleetPromotions->TaxiFleetPromotionRows->TaxiFleetCarBuses
            ->find()->select(['id'])->where(['TaxiFleetCarBuses.name Like' =>'%'.$search_bar.'%']);
                   
            if(!empty($search_bar_citys)) 
            { 

                $search_bar_city_datas = $this->TaxiFleetPromotions->TaxiFleetPromotionRows->find()
                ->select(['taxi_fleet_promotion_id'])->where(['TaxiFleetPromotionRows.taxi_fleet_car_bus_id IN' =>$search_bar_citys])->toArray();

                if(!empty($search_bar_city_datas))
                {
                    foreach($search_bar_city_datas as $data)
                    {
                        $data_arr[] = $data->taxi_fleet_promotion_id;
                    }
                }
            }
            
            $search_bar_title = array_merge($data_arr_title,$data_arr);

            if(!empty($search_bar_title)){
            $search_bar_title = ['TaxiFleetPromotions.id IN' =>$search_bar_title];
            }else
            {
                $search_bar_title = ['TaxiFleetPromotions.id IN' =>''];
            }    

        }
             
        $OrRating=array();
        if(!empty($sellerRate)){
            $fiveRange=array();
            $fourRange=array();
            $threeRange=array();
            $twoRange=array();
            $oneRange=array(); 
            $OrRating['OR']=array();;
            foreach ($sellerRate as $dataFetch) {
                if($dataFetch==5){
                    $OrRating['OR'][]['Users.trip_transport_rating']='5';
                }
                if($dataFetch==4){
                    $OrRating['OR'][]=array('Users.trip_transport_rating <=' => '4.9' , 'Users.trip_transport_rating >=' => '4.0');
                }
                if($dataFetch==3){
                   $OrRating['OR'][]=array('Users.trip_transport_rating <=' => '3.9' , 'Users.trip_transport_rating >=' => '3.0');
                }
                if($dataFetch==2){
                    $OrRating['OR'][]=array('Users.trip_transport_rating <=' => '2.9' , 'Users.trip_transport_rating >=' => '2.0');
                 }
                if($dataFetch==1){
                    $OrRating['OR'][]=array('Users.trip_transport_rating <=' => '1.9' , 'Users.trip_transport_rating >=' => '1.0');
                 }
            }
        } 
         //pr($OrRating);
        if(!empty($VarifiedSeller)){
            $condition['Users.isVerified'] = $VarifiedSeller;
        }
 
        //-- OTher Table FIlter
        $countryFilter=null;
        if(!empty($country))
        { 
            $countryFilter = ['TaxiFleetPromotionStates.state_id IN' =>$country];
        }
        $cityFilter=null;
        if(!empty($city))
        { 
            $cityFilter = ['city_id IN'=>$city];
        }
        $categoryFilter=null;
        $postiion=null;
        if(sizeof($category)==1)
        {
            if (in_array("999", $category))
            {    
                $categoryFilter = '';
            }
            else if($category[0]=='0'){
                $postiion['TaxiFleetPromotions.position !=']=11;  
            }
            else{
                 $categoryFilter = ['taxi_fleet_car_bus_id IN'=>$category];
            }
        }
        else{
            $categoryFilter = ['taxi_fleet_car_bus_id IN'=>$category]; 
        }
       
        //-- Sorting
        $where_short=[]; 
        if(!empty($sort)){
            if($sort=='like_H2L'){
                $where_short = ['TaxiFleetPromotions.trip_total_like' => 'DESC'];
            }
            if($sort=='view_H2L'){
                $where_short = ['TaxiFleetPromotions.trip_views' => 'DESC']; 
            }
            if($sort=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC']; 
            } 
        }
        if(!empty($SortByMobile)){
            if($SortByMobile=='like_H2L'){
                $where_short = ['TaxiFleetPromotions.trip_total_like' => 'DESC'];
            }
            if($SortByMobile=='view_H2L'){
                $where_short = ['TaxiFleetPromotions.trip_views' => 'DESC']; 
            }
            if($SortByMobile=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC'];
            }
             
        }
        //-- Shoting

        // QUERY
        $limit=10;
        if(empty($page_no)){
           $page_no=1; 
        }
 
        $taxiFleetPromotions=$this->TaxiFleetPromotions->find();
        $taxiFleetPromotions->contain([
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','web_url','address'])
                ->contain(['Cities','States','Countries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Transport'])->contain(['TripitoesUsers'=>['SocialProfiles']]);
                }]);
            }
            ,'TaxiFleetPromotionCities'=>['Cities'=>['States']]
            ,'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']
            ,'TaxiFleetPromotionStates'=>['States']
        ]);

        if($categoryFilter){
            $taxiFleetPromotions->matching('TaxiFleetPromotionRows', function ($q) use($categoryFilter) {
                return $q->where($categoryFilter);
            });
        }
        if($cityFilter){
            $taxiFleetPromotions->matching('TaxiFleetPromotionCities', function ($q) use($cityFilter) {
                return $q->where($cityFilter);
            });
        }
        if($countryFilter){
            $taxiFleetPromotions->matching('TaxiFleetPromotionStates', function ($q)use($countryFilter){ 
                return $q->where($countryFilter); 
            });
        }  
        if(!empty($CartsId)){
            $taxiFleetPromotions->where(['TaxiFleetPromotions.id IN' => $CartsId]);
        }
        $taxiFleetPromotions->where([            
            'TaxiFleetPromotions.is_deleted' =>0,
            'TaxiFleetPromotions.share'=>1,
            'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d')
        ]);
        if(!empty($condition)){
         $taxiFleetPromotions->where($condition);  
        }
        if(!empty($postiion)){
         $taxiFleetPromotions->where($postiion);  
        }
        //- Rating

        if(!empty($OrRating)){
         $taxiFleetPromotions->where($OrRating);  
        }
 
        //-- Rate 
        //pr($search_bar_title);
        if(!empty($search_bar_title)){
           $taxiFleetPromotions->where($search_bar_title);
        }
        $socialController='TaxiFleetPromotions';
        $socialAction='transportList'; 
        $taxiFleetPromotions->group(['TaxiFleetPromotions.id'])
            ->order($where_short)
            ->order(['Users.isVerified' =>'DESC','TaxiFleetPromotions.position' =>'ASC','TaxiFleetPromotions.id' =>'DESC'])
            ->autoFields(true);
        //pr($taxiFleetPromotions->toArray());
        $total_counts=$taxiFleetPromotions->count();
        $taxiFleetPromotions->limit($limit)->page($page_no); 
        $this->set(compact('taxiFleetPromotions','total_counts','socialController','socialAction'));   
    }

    public function details($id = null)
    {
        $this->viewBuilder()->setLayout('detail_layout');
        //-- Increment View Count
        $Existcount=0;        
        $taxiFleetPromotion=$this->TaxiFleetPromotions->find()->contain(['Countries','Users'=>['Cities','States','Countries','TripitoesRatings'=>function($q){
                return $q->where(['TripitoesRatings.type'=>'Transport'])->contain(['TripitoesUsers'=>['SocialProfiles']]);
            }],
            'TaxiFleetPromotionCities'=>['Cities'],
            'TaxiFleetPromotionStates'=>['States'], 
            'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses'], 
        ])
        ->where([            
            'TaxiFleetPromotions.id' =>$id,
            'TaxiFleetPromotions.is_deleted' =>0,
            'TaxiFleetPromotions.share'=>1,
            'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d')
        ]);
        $Existornot=$taxiFleetPromotion->count();
        if($Existornot==0){
            return $this->redirect(['action' => 'transportList']);
        }
        $taxiFleetPromotion= $taxiFleetPromotion->first();
        $Existcount=$taxiFleetPromotion->trip_views;

        $taxiFleetPromotion->trip_views=$Existcount+1;
        $this->TaxiFleetPromotions->save($taxiFleetPromotion);

        //-- Featured Packages
        $where_short=['Users.isVerified' =>'DESC','TaxiFleetPromotions.position' =>'ASC','TaxiFleetPromotions.id' =>'DESC'];
        $taxiFleetPromotions=$this->TaxiFleetPromotions->find()
          ->contain(['Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Transport']);
                }]);
            },'TaxiFleetPromotionStates'=>['States'],'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']])
          ->where(['TaxiFleetPromotions.is_deleted' =>0,'TaxiFleetPromotions.share'=>1,'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d'),'TaxiFleetPromotions.position !='=>11])
          ->group(['TaxiFleetPromotions.id'])
          ->order($where_short)
          ->autoFields(true);
         
        $socialController='TaxiFleetPromotions';
        $socialAction='details/'.$id; 
        $this->set('taxiFleetPromotion', $taxiFleetPromotion);
        $this->set('taxiFleetPromotions', $taxiFleetPromotions);
        $this->set('socialController', $socialController);
        $this->set('socialAction', $socialAction);
        $this->set('t_first', 't_first');
    }

    public function transportLike($taxi_fleet_promotion_id,$tripitoes_user_id)
    {
        $tripitoesTaxiLikes=$this->TaxiFleetPromotions->TripitoesTaxiLikes;
        $query = $tripitoesTaxiLikes->query();
        $query->insert(['taxi_fleet_promotion_id', 'tripitoes_user_id'])
            ->values([
                'taxi_fleet_promotion_id' => $taxi_fleet_promotion_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $taxiFleetPromotions = $this->TaxiFleetPromotions->get($taxi_fleet_promotion_id);
        $taxiFleetPromotions->trip_total_like+=1;
        $taxiFleetPromotions = $this->TaxiFleetPromotions->save($taxiFleetPromotions);
        echo $taxiFleetPromotions->trip_total_like;
        exit;
    }
    public function transportSave($taxi_fleet_promotion_id,$tripitoes_user_id)
    {
        $tripitoesTaxiCarts=$this->TaxiFleetPromotions->TripitoesTaxiCarts;
        $query = $tripitoesTaxiCarts->query();
        $query->insert(['taxi_fleet_promotion_id', 'tripitoes_user_id'])
            ->values([
                'taxi_fleet_promotion_id' => $taxi_fleet_promotion_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $tripitoesUsers = $this->TaxiFleetPromotions->TripitoesUsers->get($tripitoes_user_id);
        $tripitoesUsers->total_saved+=1;
        $tripitoesUsers = $this->TaxiFleetPromotions->TripitoesUsers->save($tripitoesUsers);
        echo $tripitoesUsers->total_saved;
        exit;
    }
    public function packageReport()
    {
        $packageid=$this->request->query('packageid');
        $report=$this->request->query('report');
        $tripitoes_user_id = $this->Auth->User('id');

        $tripitoesTaxiReports=$this->TaxiFleetPromotions->TripitoesTaxiReports;
        $query = $tripitoesTaxiReports->query();
        $query->insert(['taxi_fleet_promotion_id', 'tripitoes_user_id','reason'])
            ->values([
                'taxi_fleet_promotion_id' => $packageid,
                'tripitoes_user_id' => $tripitoes_user_id,
                'reason' => $report
            ])
            ->execute();
        echo "success";
        exit;
    }
    public function saveCall($id=null){
        $taxiFleetPromotions = $this->TaxiFleetPromotions->get($id);
        $taxiFleetPromotions->trip_calls+=1;
        $taxiFleetPromotions = $this->TaxiFleetPromotions->save($taxiFleetPromotions);
        exit;
    }
    public function saveShare($id=null){
        $hotelPromotions = $this->TaxiFleetPromotions->get($id);
        $hotelPromotions->trip_shares+=1;
        $hotelPromotions = $this->TaxiFleetPromotions->save($hotelPromotions);
        exit;
    }
}
