<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\HotelPromotion $hotelPromotion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $hotelPromotion->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $hotelPromotion->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Categories'), ['controller' => 'HotelCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Category'), ['controller' => 'HotelCategories', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Price Masters'), ['controller' => 'PriceMasters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Master'), ['controller' => 'PriceMasters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Carts'), ['controller' => 'HotelPromotionCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion Cart'), ['controller' => 'HotelPromotionCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Cities'), ['controller' => 'HotelPromotionCities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion City'), ['controller' => 'HotelPromotionCities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Likes'), ['controller' => 'HotelPromotionLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion Like'), ['controller' => 'HotelPromotionLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Price Before Renews'), ['controller' => 'HotelPromotionPriceBeforeRenews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion Price Before Renews'), ['controller' => 'HotelPromotionPriceBeforeRenews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Reports'), ['controller' => 'HotelPromotionReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion Report'), ['controller' => 'HotelPromotionReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Hotel Promotion Views'), ['controller' => 'HotelPromotionViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Hotel Promotion View'), ['controller' => 'HotelPromotionViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Carts'), ['controller' => 'TripitoesHotelCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Cart'), ['controller' => 'TripitoesHotelCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Likes'), ['controller' => 'TripitoesHotelLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Like'), ['controller' => 'TripitoesHotelLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Reports'), ['controller' => 'TripitoesHotelReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Report'), ['controller' => 'TripitoesHotelReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Views'), ['controller' => 'TripitoesHotelViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel View'), ['controller' => 'TripitoesHotelViews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hotelPromotions form large-9 medium-8 columns content">
    <?= $this->Form->create($hotelPromotion) ?>
    <fieldset>
        <legend><?= __('Edit Hotel Promotion') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('hotel_name');
            echo $this->Form->control('hotel_location');
            echo $this->Form->control('hotel_category_id', ['options' => $hotelCategories]);
            echo $this->Form->control('cheap_tariff');
            echo $this->Form->control('expensive_tariff');
            echo $this->Form->control('website');
            echo $this->Form->control('status');
            echo $this->Form->control('hotel_pic');
            echo $this->Form->control('payment_status');
            echo $this->Form->control('total_charges');
            echo $this->Form->control('price_master_id', ['options' => $priceMasters]);
            echo $this->Form->control('visible_date');
            echo $this->Form->control('hotel_rating');
            echo $this->Form->control('created_on');
            echo $this->Form->control('updated_on');
            echo $this->Form->control('accept_date');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('submitted_from');
            echo $this->Form->control('notified');
            echo $this->Form->control('position');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
