 <?php
//pr($hotelPromotionsData->toArray()); exit;
$one_star_user=0;
$total_users=0;
$one_star_percentage=0;
$two_star_user=0;
$two_star_percentage=0;
$three_star_user=0;
$three_star_percentage=0;
$three_star_user=0;
$four_star_percentage=0;
$four_star_user=0;
$five_star_percentage=0;
$five_star_user=0;
$overallrating=0; 
if(!empty($hotelPromotionsData->user->tripitoes_ratings)){
  foreach ($hotelPromotionsData->user->tripitoes_ratings as $key => $value) {
     $rating=$value->rating;
     if($rating==5){
      $five_star_user++;
     }
     if($rating==4){
      $four_star_user++;
     }
     if($rating==3){
      $three_star_user++;
     }
     if($rating==2){
      $two_star_user++;
     }
     if($rating==1){
      $one_star_user++;
     }
     $total_users++;
  }
  if($total_users>0){
    $one_star_percentage=($one_star_user/$total_users)*100;
    $two_star_percentage=($two_star_user/$total_users)*100;
    $three_star_percentage=($three_star_user/$total_users)*100;
    $four_star_percentage=($four_star_user/$total_users)*100;
    $five_star_percentage=($five_star_user/$total_users)*100;


  $overallrating=(((1*$one_star_user)+(2*$two_star_user)+(3*$three_star_user)+(4*$four_star_user)+(5*$five_star_user))/$total_users); 
  }
}
// DUMMY Graph
else{
  
  $total_users=1;
  $one_star_user=5;
  $one_star_percentage=5;

  $two_star_user=15;
  $two_star_percentage=15;

  $three_star_percentage=5;
  $three_star_user=5;

  $four_star_percentage=20;
  $four_star_user=20;

  $five_star_percentage=55;
  $five_star_user=55; 

  $overallrating=5; 
}
$url = (strtolower($hotelPromotionsData->hotel_name));
$urlTitle=str_replace(" ","-",$url);
$urlTitle=str_replace("/","-",$urlTitle);
$ShareMessage=urlencode($site_url.'hotel-promotions/details/'.$hotelPromotionsData->id.'/'.$urlTitle); 

?>
<?php  $shareLink = $this->Paginator->generateUrl(['_full'=>true,'_ssl'=>true],null,true); ?> 
<div class="content"> 
<input type="hidden" value="<?=$hotelPromotionsData->id;?>" class="hotel_id">
<input type="hidden" value="<?=$hotelPromotionsData->user_id;?>" class="user_id">
  <section class="animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
      <div class="container">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li>
            <?= $this->Html->link('Home','https://www.tripitoes.com',['alt'=>'']);?>
          </li>
          <li> <?= $this->Html->link('Hotel',['controller'=>'HotelPromotions','action'=>'hotelList'],['alt'=>'']);?></li>
          <li class="active"><?= $hotelPromotionsData->hotel_name;?></li>
        </ol>
      </div>
    </section>

    <!--======= PAGES INNER =========-->
    <section class="section-p-30px pages-in item-detail-page">
      <div class="container">
        <div class="row desktop"> 
          <!--======= IMAGES SLIDER =========-->
          <div class="col-sm-4 text-center large-detail animate fadeInLeft" data-wow-delay="0.4s">
            <div class="product-slides port-folio-row items">
              <div>
                 <?= $this->Html->image('hello.png',['class'=>'lazy','alt'=>'Tripitoes','style'=>'height:350px;width:360px','data-src'=>$cdn_path.$hotelPromotionsData->hotel_pic]);?>
                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', $cdn_path.$hotelPromotionsData->hotel_pic, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
             
              <?php
        $handle = @fopen('https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_1_pic,'r');        
              if($handle){ ?>
              <div>
                 <?= $this->Html->image('hello.png',['class'=>'lazy','alt'=>'Tripitoes','style'=>'height:350px;width:360px','data-src'=>'https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_1_pic]);?>
                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', 'https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_1_pic, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
              <?php
              }
        $handle1 = @fopen('https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_2_pic,'r');
              if($handle1){ ?>
              <div>
                 <?= $this->Html->image('hello.png',['class'=>'lazy','alt'=>'Tripitoes','style'=>'height:350px;width:360px','data-src'=>'https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_2_pic]);?>
                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', 'https://travelb2bhub.com/app/img/user_docs/'.$hotelPromotionsData->user_id.'/'.$hotelPromotionsData->user->company_img_2_pic, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
              <?php } ?>
            </div>
             
            
            <ul class="share-with">
              <li><a href="javascript:void(0);"><i class="ion ion-ios-eye-outline"></i></a><span><?= $hotelPromotionsData->trip_views; ?></span></li>
              <li>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotionsData->trip_total_like.'</span>',['controller'=>'hotelPromotions','action'=>'hotelLike',$hotelPromotionsData->id,$tripitoes_user_id],['escape'=>false,'class'=>'hotelLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotionsData->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>   
              </li>
              <li class="mobile"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
              <li class="mobile"><a href="#contact-info"><i class="ion-information"></i></a><span>Seller</span></li>
              <li><a href="#reportPopup" class="link popup-vedio video-btn"><i class="ion-ios-flag"></i></a><span>Report</span></li>
              
              <li class="share desktop">
                <i class="ion-android-share-alt"></i>
                <ul>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                    <a href="javascript:void(0);"><i class="fa fa-instagram"></i></a>
                    </li>-->
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                    <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello" ><i class="fa fa-whatsapp "></i></a>
                    </li>
                </ul>
              </li>
              <li class="desktop" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip"><a href="https://api.whatsapp.com/send?phone=+91<?= $hotelPromotionsData->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a></li>
              <li class="desktop" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip"><a href="javascript:void(0);"><i class="ion ion-ios-telephone-outline"></i></a><span>Call</span></li>
              
               
            </ul>
          </div>
          
          <!--======= ITEM DETAILS =========-->
          <div class="col-sm-8 animate fadeInRight" data-wow-delay="0.4s">
            <div class="row">
              <div class="col-sm-12">
                <h3>
                <span class="title"><?= $hotelPromotionsData->hotel_name;?> </span>
                <?php
                $seller_loaction=$hotelPromotionsData->hotel_location.', '.$hotelPromotionsData->hotel_cities->name.', '.$hotelPromotionsData->hotel_cities->state->state_name;

                $map_loaction=$hotelPromotionsData->hotel_location.', '.$hotelPromotionsData->hotel_cities->name.', '.$hotelPromotionsData->hotel_cities->state->state_name;
                $map_loaction=str_replace(", ","+",$map_loaction);
                $map_loaction='http://maps.google.com/?q='.$map_loaction;


                if($hotelPromotionsData->user->isVerified==1){ ?>
                <span class="verified">
                  <img src="<?= $cdn_path?>tripitoes_images/verified.png" class="img-responsive">
                </span> 
                <?php } ?>
                </h3>
                <div class="stars" style="margin-top:10px; margin-bottom:0px;">
                   <?php 
                     
                    for($x=1;$x<=5 ;){
                        if($hotelPromotionsData->hotel_rating>=$x)  {
                          echo'<i class="fa fa-star"></i>';
                        }
                        else{
                          echo'<i class="fa fa-star-o"></i>';
                        }
                         $x++;
                    }
                     ?>
                    <text style="color:#39aeff; font-size:12px;"><?= $hotelPromotionsData->hotel_rating ;?> Star</text>
                </div>
                <p>Category: <span><?= $hotelPromotionsData->hotel_category->name ?></span></p>
              </div>
              <div class="col-sm-12">
                <div class="seller_rating">
                    <div style="width: 20px;float: left;font-size: 30px;margin-right: 15px; line-height: 36px;">
                        <i class="fa fa-star"></i>
                    </div>
                    <div style="width: 50px;float: left; line-height: 16px; font-weight:600;">
                        <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span style="color:#000; font-size:10px;">/5</span>
                        <span style="color:#000; font-size:10px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span>
                    </div>
                    <div style="width: 80px;float: left;border-left: 1px solid #000;padding-left: 10px; line-height: 16px;">
                        <span>User Rating</span>
                    </div>
                </div>
              </div>
            </div>
            <p style="margin-top:15px;margin-bottom: 0px;">Website: <span class="red"><a target="_blank" href="http://<?= $hotelPromotionsData->website?>"><?= $hotelPromotionsData->website?></a></span></p> 
             
            
            
            <p class="data font-montserrat" style="margin-top:0px; margin-bottom:20px;">
                <i class="ion-ios-location-outline"></i>
                Hotel Location: <span><a  target="_blank" href="<?= $map_loaction?>"><?= $seller_loaction?></a></span>
            </p>
            <div class="row"> 
              
              <div class="col-sm-6 col-md-6">
                <p class="price font-montserrat" style="margin-top:10px;">
                Price: 
                <span class="blue">
                Rs. <?= $hotelPromotionsData->trip_price; ?>
                </span>
                <span style="font-size:12px;">(Per Room)</span>
                </p> 
              </div>
              <div class="col-sm-12 col-md-12">
                  <ul class="share-with">
                      <li class="mobile" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="https://api.whatsapp.com/send?phone=+91<?= $hotelPromotionsData->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a>
                        </li>
                      <li class="share mobile">
                          <i class="ion-android-share-alt"></i>
                        <ul>
                            <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                            <a href="javascript:void(0);"><i class="fa fa-instagram"></i></a>
                            </li>-->
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                            <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                              <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                              <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                            </li>
                        </ul>
                        </li>
                      <li class="mobile" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="#."><i class="ion ion-ios-telephone-outline"></i></a>
                      </li>

                  </ul>
                  <?php
                  if($hotelPromotionsData->booknowlink){
					  
          					$website=$hotelPromotionsData->booknowlink; 
          					$website=str_replace("http://","",$website);
          					$website=str_replace("https://","",$website); 
 
                    echo'
                      <div class="desktop">
                        <a href="http://'.@$website.'" target="_blank" class="btn-small btn-dark-big">Book Now</a>
                      </div>
                    ';
                  }
                  ?>
                  
                  
                  <div class="con-info desktop">
                  <a href="#contact-info" class="btn-small contact-info">Contact Info</a> 
                  </div>
                  <?php 
                  if($hotelPromotionsData->video_link){
                    echo'
                      <div class="desktop">
                       <a href="'.$hotelPromotionsData->video_link.'" target="_blank" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>
                      </div>
                    ';
                  }
                  ?>
                  
              </div>
              
            </div>
          </div>
        </div>
        
        <div class="row mobile">
          <!--======= ITEM DETAILS =========-->
          <div class="col-sm-8 animate fadeInRight" data-wow-delay="0.4s">
            <div class="row">
              <div class="col-sm-12">
                <h3>
                <span class="title"><?= $hotelPromotionsData->hotel_name;?></span>
                <?php
                if($hotelPromotionsData->user->isVerified==1){ ?>
                <span class="verified">
                  <img src="<?= $cdn_path?>tripitoes_images/verified.png" class="img-responsive">
                </span> 
                <?php } ?>
                </h3>
                <div class="stars" style="margin-top:10px; margin-bottom:0px;">
                   <?php 
                    for($x=1;$x<=$hotelPromotionsData->hotel_rating ;){
                        $x++;  
                        echo'<i class="fa fa-star"></i>';
                    }
                     ?>
                    <text style="color:#39aeff; font-size:12px;"><?= $hotelPromotionsData->hotel_rating ;?> Star</text>
                  </div>
                  <p>Category: <span><?= $hotelPromotionsData->hotel_category->name ?></span></p>
                </div>
              <div class="col-sm-12">
                <div class="seller_rating">
                    <div style="width: 20px;float: left;font-size: 30px;margin-right: 15px; line-height: 36px;">
                        <i class="fa fa-star"></i>
                    </div>
                    <div style="width: 50px;float: left; line-height: 16px; font-weight:600;">
                        <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span style="color:#000; font-size:10px;">/5</span>
                        <span style="color:#000; font-size:10px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span>
                    </div>
                    <div style="width: 80px;float: left;border-left: 1px solid #000;padding-left: 10px; line-height: 16px;">
                        <span>User Rating</span>
                    </div>
                </div>
              </div>
            </div> 
            
              
            <p class="data font-montserrat" style="margin-top:0px; margin-bottom:0px;">
                <i class="ion-ios-location-outline"></i>
                Hotel Location: <span><a  target="_blank" href="<?= $map_loaction?>"><?= $seller_loaction?></a></span>
            </p>
            <div class="row" style="margin-bottom:20px;"> 
              
              <div class="col-sm-6 col-md-6">
                <p class="price font-montserrat" style="margin-top:10px;">
                Price: 
                <span class="blue">
                Rs. <?= $hotelPromotionsData->trip_price; ?> 
                </span>
                <span style="font-size:12px;">(Per Room)</span>
                </p> 
              </div>
              <div class="col-sm-6 col-md-6">
                  <ul class="share-with">
                      <li class="mobile" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="https://api.whatsapp.com/send?phone=+91<?= $hotelPromotionsData->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a>
                        </li>
                      <li class="share mobile">
                          <i class="ion-android-share-alt"></i>
                        <ul>
                            <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                            <a href="javascript:void(0);"><i class="fa fa-instagram"></i></a>
                            </li>-->
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                            <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                              <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                              <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                            </li>
                        </ul>
                        </li>
                      <li class="mobile" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="tel:<?= $hotelPromotionsData->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveCall/'.$hotelPromotionsData->id]) ?>"><i class="ion ion-ios-telephone-outline"></i></a>
                      </li>
                      <li class="mobile" style="width:47%;">
                         
                        <?php
                          if($hotelPromotionsData->booknowlink){
              							$website=$hotelPromotionsData->booknowlink; 
              							$website=str_replace("http://","",$website);
              							$website=str_replace("https://","",$website);
                            echo'
                                <a href="http://'.@$website.'" target="_blank" class="btn-small btn-dark-big">Book Now</a>
                            ';
                          }
                        ?>
                      </li>

                  </ul>
                  <div class="con-info desktop">
                  <a href="#contact-info" class="btn-small contact-info">Contact Info</a> 
                  </div>
                  
                  <!--<div class="desktop">
                   <a href="#" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>
                  </div>-->
              </div>
            </div>
          </div>
          
           <!--======= IMAGES SLIDER =========-->
          <div class="col-sm-4 text-center large-detail animate fadeInLeft" data-wow-delay="0.4s">
            <div class="product-slides port-folio-row items">
              <div>
                  <?= $this->Html->image('hello.png',['alt'=>'Tripitoes','class'=>'lazy','data-src'=>$cdn_path.$hotelPromotionsData->hotel_pic]);?>

                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', $cdn_path.$hotelPromotionsData->hotel_pic, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
            </div>
            
            <ul class="share-with">
              <li><a href="javascript:void(0);"><i class="ion ion-ios-eye-outline"></i></a><span><?= $hotelPromotionsData->trip_views?></span></li>
              <li>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotionsData->trip_total_like.'</span>',['controller'=>'hotelPromotions','action'=>'hotelLike',$hotelPromotionsData->id,$tripitoes_user_id],['escape'=>false,'class'=>'hotelLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotionsData->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>  
                <!--<a href="#"><i class="ion ion-ios-heart-outline"></i></a><span><?= $hotelPromotionsData->trip_likes?></span>-->

              </li>
              <li class="mobile">
              <?php 
                  if($hotelPromotionsData->video_link){
                    echo'
                       <a href="'.$hotelPromotionsData->video_link.'" target="_blank" ><i class="fa fa-youtube-play"></i><span>Watch </span></a>
                    ';
                  }
                  ?>
              </li>
              <li class="mobile"><a href="#contact-info"><i class="ion-information"></i></a><span>Seller</span></li>
              <li><a href="#reportPopup" class="link popup-vedio video-btn"><i class="ion-ios-flag"></i></a><span>Report</span></li>
              
              <li class="share desktop">
                <i class="ion-android-share-alt"></i>
                <ul>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                    <a href="javascript:void(0);"><i class="fa fa-instagram"></i></a> 
                    </li>-->
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                    <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveShare/'.$hotelPromotionsData->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                    </li>
                </ul>
                
              </li>
              <li class="desktop" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip"><a href="https://api.whatsapp.com/send?phone=+91<?= $hotelPromotionsData->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a></li>
              <li class="desktop" data-original-title="<?= $hotelPromotionsData->user->mobile_number; ?>" data-toggle="tooltip"><a href="javascript:void(0);"><i class="ion ion-ios-telephone-outline"></i></a><span>Call</span></li>
            </ul>
          </div>
          
        </div>
        </div>
       
       <?php  
       if(sizeof($hotelPromotionsData->user->tripitoes_ratings)>0) { ?>
       <div class="container">
       <!--Review Graph-->
       <div class="section-p-30px tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; margin-top: 30px;">
         <h4>Reviews</h4>
         <div class="row">
            <div class="col-lg-6">
              <div style="float:left; width:100%;">
                  <div class="stars"> 
                      <span><?= $overallrating; ?></span> 
                        <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <span><?= $total_users; ?> Reviews</span>
                  </div>
                <div class="graph">
                  <div class="rate-5">
                      <div class="side">
                          <div>5 star</div>
                      </div>
                      <div class="r-5" style="width:<?= @$five_star_percentage/2?>% !important;">
                          <div class="bar-5"></div>
                      </div>
                      <div class="side right">
                          <div><?= $five_star_percentage;?><span class="size">%</span> (<?= $five_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-4">
                      <div class="side">
                          <div>4 star</div>
                      </div>
                      <div class="r-4"  style="width:<?= @$four_star_percentage/2?>% !important;">
                              <div class="bar-4"></div>
                      </div>
                      <div class="side right">
                          <div><?= $four_star_percentage;?><span class="size">%</span> (<?= $four_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-3">
                      <div class="side">
                          <div>3 star</div>
                      </div>
                      <div class="r-3"  style="width:<?= @$three_star_percentage/2?>% !important;">
                              <div class="bar-3"></div>
                      </div>
                      <div class="side right">
                          <div><?= $three_star_percentage;?><span class="size">%</span> (<?= $three_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-2">
                      <div class="side">
                          <div>2 star</div>
                      </div>
                      <div class="r-2"  style="width:<?= @$two_star_percentage/2?>% !important;">
                              <div class="bar-2"></div>
                      </div>
                      <div class="side right">
                          <div><?= $two_star_percentage;?><span class="size">%</span> (<?= $two_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-1">
                      <div class="side">
                          <div>1 star</div>
                      </div>
                      <div class="r-1"  style="width:<?= @$one_star_percentage/2?>% !important;">
                              <div class="bar-1"></div>
                      </div>
                      <div class="side right" >
                          <div><?= $one_star_percentage;?><span class="size">%</span> (<?= $one_star_user;?>)</div>
                      </div>
                  </div>
               </div>
               </div>
               </div>
               
               <div class="col-lg-6">
                <!-- Slider Section -->
                    <div class="testi-slides testimonial animate fadeInUp"  data-wow-delay="0.4s">
                    <?php   
                    foreach ($hotelPromotionsData->user->tripitoes_ratings as $key => $value) {
                      $rating=$value->rating; 
                      ?>
                      <!-- Slider 1 -->
                      <div class="testi">
                        <div class="avatar"> <img class="rateUser" src="<?= @$value->tripitoes_user->social_profiles->photo_url?>" alt=""> </div>
                        <div class="comment">
                            <p>“<?= $value->comments?>”</p>
                            <hr>
                            <h6 ><?= @$value->tripitoes_user->social_profiles->display_name?></h6>
                             
                        </div>
                      </div>
                      <?php } ?>                     
                    </div> 
               </div>
               
            </div>
          </div>
       </div>
       <?php } ?>  
       
       <div class="container" id="contact-info"> 
        <div class="section-p-30px tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
        
          <div class="row">
              
                <div class="col-lg-5 col-md-5 col-xs-12 item-decribe" >
                  <div class="seller-info">
                      <h4>Seller Contact Info</h4>
                      <p>Name: <span><?= $hotelPromotionsData->user->company_name; ?></span></p>
                        <p>Contact Person: <span><?= $hotelPromotionsData->user->first_name.' '.$hotelPromotionsData->user->last_name; ?></span></p>
                        <p>Mobile: <span><?php if($hotelPromotionsData->user->mobile_number){ echo $hotelPromotionsData->user->mobile_number;} else {echo"N/A";} ?></span></p>
                        <p>Email ID: <span><?php if($hotelPromotionsData->user->email){ echo $hotelPromotionsData->user->email;} else {echo"N/A";} ?></span></p>
                        <p>Website: <span><?php if($hotelPromotionsData->user->web_url){ echo $hotelPromotionsData->user->web_url;} else { echo $hotelPromotionsData->website;} ?></span></p>
                        <div class="add" style="height: 80px;">Address:&nbsp;</div>
                        <div class="add-data"> 
                             <?= $hotelPromotionsData->user->address; ?>, <?= $hotelPromotionsData->user->city->name?>, <?=$hotelPromotionsData->user->hotel_state->state_name?>, <?=$hotelPromotionsData->user->country->country_name?>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-7 col-md-7 col-xs-12 item-decribe">
                  <div style="border:1px solid #ebebeb; padding:20px; padding-bottom: 40px; border-radius: 10px;">
                    <h4>Leave a Rating</h4>
                  <form id="review-form">
                        <ul class="row">
                          <li class="col-sm-6" style="margin-bottom: 20px;"> 
                        <div class="reviewRatings"> 
                              <label>Seller Rating</label> 
                                <input style="display:none;" type="radio" checked value="0" name="review"/>
                                <input class="RRs RRs-5" id="star-5-21" type="radio" value="5" name="review"/>
                                <span class="RRs RRs-5" for="star-5-21"></span>
                                <input class="RRs RRs-4" id="star-4-21" type="radio" value="4" name="review"/>
                                <span class="RRs RRs-4" for="star-4-21"></span>
                                <input class="RRs RRs-3" id="star-3-21" type="radio" value="3" name="review"/>
                                <span class="RRs RRs-3" for="star-3-21"></span>
                                <input class="RRs RRs-2" id="star-2-21" type="radio" value="2" name="review"/>
                                <span class="RRs RRs-2" for="star-2-21"></span>
                                <input class="RRs RRs-1" id="star-1-21" type="radio" value="1" name="review"/>
                                <span class="RRs RRs-1" for="star-1-21"></span>
                             </div>
                        </li>
                        <li class="col-sm-12" style="margin-bottom: 20px;">
                          <label> Comment
                            <textarea rows="3" id="reviewConmments" cols="50"></textarea>
                          </label>
                          <div class="ErrorMessage" style="color:red"></div>
                        </li>
                      <li align="center">
                      <ul>
                      <div class="col-sm-12" align="center">
                          <?php 
                        if($auth_login=='Yes')
                        {
                            echo "<li class='SubmitRating' style='cursor:pointer'><a href='javascript:void(0);' class='social-login SubmitRating  reportButtonS'>Submit</a></li>";
                        }
                        else{
                        echo "";
                          $redirect= $this->Url->build(['controller'  => $socialController,'action'=> $socialAction,'plugin' => false,'_full'=>true,'_ssl'=>true]);
                          echo $this->Form->postLink('<li><span> LOG IN WITH </span><i class="fa fa-facebook"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Facebook']],['escape'=>false,'class'=>'social-login ratingButton','block'=>true]
                          ); ?>
                           
                          
                          <?php echo $this->Form->postLink('<li><span> LOG IN WITH </span> <i class="fa fa-google-plus"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Google']],['escape'=>false,'class'=>'social-login ratingButton','block'=>true]
                          ); ?>
                         
                          <?php echo $this->Form->postLink('<li><span> LOG IN WITH </span> <i class="fa fa-linkedin"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'LinkedIn']],['escape'=>false,'class'=>'social-login ratingButton','style'=>'margin-right: 0;','block'=>true]
                          );  
                        }
                        ?> 
                          
                           
                       </div>
                     </ul>
                     </li>
                       </ul>    
                  </form>
<?= $this->fetch('postLink');?>
                    </div>
                </div>
                
            </div>
            </div>
        </div>
       
      </div>
    </section>
    
    
     <section class="section-p-30px new-arrival new-arri-w-slide">
        <div class="container"> 
          
          <!--  Tittle -->
          <div class="tittle-2 animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
          <h5><span>Featured Hotels</span></h5>
        </div>          
          <!--  New Arrival Tabs Products  -->
          <div class="popurlar_hotel fur-slide" data-wow-delay="0.4s"> 
              <?php  echo $this->element('hotel') ?>  
          </div>
        </div>
    </section>

<?= $this->element('social_popup') ?>
<?= $this->element('report_popup') ?>

<!--   <a onClick="onLinkedInLoad();" href="javascript: void(0)">
      Share our IN page!
  </a> -->

</div>

<!--  Facbook share page -->

<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
 
function sharefb(){
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: '<?php echo $shareLink; ?>',
  }, function(response){});
}
</script>

<!-- Linked In Share Page -->

<!--<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
 <script type="IN/Share" data-url="<?php echo $shareLink; ?>"></script> -->

<!--  <script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key: '81hcd9jf57sord'
  lang: en_US
</script> -->
<!-- 
<script type="text/javascript">
    
  // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {  alert();
      IN.Event.on(IN, "auth", shareContent);
    }

  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
  }

  // Use the API call wrapper to share content on LinkedIn
  function shareContent() {
        
    // Build the JSON payload containing the content to be shared
    var payload = { 
      "comment": "Check out developer.linkedin.com! http://linkd.in/1FC2PyG", 
      "visibility": { 
        "code": "anyone"
      } 
    };
    IN.User.authorize(callbackFunction, callbackScope);
    IN.API.Raw("/people/~/shares?format=json")
      .method("POST")
      .body(JSON.stringify(payload))
      .result(onSuccess)
      .error(onError);

  }

</script>  -->
 
<?php
$js=" 
  $(document).ready(function(){
    $(document).on('click','.RRs', function(){
      var valu=$(this).attr('for');
      $(this).closest('div.reviewRatings').find('#'+valu).prop('checked','checked')
    }); 
  }); 
";
$js.="
      $(document).on('click','.hello',function(e){
        var url= $(this).attr('href');
         window.location.href = url;
      });
      $(document).on('click','.shareBtn',function(e){
        sharefb();
      });
      $(document).on('click','a.hotelLike',function(e){
        e.preventDefault();
        var like=$(this).find('span');
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              like.html(response);
            },
            error: function(e) 
            {
            }
        });
      });
      $(document).on('click','a.packageSave',function(e){
        e.preventDefault();
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              $('span.total_saved').html(response);
            },
            error: function(e) 
            {

            }
        });
      });
      $(document).on('click','.packageReport',function(e){  
        e.preventDefault();
        var hotelid= $('.hotel_id').val();
        var report = $('input[name=report]:checked').val();
        var url='".$this->Url->build(['controller'=>'hotelPromotions','action'=>'hotelReport'])."';
        url = url +'?hotelid='+hotelid+'&report='+report;
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              location.reload();
            },
            error: function(e) 
            {
              alert('Something went wrong. Please try again.');
            }
        });
      });

      $(document).on('click','.SubmitRating',function(e){
        e.preventDefault();
        var userid= $('.user_id').val();
        var reviewConmments= $('#reviewConmments').val();
        var type= 'Hotel';
        var review = $('input[name=review]:checked').val();
        if(reviewConmments !=0){
          if(review != 0)
          {
            $('.ErrorMessage').html('');
            var url='".$this->Url->build(['controller'=>'TripitoesUsers','action'=>'submitRating'])."';
            url = url +'?userid='+userid+'&review='+review+'&reviewConmments='+reviewConmments+'&type='+type;
             $.ajax({
                type: 'get',
                url: url,
                success: function(response) 
                {
                  location.reload();
                },
                error: function(e) 
                {
                  //alert('Something went wrong. Please try again.');
                }
            }); 
          }
          else {
            $('.ErrorMessage').html('Please Select Seller Rating.');
          }
        }
        else {
          $('.ErrorMessage').html('Please enter some text to describe your experience.');
        }
      });
  $(document).on('click','.redirect',function(){  
      var url=$(this).attr('redirectid');
      window.location.href = url;
  });

$(document).on('click','.CallCount,.ShareCount',function(e){ 
    var url=$(this).attr('redirecturl'); 
    $.ajax({
      type: 'get',
      url: url,
      beforeSend: function(msg){
      },
      success: function(response) 
      {
      },
      error: function(e) 
      {
      }
    });
});

   ";
echo $this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>