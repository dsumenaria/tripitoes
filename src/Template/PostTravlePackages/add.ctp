<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PostTravlePackage $postTravlePackage
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Post Travle Packages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Currencies'), ['controller' => 'Currencies', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Currency'), ['controller' => 'Currencies', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Price Masters'), ['controller' => 'PriceMasters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Master'), ['controller' => 'PriceMasters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Carts'), ['controller' => 'PostTravlePackageCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Cart'), ['controller' => 'PostTravlePackageCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Cities'), ['controller' => 'PostTravlePackageCities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package City'), ['controller' => 'PostTravlePackageCities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Countries'), ['controller' => 'PostTravlePackageCountries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Country'), ['controller' => 'PostTravlePackageCountries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Likes'), ['controller' => 'PostTravlePackageLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Like'), ['controller' => 'PostTravlePackageLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Price Before Renews'), ['controller' => 'PostTravlePackagePriceBeforeRenews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Price Before Renews'), ['controller' => 'PostTravlePackagePriceBeforeRenews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Reports'), ['controller' => 'PostTravlePackageReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Report'), ['controller' => 'PostTravlePackageReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Rows'), ['controller' => 'PostTravlePackageRows', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package Row'), ['controller' => 'PostTravlePackageRows', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package States'), ['controller' => 'PostTravlePackageStates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package State'), ['controller' => 'PostTravlePackageStates', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Post Travle Package Views'), ['controller' => 'PostTravlePackageViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Post Travle Package View'), ['controller' => 'PostTravlePackageViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Carts'), ['controller' => 'TripitoesPackageCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Cart'), ['controller' => 'TripitoesPackageCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Likes'), ['controller' => 'TripitoesPackageLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Like'), ['controller' => 'TripitoesPackageLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Reports'), ['controller' => 'TripitoesPackageReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Report'), ['controller' => 'TripitoesPackageReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Views'), ['controller' => 'TripitoesPackageViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package View'), ['controller' => 'TripitoesPackageViews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="postTravlePackages form large-9 medium-8 columns content">
    <?= $this->Form->create($postTravlePackage) ?>
    <fieldset>
        <legend><?= __('Add Post Travle Package') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('duration_night');
            echo $this->Form->control('duration_day');
            echo $this->Form->control('valid_date');
            echo $this->Form->control('currency_id', ['options' => $currencies]);
            echo $this->Form->control('starting_price');
            echo $this->Form->control('country_id', ['options' => $countries]);
            echo $this->Form->control('package_detail');
            echo $this->Form->control('excluded_detail');
            echo $this->Form->control('image');
            echo $this->Form->control('document');
            echo $this->Form->control('price_master_id', ['options' => $priceMasters]);
            echo $this->Form->control('price');
            echo $this->Form->control('like_count');
            echo $this->Form->control('visible_date');
            echo $this->Form->control('duration_day_night');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('created_on');
            echo $this->Form->control('edited_by');
            echo $this->Form->control('edited_on');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('submitted_from');
            echo $this->Form->control('notified');
            echo $this->Form->control('position');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
