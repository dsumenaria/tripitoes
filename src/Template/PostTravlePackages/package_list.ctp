<?php $duration_range=$duration; ?>

<!-- CONTENT START -->
  <div class="content"> 
    
    <!--======= SUB BANNER =========-->
    <section class="sub-banner animate fadeInUp heading-bg" data-wow-delay="0.4s">
      <div class="container">
        <h4>TOUR PACKAGES</h4>
      </div>
    </section>
    
    <!--======= PAGES INNER =========-->
    <section class="section-p-30px pages-in">
      <div class="container">
        <div class="row">
          <!--======= SIDE BAR =========-->
          <div class="col-sm-3 animate fadeInLeft" data-wow-delay="0.2s">
            
            <div class="side-bar desktop aside" id="sidebar">
              <div class="inside">
              <div  class="search-main">
                  <div style="float:left; width:80%">
                       <input type="search" class="form-control search-box dasktopsearch" placeholder="City or State or Country" value="<?= $FilterState;?>"> 
                       <?php $this->Form->hidden('package_destination',['class'=>'package_destination dasktopsearch','style'=>'width:100%','placeholder'=>'City or State or Country']) ?> 
                  </div>             
                  <div style="float:left; width:20%">
                       <button class="btn_search FiterData" type="submit"><i class="fa fa-search"></i></button>
                  </div> 
              </div>
              <h6><span>Filters</span><span style="float: right;">Reset</span></h6>
              
              <!-- Seller Location -->
              <div class="heading">
                <h6>
                Search By Seller's Location 
                <span class="share"><em>(i)</em>
                <ul>
                    <b>Personal interaction matters!</b> 
                    <p>Buying packages from sellers near you will help you:<p>
                    <li>
                    1) Personally meet the seller
                    </li>
                    <li>
                    2) Receive greater clarity about package details
                    </li>
                    <li>
                    3) Get more reliable service
                    </li>
                </ul>
                </span>
                </h6>
                                
                <label class="seller-location">Near Me
                    <input type="radio" id="tab-1" name="radio" value="nearme"  class="nearme FiterData" onclick="show0(); getLocation();" />
                    <span class="checkmark"></span>
                </label>
                <label class="seller-location">All India
                    <input type="radio" id="tab-2" name="radio" value="101"  class="FiterData" onclick="show1();" />
                    <span class="checkmark"></span>
                </label>
                <label class="seller-location" style="margin-bottom:0;">Select Specific City
                    <input type="radio" id="tab-3" name="radio" value="sellercity" onclick="show2();" />
                    <span class="checkmark"></span>
                </label>
                <div id="div1" class="search-hide">
                  <div style="float:left; width:80%">
                    <?= $this->Form->hidden('Seller_city',['class'=>'Seller_city','style'=>'width:100%','placeholder'=>'Enter City']) ?> 
 
                  </div>


                    <div style="float:left; width:20%">
                    <button class="btn_search FiterData" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
              </div>
              
              <!-- Destination -->
              <ul class="cate">
                <li class="drop-menu">
                <h6><a class="title" data-toggle="collapse" data-target="#destination">Select Destination</a></h6>
                  <div class="collapse in clearAllcountry" id="destination">
                    <div class="well">
                        <ul id="list">
                          <?php
                            foreach ($countrylist as $country) {
                               echo "<li class='country_list FiterData' value='".$country->id."'>".$country->country_name."</li>";
                            }
                          ?>
                        </ul>
                    </div>
                    <div class="city_list_from_ajax">
                    </div>
                    <div class="more-clear">
                         <span class="clear clearAllbtnmobcountry"><a href="javascript:void(0);">CLEAR</a></span>
                    </div>
                  </div>
                </li>
              </ul>
              
              <!-- duration -->
              <ul class="cate">
                <li class="drop-menu">
                <h6><a class="title" data-toggle="collapse" data-target="#duration">Select Package Duration</a></h6> 
                  <div class="collapse in clearAllduration" id="duration">
                    <div class="well duration">
                      <div>
                      <ul>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox301D" <?php if($duration_range==1){ echo "checked"; } ?> name="duration" value="1" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox301D">1 N/2 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox302D" <?php if($duration_range==2){ echo "checked"; } ?> name="duration" value="2" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox302D">2 N/3 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox303D" <?php if($duration_range==3){ echo "checked"; } ?> name="duration" value="3" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox303D">3 N/4 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox304D" <?php if($duration_range==4){ echo "checked"; } ?> name="duration" value="4" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox304D">4 N/5 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox305D" <?php if($duration_range==5){ echo "checked"; } ?> name="duration" value="5" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox305D">5 N/6 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox306D" <?php if($duration_range==6){ echo "checked"; } ?> name="duration" value="6" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox306D">6 N/7 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox307D" <?php if($duration_range==7){ echo "checked"; } ?> name="duration" value="7" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox307D">7 N/8 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox308D" name="duration" <?php if($duration_range==8){ echo "checked"; } ?> value="8" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox308D">8 N/9 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox309D" <?php if($duration_range==9){ echo "checked"; } ?> name="duration" value="9" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox309D">9 N/10 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox310D" <?php if($duration_range==10){ echo "checked"; } ?> name="duration" value="10" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox310D">10 N/11 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox311D" <?php if($duration_range==11){ echo "checked"; } ?> name="duration" value="11" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox311D">11 N/12 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox312D" <?php if($duration_range==12){ echo "checked"; } ?> name="duration" value="12" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox312D">12 N/13 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox313D" <?php if($duration_range==13){ echo "checked"; } ?> name="duration" value="13" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox313D">13 N/14 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox"> 
                          <input id="checkbox314D" <?php if($duration_range==14){ echo "checked"; } ?> name="duration" value="14" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox314D">14 N/15 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox315D" <?php if($duration_range==15){ echo "checked"; } ?> name="duration" value="15" class="styled FiterData PD" type="checkbox">
                          <label for="checkbox315D">15+ Days</label>
                        </div>
                        </li>
                      </ul>
                      </div>
                    </div>
                    <div class="more-clear">
                        <span class="clear clearAllbtnduration"><a href="javascript:void(0);">CLEAR</a></span>
                    </div>
                  </div>
                </li>
              </ul>
                            
              <!-- Category -->
              <ul class="cate">
                <li class="drop-menu">
                <h6><a class="title" data-toggle="collapse" data-target="#category">Select Package Category</a></h6>
                  <div class="collapse in clearAll" id="category">
                    <div class="well duration">
                      <ul>
                        <?php
                        foreach ($packageCategories as $packageCategory) {
                          ?>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox31-<?= $packageCategory->id;?>" class="styled FiterData Cate" value="<?= $packageCategory->id;?>" type="checkbox">
                          <label for="checkbox31-<?= $packageCategory->id;?>"><?= $packageCategory->name;?></label>
                        </div>
                        </li>
                        <?php } ?>                       
                      </ul>
                    </div>
                    <div class="more-clear">
                       <span class="clear clearAllbtn"><a href="javascript:void(0);">CLEAR</a></span>
                    </div>
                  </div>
                </li>
              </ul>
              
              
              <!-- Rating -->
              <ul class="cate">
                <li class="drop-menu">
                <h6><a class="title" data-toggle="collapse" data-target="#rating">Select By Seller Rating</a></h6>
                  <div class="collapse in clearAll" id="rating">
                    <div class="well">
                      <ul>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-35" class="styled FiterData sellerRating" value="5" type="checkbox">
                          <label for="checkbox3-35">
                          <div class="stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                          </div>
                          5 Star
                          </label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-36" class="styled FiterData sellerRating" value="4" type="checkbox">
                          <label for="checkbox3-36">
                          <div class="stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                          4 Star
                          </label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-37" class="styled FiterData sellerRating" value="3" type="checkbox">
                          <label for="checkbox3-37">
                          <div class="stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                          3 Star
                          </label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-38" class="styled FiterData sellerRating" value="2" type="checkbox">
                          <label for="checkbox3-38">
                          <div class="stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                          2 Star
                          </label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-39" class="styled FiterData sellerRating" value="1" type="checkbox">
                          <label for="checkbox3-39">
                          <div class="stars">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                          1 Star
                          </label>
                        </div>
                        </li>
                      </ul>
                      <div class="more-clear">
                       <span class="clear clearAllbtn"><a href="javascript:void(0);">CLEAR</a></span>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
              
              
              <!-- Hotel Included -->
              <ul class="cate">
                <li class="drop-menu">
                <h6><a class="title" data-toggle="collapse" data-target="#included">Select By Hotel Included</a></h6>
                  <div class="collapse in clearAll" id="included">
                    <div class="well">
                      <ul>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-40" class="styled FiterData HtoelClass" value="5" type="checkbox">
                          <label for="checkbox3-40">5 Star</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-41" class="styled FiterData HtoelClass" value="4" type="checkbox">
                          <label for="checkbox3-41">4 Star</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-42" class="styled FiterData HtoelClass" value="3" type="checkbox">
                          <label for="checkbox3-42">3 Star</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-43" class="styled FiterData HtoelClass" value="2" type="checkbox">
                          <label for="checkbox3-43">2 Star</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox3-44" class="styled FiterData HtoelClass" value="1" type="checkbox">
                          <label for="checkbox3-44">1 Star</label>
                        </div>
                        </li>
                      </ul>
                    </div>
                    <div class="more-clear">
                            <span class="clear clearAllbtn"><a href="javascript:void(0);">CLEAR</a></span>
                    </div>
                  </div>
                </li>
              </ul>
              
              
              <!-- PRICE -->
              <div class="cost-price-content">
                <h6>Package By Price Range</h6>

                <div id="price-range" class="price-range FiterData" style="margin-top: 40px;"></div>
                <span id="price-min" class="price-min" style="top:80px; left:27%;">20</span> 
                <span id="price-max" class="price-max" style="top:80px;">1,00,000</span> 
              </div>
              
              <ul class="cate" style="margin-top: 20px; border-top:1px solid #e8e8e8;">
                <li class="drop-menu">
                  <div class="collapse in " id="included">
                  <div class="well">
                      <ul>
                        <li>
                          <div class="checkbox">
                              <input id="checkbox3-45"  value="1" class="styled FiterData verifiedUser" type="checkbox">
                              <label for="checkbox3-45"><i class="verified-i"></i>Verified Seller</label>
                          </div>
                        </li>
                      </ul>
                   </div>
                   </div>
                 </li>
              </ul>
              
            </div>
            </div>
          </div>
          
          <!--======= ITEMS =========-->
          <div class="col-sm-9 package animate fadeInRight" data-wow-delay="0.2s" id="content"> 
          
            <div  class="search-main mobile" style="margin-bottom:5px;margin-bottom:20px;">
              <h6>Search by Destination</h6>
              <div style="float:left; width:80%">
                   <?php $this->Form->hidden('mobile_destination',['class'=>'mobile_destination mobileSearchBox','style'=>'width:100%','placeholder'=>'City or State or Country']) ?>
                  <input type="search" class="form-control search-box mobileSearchBox" placeholder="City or State or Country">
              </div>             
              <div style="float:left; width:20%">
                   <button class="btn_search FiterDataSearch" type="submit"><i class="fa fa-search"></i></button>
              </div> 
            </div>
          
            <!--<div class="side-bar" style="float:left; width:100%; margin-bottom:20px;">
              <div class="heading mobile">
                <h6>
                Search By Seller's Locationss 
                <span class="share"><em>(i)</em>
                <ul>
                    <b>Personal interaction matters!</b> 
                    <p>Buying packages from sellers near you will help you:</p>
                    <li>
                    1) Personally meet the seller
                    </li>
                    <li>
                    2) Receive greater clarity about package details
                    </li>
                    <li>
                    3) Get more reliable service
                    </li>
                </ul>
                </span>
                </h6>
                
                <label class="seller-location">Near Me
                    <input type="radio" id="tab-1-m" name="radio" class="nearme FiterData" value="nearme" onclick="show0m();" />
                    <span class="checkmark"></span>
                </label>
                <label class="seller-location">All India
                    <input type="radio" id="tab-2-m" name="radio" value="101" class="FiterData" onclick="show1m();" />
                    <span class="checkmark"></span>
                </label>
                <label class="seller-location" style="margin-bottom:0;">Select Specific City
                    <input type="radio" id="tab-3-m" name="radio" value="sellercity" onclick="show2m();" />
                    <span class="checkmark"></span>
                </label>
                <div id="div1m" class="search-hide">
                  <div style="float:left; width:80%">
                    <?= $this->Form->hidden('Seller_city_mobile',['class'=>'Seller_city','style'=>'width:100%','placeholder'=>'Enter City']) ?>
                   </div>
                    <div style="float:left; width:20%">
                    <button class="btn_search FiterData" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </div>
              </div>
            </div>-->
          
          
          <div style="margin-bottom: 20px;float: left;width: 100%;" class="">
            <ul class="nav nav-tabs"  data-wow-delay="0.4s">
              <li class="<?php if(empty($category)){ echo "active";}?>" id="first">
                <?= $this->Html->link('All','javascript:void(0)',['escape'=>false,'dataid'=>999,'class'=>'FiterDataNew']) ?>
              </li>
              <li class="">
                <?= $this->Html->link('Featured Packages','javascript:void(0)',['escape'=>false,'dataid'=>0,'class'=>'FiterDataNew']) ?>
              </li>
              <?php
                $i=0;
                foreach ($packageCategories as $packageCategory) {
                  if($i==0)
                  {
                    $class_active='';//active
                    $active_id=$packageCategory->id;
                  }
                  else
                  {
                    $class_active='';
                  }
                  $i++;
                  ?>
                  <li class="<?= $class_active ?> <?php if($category==$packageCategory->id) { echo "active";} ?>">
                    <?= $this->Html->link($packageCategory->name,'javascript:void(0)',['escape'=>false,'dataid'=>$packageCategory->id,'class'=>'FiterDataNew']) ?>
                  </li>
                  <?php
                }
              ?>
            </ul>
          </div>
            
            
            <!--======= ITExM CETOGRIES =========-->
          <div class="items-short-type animate fadeInUp" data-wow-delay="0.4s" style="border-top:1px solid #e8e8e8; border-bottom:1px solid #e8e8e8; margin-bottom: 20px;"> 
                            
              <!--======= VIEW ITEM NUMBER =========-->
              <div class="view-num">
                <p style="margin-bottom: 0; line-height: 32px;" class="no_of_counts"> <span id="TotalFoundRecordShow"><?= $total_counts; ?></span> Packages Found</p>
              </div>              
              
              <!--======= SHORT BY =========-->
              <div class="short-by mobile">
              <p id="sorting"><i class="fa fa-sort-amount-asc"></i> Sort</p>
                <ol id="listing" style="display:none; padding: 0px 6px;font-size: 10px;position: absolute;background: rgb(255, 255, 255);box-shadow: rgba(0, 0, 0, 0.14) 0px 0px 10px;z-index: 9;right: 15px;">
                  <li style="padding: 5px 0;">
                    <a href="javascript:void(0);" class="SortingWise SortByMobile" val="like_H2L">Likes (High to Low)</a>
                  </li>
                  <li style="padding: 5px 0;">
                    <a href="javascript:void(0);" class="SortingWise SortByMobile" val="view_H2L">Views (High to Low)</a>
                  </li>
                  <li style="padding: 5px 0;">
                    <a href="javascript:void(0);" class="SortingWise SortByMobile" val="rating_H2L">Seller Rating (High to Low)</a>
                  </li>
                  <li style="padding: 5px 0;">
                    <a href="javascript:void(0);" class="SortingWise SortByMobile" val="price_L2H">Price (Low to High)</a>
                  </li>
                </ol>
              </div>
              
              <div class="short-by desktop"> 
              Sort By
                <select class="selectpicker SortingWise">
                  <option value="like_H2L">Likes (High to Low)</option>
                  <option value="view_H2L">Views (High to Low)</option>
                  <option value="rating_H2L">Seller Rating (High to Low)</option>
                  <option value="price_L2H">Price (Low to High)</option>
                </select>
              </div>
          </div>
            
            <!--======= List =========-->
            <div class="tripitoes list-style animate fadeInUp" id="beach_1" data-wow-delay="0.4s">
              <ul>
                <?= $this->element('filter_package') ?>
              </ul>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>

  <!-- GO TO TOP --> 
    <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP End -->
  
  <div class="fixed-bar-m">
        <ul>
            <li>
                <a href="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageList']) ?>" class="fix-package">
                    <div class="package-icon"></div>
                    <span>Packages</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'hotelList']) ?>" class="fix-hotel">
                    <div class="hotel-icon"></div>
                    <span>Hotels</span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'transportList']) ?>" class="fix-transport">
                    <div class="transport-icon"></div>
                    <span>Transport</span>
                </a>
            </li>
            <li>
                <a href="#" id="myBtn" class="fix-filter">
                    <div class="filter-icon"></div>
                    <span>Filter</span>
                </a>
            </li>
        </ul>
  </div>
<input type="hidden" value="<?= $saved ?>" id="saved"/> 
  <div id="myModal" class="modal">
      <div class="modal-content">
        <div class="modal-header">
        <ul>
            <li>
                <a href="#" class="close"><i class="ion-android-arrow-back"></i></a>
            </li>
            <li>
                <span>Filters</span>
            </li>
            <li>
                <a href="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageList']) ?>" class="reset-btn">Reset</a>
            </li>
        </ul>
        </div>
        <div class="modal-body">
          <div class="side-bar">    
          
          <div class="heading">
            <h6>
            Search By Seller's Location 
            <span class="share"><em>(i)</em>
            <ul>
                <b>Personal interaction matters!</b> 
                <p>Buying packages from sellers near you will help you:</p>
                <li>
                1) Personally meet the seller
                </li>
                <li>
                2) Receive greater clarity about package details
                </li>
                <li>
                3) Get more reliable service
                </li>
            </ul>
            </span>
            </h6>
            
            <label class="seller-location">Near Me
                <input type="radio" id="tab-1-m" name="radio" class="nearme" value="nearme"   onclick="show0mf(); getLocation();" />

                <span class="checkmark"></span>
            </label>
            <label class="seller-location">All India
                <input type="radio" id="tab-2-m" name="radio" class="" value="101" onclick="show1mf();" />
                <span class="checkmark"></span>
            </label>
            <label class="seller-location" style="margin-bottom:0;">Select Specific City
                <input type="radio" id="tab-3-m" name="radio" value="sellercity" onclick="show2mf();" />
                <span class="checkmark"></span>
            </label>
            <div id="div1mf" class="search-hide">
                <div style="float:left; width:80%">
                  <?= $this->Form->hidden('Seller_city_mobile2',['class'=>'Seller_city','style'=>'width:100%','placeholder'=>'Enter City']) ?>
               </div>
                <div style="float:left; width:20%">
                <button class="btn_search apply-btn" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </div>
          </div>
                          
          <!-- Destination -->
          <ul class="cate">
            <li class="drop-menu">
            <h6><a class="title collapsed" data-toggle="collapse" data-target="#destination-m">Select Destination</a></h6>
              <div class="collapse clearAllcountry" id="destination-m">
                <div class="ml-input-container">
                    <input id="list-m-1_input_filter" class="ml-input-filter country" type="text" placeholder="Type to Search Country" onkeydown="if (event.keyCode == 13) return false;">
                  </div>
                <div class="well duration ">
                  
                    <ul id="list-m-1" class="list-m-1">
                        <?php
                            foreach ($countrylist as $country) { $cId=$country->id;?>
                              <li class='country_list_mob list-m-1_list_item' value="<?=$country->id ?>">
                                <div class="checkbox" style="margin: 0px 0 !important ;">
                                  <input id="checkboxPOP<?=$country->id ?>" class="styled PoPUP" type="checkbox">
                                  <label for="checkboxPOP<?=$country->id ?>"><?=$country->country_name ?></label>
                                </div>
                              </li> 
                          <?php
                            }
                          ?>
                    </ul>
                </div>
                <div class="" style="max-height: 236px !important;">
                  <div class="city_list_from_ajaxpopup">
                       
                  </div>
                </div>
                <div class="more-clear">
                     <span class="clear clearAllbtnmobcountry"><a href="javascript:void(0);">CLEAR</a></span>
                </div>
              </div>
            </li>
          </ul>
          
          <!-- duration -->
          <ul class="cate">
            <li class="drop-menu">
            <h6>
            <a class="title collapsed" data-toggle="collapse" data-target="#duration-m">Select Package Duration</a>
            </h6>
              <div class="collapse clearAllduration" id="duration-m">
                <div class="well duration">
                  <div>
                   <ul>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox301" name="duration" value="1" class="styled PD" type="checkbox">
                          <label for="checkbox301">1 N/2 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox302" name="duration" value="2" class="styled PD" type="checkbox">
                          <label for="checkbox302">2 N/3 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox303" name="duration" value="3" class="styled PD" type="checkbox">
                          <label for="checkbox303">3 N/4 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox304" name="duration" value="4" class="styled PD" type="checkbox">
                          <label for="checkbox304">4 N/5 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox305" name="duration" value="5" class="styled PD" type="checkbox">
                          <label for="checkbox305">5 N/6 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox306" name="duration" value="6" class="styled PD" type="checkbox">
                          <label for="checkbox306">6 N/7 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox307" name="duration" value="7" class="styled PD" type="checkbox">
                          <label for="checkbox307">7 N/8 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox308" name="duration" value="8" class="styled PD" type="checkbox">
                          <label for="checkbox308">8 N/9 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox309" name="duration" value="9" class="styled PD" type="checkbox">
                          <label for="checkbox309">9 N/10 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox310" name="duration" value="10" class="styled PD" type="checkbox">
                          <label for="checkbox310">10 N/11 D</label>
                        </div>
                        </li>
                        <li>
                        <div class="checkbox">
                          <input id="checkbox311" name="duration" value="11" class="styled PD" type="checkbox">
                          <label for="checkbox311">11 N/12 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox312" name="duration" value="12" class="styled PD" type="checkbox">
                          <label for="checkbox312">12 N/13 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox313" name="duration" value="13" class="styled PD" type="checkbox">
                          <label for="checkbox313">13 N/14 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox"> 
                          <input id="checkbox314" name="duration" value="14" class="styled PD" type="checkbox">
                          <label for="checkbox314">14 N/15 D</label>
                        </div>
                        </li>

                        <li>
                        <div class="checkbox">
                          <input id="checkbox315" name="duration" value="15" class="styled PD" type="checkbox">
                          <label for="checkbox315">15+ Days</label>
                        </div>
                        </li>
                      </ul>
                  </div>
                </div>
                 <div class="more-clear">
                        <span class="clear clearAllbtnduration"><a href="javascript:void(0);">CLEAR</a></span>
                 </div>
              </div>
            </li>
          </ul>
                        
          <!-- Category -->
          <ul class="cate">
            <li class="drop-menu">
            <h6><a class="title collapsed" data-toggle="collapse" data-target="#category-m">Select Package Category</a></h6>
              <div class="collapse clearAll" id="category-m">
                <div class="well duration">
                  <ul>
                    <?php
                        foreach ($packageCategories as $packageCategory) {
                          ?>
                        <li>
                          <div class="checkbox">
                            <input id="checkbox3s1-<?= $packageCategory->id;?>" class="styled Cate"value="<?= $packageCategory->id;?>" type="checkbox">
                            <label for="checkbox3s1-<?= $packageCategory->id;?>"><?= $packageCategory->name;?></label>
                          </div>
                        </li>
                    <?php } ?>
                  </ul>
                </div>
                 <div class="more-clear">
                    <span class="clear clearAllbtnmob"><a href="javascript:void(0);">CLEAR</a></span>
                 </div>
              </div>
            </li>
          </ul>
          
          
          <!-- Rating -->
          <ul class="cate">
            <li class="drop-menu">
            <h6>
            <a class="title collapsed" data-toggle="collapse" data-target="#rating-m">Select By Seller Rating</a>
            </h6>
              <div class="collapse clearAll" id="rating-m">
                <div class="well">
                  <ul>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-13" class="styled sellerRating" value="5" type="checkbox">
                      <label for="checkbox3-13" style="width: 180px !important;">
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                      </div>
                      5 Star
                      </label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-14" class="styled sellerRating"  value="4" type="checkbox">
                      <label for="checkbox3-14" style="width: 180px !important;">
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                      </div>
                      4 Star
                      </label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-15" class="styled sellerRating"  value="3" type="checkbox">
                      <label for="checkbox3-15" style="width: 180px !important;">
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                      </div>
                      3 Star
                      </label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-16" class="styled sellerRating"  value="2" type="checkbox">
                      <label for="checkbox3-16" style="width: 180px !important;">
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                      </div>
                      2 Star
                      </label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-17" class="styled sellerRating"  value="1" type="checkbox">
                      <label for="checkbox3-17" style="width: 180px !important;">
                      <div class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star-o"></i>
                      </div>
                      1 Star
                      </label>
                    </div>
                    </li>
                  </ul>
                </div>
                 <div class="more-clear">
                    <span class="clear clearAllbtnmob"><a href="javascript:void(0);">CLEAR</a></span>
                 </div>
              </div>
            </li>
          </ul>
          
          
          <!-- Hotel Included -->
          <ul class="cate">
            <li class="drop-menu">
            <h6><a class="title collapsed" data-toggle="collapse" data-target="#included-m">Select By Hotel Included</a></h6>
              <div class="collapse clearAll" id="included-m">
                <div class="well">
                  <ul>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-18" class="styled HtoelClass" value="5"  type="checkbox">
                      <label for="checkbox3-18">5 Star</label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-19" class="styled HtoelClass" value="4"  type="checkbox">
                      <label for="checkbox3-19">4 Star</label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-20" class="styled HtoelClass" value="3"  type="checkbox">
                      <label for="checkbox3-20">3 Star</label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-21" class="styled HtoelClass" value="2"  type="checkbox">
                      <label for="checkbox3-21">2 Star</label>
                    </div>
                    </li>
                    <li>
                    <div class="checkbox">
                      <input id="checkbox3-22" class="styled HtoelClass" value="1" type="checkbox">
                      <label for="checkbox3-22">1 Star</label>
                    </div>
                    </li>
                  </ul>
                </div>
                 <div class="more-clear">
                        <span class="clear clearAllbtnmob"><a href="javascript:void(0);">CLEAR</a></span>
                 </div>
              </div>
            </li>
          </ul>
          
          
          <!-- PRICE -->
          <div class="cost-price-content">
            <h6>Package By Price Range</h6>
            <div id="price-range-m" class="price-range" style="margin-top: 40px;"></div>
            <span id="price-min-m" class="price-min" style="top:80px; left:27%;">20 wrawrqeww</span> 
            <span id="price-max-m" class="price-max" style="top:80px;">100000</span> 
          </div>
          
          <!-- VERIFIED -->
          <ul class="cate" style="margin-top: 20px; margin-bottom: 110px; border-top:1px solid #e8e8e8;">
            <li class="drop-menu">
              <div class="collapse in" id="included">
              <div class="well">
                      <ul>
                        <li>
                          <div class="checkbox">
                              <input id="checkbox3x-46" class="styled verifiedUser" type="checkbox">
                              <label for="checkbox3x-46"><i class="verified-i"></i>Verified Seller</label>
                          </div>
                        </li>
                      </ul>
               </div>
               </div>
             </li>
          </ul>
          
        </div>
        <!-- Sidebar end-->
        </div>
        <div class="modal-footer">
          <input type="button" value="Apply" class="apply-btn">
        </div>
      </div>
  </div>




<input type="hidden" value="" id="cityasdasds"> 
<input type="hidden" value="2" id="page_no">
<?= $this->element('report_popup') ?> 
<?= $this->element('social_popup') ?>

<?= $this->Html->css('/trip_front/css/select2.css',['block'=>'select2css']) ?>
<?= $this->Html->script('/trip_front/js/select2.js',['block'=>'select2js']) ?>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfKy3gl3A77WrmsOUwusOD1yfUjVXXfw8&callback=initMap"
  type="text/javascript"></script>
<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
function sharefb(shareLink){
  
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: shareLink,
  }, function(response){});
}
function initMap(){} 
</script>
<?php
$js="
    $(document).ready(function(){
		getLocation();
      $(document).on('click','.shareBtn',function(e){
         sharefb($(this).attr('shareLink'));
      });
      $(document).on('click','.hello',function(e){
        var url= $(this).attr('href');
         window.location.href = url;
      });
      // FILter DATA 
      $(document).on('click','.FiterData,.apply-btn,.clearAllbtn,.clearAllbtnmobcountry,.clearAllbtnduration,.clearAllbtnmob',function(e){
        setTimeout(function(){
        $('#myModal').hide();
        $('a.FiterDataNew').closest('li').removeClass('active');
        $('a.FiterDataNew').closest('li#first').addClass('active');
        $('.mobileSearchBox').val('');
        $('html,body').animate({scrollTop:$('.container').offset().top},'1000');
        
          var country = [];
          var near = '';
          var stateFilter = '';

          var SellerCity ='';
          var Seller_city_mobile ='';
          var Seller_city_mobile2 ='';
          var radioValue = $('input[name=radio]:checked'). val();
          if(radioValue == 101){
             stateFilter='101';
          }else if(radioValue == 'nearme'){
            near=$('#cityasdasds').val();
          }
          else if(radioValue=='sellercity'){
             SellerCity = $('input[name=Seller_city]').val(); 
             Seller_city_mobile2 = $('input[name=Seller_city_mobile2]').val();
          }
           

          $('.country_list').each(function(){
            var selectedcountry= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcountry){
              country.push(selectedcountry);
            }
          });

          $('.PoPUP').each(function(){
            var selectedcountry= $(this).closest('li').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcountry){
              country.push(selectedcountry);
            }
          });

          var city = [];
          $('.city_list').each(function(){
            var selectedcity= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcity){
              city.push(selectedcity);
            }
          });

          var duration = [];
          $('.PD').each(function(){
            var selectedduration= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedduration){
              duration.push(selectedduration);
            }
          });

          var category = [];
          var cat = $('.nav-tabs').find('li.active').find('a').attr('dataid');
          //alert(cat);
          if (cat != undefined){category.push(cat);}
          $('.Cate').each(function(){
            var selectedcategory= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedcategory){
              category.push(selectedcategory);
            }
          }); 

          var sellerRate = [];
          $('.sellerRating').each(function(){
            var selectedrate= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedrate){
              sellerRate.push(selectedrate);
            }
          });

          var hotelclass = [];
          $('.HtoelClass').each(function(){
            var selectedclass= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedclass){
              hotelclass.push(selectedclass);
            }
          }); 
          
          
          var saved = $('#saved').val();
          
          var VarifiedSeller=0;
          var verifiedUser = $('.verifiedUser:checked').val();
          if(verifiedUser){
              VarifiedSeller=1;
          } 
          var sort = $('.selectpicker option:selected').val();
          var SortByMobile=$('.SortByMobile').closest('li.active').find('a').attr('val');
   
          var startPrice = $('.price-min').html().replace('Rs. ', '');
          var endPrice = $('.price-max').html().replace('Rs. ', ''); 

          var startPricem = $('#price-min-m').html().replace('Rs. ', '');
          var endPricem = $('#price-max-m').html().replace('Rs. ', ''); 

          var dasktopsearch = $('.dasktopsearch').val(); 
          var mobileSearchBox = $('.mobileSearchBox').val();
          
          var postData=[];
          postData.push({country : country,city : city, duration : duration, category : category, sellerRate : sellerRate, hotelclass : hotelclass, VarifiedSeller : VarifiedSeller, startPrice : startPrice, endPrice : endPrice, near : near, sort : sort, stateFilter : stateFilter, startPricem : startPricem, endPricem : endPricem, SortByMobile : SortByMobile, dasktopsearch:dasktopsearch, mobileSearchBox:mobileSearchBox,SellerCity:SellerCity,SellerCityMobile:Seller_city_mobile,SellerCityMobile2:Seller_city_mobile2,saved:saved}); 

          var myJSON = JSON.stringify(postData);
          var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'filterData'])."';
          url=url+'?myJSON='+myJSON; 
	 
          $.ajax({
              type: 'get',
              url: url,
              beforeSend: function(msg){
                $('#beach_1').append($('#beforeload').html());
              },
              success: function(response) 
              { 
                if(response !=''){
                  $('#beach_1').html(response);
                  $('#TotalFoundRecordShow').html($('#TotalFoundRecord').val());
                  $('#page_no').val('2');
                  $('[data-toggle=tooltip]').tooltip();

                  $('.popup-vedio').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: true,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-slide-bottom'
                  });
                 // getHeight();
                  loadMore();

                }
                else{
                  $('#beach_1').html('');
                  $('#TotalFoundRecordShow').html('0');
                   
                } 
                $('.lazy').lazy();
              },
              error: function(e) 
              {
                  alert('An error occurred: ' + e.responseText.message);
                  $('#beach_1').find('.overlay-divloader').remove();
              }
            }); 
         }, 100);
      }); 


      //-- New Category wise
      $(document).on('click','.FiterDataNew',function(e){
          $('input[type=checkbox]').each(function(){
             $(this).prop('checked' , false);
          });
          var category = [];
          $('.mobileSearchBox').val('');
          var cat = $(this).attr('dataid');           
          if (cat != undefined){category.push(cat);}
          var country = [];
          var city = [];
          var duration = [];
          var sellerRate = [];
          var hotelclass = [];
          var VarifiedSeller = [];
          var startPrice = [];
          var endPrice = [];
          var near = [];
          var sort = $('.selectpicker option:selected').val();
          var SortByMobile=$('.SortByMobile').closest('li.active').find('a').attr('val');
          var stateFilter = [];
          var startPricem = [];
          var endPricem = [];
           
          var dasktopsearch = [];
          var mobileSearchBox = [];
          var SellerCity = [];
          var Seller_city_mobile = [];
          var Seller_city_mobile2 = [];
          var saved = []; 
          var postData=[];
          postData.push({country : country,city : city, duration : duration, category : category, sellerRate : sellerRate, hotelclass : hotelclass, VarifiedSeller : VarifiedSeller, startPrice : startPrice, endPrice : endPrice, near : near, sort : sort, stateFilter : stateFilter, startPricem : startPricem, endPricem : endPricem, SortByMobile : SortByMobile, dasktopsearch:dasktopsearch, mobileSearchBox:mobileSearchBox,SellerCity:SellerCity,SellerCityMobile:Seller_city_mobile,SellerCityMobile2:Seller_city_mobile2,saved:saved});  

          var myJSON = JSON.stringify(postData);
          var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'filterData'])."';
          url=url+'?myJSON='+myJSON; 
  
          $.ajax({
            type: 'get',
            url: url,
            beforeSend: function(msg){
              $('#beach_1').append($('#beforeload').html());
            },
            success: function(response) 
            { 
              if(response !=''){
                $('#beach_1').html(response);
                $('#TotalFoundRecordShow').html($('#TotalFoundRecord').val());
                $('#page_no').val('2');
                $('[data-toggle=tooltip]').tooltip();

                $('.popup-vedio').magnificPopup({
                  type: 'inline',
                  fixedContentPos: false,
                  fixedBgPos: true,
                  overflowY: 'auto',
                  closeBtnInside: true,
                  preloader: true,
                  midClick: true,
                  removalDelay: 300,
                  mainClass: 'my-mfp-slide-bottom'
                });
               // getHeight();
                loadMore();
              }
              else{
                $('#beach_1').html('');
                $('#TotalFoundRecordShow').html('0');
                $('a.FiterDataNew').closest('li').removeClass('active');
                $('a.FiterDataNew').closest('li#first').addClass('active');
                $('li#first a').trigger('click'); 
              }
              $('.lazy').lazy(); 
            },
            error: function(e) 
            {
                alert('An error occurred: ' + e.responseText.message);
                $('#beach_1').find('.overlay-divloader').remove();
            }
          });  
      });

      //--  Mobile Search
      $(document).on('click','.FiterDataSearch',function(e){
          $('input[type=checkbox]').each(function(){
             $(this).prop('checked' , false);
          });
          $('a.FiterDataNew').closest('li').removeClass('active');
          $('a.FiterDataNew').closest('li#first').addClass('active');
          var category = [];
          var cat = $('.nav-tabs').find('li.active').find('a').attr('dataid');
          if (cat != undefined){category.push(cat);}
          var country = [];
          var city = [];
          var duration = [];
          var sellerRate = [];
          var hotelclass = [];
          var VarifiedSeller = [];
          var startPrice = [];
          var endPrice = [];
          var near = [];
          var sort = $('.selectpicker option:selected').val();
          var SortByMobile=$('.SortByMobile').closest('li.active').find('a').attr('val');
          var stateFilter = [];
          var startPricem = [];
          var endPricem = []; 
          var dasktopsearch = [];
          var mobileSearchBox = $('.mobileSearchBox').val();
          var SellerCity = [];
          var Seller_city_mobile = [];
          var Seller_city_mobile2 = [];
          var saved = []; 
          var postData=[];
          postData.push({country : country,city : city, duration : duration, category : category, sellerRate : sellerRate, hotelclass : hotelclass, VarifiedSeller : VarifiedSeller, startPrice : startPrice, endPrice : endPrice, near : near, sort : sort, stateFilter : stateFilter, startPricem : startPricem, endPricem : endPricem, SortByMobile : SortByMobile, dasktopsearch:dasktopsearch, mobileSearchBox:mobileSearchBox,SellerCity:SellerCity,SellerCityMobile:Seller_city_mobile,SellerCityMobile2:Seller_city_mobile2,saved:saved});  

          var myJSON = JSON.stringify(postData);
          var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'filterData'])."';
          url=url+'?myJSON='+myJSON; 
 
          $.ajax({
            type: 'get',
            url: url,
            beforeSend: function(msg){
              $('#beach_1').append($('#beforeload').html());
            },
            success: function(response) 
            { 
              if(response !=''){
                $('#beach_1').html(response);
                $('#TotalFoundRecordShow').html($('#TotalFoundRecord').val());
                $('#page_no').val('2');
                $('[data-toggle=tooltip]').tooltip();

                $('.popup-vedio').magnificPopup({
                  type: 'inline',
                  fixedContentPos: false,
                  fixedBgPos: true,
                  overflowY: 'auto',
                  closeBtnInside: true,
                  preloader: true,
                  midClick: true,
                  removalDelay: 300,
                  mainClass: 'my-mfp-slide-bottom'
                });
               // getHeight();
                loadMore();
              }
              else{
                $('#beach_1').html('');
                $('#TotalFoundRecordShow').html('0');
                 
              }
              $('.lazy').lazy(); 
            },
            error: function(e) 
            {
                alert('An error occurred: ' + e.responseText.message);
                $('#beach_1').find('.overlay-divloader').remove();
            }
          });  
      });

      //--Sorting wise
      $(document).on('click','.SortingWise',function(e){
        setTimeout(function(){
        $('#myModal').hide();
        $('html,body').animate({scrollTop:$('.container').offset().top},'1000');
           var country = [];
          var near = '';
          var stateFilter = '';

          var SellerCity ='';
          var Seller_city_mobile ='';
          var Seller_city_mobile2 ='';
          var radioValue = $('input[name=radio]:checked'). val();
          if(radioValue == 101){
             stateFilter='101';
          }else if(radioValue == 'nearme'){
            near=$('#cityasdasds').val();
          }
          else if(radioValue=='sellercity'){
             SellerCity = $('input[name=Seller_city]').val(); 
             Seller_city_mobile2 = $('input[name=Seller_city_mobile2]').val();
          }
           

          $('.country_list').each(function(){
            var selectedcountry= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcountry){
              country.push(selectedcountry);
            }
          });

          $('.PoPUP').each(function(){
            var selectedcountry= $(this).closest('li').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcountry){
              country.push(selectedcountry);
            }
          });

          var city = [];
          $('.city_list').each(function(){
            var selectedcity= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
            if(selectedcity){
              city.push(selectedcity);
            }
          });

          var duration = [];
          $('.PD').each(function(){
            var selectedduration= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedduration){
              duration.push(selectedduration);
            }
          });

          var category = [];
          var cat = $('.nav-tabs').find('li.active').find('a').attr('dataid');
          if (cat != undefined){category.push(cat);}
          $('.Cate').each(function(){
            var selectedcategory= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedcategory){
              category.push(selectedcategory);
            }
          }); 

          var sellerRate = [];
          $('.sellerRating').each(function(){
            var selectedrate= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedrate){
              sellerRate.push(selectedrate);
            }
          });

          var hotelclass = [];
          $('.HtoelClass').each(function(){
            var selectedclass= $(this).closest('li').find('input[type=checkbox]:checked').val();
            if(selectedclass){
              hotelclass.push(selectedclass);
            }
          }); 
          
          
          var saved = $('#saved').val();
          
          var VarifiedSeller=0;
          var verifiedUser = $('.verifiedUser:checked').val();
          if(verifiedUser){
              VarifiedSeller=1;
          } 
          var sort = $('.selectpicker option:selected').val();
          var SortByMobile=$('.SortByMobile').closest('li.active').find('a').attr('val');
   
          var startPrice = $('.price-min').html().replace('Rs. ', '');
          var endPrice = $('.price-max').html().replace('Rs. ', ''); 

          var startPricem = $('#price-min-m').html().replace('Rs. ', '');
          var endPricem = $('#price-max-m').html().replace('Rs. ', ''); 

          var dasktopsearch = $('.dasktopsearch').val(); 
          var mobileSearchBox = $('.mobileSearchBox').val();
          
          var postData=[];
          postData.push({country : country,city : city, duration : duration, category : category, sellerRate : sellerRate, hotelclass : hotelclass, VarifiedSeller : VarifiedSeller, startPrice : startPrice, endPrice : endPrice, near : near, sort : sort, stateFilter : stateFilter, startPricem : startPricem, endPricem : endPricem, SortByMobile : SortByMobile, dasktopsearch:dasktopsearch, mobileSearchBox:mobileSearchBox,SellerCity:SellerCity,SellerCityMobile:Seller_city_mobile,SellerCityMobile2:Seller_city_mobile2,saved:saved}); 

          var myJSON = JSON.stringify(postData);
          var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'filterData'])."';
          url=url+'?myJSON='+myJSON; 
 
          $.ajax({
              type: 'get',
              url: url,
              beforeSend: function(msg){
                $('#beach_1').append($('#beforeload').html());
              },
              success: function(response) 
              { 
                if(response !=''){
                  $('#beach_1').html(response);
                  $('#TotalFoundRecordShow').html($('#TotalFoundRecord').val());
                  $('#page_no').val('2');
                  $('[data-toggle=tooltip]').tooltip();

                  $('.popup-vedio').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: true,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-slide-bottom'
                  });
                 // getHeight();
                  loadMore();

                }
                else{
                  $('#beach_1').html('');
                  $('#TotalFoundRecordShow').html('0');
                   
                } 
                $('.lazy').lazy();
              },
              error: function(e) 
              {
                  alert('An error occurred: ' + e.responseText.message);
                  $('#beach_1').find('.overlay-divloader').remove();
              }
            }); 
         }, 100);
      });

      $(document).on('click','.SortByMobile',function(e){
        $('.SortByMobile').each(function(){
          $(this).closest('li').removeAttr('class')
        });
        var search = $(this).closest('li').attr('class','active');
        $('#listing').hide();
      }); 

      $(document).on('keyup','#list-2_input_filter',function(e){
        var search = $(this).val();
        $('.list-2_list_item').each(function() {
          if ( (($(this).text()).toUpperCase()).indexOf(search.toUpperCase()) === 0 ) {
          $(this).show();
          } else {
          $(this).hide();
          }
        });
      }); 

      $(document).on('keyup','#list-m-1_input_filter',function(e){
        var search = $(this).val();
        if(search.length>0){
          $('.list-m-1_list_item').each(function() {
            var contain=$(this).text().toUpperCase();
            if ( (($(this).text()).toUpperCase()).indexOf(search.toUpperCase()) === 103 ) {
              $(this).show();
            } else {
              $(this).hide();
            }
          });
        }
        else{
          $('.list-m-1_list_item').each(function() {
            var contain=$(this).text().toUpperCase();
            if ( (($(this).text()).toUpperCase()).indexOf(search.toUpperCase()) > -1 ) {
              $(this).show();
            } else {
              $(this).hide();
            }
          });
        }
      });

      $(document).on('keyup','#list-m-2_input_filter',function(e){
        var search = $(this).val();
        $('.list-m-2_list_item').each(function() {
          //console.log((($(this).text()).toUpperCase()).indexOf(search.toUpperCase()));
          if ( (($(this).text()).toUpperCase()).indexOf(search.toUpperCase()) === 0 ) {
          $(this).show();
          } else {
          $(this).hide();
          }
        });
      }); 

      $(document).on('click','.country_list',function(e){
        setTimeout(function(){  
            var countries=$('#list_input_values').val();
            var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'citylist'])."';
            url=url+'?countries='+countries;
            e.preventDefault();
            $.ajax({
              type: 'get',
              url: url,
              beforeSend: function(msg){
                $('.city_list_from_ajax').html('Please wait...');
              },
              success: function(response) 
              {
                if(response !='')
                {
                  $('.city_list_from_ajax').addClass('well');
                  $('.city_list_from_ajax').html(response);
                  $('#list-2').multiList();
                  $('#list-2').on('multiList.elementChecked', function(event, value, text) {
                    set_li();
                  });
                  $('#list-2').on('multiList.elementUnchecked', function(event, value, text) {
                    set_li();
                  });";
                  if($Filtertype=='City'){
                    $js.=" 
                        var city = '".$Filtervalue."'; 
                        $('.city_list').each(function(){
                          var val = $(this).attr('value');
                          if(val == city){  
                            $(this).trigger('click');
                            $(this).closest('label').find('input[type=checkbox]').attr('checked', 'checked');
                          }
                        });
                    "; 
                  }
                  $js.="
                    $('#list-2').trigger('multiList.elementChecked');
                  }
                  else{
                    $('.city_list_from_ajax').html('');
                  }
               },
              error: function(e) 
              {
                  alert('An error occurred: ' + e.responseText.message);
                  $('#list-2').find('.overlay-divloader').remove();
              }
            });

        }, 100);
      });

      $(document).on('click','.country_list_mob',function(e){
        setTimeout(function(){ 
            var country=[];
            $('.PoPUP').each(function(){
              var selectedcountry= $(this).closest('li').find('input[type=checkbox]:checked').closest('li').val();
              if(selectedcountry){
                country.push(selectedcountry);
              }
            });
            var countries = country.toString(); 
            var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'citylistpopup'])."';
            url=url+'?countries='+countries;
            e.preventDefault();
            $.ajax({
              type: 'get',
              url: url,
              beforeSend: function(msg){
                $('.city_list_from_ajaxpopup').html('Please wait...');
              },
              success: function(response) 
              {
                if(response !='')
                {
                  $('.city_list_from_ajaxpopup').addClass('well');
                  $('.city_list_from_ajaxpopup').html(response);
                  $('#list-m-2').multiList();
                  $('#list-m-2').on('multiList.elementChecked', function(event, value, text) {
                  set_li();
                  });
                  $('#list-m-2').on('multiList.elementUnchecked', function(event, value, text) {
                    set_li();
                  });
                  $('#list-m-2').trigger('multiList.elementChecked');
                }
                else{  
                  $('.city_list_from_ajaxpopup').html('');
                }
              },
              error: function(e) 
              {
                  alert('An error occurred: ' + e.responseText.message);
                  $('#list-m-2').find('.overlay-divloader').remove();
              }
            });

        }, 100);
      });

      $(document).on('click','a.FiterDataNew',function(e){
        var this_obj = $(this);
        $('a.FiterDataNew').closest('li').removeClass('active');
        this_obj.closest('li').addClass('active');
        e.preventDefault();
      });
    });
 "; 
$js.="
  $(window).on('load', function() {
    //getHeight();
    loadMore();
  });
  function getHeight()
  {
    $('.divheight').each(function(){
      var divheight = $(this).height();
      $(this).find('div.img').css('height',divheight+'px');
      $(this).find('img.imgheight').css('height',divheight+'px');
    });
  }
  function loadMore(){
    var page_no = $('#page_no').val();
    var saved = $('#saved').val();
    //alert(page_no);
    var country = [];
    var near = '';
    var stateFilter = '';
    var SellerCity ='';
    var Seller_city_mobile ='';
    var Seller_city_mobile2 ='';
    var radioValue = $('input[name=radio]:checked'). val();
    if(radioValue == 101){
       stateFilter='101';
    }else if(radioValue == 'nearme'){
      near=$('#cityasdasds').val();
    }
    else if(radioValue=='sellercity'){
       SellerCity = $('input[name=Seller_city]').val();
        
       Seller_city_mobile2 = $('input[name=Seller_city_mobile2]').val();
    }

    $('.country_list').each(function(){
      var selectedcountry= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
      if(selectedcountry){
        country.push(selectedcountry);
      }
    });

    $('.PoPUP').each(function(){
      var selectedcountry= $(this).closest('li').find('input[type=checkbox]:checked').closest('li').val();
      if(selectedcountry){
        country.push(selectedcountry);
      }
    });

    var city = [];
    $('.city_list').each(function(){
      var selectedcity= $(this).closest('label').find('input[type=checkbox]:checked').closest('li').val();
      if(selectedcity){
        city.push(selectedcity);
      }
    });

    var duration = [];
    $('.PD').each(function(){
      var selectedduration= $(this).closest('li').find('input[type=checkbox]:checked').val();
      if(selectedduration){
        duration.push(selectedduration);
      }
    });

    var category = [];
    var cat = $('.nav-tabs').find('li.active').find('a').attr('dataid');
    //alert(cat);
    if (cat != undefined){category.push(cat);}
    $('.Cate').each(function(){
      var selectedcategory= $(this).closest('li').find('input[type=checkbox]:checked').val();
      if(selectedcategory){
        category.push(selectedcategory);
      }
    });


    var sellerRate = [];
    $('.sellerRating').each(function(){
      var selectedrate= $(this).closest('li').find('input[type=checkbox]:checked').val();
      if(selectedrate){
        sellerRate.push(selectedrate);
      }
    });

    var hotelclass = [];
    $('.HtoelClass').each(function(){
      var selectedclass= $(this).closest('li').find('input[type=checkbox]:checked').val();
      if(selectedclass){
        hotelclass.push(selectedclass);
      }
    });
    
    var VarifiedSeller=0;
    var verifiedUser = $('.verifiedUser:checked').val();
    if(verifiedUser){
        VarifiedSeller=1;
    } 
    var sort = $('.selectpicker option:selected').val();
    var SortByMobile=$('.SortByMobile').closest('li.active').find('a').attr('val');

    var startPrice = $('.price-min').html().replace('Rs. ', '');
    var endPrice = $('.price-max').html().replace('Rs. ', ''); 

    var startPricem = $('#price-min-m').html().replace('Rs. ', '');
    var endPricem = $('#price-max-m').html().replace('Rs. ', ''); 

    var dasktopsearch = $('.dasktopsearch').val();  
    var mobileSearchBox = $('.mobileSearchBox').val()   
    

    var postData=[];
    postData.push({country : country,city : city, duration : duration, category : category, sellerRate : sellerRate, hotelclass : hotelclass, VarifiedSeller : VarifiedSeller, startPrice : startPrice, endPrice : endPrice, near : near, sort : sort, stateFilter : stateFilter, startPricem : startPricem, endPricem : endPricem, SortByMobile : SortByMobile, dasktopsearch:dasktopsearch, mobileSearchBox:mobileSearchBox, page_no:page_no,SellerCity:SellerCity,SellerCityMobile2:Seller_city_mobile2,SellerCityMobile:Seller_city_mobile,saved:saved}); 

    var myJSON = JSON.stringify(postData);
    var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'filterData'])."';
    url=url+'?myJSON='+myJSON; 
	
     $.ajax({
        type: 'get',
        url: url,
        beforeSend: function(msg){
        },
        success: function(response) 
        { 
          if(response !=''){ 
            $('#beach_1').append(response);
            $('#page_no').val(parseInt(page_no)+1);
            $('[data-toggle=tooltip]').tooltip();

            $('.popup-vedio').magnificPopup({
              type: 'inline',
              fixedContentPos: false,
              fixedBgPos: true,
              overflowY: 'auto',
              closeBtnInside: true,
              preloader: true,
              midClick: true,
              removalDelay: 300,
              mainClass: 'my-mfp-slide-bottom'
            });
            //getHeight();
           loadMore();
           $('.lazy').lazy();
          } 
        },
        error: function(e) 
        {
            $('#beach_1').find('.overlay-divloader').remove();
        }
      });
  }
";
$js.=" 
  $(document).ready(function() {
    $('#sidebar').stickySidebar({
      sidebarTopMargin: 90,
      footerThreshold: 140
    });

  $(document).on('click','.clearAllbtn,.clearAllbtnmob',function(e){
      $(this).closest('div.clearAll').find('input[type=checkbox]').prop('checked' , false);
  });
  $(document).on('click','.clearAllbtnmobcountry',function(e){
      var gg=$('div.clearAllcountry').find('input[type=checkbox]').prop('checked' , false);
  });
  $(document).on('click','.clearAllbtnduration',function(e){
      var gg=$('div.clearAllduration').find('input[type=checkbox]').prop('checked' , false);
  });
  
  });
  function show0(){
    document.getElementById('div1').style.display ='none';
  }
  function show1(){
    document.getElementById('div1').style.display ='none';
  }
  function show2(){
  document.getElementById('div1').style.display = 'block';
  }
  
  function show0m(){
    document.getElementById('div1m').style.display ='none';
  }
  function show1m(){
    document.getElementById('div1m').style.display ='none';
  }
  function show2m(){
  document.getElementById('div1m').style.display = 'block';
  }
  
  function show0mf(){
    document.getElementById('div1mf').style.display ='none';
  }
  function show1mf(){
    document.getElementById('div1mf').style.display ='none';
  }
  function show2mf(){
  document.getElementById('div1mf').style.display = 'block';
  }
  
  $(document).ready(function(){
    $('[data-toggle=tooltip]').tooltip();   
  });
";
if(!empty($startPrice) && !empty($endPrice)){
  $js.='
     jQuery(document).ready(function($) {  
      //  Price Filter ( noUiSlider Plugin)
        $("#price-range").noUiSlider({
        range: {
          "min": [ 1 ],
          "max": [ 500000 ]
        },
        start: ['.$startPrice.', '.$endPrice.'],
            connect:true,
            serialization:{
                lower: [
            $.Link({
              target: $("#price-min")
            })
          ],
          upper: [
            $.Link({
              target: $("#price-max")
            })
          ],
          format: {
          // Set formatting
            decimals: 0,
            prefix: "Rs. "
          }
            }
      })
    })';
}
else{
  $js.='
     jQuery(document).ready(function($) {  
      //  Price Filter ( noUiSlider Plugin)
        $("#price-range").noUiSlider({
        range: {
          "min": [ 1 ],
          "max": [ 500000 ]
        },
        start: [1, 500000],
            connect:true,
            serialization:{
                lower: [
            $.Link({
              target: $("#price-min")
            })
          ],
          upper: [
            $.Link({
              target: $("#price-max")
            })
          ],
          format: {
          // Set formatting
            decimals: 0,
            prefix: "Rs. "
          }
            }
      })
    })

';
}
$js.='
  
    jQuery(document).ready(function($) {  
      //  Price Filter ( noUiSlider Plugin)
        $("#price-range-m").noUiSlider({
        range: {
          min: [ 1 ],
          max: [ 500000 ]
        },
        start: [1, 500000],
            connect:true,
            serialization:{
                lower: [
            $.Link({
              target: $("#price-min-m")
            })
          ],
          upper: [
            $.Link({
              target: $("#price-max-m")
            })
          ],
          format: {
          // Set formatting
            decimals: 0,
            prefix: "Rs. "
          }
            }
      })
    });
  
';
$js.="
  $(document).ready(function(){
    var country = '".$FilterCountry."';
       setTimeout(function(){
        $('.country_list').each(function(){
          var val = $(this).attr('value');
          if(val == country){
            $(this).trigger('click'); 
            //$('#checkboxPOP'+country).trigger('click');
            $(this).closest('label').find('input[type=checkbox]').attr('checked', 'checked');
          }
        });
      }, 200);
  });
"; 

$js.='
    $(document).ready(function() {
        $("#list").multiList();
        $("#list-2").multiList();
        $("#list-m-1").multiList();
        $("#list-m-2").multiList();

        // elementChecked
        $("#list").on("multiList.elementChecked", function(event, value, text) {
          set_li();
        });
        $("#list-2").on("multiList.elementChecked", function(event, value, text) {
          set_li();
        });
        $("#list-m-1").on("multiList.elementChecked", function(event, value, text) {
          set_li();
        });
        $("#list-m-2").on("multiList.elementChecked", function(event, value, text) {
          set_li();
        });
        

        // elementUnchecked
        $("#list").on("multiList.elementUnchecked", function(event, value, text) {
          set_li();
        });
        $("#list-2").on("multiList.elementUnchecked", function(event, value, text) {
          set_li();
        });
        $("#list-m-1").on("multiList.elementUnchecked", function(event, value, text) {
          set_li();
        });
        $("#list-m-2").on("multiList.elementUnchecked", function(event, value, text) {
          set_li();
        });
        
        
        $("#list").trigger("multiList.elementChecked");
        $("#list-2").trigger("multiList.elementChecked");
        $("#list-m-1").trigger("multiList.elementChecked");
        $("#list-m-2").trigger("multiList.elementChecked");
  });

    function set_li() {
      var selected_text = "";
      var selected_elements = $("#list, #list-2, #list-m-1, #list-m-2").multiList("getSelectedFull");
      if (selected_elements.length > 0) {
        for (var i = 0; i < selected_elements.length; i++) {
          selected_text += selected_elements[i][1] + " (<i>" + selected_elements[i][0] + "</i>)";
          if (i < selected_elements.length - 1) selected_text += ", ";
        }
      }
      $("#selected_elements").html(selected_text);

      var unselected_text = "";
      var unselected_elements = $("#list, #list-2, #list-m-1, #list-m-2").multiList("getUnselected");
      if (unselected_elements.length > 0) {
        for (var i = 0; i < unselected_elements.length; i++) {
          unselected_text += "<i>" + unselected_elements[i] + "</i>";
          if (i < unselected_elements.length - 1) unselected_text += ", ";
        }
      }
      $("#unselected_elements").html(unselected_text);
    }
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
';

$js.="
  $(document).on('click','#sorting',function(e){     
      e.preventDefault(); // stops link from making page jump to the top
      e.stopPropagation(); 
      // when you click the button, it stops the page from seeing it as clicking the body too
      $('#listing').toggle(); 
  });
  $(document).on('click','#listing',function(e){   
      
      e.stopPropagation(); // when you click within the content area, it stops the page from seeing it as clicking the body too
      
  }); 
  $(document).on('click','body',function(e){   
      $('#listing').hide();
  });
   $(document).on('click','.redirect',function(e){
      var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'view'])."';
      var pid= $(this).attr('redirectid');
      var urlid= $(this).attr('urlTitle');
      url=url+'/'+pid+'/'+urlid;
      window.location.href = url;
   });
  
  $(document).on('click','.reportPopup',function(e){
     $('#package_selected').val($(this).attr('p_id'));
  });

  $(document).on('click','.packageReport',function(e){  
    e.preventDefault();
    var packageid= $('#package_selected').val();
    var report = $('input[name=report]:checked').val();
    var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageReport'])."';
    url = url +'?packageid='+packageid+'&report='+report;

    $.ajax({
        type: 'get',
        url: url,
        success: function(response) 
        {
          location.reload();
        },
        error: function(e) 
        {
          alert('Something went wrong. Please try again.');
        }
    });
  });




 ";

 

  

$js.="
     
      $(document).on('click','a.packageLike',function(e){
        e.preventDefault();
        var like=$(this).find('span');
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              like.html(response);
            },
            error: function(e) 
            {
            }
        });
      });
      $(document).on('click','a.packageSave',function(e){
        e.preventDefault();
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              $('span.total_saved').html(response);
            },
            error: function(e) 
            {

            }
        });
      });
 
   ";


 

$js.="
		

		/*function getLocation() {
			if(navigator.geolocation) {
				var currgeocoder;
				navigator.geolocation.getCurrentPosition(function(position) {
					geo_loc = processGeolocationResult(position);
					currLatLong = geo_loc.split(',');
					initializeCurrent(currLatLong[0], currLatLong[1]);
					initMap(currLatLong[0], currLatLong[1]);
				});
			}
		}*/
		function showLocation(position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
             
			initializeCurrent(latitude, longitude);
        }

        function errorHandler(err) {
            if(err.code == 1) {
                
            } else if( err.code == 2) {
                
            }
        }
			
        function getLocation() {

            if(navigator.geolocation) {
               
               // timeout at 60000 milliseconds (60 seconds)
               var options = {timeout:60000};
               navigator.geolocation.getCurrentPosition(showLocation, errorHandler, options);
            } else {
               alert('Sorry, browser does not support geolocation!');
            }
        }
		
		 
		 
        //Get geo location result

       function processGeolocationResult(position) {
             html5Lat = position.coords.latitude; //Get latitude
             html5Lon = position.coords.longitude; //Get longitude
             html5TimeStamp = position.timestamp; //Get timestamp
             html5Accuracy = position.coords.accuracy; //Get accuracy in meters
             return (html5Lat).toFixed(8) + ', ' + (html5Lon).toFixed(8);
       }

        //Check value is present or not & call google api function

        function initializeCurrent(latcurr, longcurr) {
             currgeocoder = new google.maps.Geocoder();

             if (latcurr != '' && longcurr != '') {
                 var myLatlng = new google.maps.LatLng(latcurr, longcurr);
                 return getCurrentAddress(myLatlng);
             }
		}
		 

        //Get current address
		
        function getCurrentAddress(location) {
              currgeocoder.geocode({
                  'location': location

            }, function(results, status) {
				var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
                if (status == google.maps.GeocoderStatus.OK) {
                    //console.log(results[0]);
					/*if (isMobile) {
						$('#cityasdasds').val(results[0].formatted_address);
					} else {
						$('#cityasdasds').val(results[0].address_components[0].long_name);
						 
					}*/
					$('#cityasdasds').val(results[0].formatted_address); 
                    
                } else {
                    //alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }";
		  
echo $this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>