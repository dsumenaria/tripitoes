<!-- CONTENT START -->
  <div class="content"> 
    <!-- Blog -->
    <section class="section-p-30px blog-page">
      <div class="container">
        <div class="row"> 
          <!-- Blog Bar -->
          <div class="col-sm-10 center-auto"> 
            <!--  Blog Posts -->
            <div class="blog-posts">
              <ul>
                <!--  Posts 1 -->
                <li class="animate fadeInUp" data-wow-delay="0.4s"> 
                  <!--  Image --> 
                  <img class="img-responsive" src="images/blog-3.jpg" alt=""> 
                  <?= $this->Html->image($cdn_path.$blog->image_url,['alt'=>'','class'=>'img-responsive']);?>
                  <!-- Tag Icon -->
                  <a href="#." class="post-tittle"><?= $blog->title;?></a>
                  <ul class="info" style="border-bottom: 0px solid #ebebeb !important;border-top: 0px solid #ebebeb !important;padding: 0px 0 !important;">
                    <li><i class="fa fa-user"></i> Written by - <span style="text-transform: capitalize;color: #5f9a02;">Preeti Singh</span></li>
                  </ul>
                  <p style="margin-top: 13px;"><?php echo  $blog->description;?></p>
                  <!--  Post Info -->
                  <ul class="info"> 
                    <li><i class="fa fa-calendar-o"></i> <?= $blog->created_on->format('d-M-Y');?></li>
                    <li><i class="fa fa-comment"></i> <?= sizeof($blog->tripitoes_blog_comments);?></li>
                     <?php  $shareLink = $this->Url->build(['controller'=>'Pages','action'=>'blogDetails',$blog->id,'_full'=>true,'_ssl'=>true]); ?> 
                    <li style="float: right;"><i class="fa fa-share"></i> Share on -  
                      <a href="javascript:void(0);" class="shareBtn" shareLink="<?= $shareLink ?>"><i style=" padding: 8px 10px;  background: #000; color: #fff;   margin-right: 5px;" class="fa fa-facebook"></i></a>
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"  ><i style="padding: 8px 5.7px; background: #000; color: #fff;" class="fa fa-google-plus"></i></a>
                    </li>
                  </ul>

                  <p style="color: #5f9a02;padding: 0; padding-top: 20px;padding-bottom: 10px; font-size: 15px"><b>COMMENTS POST</b></p>
                  <hr style="margin-top:0px !important"></hr>
                  <ul>
                    <?php foreach ($blog->tripitoes_blog_comments as $key => $value) {
                      ?>
                      <li style="border-bottom:1px solid #eee; padding-bottom:10px;    margin-bottom: 10px;"><i style="float: left; margin-right: 6px; height: 50px;   margin-top: 5px;" class="fa fa-comment"></i><p><?= $value->comments?></p> </li>
                      <?php 
                    }
                    ?> 
                  </ul>

                  <?= $this->Form->create($tripitoesBlogComments,['id'=>'CityForm']) ?>
                    <p>
                      <?= $this->Form->textarea('comments',['class'=>'form-control','style'=>'border: 1px solid #eee','rows'=>'3','placeholder'=>'Comment here! ']);?>
                    </p>
                    <div align="right" class="cmntbtn"> <button type="submit" class="btn btn-small btn-dark" style="font-family: 'Montserrat', sans-serif !important;">Submit</button></div>
                   <?= $this->Form->end() ?>
<!--<a href="whatsapp://send?text=The text to share!" 
data-action="share/whatsapp/share">Share via 
Whatsapp</a>-->


                </li>
              </ul>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
  <script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
function sharefb(shareLink){
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: shareLink,
  }, function(response){});
}
</script>
<?php
$js="
 $(document).on('click','.shareBtn',function(e){
     sharefb($(this).attr('shareLink'));
  });
";
$this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>