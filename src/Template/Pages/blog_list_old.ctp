<!-- CONTENT START -->
  <div class="content"> 
    <!--======= SUB BANNER =========-->
    <section class="sub-banner animate fadeInUp heading-bg" data-wow-delay="0.4s">
      <div class="container">
        <h4>BLOGS</h4>
      </div>
    </section>
    
    <!-- Blog -->
    <section class="section-p-30px blog-page">
      <div class="container">
        <div class="row"> 
          
          <!-- Right Side Bar -->
          <div class="col-sm-3 animate fadeInLeft" data-wow-delay="0.4s">
            <div class="side-bar"> 
              
              <!--  SEARCH 
              <div class="search">
                <form>
                  <input type="text" placeholder="SEARCH FAQ">
                  <button type="submit"> <i class="fa fa-search"></i></button>
                </form>
              </div>-->
              
              <!-- HEADING -->
              <div class="heading">
                <h4>Latest post</h4>
              </div>
              <!-- CATEGORIES -->
              <ul class="cate latest-post">
                <?php 
                  foreach ($letestBlog as $key => $letest) {
                  ?>
                    <li>
                      <div class="media">
                        <div class="media-left"> 
                          <a href="#.">
                             
                             <?= $this->Html->image($cdn_path.$letest->image_url,['alt'=>'','class'=>'redirect','style'=>'vertical-align: top !important;','redirectid'=>$this->Url->build(['controller'=>'Pages','action'=>'blogDetails/'.$letest->id])]);?>
                          </a>
                        </div>
                        <div class="media-body" style="vertical-align: bottom !important;">  
                          <?= $this->Html->link($this->Text->truncate($letest->title, 16, ['ellipsis' => '...','exact' => true]), ['controller'=>'Pages','action'=>'blogDetails',$letest->id], ['escape'=>false]) ?>
                            <ul class="info">
                              <li style="line-height: 10px !important;"><i class="fa fa-calendar-o"></i> <?= $letest->created_on->format('d-M');?>&nbsp; 
                              <i class="fa fa-comment"></i> <?= $letest->total_comment;?></li>
                            </ul> 
                        </div>
                      </div>
                    </li>
                <?php     
                  }
                ?>
              </ul>
              
              <!-- HEADING -->
             <!-- <div class="heading">
                <h4>Archive</h4>
              </div>
              <!-- CATEGORIES -->
              <!--<ul class="cate">
                <li><a href="#.">March 2015
                  Jan 2015</a></li>
                <li><a href="#."> December 2014</a></li>
                <li><a href="#."> November 2014</a></li>
                <li><a href="#."> July 2014</a></li>
              </ul>
              
              <!-- TAGS -->
              <!--<div class="heading">
                <h4>Tags</h4>
              </div>
			  
              <!--<ul class="tags">
                <li><a href="#.">FASHION</a></li>
                <li><a href="#.">BAGS</a></li>
                <li><a href="#.">TABLET</a></li>
                <li><a href="#.">ELECTRONIC</a></li>
                <li><a href="#.">BEAUTY</a></li>
                <li><a href="#.">TRtENDING</a></li>
                <li><a href="#.">SHOES</a></li>
              </ul>-->
            </div>
          </div> 
          <!-- Blog Bar -->
          <div class="col-sm-9 animate fadeInRight" data-wow-delay="0.4s"> 
            <!--  Blog Posts -->
            <div class="blog-posts Details medium-images">
              <ul class="more_data">
                <?= $this->element('blogs') ?>
              </ul>
            </div>
            <input type="hidden" id ="page_no" value="2" class="page_no">
          </div>
        </div>
      </div>
    </section>
  </div> 
<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
function sharefb(shareLink){
   
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: shareLink,
  }, function(response){});
}
</script>
<?php
$js="
  $(document).on('click','.redirect',function(e){
      var url= $(this).attr('redirectid');
      url=url;
      window.location.href = url;
  });
  $(document).on('click','.shareBtn',function(e){
       sharefb($(this).attr('shareLink'));
  });
  $(window).on('load', function() {
      loadMore();

  });
  function loadMore(){ 
    var page_no = $('#page_no').val();
    var url='".$this->Url->build(['controller'=>'Pages','action'=>'loadMore'])."';
    url=url+'?page_no='+page_no;
    $.ajax({
        type: 'get',
        url: url,
        beforeSend: function(msg){
        },
        success: function(response) 
        { 
          if(response ==''){ 
          }
          else{
            $('.more_data').append(response);
            $('#page_no').val(parseInt(page_no)+1);
            loadMore();
          } 
        },
        error: function(e) 
        {
        }
      });
  }
";
$this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>