<!-- CONTENT START -->
  <div class="content"> 
    <!--======= ABOUT IMG LARGE =========-->
  <div class="container">
    <div class="row">
      <div class="col-md-12 animate fadeInRight" data-wow-delay="0.4s"> 
        <!--======= About Us =========-->
        <section class="section-p-30px about-us">
          <div class="sma-hed">
            <h4>About Us</h4>
          </div>
          <div class="about-detail">
            <p>Tripitoes.com is a travel search engine for customers in India to buy tourism related services
      directly from service providers. We are free, which means that we do not charge any
      booking fee from you!
            </p>
          </div>
        </section>
      </div>
    </div>
  </div>
    
    <!--======= Services =========-->
    <section class="section-p-30px about-us" style="background: #f4ffef;">
      <div class="container">
        <div class="row">
          <div class="col-md-12 animate fadeInLeft" data-wow-delay="0.4s">
            <div class="sma-hed">
              <h4>How are we different from other online travel sites?</h4>
            </div>
            <div class="about-detail">
              <!--  About Featured -->
              <ul class="about-feat">
                
                <li class="animate fadeInUp" data-wow-delay="0.4s">
        <div class="media">
        <div class="media-left" style="width: 6%;">
          <img class="img-responsive" src="<?= $cdn_path?>tripitoes_images/about-icon-1.png">
        </div>
                  <div class="media-body">
                      <p> Unlike other travel search engines that charge up to 35% commission from the listed
            service providers, we do not charge any commission from our member service
            providers. This means that they can sell tourism services (Tour Packages, Hotels, or Taxi) to you at <b>LOWER PRICES</b>.
            </p>
          </div>
        </div>
      </li>
                
        <li class="animate fadeInUp" data-wow-delay="0.4s">
        <div class="media">
        <div class="media-left" style="width: 6%;">
          <img class="img-responsive" src="<?= $cdn_path?>tripitoes_images/about-icon-2.png">
        </div>
                  <div class="media-body">
                      <p class="w">
             You can <b>CONTACT SELLERS DIRECTLY</b> once you have shortlisted your requirements to get reliable and customizable service.
            </p>
                  </div>
        </div>
                </li>
        
        <li class="animate fadeInUp" data-wow-delay="0.4s">
        <div class="media">
        <div class="media-left" style="width: 6%;">
          <img class="img-responsive" src="<?= $cdn_path?>tripitoes_images/about-icon-4.png">
        </div>
                  <div class="media-body">
                      <p>
             You can <b>search for Tourism Packages based on the seller’s location;</b> this will help to meet the seller before you make
            payments. Also, you will have a direct contact person in case you need assistance during your vacations.
            </p>
                  </div>
        </div>
                </li>
        
        <li class="animate fadeInUp" data-wow-delay="0.4s">
        <div class="media">
        <div class="media-left" style="width: 6%;">
          <img class="img-responsive" src="<?= $cdn_path?>tripitoes_images/about-icon-3.png">
        </div>
                  <div class="media-body">
                      <p class="w">
             You will still be able to <b>review and rate the services you receive,</b> and also shortlist your service provider based on 
            their service history.</p>
                  </div>
        </div>
                </li>
        
              </ul>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    
    
    
    <section class="section-p-30px about-us">
      <div class="container">
        <div class="row">
          <div class="col-md-12 animate fadeInLeft" data-wow-delay="0.4s">
            <div class="sma-hed">
              <h4>Vision</h4>
        <p>To help create precious memories!</p>
            </div>
            <div class="about-detail">
            <p style="margin-bottom:20px;">Through Tripitoes, we hope to bring a large variety of excellent holiday options to end
      customers by allowing travel experts, service providers, and boutique hotels to flourish in a
      monopolistic environment that has been created by large online travel agencies that charge
      hefty commission from Hotels and Travel Agents for listing their services.
            </p>
      
      <div class="sma-hed">
              <h4>Company</h4>
            </div>
            <div class="about-detail">
            <p style="margin-bottom:20px;">Tripitoes.com is powered by www.TravelB2BHub.com and is owned by Durja Travel Network
      Pvt. Ltd., based out of Udaipur, Rajasthan, India.
            </p>
      
      <div class="sma-hed">
              <h4>Proviso</h4>
            </div>
            <div class="about-detail">
            <p style="margin-bottom:20px;">Tripitoes is a search engine. We are not a travel agent, and we don’t sell Travel Packages,
      Hotel Rooms, or Taxi Services. If you have questions concerning a booking, please contact
      the service provider directly.
            </p>
          </div>
          </div>
          
        </div>
      </div>
    </section>
  </div>