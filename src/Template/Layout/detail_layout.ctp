<?php
$controller = strtolower($this->request->getParam('controller'));
$action = strtolower($this->request->getParam('action'));
$queryArray = $this->request->getParam('pass'); 
$packageName = @$queryArray[1];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="<?php echo $full_url; ?>" /> 

<?php
 
  if($controller=='posttravlepackages')
  { 
    if(!empty(@$postTravlePackage->seo_title)){
      ?>
      <title><?= $postTravlePackage->seo_title ?></title>
      <meta name="description" content="<?= $postTravlePackage->seo_description ?>" />
      <?php
    }
    else{ 
      ?>
      <title>Book Tour Packages Online | Holiday Packages | Taxi Services | Cab Booking | Tripitoes</title>
      <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." /> 
    <?php 
    } 
    ?>
      <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
  <?php 
  }
 ?>
<?php 
  if($controller=='hotelpromotions')
  { 
    if(!empty(@$hotelPromotionsData->seo_title)){
      ?>
      <title><?= $hotelPromotionsData->seo_title ?></title>
      <meta name="description" content="<?= $hotelPromotionsData->seo_description ?>" />
      <?php
    }
    else{ 
    ?>
      <title>Hotel Booking Deals | Book Hotel at affordable price | Best Hotel Deals | Tripitoes</title>
      <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
    <?php
    }
    echo '<meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">'; 
  } 
?>
<?php 
  if($controller=='taxifleetpromotions')
  { 
    if(!empty(@$taxiFleetPromotion->seo_title)){
      ?>
      <title><?= $taxiFleetPromotion->seo_title ?></title>
      <meta name="description" content="<?= $taxiFleetPromotion->seo_description ?>" />
      <?php
    }
    else{
      ?>
        <title>Taxi Services |  Car Rental Services | Book Taxi India | Cab Booking | Transportation | Tripitoes</title>
        <meta name="description" content="Book Taxi from the best car rental services across India. Wide range of vehicle rentals - Luxury Cars, Sedans, Tempo Travellers, Buses, Chartered Planes and Boats. Book Taxi now and get amazing discount." /> 
      <?php 
    }
    ?> 
    <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
    <?php 
  } 
?>
<?php if($controller=='pages' && $action=='index'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Best Hotel Deals Online | Taxi Services | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
   <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Taxi Services in India, Tripitoes">
<?php }

else if($controller=='pages' && $action=='termconditions'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Cab Booking in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php }

else if($controller=='pages' && $action=='aboutus'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Taxi Services | Cab Booking | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } 

else if($controller=='pages' && $action=='privacypolicy'){ ?>
  <title>Book Tour Packages Online | International Holiday Packages | Taxi Services in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Taxi Services in India, Tripitoes">
<?php } 

else if($controller=='pages' && $action=='contactus'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Car Rental in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } ?>
<!-- FONTS -->
<?php echo $this->Html->css(
        [
          $cdn_path.'trip_front/css/MS_0.css',
          $cdn_path.'trip_front/css/MS_29.css',
          $cdn_path.'trip_front/css/bootstrap.min.css',
          $cdn_path.'trip_front/css/main.css',
          $cdn_path.'trip_front/css/styles.css',
          $cdn_path.'trip_front/css/responsives-2.css',
          $cdn_path.'trip_front/css/animate.css',
          $cdn_path.'trip_front/css/font-awesom.min.css',
          $cdn_path.'trip_front/css/custom.css'],['rel'=>"stylesheet", 'type'=>"text/css", 'media'=>'screen', 'async'=>'async']);

      echo $this->Html->css($cdn_path.'trip_front/rs-plugin/css/settings.css',['media'=>'screen']);   
      echo $this->Html->css($cdn_path.'trip_front/css/jquery.scrolling-tabs.css'); ?>

      <?php echo $this->Html->script($cdn_path.'trip_front/js/modernizr.js'); ?>
<!-- JavaScripts -->

<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon32.png" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon16.png" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon.ion" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon76.png" type="image/x-icon"/>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131654937-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131654937-1');
</script>


</head>
<body>
<?= $this->element('div_loader'); ?>
<!-- LOADER ===========================================-->
<div id="loader">
  <div class="loader">
    <div class="position-center-center">
  <?= $this->Html->image($cdn_path.'tripitoes_images/logo.svg',['alt'=>'Tripitoes','height'=>'40']);?>
   <p class="text-center">Please Wait...</p>
      <div class="loading">
        <div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
      </div>
    </div>
  </div>
</div>

<!-- Page Wrap -->
<div id="wrap">
<!-- Header starts -->
  <?= $this->element('trip_header'); ?>
  <?= $this->element('sell_popup'); ?>
  <?= $this->element('social_popup'); ?>

<!-- Header End --> 
 
<!--======= SEARCH BAR =========--> 
<!--======= WHY TRIPITOES =========-->
<!--======= FEATURED PACKAGES =========-->
  <?= $this->fetch('content'); ?> 
<!--======= Footer =========-->
<!--<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'fdcb6d2a-dc51-4bcb-a1c6-7ceaa071de3b', f: true }); done = true; } }; })();</script>-->
<?= $this->element('trip_footer'); ?>
<a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP End -->
</div>
<!-- Wrap End -->        

 
<?php    
  $this->Html->script([
    '/trip_front/js/jquery-1.11.3.js',
    '/trip_front/js/wow.min.js',
    '/trip_front/js/bootstrap.min.js',
    '/trip_front/js/own-menu.js',
    '/trip_front/js/owl.carousel.min.js',
    '/trip_front/js/jquery.magnific-popup.min.js',
    '/trip_front/js/jquery.isotope.min.js', 
    '/trip_front/js/jquery.flexslider-min.js',
    '/trip_front/js/jquery.nouislider.min.js',
    '/trip_front/js/jquery.scrolling-tabs.js',
    '/trip_front/js/st-demo.js',
    '/trip_front/rs-plugin/js/jquery.themepunch.tools.min.js',
    '/trip_front/rs-plugin/js/jquery.themepunch.revolution.min.js',
    '/trip_front/js/main.js',
    '/trip_front/js/stickySidebar.js',
    '/trip_front/js/multi-list.js',
  ]); ?>
  
<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery-1.11.3.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/wow.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/bootstrap.min.js"></script>
<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/own-menu.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/owl.carousel.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.magnific-popup.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.isotope.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.flexslider-min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.nouislider.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/bootstrap-select.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.scrolling-tabs.js"></script> 
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/main.js"></script>
<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/stickySidebar.js"></script>
<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/multi-list.js"></script>
<script type="text/javascript" defer >
;(function() {
  'use strict';
  $(activate);
  function activate() {
  $('.nav-tabs')
    .scrollingTabs()
    .on('ready.scrtabs', function() {
    $('.tab-content').show();
    });
  }
}());
</script> 

<?= $this->Html->script($cdn_path.'trip_front/jquery.lazy.min.js'); ?>  
<?php $this->Html->css(['/trip_front/css/bootstrap-select.min.css']);?>         
<?= $this->fetch('select2css') ?>
<?= $this->fetch('select2js') ?>

<script type="text/javascript">if (window.location.hash == '#_=_')window.location.hash = '';</script>
<script>
$(function() {
  $('.lazy').lazy();
});
$(".package_destination,.mobile_destination").select2({
    minimumInputLength: 2,
    //placeholder: "Select Your Destination",
    allowClear: true,
    ajax: {
        url: "<?php echo $this->Url->build(['controller'=>'Pages','action'=>'autocompleteDestination']); ?>",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) { 
                    return {
                        id: item.id,
                        text: item.itemName,
                        slug: item.slug
                    }
                })
            };
        }
    }
});
$(".Seller_city,.Seller_city_mobile2,.Seller_city_mobile").select2({
    minimumInputLength: 2,
    //placeholder: "Select City",
    allowClear: true,
    ajax: {
        url: "<?php echo $this->Url->build(['controller'=>'Pages','action'=>'autocompleteCity']); ?>",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) { 
                    return {
                        id: item.id,
                        text: item.itemName
                    }
                })
            };
        }
    }
});
$(".package_destination,.mobile_destination").on("change", function(e) {
   $(this).attr('value',e['added'].id+','+e['added'].slug);
});
$(document).on('click','.CallCount,.ShareCount',function(e){ 
     //e.preventDefault();
    var url=$(this).attr('redirecturl'); 

    $.ajax({
      type: 'get',
      url: url,
      beforeSend: function(msg){
      },
      success: function(response) 
      {
      },
      error: function(e) 
      {
      }
    });
});
</script> 
<?= $this->fetch('scriptBottom')?>
<script>
  $(document).ready(function(){ 
      $('[data-toggle="tooltip"]').tooltip();   
  });
  
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '410461843008540');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=410461843008540&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</body>
</html>
