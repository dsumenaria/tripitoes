<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TaxiFleetPromotion $taxiFleetPromotion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Price Masters'), ['controller' => 'PriceMasters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Price Master'), ['controller' => 'PriceMasters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Carts'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Cart'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Cities'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion City'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Likes'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Like'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Price Before Renews'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Price Before Renews'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Reports'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Report'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Rows'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Row'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion States'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion State'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Views'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion View'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Carts'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Cart'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Likes'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Like'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Reports'), ['controller' => 'TripitoesTaxiReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Report'), ['controller' => 'TripitoesTaxiReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Views'), ['controller' => 'TripitoesTaxiViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="taxiFleetPromotions form large-9 medium-8 columns content">
    <?= $this->Form->create($taxiFleetPromotion) ?>
    <fieldset>
        <legend><?= __('Add Taxi Fleet Promotion') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('country_id', ['options' => $countries]);
            echo $this->Form->control('fleet_detail');
            echo $this->Form->control('image');
            echo $this->Form->control('document');
            echo $this->Form->control('price_master_id', ['options' => $priceMasters]);
            echo $this->Form->control('price');
            echo $this->Form->control('like_count');
            echo $this->Form->control('visible_date');
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('created_on');
            echo $this->Form->control('edited_by');
            echo $this->Form->control('edited_on');
            echo $this->Form->control('is_deleted');
            echo $this->Form->control('submitted_from');
            echo $this->Form->control('notified');
            echo $this->Form->control('position');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
