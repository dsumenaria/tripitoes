<?php
foreach ($hotelPromotions as $hotelPromotion) 
{
  $ratingData=$TripitoesRating->rating($hotelPromotion);
  $total_users=$ratingData['total_users'];
  $overallrating=$ratingData['overallrating'];
   if($total_users==0){
        $total_users=1;
        $overallrating=5;
    }
	$url = (strtolower($hotelPromotion->hotel_name));
	$urlTitle=str_replace(" ","-",$url);
	$urlTitle=str_replace("/","-",$urlTitle);
  ?>
  <div class="items-in">
    <div class="row">
      <div class="col-sm-4 col-md-12 col-lg-5"> 
        <!-- Image -->
        <div class="img redirect" redirectid="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'details/'.$hotelPromotion->id.'/'.$urlTitle]) ?>">
          <?= $this->Html->image($cdn_path.$hotelPromotion->hotel_pic,['alt'=>'','class'=>'spaical']);?>
        </div>
      </div>
      <div class="col-sm-8 col-md-12 col-lg-7">
        <div class="details-sec"> 
            <?= $this->Html->link(ucwords(strtolower($hotelPromotion->hotel_name)),['controller'=>'HotelPromotions','action'=>'details/'.$hotelPromotion->id.'/'.$urlTitle],['class'=>'title','data-toggle'=>'tooltip','title'=>ucwords(strtolower($hotelPromotion->hotel_name))]) ?>
        </div>
        <p class="h_category font-montserrat responseive" style="display:none"> 
          City: <span color="#A7A7A7"><?= $hotelPromotion->city->name.'('.$hotelPromotion->city->state->state_name.')'; ?></span>
        </p>
        <div class="stars">
            <?php 
              for($x=1;$x<=5 ;){
                 
                  if($hotelPromotion->hotel_rating>=$x)  {
                    echo'<i class="fa fa-star"></i>';
                  }
                  else{
                    echo'<i class="fa fa-star-o"></i>';
                  }
                   $x++;
              }
               ?>
            <span style="color:#39aeff; font-size:14px;"><?= $hotelPromotion->hotel_rating ?> Star</span>
        </div>
        <?php
          if($hotelPromotion->user->isVerified==1)
          {?>
              <div class="verified dasktopverify">
                <?= $this->Html->image($cdn_path.'tripitoes_images/verified.png',['class'=>'img-responsive ','alt'=>'']);?>
              </div>
              <span class="verified mobileverify" style="display:none">
                  <?= $this->Html->image($cdn_path.'tripitoes_images/verifiedmobile.png',['style'=>'height: 25px !important; width: 26px !important;','alt'=>'']);?>
              </span>
          <?php
          }
          ?>
        
        <p class="font-montserrat h_category" data-toggle="tooltip" title="<?= $hotelPromotion->hotel_category->name ?>">Category: 
          <span>
            <?= $hotelPromotion->hotel_category->name; ?>
          </span>
        </p> 
        <div class="seller_rating">
          <div class="seller-icon">
            <i class="fa fa-star"></i>
          </div>
          <div class="s-rate">
            <span style="color:#39aeff; font-size:12px;"><?= $overallrating ?></span><span style="color:#000; font-size:10px;">/5</span>
            <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span>
          </div>
          <div class="s-text">
            <span>Seller Rating</span>
          </div>
        </div>
        
        <p class="h_location font-montserrat hotelData"> 
          City: <span color="#A7A7A7"><?= $hotelPromotion->city->name.'('.$hotelPromotion->city->state->state_name.')'; ?></span>
        </p>
        <p class="price font-montserrat">Starting Price: <span>Rs. <?= $hotelPromotion->trip_price ?></span></p>
        <div class="full-width item-btn-com"> 
          <a href="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'details/'.$hotelPromotion->id,$urlTitle]) ?>" ><i class="ion ion-ios-eye-outline"></i><span><?= $hotelPromotion->trip_views ?></span></a>
         
          <?php
          if($auth_login=='Yes')
          {
            echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotion->trip_total_like.'</span>',['controller'=>'HotelPromotions','action'=>'hotelLike',$hotelPromotion->id,$tripitoes_user_id],['escape'=>false,'class'=>'hotelLike']);
          }
          else
          {
            echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$hotelPromotion->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
          }
          ?>
          <a href="tel:<?= $hotelPromotion->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'saveCall/'.$hotelPromotion->id]) ?>"><i class="ion ion-ios-telephone-outline"  data-original-title="<?= $hotelPromotion->user->mobile_number; ?>" data-toggle="tooltips"></i><span>Call</span></a>
        </div>
        <?php 
        if(!empty($hotelPromotion->booknowlink)){
    			$website=$hotelPromotion->booknowlink; 
    			$website=str_replace("http://","",$website);
    			$website=str_replace("https://","",$website);
          echo '<a href="http://'.$website.'" class="btn-small btn-dark" rel="noreferrer" target="_blank">BOOK NOW</a>';
        }
        ?>
      </div>
    </div>
  </div>
  <?php
}
?>