<div id="pop-open" class="zoom-anim-dialog mfp-hide pop-open-style">
  <div class="pop_up">
    <div class="video"> 
        <h5>SOCIAL SIGN IN</h5>
        <p style="line-height: 16px;font-size: 12px;color: #000; ">Save Trips, Hotels, and Taxi services across devices, write reviews, or share trips with friends and family! </p>
        <h6 style="color: #000;"> SIGN UP EASILY WITH: </h6>

        <div align="center" style="width:106%;float: left; ">
           <?php echo $this->Form->postLink(
              '<i class="fa fa-facebook"></i><span></span>',
              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Facebook']],['escape'=>false,'class'=>'social-login reportButton']
          ); ?>
          <?php echo $this->Form->postLink(
              '<i class="fa fa-google-plus"></i><span></span>',
              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Google']],['escape'=>false,'class'=>'social-login reportButton']
          ); ?>
          <?php echo $this->Form->postLink(
              '<i class="fa fa-linkedin"></i><span></span>',
              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'LinkedIn']],['escape'=>false,'class'=>'social-login reportButton']
          ); ?>
        </div> 
      <p style="line-height: 16px;margin-top:10px;float: left;font-size: 12px;color: #000;">By signing up you agree to Tripitoes.com's <?= $this->Html->link('Terms & Conditions', ['controller'=>'Pages','action'=>'termConditions'], ['escape'=>false,'target'=>'_blank' ,'rel'=>"noreferrer"]) ?> and <?= $this->Html->link('Privacy Policy', ['controller'=>'Pages','action'=>'privacyPolicy'], ['escape'=>false,'target'=>'_blank' ,'rel'=>"noreferrer"]) ?></p>                   
    </div>
  </div>
</div>