<?php if(!empty($getTravelPackages->toArray())){ echo '<input type="hidden" id="TotalFoundRecord" value="'.$total_counts.'">';
   foreach ($getTravelPackages as  $getPackage) {
    
        $ratingData=$TripitoesRating->rating($getPackage);
        $total_users=$ratingData['total_users'];
        $overallrating=$ratingData['overallrating'];
         if($total_users==0){
            $total_users=1;
            $overallrating=5;
        }
	$url = (strtolower($getPackage->title.' Package'));
	$urlTitle=str_replace(" ","-",$url);
	$urlTitle=str_replace("/","-",$urlTitle);
    $ShareMessage=urlencode($site_url.'PostTravelPackages/view/'.$getPackage->id.'/'.$urlTitle);
   ?>
    <!-- Package List -->
    <li class="animate fadeInUp" data-wow-delay="0.4s">
      <div class="items-in">
        <div class="row divheight">
          <div class="col-xs-3 col-sm-4 padding-right"> 
            <div class="hot-tag"><span><?= $getPackage->duration_day_night;?></span></div>
            <?php
            if($auth_login=='Yes')
            {
              echo $this->Html->link('<div class="save"><i class="ion-ios-bookmarks-outline"></i></div>',['controller'=>'PostTravlePackages','action'=>'packageSave',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageSave desktop']);
            }
            else
            {
              echo $this->Html->link('<div class="save"><i class="ion-ios-bookmarks-outline"></i></div>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn desktop']);
            }
            ?>

            <!--<a href="" class="desktop"><div class="save"> <i class="ion-ios-bookmarks-outline"></i></div></a>-->

            <div class="img redirect" redirectid="<?= $getPackage->id?>" urlTitle = "<?= $urlTitle ?>" > <?= $this->Html->image('hello.png',['alt'=>'Tripitoes','class'=>'spaical lazy','data-src'=>$cdn_path.$getPackage->trip_image]);?> </div>
          </div>
          <div class="col-xs-6 col-sm-8 padding-right">
            <div class="details-sec">
                <?= $this->Html->link('<span style="float: left;">'.$getPackage->title.'</span>', ['controller'=>'PostTravelPackages','action'=>'view',$getPackage->id,$urlTitle], ['escape'=>false]) ?> 
                 
                <?php 
                if($getPackage->user->isVerified==1){ ?>
                  <span class="verified dasktopverify">
                      <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Tripitoes','data-src'=>$cdn_path.'tripitoes_images/verified.png']);?>
                  </span>

                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image('hello.png',['style'=>'height: 12px !important; width: 12px !important; ','alt'=>'Tripitoes','data-src'=>$cdn_path.'tripitoes_images/verifiedmobile.png','class'=>'lazy']);?>
                  </span>
                <?php } ?>
                </a> 
            </div>
            <div class="seller_rating redirect" redirectid="<?= $getPackage->id?>" urlTitle = "<?= $urlTitle ?>">
                <div class="seller-icon">
                    <i class="fa fa-star"></i>
                </div>
                <div class="s-rate">
                    <span style="color:#39aeff; font-size:12px;"><?= $overallrating;?></span><span style="color:#000; font-size:10px;">/5</span>
                     
                    <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users;?>)</span>
                </div>
                <div class="s-text">
                    <span>Seller Rating</span>
                </div>
            </div>
           <?php
            $package_name=[];
            foreach ($getPackage->post_travle_package_rows as $post_travle_package_row) {
                $package_name[]=$post_travle_package_row->post_travle_package_category->name;
            }
            $packages=implode(', ', array_unique($package_name));
            ?> 
            <p class="data font-montserrat tip" data-toggle="tooltip" title="<?= $packages ?>">Category: <span><?= $packages ?></span> 
        	</p> 
            <p class="hotel font-montserrat redirect" redirectid="<?= $getPackage->id?>" urlTitle = "<?= $urlTitle ?>">Hotel Included: 
            <span><?= $getPackage->hotel_class; ?> Star</span></p>

            <?php
            $city_name=[];
            foreach ($getPackage->post_travle_package_cities as $post_travle_package_city) {
                $city_name[]=$post_travle_package_city->city->name;
            }
            $cities=implode(', ', array_unique($city_name));
            ?>
            <p class="data font-montserrat tip" data-toggle="tooltip" title="<?= $cities ?>">Cities: 
            <span><?= $cities ?></span> 
            </p>
            <?php
            $country_name=[];
            foreach ($getPackage->post_travle_package_countries as $post_travle_package_country) {
                $country_name[]=$post_travle_package_country->country->country_name;
            }
             
            $countries=implode(', ',array_unique($country_name));
            ?>
            <p class="country data font-montserrat">Country: 
            <span><?= $countries ?></span></p>

            <p class="location font-montserrat">
                <i class="ion-ios-location-outline"></i>
                Seller Location:
                <?php 
                $seller_loaction=$getPackage->user->address.', '.$getPackage->user->city->name.', '.$getPackage->user->state->state_name.', '.$getPackage->user->p_country->country_name;

                $map_loaction=$getPackage->user->address.', '.$getPackage->user->city->name.', '.$getPackage->user->state->state_name.', '.$getPackage->user->p_country->country_name;
                  $map_loaction=str_replace(", ","+",$map_loaction);
                  $map_loaction='http://maps.google.com/?q='.$map_loaction;
                ?> 
                <span><a target="_blank" href="<?= $map_loaction; ?>">
                    <?php echo $seller_loaction;?></a>
                </span>
            </p>
            
            <p class="price font-montserrat redirect" redirectid="<?= $getPackage->id?>" urlTitle = "<?= $urlTitle ?>" style="margin-top:10px;">Starting Price: <span>Rs. <?= $getPackage->trip_price?></span></p>
             <a href="#" style="visibility:hidden" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a> 
            <div class="half-width item-btn-com desktop"> 
                <a href="javascript:void(0);" class="redirect" redirectid="<?= $getPackage->id?>" urlTitle = "<?= $urlTitle ?>"><i class="ion ion-ios-eye-outline"></i><span><?= $getPackage->trip_views?></span></a>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>
                <!--<a href="#"><i class="ion ion-ios-heart-outline"></i><span><?= $getPackage->trip_likes?></span></a>-->
                <a class="contactno">
                <i class="ion ion-ios-telephone-outline"></i>
                <span class="font-montserrat tooltipmobile"><?= $getPackage->user->mobile_number; ?></span>
                </a>
                <div class="share">
                <i class="ion-android-share-alt"></i>
                <?php  $shareLink = $this->Url->build(['controller'=>'postTravelPackages','action'=>'view',$getPackage->id,$urlTitle,'_full'=>true,'_ssl'=>true]); ?> 
                <ul> 
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'postTravlePackages','action'=>'saveShare/'.$getPackage->id]) ?>">
                        <a href="javascript:void(0);" class="shareBtn" shareLink="<?= $shareLink ?>"><i class="fa fa-facebook"></i></a>
                    </li>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'postTravlePackages','action'=>'saveShare/'.$getPackage->id]) ?>">
                        <a hrefjavascript:void(0); onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?php echo  $shareLink ?>'), '', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');"><i class="fa fa-linkedin"></i></a> 
                    </li>-->

                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'postTravlePackages','action'=>'saveShare/'.$getPackage->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'postTravlePackages','action'=>'saveShare/'.$getPackage->id]) ?>">
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                    </li>

                </ul>
                </div>
            </div>
            <div class="half-width item-btn-com desktop" style="margin-top:12px;"> 
                <a href="#reportPopup" p_id="<?= $getPackage->id ?>" class="reportPopup link popup-vedio video-btn"><i class="ion-ios-flag"></i><span>Report</span></a>
                <a href="#userinfo<?= $getPackage->id ?>" class="popup-vedio seller-detail">
                    <i class="ion-information"></i>
                    <span>Info</span>
                </a>
                <div>
                <?= $this->Html->link('View Details',['controller'=>'postTravelPackages','action'=>'view',$getPackage->id,$urlTitle],['escape'=>false,'class'=>'btn-small btn-dark']) ?>
                 
                </div>
            </div>
            </div>
            
            <div class="col-xs-3 mobile">
                
                <div class="half-width item-btn-com">
                    <?php 
                        if($auth_login=='Yes')
                        {
                          echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
                        }
                        else
                        {
                          echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                        }
                        ?>




                    <!--<a href="#"><i class="ion ion-ios-heart-outline"></i><span>40</span></a>-->



                    <a href="#userinfo<?= $getPackage->id ?>" class="popup-vedio seller-detail">
                    <i class="ion-information"></i>
                    <span>Seller</span>
                    </a>
                </div>
            
                <div class="half-width item-btn-com"> 
                    

                    <?php
                    if($auth_login=='Yes')
                    {
                      echo $this->Html->link('<i class="ion ion-ios-bookmarks-outline"></i><span>Save</span>',['controller'=>'PostTravlePackages','action'=>'packageSave',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageSave ']);
                    }
                    else
                    {
                      echo $this->Html->link('<i class="ion ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn ']);
                    }
                    ?>
                <a class="contactno CallCount" href="tel:<?= $getPackage->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveCall/'.$getPackage->id]) ?>">
                    <i class="ion ion-ios-telephone-outline"></i> <span>Call</span>
                    <span class="font-montserrat tooltipmobile"><?= $getPackage->user->mobile_number; ?></span>
                </a>
            </div>
            
                 
            </div>
            
        </div>
      </div>

<div id="userinfo<?= $getPackage->id ?>" class="zoom-anim-dialog mfp-hide pop-open-style">
  <div class="pop_up">
    <div class="video"> 
        <span class="details">
        <table width="100%" style="color: #6b6b6b;font-family: 'Montserrat', sans-serif;">
        <tr>
            <td colspan="2" height="25px">
            <p style="color:#5f9a02; font-size:16px;"><?= $getPackage->user->company_name; ?></p>
            </td>
        </tr>
        <tr>
            <td height="25px" width="8%"><i class="fa fa-user" ></i></td>
            <td height="25px" width="90%">
                <span><?= $getPackage->user->first_name.' '.$getPackage->user->last_name; ?></span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-phone" ></i></td>
            <td height="25px">
                <span ><?php if($getPackage->user->mobile_number){ echo $getPackage->user->mobile_number;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-envelope"></i></td>
            <td height="25px">
                <span style="font-size: 13px; word-break: break-all;line-height: 14px;"><?php if($getPackage->user->email){ echo $getPackage->user->email;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-globe"></i></td>
            <td height="25px"> 
                <span style="font-size: 13px;word-break: break-all;line-height: 16px;"><?php if($getPackage->user->web_url){ echo $getPackage->user->web_url;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-home"></i></td>
            <td height="25px">
                <span>
                <?php if($getPackage->user->address){ 
                        echo $getPackage->user->address.', ';
                        echo $getPackage->user->city->name.', ';
                        echo $getPackage->user->state->state_name.', ';
                        echo $getPackage->user->p_country->country_name;
                    }else{echo"N/A";} ?> 
                <span>
            </td>
        </tr>
    </table>
        </span>                    
    </div>
  </div>
</div>





    </li>
    <?php 
    }
}?>