<?php
if(sizeof($tripitoesBlogs->toArray())>0){  
    foreach ($tripitoesBlogs as $key => $blog) {
    ?>
      <li class="animate fadeInUp" data-wow-delay="0.4s">
        <div class="row"> 
          <!--  Image -->
          <div class="col-sm-5"> 
            <?= $this->Html->image($cdn_path.$blog->image_url,['alt'=>'','class'=>'redirect img-responsive','width'=>'336px','height'=>'214px','redirectid'=>$this->Url->build(['controller'=>'Pages','action'=>'blogDetails/'.$blog->id])]);?>
          </div>
          <div class="col-sm-7"> 
            <?= $this->Html->link($blog->title, ['controller'=>'Pages','action'=>'blogDetails',$blog->id], ['escape'=>false,'class'=>'tittle-post']) ?>
            <ul class="info">
              <li><i class="fa fa-user"></i> Written by - <span style="text-transform: capitalize;color: #5f9a02;">Preeti Singh</span></li>
            </ul>
            <p>
              <?= $this->Text->truncate($blog->short_description, 100, ['ellipsis' => '...','exact' => true]); ?> 
            </p>
            <!--  Post Info -->
            <ul class="info"> 
              <li><i class="fa fa-calendar-o"></i> <?= $blog->created_on->format('d-M');?></li>
              <li><i class="fa fa-comment"></i> <?= $blog->total_comment;?></li>
               <?php  $shareLink = $this->Url->build(['controller'=>'Pages','action'=>'blogList','_full'=>true,'_ssl'=>true]); ?> 
              <li style="float: right;"><i class="fa fa-share"></i> Share on -  
                <a href="javascript:void(0);" class="shareBtn" shareLink="<?= $shareLink ?>"><i style=" padding: 8px 10px;  background: #000; color: #fff;   margin-right: 5px;" class="fa fa-facebook"></i></a>
                <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i style="padding: 8px 5.7px; background: #000; color: #fff;" class="fa fa-google-plus"></i></a>


              </li>
            </ul>
            <?= $this->Html->link('READ MORE', ['controller'=>'Pages','action'=>'blogDetails',$blog->id], ['escape'=>false,'class'=>'btn btn-small btn-dark']) ?>
          </div>
        </div>
      </li>
      
    <?php
    }
}
?>