 <div id="page-wrap">
    <div class="tabs">
      
       <div class="tab" style="text-align:right;">
           <input type="radio" id="tab-1" name="tab-group-1" checked="checked">
           <label for="tab-1" class="service">Packages</label>
           
           <div class="content_search">
               <div class="row">
                  <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select Your Destination</label>
                      <?= $this->Form->hidden('package_destination',['class'=>'package_destination','style'=>'width:100%']) ?> 
                    </div>
                    <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select a Duration</label>
                      <select class="form-control selectpicker" id="duration_range" data-live-search="true">
                            <option value="">Duration</option>
                            <option value="1">1 N/2 D</option>
                            <option value="2">2 N/3 D</option>
                            <option value="3">3 N/4 D</option>
                            <option value="4">4 N/5 D</option>
                            <option value="5">5 N/6 D</option>

                            <option value="6">6 N/7 D</option>
                            <option value="7">7 N/8 D</option>
                            <option value="8">8 N/9 D</option>
                            <option value="9">9 N/10 D</option>
                            <option value="10">10 N/11 D</option>

                            <option value="11">11 N/12 D</option>
                            <option value="12">12 N/13 D</option>
                            <option value="13">13 N/14 D</option>
                            <option value="14">14 N/15 D</option>
                            <option value="15">15+ Days</option>
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-3 margin_bottom">
                      <div class="cost-price-content" style="margin-top:0;">
                          <label>Price Range (Per Person)</label>
                            <div id="price-range" class="price-range-package" style="margin-top:10px;"></div>
                            <span id="price-min" class="price-min">0</span> 
                            <span id="price-max" class="price-max">100000</span> 
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <a href="Javascript:void(0);" class="btn_search PackageSearch"><i class="fa fa-search"></i> SEARCH</a>
                    </div>
               </div>
           </div> 
       </div>
    
       <div class="tab">
           <input type="radio" id="tab-2" name="tab-group-1">
           <label for="tab-2" class="service">Hotels</label>
           
           <div class="content_search">
               <div class="row">
                  <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select Your Destination</label>
                      <?= $this->Form->hidden('hotel_destination',['class'=>'hotel_destination','style'=>'width:100%']) ?> 
                    </div>
                    <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select a Category</label>
                      <select class="form-control selectpicker" name="category" id="category" data-live-search="true">
                         <option value="">Your Category</option>
                          <?php
                            foreach ($hotelCategories as $hotelCategory) {
                              ?>
                            <option data-tokens="<?= $hotelCategory->name ?>" value="<?= $hotelCategory->id ?>"><?= $hotelCategory->name ?></option>
                            <?php } ?> 
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-3 margin_bottom">
                      <div class="cost-price-content" style="margin-top:0;">
                          <label>Price Range</label>
                            <div id="price-range-hotel" class="price-range" style="margin-top:10px;"></div>
                            <span id="price-min-h" class="price-min">0</span> 
                            <span id="price-max-h" class="price-max">100</span> 
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <a href="#." class="btn_search HotelSearch"><i class="fa fa-search"></i> SEARCH</a>
                    </div>
               </div>
           </div> 
       </div>
    
       <div class="tab" style="text-align:left;">
           <input type="radio" id="tab-3" name="tab-group-1">
           <label for="tab-3" class="service">Transport</label>
         
           <div class="content_search">
               <div class="row">
                  <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select Pickup City</label>
                       <?= $this->Form->hidden('pickup_city',['class'=>'pickup_city','style'=>'width:100%']) ?> 
                    </div>
                    <div class="col-sm-6 col-md-3 margin_bottom">
                      <label>Select Destination City</label>
                       <?= $this->Form->hidden('destination_city',['class'=>'destination_city','style'=>'width:100%']) ?> 
                    </div>
                    <div class="col-sm-6 col-md-3"  style="margin-bottom:14px;">
                      <label>Select Category</label>
                      <select class="form-control selectpicker" name="t_category" id="t_category" data-live-search="true">
                            <option value="">Your Category</option>
                            <?php
                            foreach ($taxiCategories as $taxiCategory) {
                              echo"<option value='".$taxiCategory->id."'>".$taxiCategory->name."</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-6 col-md-3">
                      <a href="#." class="btn_search TransportSearch"><i class="fa fa-search"></i> SEARCH</a>
                    </div>
               </div>
           </div> 
       </div>
    </div>  
  </div>
	
	 <div class="container">
      <div class="section-p-30px proccess">
      <h4>How it Works</h4>
      <p>Vacations are precious, get the best service from destination experts!</p>
        <div class="row">
          <div class="col-xs-12 col-md-4">
          <?= $this->Html->image($cdn_path.'tripitoes_images/requirements.png',['class'=>'img-responsive','alt'=>'Select your Requirements','width'=>'50px']);?>
            <!--<img class="img-responsive" src="images/500+ destinations.png">-->
            <h6>Select your Requirements</h6>
          </div>
          <div class="col-xs-12 col-md-4">
          <?= $this->Html->image($cdn_path.'tripitoes_images/buy_p-h-t-1.png',['class'=>'img-responsive','alt'=>'Buy Holiday Packages, or Book Hotels, or Taxi Service','width'=>'52px']); ?>
            <!--<img class="img-responsive" src="images/fast booking.png">-->
            <h6>Buy Holiday Packages, or Book Hotels, or Taxi Service</h6>
          </div>
          <div class="col-xs-12 col-md-4">
          <?= $this->Html->image($cdn_path.'tripitoes_images/perfect_holiday.png',['class'=>'img-responsive','alt'=>'Enjoy a Perfect Holiday!','width'=>'53px']);?>
            <!--<img class="img-responsive" src="images/price guarantee.png">-->
            <h6>Enjoy a Perfect Holiday!</h6>
          </div>
        </div>
      </div>
    </div>
  
  <!--
    <div class="container">
      <div class="section-p-30px best_deals">
        <div class="row">
          <div class="col-md-7">
            <h4>Get the Best Deals for you in <br>Holiday Packages</h4>
            
            <p class="font-montserrat">Life is an adventure that needs to be lived to the utmost.<br>
			Travel far and wide not to escape life but to live it in all<br>
			its glory…Travel is more than sightseeing. It is a transformation<br>
			that goes deep and permanent in your very ideas of life!</p>
            
            <a href="#." class="btn btn-round-border">Explore Now</a>
          </div>
          <div class="col-md-5 text-right">
			<?= $this->Html->image($cdn_path.'tripitoes_images/best-deals.png',['class'=>'img-responsive','alt'=>'']);?>
           </div>
        </div>
      </div>
    </div>-->