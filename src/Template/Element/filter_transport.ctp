<?php if(!empty($taxiFleetPromotions->toArray())){ echo '<input type="hidden" id="TotalFoundRecord" value="'.$total_counts.'">';
   foreach ($taxiFleetPromotions as  $getPackage) {
        $ratingData=$TripitoesRating->rating($getPackage);
        $total_users=$ratingData['total_users'];
        $overallrating=$ratingData['overallrating'];
        if($total_users==0){
            $total_users=1;
            $overallrating=5;
        }
$url = (strtolower($getPackage->title));
$urlTitle=str_replace(" ","-",$url);
$urlTitle=str_replace("/","-",$urlTitle);
$ShareMessage=urlencode($site_url.'TaxiFleetPromotions/details/'.$getPackage->id.'/'.$urlTitle);


   ?>
    <!-- Package List -->
    <li class="animate fadeInUp" data-wow-delay="0.4s">
      <div class="items-in">
        <div class="row divheight">
          <div class="col-xs-3 col-sm-4 padding-right"> 
            <?php
            if($auth_login=='Yes')
            {
              echo $this->Html->link('<div class="save"><i class="ion-ios-bookmarks-outline"></i></div>',['controller'=>'TaxiFleetPromotions','action'=>'transportSave',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'hotelSave desktop']);
            }
            else
            {
              echo $this->Html->link('<div class="save"><i class="ion-ios-bookmarks-outline"></i></div>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn desktop']);
            }
            ?>

            <!--<a href="" class="desktop"><div class="save"> <i class="ion-ios-bookmarks-outline"></i></div></a>-->

            <div class="img redirect" redirectid="<?= $getPackage->id?>" urlTitle="<?= $urlTitle?>"> <?= $this->Html->image('hello.png',['alt'=>'Tripitoes','class'=>'imageformob imgheight spaical lazy','data-src'=>$cdn_path.$getPackage->image]);?> </div>
          </div>
          <div class="col-xs-6 col-sm-8 padding-right" redirectid="<?= $getPackage->id?>" urlTitle="<?= $urlTitle?>">
            <div class="details-sec">
                <?= $this->Html->link('<span style="float: left;">'.$getPackage->title.'</span>', ['controller'=>'TaxiFleetPromotions','action'=>'details',$getPackage->id,$urlTitle], ['escape'=>false]) ?> 
                 
                <?php 
                if($getPackage->user->isVerified==1){ ?>
                  <span class="verified dasktopverify">
                      <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Tripitoes','data-src'=>$cdn_path.'tripitoes_images/verified.png']);?>
                  </span>

                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image('hello.png',['style'=>'height: 12px !important; width: 12px !important; ','alt'=>'Tripitoes','class'=>'lazy','data-src'=>$cdn_path.'tripitoes_images/verifiedmobile.png']);?>
                  </span>
                <?php } ?>
                </a>
            </div>
            <div class="seller_rating redirect" redirectid="<?= $getPackage->id?>" urlTitle="<?= $urlTitle?>">
                <div class="seller-icon">
                    <i class="fa fa-star"></i>
                </div>
                <div class="s-rate">
                    <span style="color:#39aeff; font-size:12px;"><?= $overallrating;?></span><span style="color:#000; font-size:10px;">/5</span>
                     
                    <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users;?>)</span>

                </div>
                <div class="s-text">
                    <span>Seller Rating</span>
                </div>
            </div>
            <?php
            $package_name=[];
            foreach ($getPackage->taxi_fleet_promotion_rows as $post_travle_package_row) {
                $package_name[]=$post_travle_package_row->taxi_fleet_car_bus->name;
            }
            $packages=implode(', ', array_unique($package_name));
            ?> 
            <p class="data font-montserrat tip t_cat" data-toggle="tooltip" title="<?= $packages ?>">Category: <span><?= $packages; ?></span> 
            </p>                
            <?php
            $cityList='';
            $role=0;
            $operate_name=[];
            if(!empty($getPackage->taxi_fleet_promotion_cities)){
                foreach($getPackage->taxi_fleet_promotion_cities as $cities)
                { 
                    if($cities->city_id==0){@$cityList='All Cities'; $role=1;}
                    else{
                        $operate_name[]=$cities->city->name.'('.$cities->city->state->state_name.')';
                    }
                }
            }
            else{
                @$cityList='All Cities'; $role=1;
            }
            if($role==1){
                $stateList=array();
                foreach($getPackage->taxi_fleet_promotion_states as $statess)
                {  
                   $operate_name[]=$cityList.'('.$statess->state->state_name.')';
                }
            }
            $operatesIN=implode(', ', $operate_name);
            $map_loaction=$getPackage->user->address.', '.$getPackage->user->city->name.', '.$getPackage->user->state->state_name.', '.$getPackage->user->country->country_name;
            $map_loaction=str_replace(", ","+",$map_loaction);
            $map_loaction='http://maps.google.com/?q='.$map_loaction;

            $seller_loaction=$getPackage->user->address.', '.$getPackage->user->city->name.', '.$getPackage->user->state->state_name.', '.$getPackage->user->country->country_name;
            ?> 
            <p class="opertes font-montserrat" data-toggle="tooltip" title="<?= $operatesIN ?>">Operates In: <span><?= $operatesIN ?></span> 
             
            <p class="seller-name font-montserrat">
                Seller Name: <span><?= $getPackage->user->first_name.' '.$getPackage->user->last_name?></span>
            </p>
            
            <p class="t-location font-montserrat hotelData">
                <i class="ion-ios-location-outline"></i>Seller Location: <span>
                <a href="<?= $map_loaction; ?>" target="_blank"><?= $seller_loaction ?></a></span>
            </p>
            <p class="t-location font-montserrat responseive" style="display:none">
                <i class="ion-ios-location-outline"></i>Address: <span>
                <a href="<?= $map_loaction; ?>" target="_blank"><?= $seller_loaction ?></a></span>
            </p>

            <?php $yes='visibility:hidden'; 
                echo '<a href="" target="_blank" style="'.$yes.'" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>';
            ?>
            
            <div class="half-width item-btn-com desktop"> 
                <a href="javascript:void(0);" class="redirect" redirectid="<?= $getPackage->id?>" urlTitle="<?= $urlTitle?>"><i class="ion ion-ios-eye-outline"></i><span><?= $getPackage->trip_views?></span></a>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportLike',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>
               
                <a class="contactno">
                <i class="ion ion-ios-telephone-outline"></i>
                <span class="font-montserrat tooltipmobile"><?= $getPackage->user->mobile_number; ?></span>
                </a>
                <div class="share">
                <i class="ion-android-share-alt"></i>
                <?php  $shareLink = $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'details',$getPackage->id,$urlTitle,'_full'=>true,'_ssl'=>true]); ?> 
                <ul> 
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveShare/'.$getPackage->id]) ?>">
                        <a href="javascript:void(0);" id="shareBtn" class="shareBtn" shareLink="<?= $shareLink ?>"><i class="fa fa-facebook"></i></a>
                    </li>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveShare/'.$getPackage->id]) ?>">
                        <a hrefjavascript:void(0); onclick="window.open('http://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent('<?php echo  $shareLink ?>'), '', 'left=0,top=0,width=650,height=420,personalbar=0,toolbar=0,scrollbars=0,resizable=0');"><i class="fa fa-linkedin"></i></a> 
                    </li>-->

                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveShare/'.$getPackage->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello" ><i class="fa fa-whatsapp "></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveShare/'.$getPackage->id]) ?>">
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                    </li>
                </ul>
                </div>
            </div>
            <div class="half-width item-btn-com desktop" style="margin-top:12px;"> 
                <a href="#reportPopup" p_id="<?= $getPackage->id;?>" class="link popup-vedio video-btn reportPopup"><i class="ion-ios-flag"></i><span>Report</span></a>
                <a href="#userinfo<?= $getPackage->id ?>" class="popup-vedio seller-detail">
                    <i class="ion-information"></i>
                    <span>Info</span>
                </a>
                
                <div>
                <?= $this->Html->link('View Details',['controller'=>'TaxiFleetPromotions','action'=>'details',$getPackage->id,$urlTitle],['escape'=>false,'class'=>'btn-small btn-dark']) ?>
                 
                </div>
            </div>
            </div>
            
            <div class="col-xs-3 mobile">
                
                <div class="half-width item-btn-com">
                    <?php
                        if($auth_login=='Yes')
                        {
                          echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportLike',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportLike']);
                        }
                        else
                        {
                          echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$getPackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                        }
                        ?> 
                    <a href="#userinfo<?= $getPackage->id ?>" class="popup-vedio seller-detail">
                        <i class="ion-information"></i>
                        <span>Seller</span>
                    </a>
                </div>
            
                <div class="half-width item-btn-com"> 
                     
                    <?php
                    if($auth_login=='Yes')
                    {
                      echo $this->Html->link('<i class="ion ion-ios-bookmarks-outline"></i> <span>Save</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportSave',$getPackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'hotelSave ']);
                    }
                    else
                    {
                      echo $this->Html->link('<i class="ion ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn ']);
                    }
                    ?>
                <a class="contactno CallCount" href="tel:<?= $getPackage->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveCall/'.$getPackage->id]) ?>">
                    <i class="ion ion-ios-telephone-outline"></i><span>Call</span>
                    <span class="font-montserrat tooltipmobile"><?= $getPackage->user->mobile_number; ?></span>
                </a>
            </div>
            
                 
            </div>
            
        </div>
      </div>

 
<div id="userinfo<?= $getPackage->id ?>" class="zoom-anim-dialog mfp-hide pop-open-style">
  <div class="pop_up">
    <div class="video"> 
        <span class="details">
        <table width="100%" style="color: #6b6b6b;font-family: 'Montserrat', sans-serif;">
        <tr>
            <td colspan="2" height="25px">
            <p style="color:#5f9a02; font-size:16px;"><?= $getPackage->user->company_name; ?></p>
            </td>
        </tr>
        <tr>
            <td height="25px" width="8%"><i class="fa fa-user" ></i></td>
            <td height="25px" width="90%">
                <span><?= $getPackage->user->first_name.' '.$getPackage->user->last_name; ?></span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-phone" ></i></td>
            <td height="25px">
                <span ><?php if($getPackage->user->mobile_number){ echo $getPackage->user->mobile_number;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-envelope"></i></td>
            <td height="25px">
                <span style="font-size: 13px; word-break: break-all;line-height: 14px;"><?php if($getPackage->user->email){ echo $getPackage->user->email;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-globe"></i></td>
            <td height="25px"> 
                <span style="font-size: 13px;word-break: break-all;line-height: 16px;"><?php if($getPackage->user->web_url){ echo $getPackage->user->web_url;}else{echo"N/A";} ?>
                </span>
            </td>
        </tr>
        <tr>
            <td height="25px"><i class="fa fa-home"></i></td>
            <td height="25px">
                <span>
                <?php if($getPackage->user->address){ 
                        echo $getPackage->user->address.', ';
                        echo $getPackage->user->city->name.', ';
                        echo $getPackage->user->state->state_name.', ';
                        echo $getPackage->user->country->country_name;
                    }else{echo"N/A";} ?> 
                <span>
            </td>
        </tr>
    </table>
        </span>                    
    </div>
  </div>
</div>

    </li>
    <?php 
    }
} ?>