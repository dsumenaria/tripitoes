
<footer>
    <div class="container">
      <div class="text-center">             
            <ul class="social_icons">
                <li class="facebook"><a href="https://www.facebook.com/Tripitoes/" target="_blank"><i class="fa fa-facebook"></i> </a></li>
                <li class="twitter"><a href="https://www.instagram.com/bestoftripitoes/" target="_blank" ><i class="fa fa-instagram" target="_blank"></i> </a></li> 
                <li class="linkedin"><a href="#."><i class="fa fa-linkedin" ></i> </a></li>
          	</ul>
            
        	<ul class="f-links">
                <li> <?= $this->Html->link('About Us', ['controller'=>'Pages','action'=>'aboutus'], ['escape'=>false]) ?></li>
                <li> <?= $this->Html->link('Contact Us', ['controller'=>'Pages','action'=>'ContactUs'], ['escape'=>false]) ?></li>
                <li> <?= $this->Html->link('Privacy & Policy', ['controller'=>'Pages','action'=>'privacyPolicy'], ['escape'=>false]) ?></li>
                <li> <?= $this->Html->link('Terms & Conditions', ['controller'=>'Pages','action'=>'termConditions'], ['escape'=>false]) ?></li>
                <li><a href="#sell-Popup" class="popup-vedio"> Sell On Tripitoes</a></li>
            </ul>
      </div>
      <!-- Rights -->
      <div class="rights">
        <p>© 2018 DESIGN & DEVELOPED BY DURJA TRAVEL NETWORK PVT. LTD. POWERED BY 
          <?= $this->Html->link('www.TravelB2BHub.com', 'https://www.travelb2bhub.com/', ['escape'=>false,'target'=>"_blank","rel"=>"noreferrer"]) ?>
      </div>
    </div>
</footer>  