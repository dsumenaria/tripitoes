<div id="reportPopup" class="zoom-anim-dialog mfp-hide pop-open-style">
	<?php 
    	$redirect= $this->Url->build(['controller'  => $socialController,'action'=> $socialAction,'plugin' => false,'_full'=>true,'_ssl'=>true]); 
    ?>
      <div class="pop_up">
        <div class="video">
          <input type="hidden" id="package_selected"> 
            <h6>Report Promotion</h6>
            <label class="seller-location">User is not responsive to communication
            <input type="radio" id="tab-1" name="report"
             checked="checked" value="User is not responsive to communication" onclick="show0();">
            <span class="checkmark"></span>
            </label>
            <label class="seller-location">Photos are unclear, hazy or objectionable
            <input type="radio" id="tab-1" name="report"
             checked="checked" value="Photos are Unclear, hazy or objectionable" onclick="show0();">
            <span class="checkmark"></span>
            </label>
            <label class="seller-location">Title or description is inaccurate
            <input type="radio" id="tab-1" name="report"
             checked="checked" value="Title or Description is inaccurate" onclick="show0();">
            <span class="checkmark"></span>
            </label>
            <label class="seller-location">Price set is too high or too low
            <input type="radio" id="tab-1" name="report"
             checked="checked" value="Price set is too high or too low" onclick="show0();">
            <span class="checkmark"></span>
            </label>
            <label class="seller-location">Other
            <input type="radio" id="tab-1" name="report"
             checked="checked" value="Other" onclick="show0();">
            <span class="checkmark"></span>
            </label>
            <div align="center" style="width:106%;margin-top:16px">
            <?php 
            if($auth_login=='Yes')
            { 
              echo "<a href='javascript:void(0);' class='social-login packageReport  reportButtonS''>Submit</a>";               
            }
            else {
              echo $this->Form->postLink(
                  '<i class="fa fa-facebook"></i><span></span>',
                  ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['redirect'=>$redirect,'provider' => 'Facebook']],['escape'=>false,'class'=>'social-login reportButton']
              ); 
              echo $this->Form->postLink(
                  '<i class="fa fa-google-plus"></i><span></span>',
                  ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Google','redirect'=>$redirect]],['escape'=>false,'class'=>'social-login reportButton']
              ); 
              echo $this->Form->postLink(
                  '<i class="fa fa-linkedin"></i><span></span>',
                  ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'LinkedIn','redirect'=>$redirect]],['escape'=>false,'class'=>'social-login reportButton']
              ); 
            }
	            ?> 
	        </div>                      
        </div>
      </div>
    </div>