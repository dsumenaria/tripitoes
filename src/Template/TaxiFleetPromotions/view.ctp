<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TaxiFleetPromotion $taxiFleetPromotion
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Taxi Fleet Promotion'), ['action' => 'edit', $taxiFleetPromotion->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Taxi Fleet Promotion'), ['action' => 'delete', $taxiFleetPromotion->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotion->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Price Masters'), ['controller' => 'PriceMasters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Price Master'), ['controller' => 'PriceMasters', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Carts'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Cart'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Cities'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion City'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Likes'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Like'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Price Before Renews'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Price Before Renews'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Reports'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Report'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Rows'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion Row'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion States'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion State'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Taxi Fleet Promotion Views'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Taxi Fleet Promotion View'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Carts'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Cart'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Likes'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Like'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Reports'), ['controller' => 'TripitoesTaxiReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Report'), ['controller' => 'TripitoesTaxiReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Views'), ['controller' => 'TripitoesTaxiViews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="taxiFleetPromotions view large-9 medium-8 columns content">
    <h3><?= h($taxiFleetPromotion->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($taxiFleetPromotion->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Country') ?></th>
            <td><?= $taxiFleetPromotion->has('country') ? $this->Html->link($taxiFleetPromotion->country->id, ['controller' => 'Countries', 'action' => 'view', $taxiFleetPromotion->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($taxiFleetPromotion->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Document') ?></th>
            <td><?= h($taxiFleetPromotion->document) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price Master') ?></th>
            <td><?= $taxiFleetPromotion->has('price_master') ? $this->Html->link($taxiFleetPromotion->price_master->id, ['controller' => 'PriceMasters', 'action' => 'view', $taxiFleetPromotion->price_master->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $taxiFleetPromotion->has('user') ? $this->Html->link($taxiFleetPromotion->user->id, ['controller' => 'Users', 'action' => 'view', $taxiFleetPromotion->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Like Count') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->like_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Edited By') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->edited_by) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deleted') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->is_deleted) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Submitted From') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->submitted_from) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Notified') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->notified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Position') ?></th>
            <td><?= $this->Number->format($taxiFleetPromotion->position) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Visible Date') ?></th>
            <td><?= h($taxiFleetPromotion->visible_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created On') ?></th>
            <td><?= h($taxiFleetPromotion->created_on) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Edited On') ?></th>
            <td><?= h($taxiFleetPromotion->edited_on) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Fleet Detail') ?></h4>
        <?= $this->Text->autoParagraph(h($taxiFleetPromotion->fleet_detail)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Carts') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col"><?= __('Is Deleted') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_carts as $taxiFleetPromotionCarts): ?>
            <tr>
                <td><?= h($taxiFleetPromotionCarts->id) ?></td>
                <td><?= h($taxiFleetPromotionCarts->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionCarts->user_id) ?></td>
                <td><?= h($taxiFleetPromotionCarts->created_on) ?></td>
                <td><?= h($taxiFleetPromotionCarts->is_deleted) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'view', $taxiFleetPromotionCarts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'edit', $taxiFleetPromotionCarts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionCarts', 'action' => 'delete', $taxiFleetPromotionCarts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionCarts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Cities') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_cities)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('City Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_cities as $taxiFleetPromotionCities): ?>
            <tr>
                <td><?= h($taxiFleetPromotionCities->id) ?></td>
                <td><?= h($taxiFleetPromotionCities->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionCities->city_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'view', $taxiFleetPromotionCities->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'edit', $taxiFleetPromotionCities->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionCities', 'action' => 'delete', $taxiFleetPromotionCities->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionCities->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Likes') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_likes as $taxiFleetPromotionLikes): ?>
            <tr>
                <td><?= h($taxiFleetPromotionLikes->id) ?></td>
                <td><?= h($taxiFleetPromotionLikes->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionLikes->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'view', $taxiFleetPromotionLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'edit', $taxiFleetPromotionLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionLikes', 'action' => 'delete', $taxiFleetPromotionLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Price Before Renews') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_price_before_renews)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Price Master Id') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Visible Date') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_price_before_renews as $taxiFleetPromotionPriceBeforeRenews): ?>
            <tr>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->id) ?></td>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->price_master_id) ?></td>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->price) ?></td>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->visible_date) ?></td>
                <td><?= h($taxiFleetPromotionPriceBeforeRenews->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'view', $taxiFleetPromotionPriceBeforeRenews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'edit', $taxiFleetPromotionPriceBeforeRenews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionPriceBeforeRenews', 'action' => 'delete', $taxiFleetPromotionPriceBeforeRenews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionPriceBeforeRenews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Reports') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Report Reason Id') ?></th>
                <th scope="col"><?= __('Comment') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_reports as $taxiFleetPromotionReports): ?>
            <tr>
                <td><?= h($taxiFleetPromotionReports->id) ?></td>
                <td><?= h($taxiFleetPromotionReports->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionReports->user_id) ?></td>
                <td><?= h($taxiFleetPromotionReports->report_reason_id) ?></td>
                <td><?= h($taxiFleetPromotionReports->comment) ?></td>
                <td><?= h($taxiFleetPromotionReports->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'view', $taxiFleetPromotionReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'edit', $taxiFleetPromotionReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionReports', 'action' => 'delete', $taxiFleetPromotionReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Rows') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_rows)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Car Bus Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_rows as $taxiFleetPromotionRows): ?>
            <tr>
                <td><?= h($taxiFleetPromotionRows->id) ?></td>
                <td><?= h($taxiFleetPromotionRows->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionRows->taxi_fleet_car_bus_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'view', $taxiFleetPromotionRows->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'edit', $taxiFleetPromotionRows->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionRows', 'action' => 'delete', $taxiFleetPromotionRows->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionRows->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion States') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_states)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('State Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_states as $taxiFleetPromotionStates): ?>
            <tr>
                <td><?= h($taxiFleetPromotionStates->id) ?></td>
                <td><?= h($taxiFleetPromotionStates->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionStates->state_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'view', $taxiFleetPromotionStates->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'edit', $taxiFleetPromotionStates->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionStates', 'action' => 'delete', $taxiFleetPromotionStates->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionStates->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Taxi Fleet Promotion Views') ?></h4>
        <?php if (!empty($taxiFleetPromotion->taxi_fleet_promotion_views)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->taxi_fleet_promotion_views as $taxiFleetPromotionViews): ?>
            <tr>
                <td><?= h($taxiFleetPromotionViews->id) ?></td>
                <td><?= h($taxiFleetPromotionViews->taxi_fleet_promotion_id) ?></td>
                <td><?= h($taxiFleetPromotionViews->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'view', $taxiFleetPromotionViews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'edit', $taxiFleetPromotionViews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TaxiFleetPromotionViews', 'action' => 'delete', $taxiFleetPromotionViews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $taxiFleetPromotionViews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Carts') ?></h4>
        <?php if (!empty($taxiFleetPromotion->tripitoes_taxi_carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->tripitoes_taxi_carts as $tripitoesTaxiCarts): ?>
            <tr>
                <td><?= h($tripitoesTaxiCarts->id) ?></td>
                <td><?= h($tripitoesTaxiCarts->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiCarts->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiCarts->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'view', $tripitoesTaxiCarts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'edit', $tripitoesTaxiCarts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'delete', $tripitoesTaxiCarts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiCarts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Likes') ?></h4>
        <?php if (!empty($taxiFleetPromotion->tripitoes_taxi_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->tripitoes_taxi_likes as $tripitoesTaxiLikes): ?>
            <tr>
                <td><?= h($tripitoesTaxiLikes->id) ?></td>
                <td><?= h($tripitoesTaxiLikes->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiLikes->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiLikes->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'view', $tripitoesTaxiLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'edit', $tripitoesTaxiLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'delete', $tripitoesTaxiLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Reports') ?></h4>
        <?php if (!empty($taxiFleetPromotion->tripitoes_taxi_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->tripitoes_taxi_reports as $tripitoesTaxiReports): ?>
            <tr>
                <td><?= h($tripitoesTaxiReports->id) ?></td>
                <td><?= h($tripitoesTaxiReports->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiReports->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiReports->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiReports', 'action' => 'view', $tripitoesTaxiReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiReports', 'action' => 'edit', $tripitoesTaxiReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiReports', 'action' => 'delete', $tripitoesTaxiReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Views') ?></h4>
        <?php if (!empty($taxiFleetPromotion->tripitoes_taxi_views)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($taxiFleetPromotion->tripitoes_taxi_views as $tripitoesTaxiViews): ?>
            <tr>
                <td><?= h($tripitoesTaxiViews->id) ?></td>
                <td><?= h($tripitoesTaxiViews->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiViews->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiViews->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'view', $tripitoesTaxiViews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiViews', 'action' => 'edit', $tripitoesTaxiViews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiViews', 'action' => 'delete', $tripitoesTaxiViews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiViews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
