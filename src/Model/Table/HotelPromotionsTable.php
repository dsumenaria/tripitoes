<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HotelPromotions Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\HotelCategoriesTable|\Cake\ORM\Association\BelongsTo $HotelCategories
 * @property \App\Model\Table\PriceMastersTable|\Cake\ORM\Association\BelongsTo $PriceMasters
 * @property \App\Model\Table\HotelPromotionCartsTable|\Cake\ORM\Association\HasMany $HotelPromotionCarts
 * @property \App\Model\Table\HotelPromotionCitiesTable|\Cake\ORM\Association\HasMany $HotelPromotionCities
 * @property \App\Model\Table\HotelPromotionLikesTable|\Cake\ORM\Association\HasMany $HotelPromotionLikes
 * @property \App\Model\Table\HotelPromotionPriceBeforeRenewsTable|\Cake\ORM\Association\HasMany $HotelPromotionPriceBeforeRenews
 * @property \App\Model\Table\HotelPromotionReportsTable|\Cake\ORM\Association\HasMany $HotelPromotionReports
 * @property \App\Model\Table\HotelPromotionViewsTable|\Cake\ORM\Association\HasMany $HotelPromotionViews
 * @property \App\Model\Table\TripitoesHotelCartsTable|\Cake\ORM\Association\HasMany $TripitoesHotelCarts
 * @property \App\Model\Table\TripitoesHotelLikesTable|\Cake\ORM\Association\HasMany $TripitoesHotelLikes
 * @property \App\Model\Table\TripitoesHotelReportsTable|\Cake\ORM\Association\HasMany $TripitoesHotelReports
 * @property \App\Model\Table\TripitoesHotelViewsTable|\Cake\ORM\Association\HasMany $TripitoesHotelViews
 *
 * @method \App\Model\Entity\HotelPromotion get($primaryKey, $options = [])
 * @method \App\Model\Entity\HotelPromotion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HotelPromotion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HotelPromotion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HotelPromotion|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HotelPromotion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HotelPromotion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HotelPromotion findOrCreate($search, callable $callback = null, $options = [])
 */
class HotelPromotionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('hotel_promotions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('HotelCategories', [
            'foreignKey' => 'hotel_category_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PriceMasters', [
            'foreignKey' => 'price_master_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('HotelPromotionCarts', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('HotelPromotionCities', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('HotelPromotionLikes', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('HotelPromotionPriceBeforeRenews', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('HotelPromotionReports', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('HotelPromotionViews', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('TripitoesHotelCarts', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('TripitoesHotelLikes', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('TripitoesHotelReports', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->hasMany('TripitoesHotelViews', [
            'foreignKey' => 'hotel_promotion_id'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('HotelCities', [
            'className' => 'Cities',
            'foreignKey' => 'city_id',
            'propertyName' => 'hotel_cities'
        ]);
        $this->belongsTo('States');
        $this->belongsTo('TripitoesUsers');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('hotel_name')
            ->maxLength('hotel_name', 150)
            ->requirePresence('hotel_name', 'create')
            ->notEmpty('hotel_name');

        $validator
            ->scalar('hotel_location')
            ->maxLength('hotel_location', 300)
            ->requirePresence('hotel_location', 'create')
            ->notEmpty('hotel_location');

        $validator
            ->numeric('cheap_tariff')
            ->requirePresence('cheap_tariff', 'create')
            ->notEmpty('cheap_tariff');

        $validator
            ->numeric('expensive_tariff')
            ->requirePresence('expensive_tariff', 'create')
            ->notEmpty('expensive_tariff');

        $validator
            ->scalar('website')
            ->maxLength('website', 400)
            ->requirePresence('website', 'create')
            ->notEmpty('website');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('hotel_pic')
            ->requirePresence('hotel_pic', 'create')
            ->notEmpty('hotel_pic');

        $validator
            ->scalar('payment_status')
            ->maxLength('payment_status', 100)
            ->requirePresence('payment_status', 'create')
            ->notEmpty('payment_status');

        $validator
            ->numeric('total_charges')
            ->requirePresence('total_charges', 'create')
            ->notEmpty('total_charges');

        $validator
            ->date('visible_date')
            ->requirePresence('visible_date', 'create')
            ->notEmpty('visible_date');

        $validator
            ->integer('hotel_rating')
            ->requirePresence('hotel_rating', 'create')
            ->notEmpty('hotel_rating');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->date('updated_on')
            ->requirePresence('updated_on', 'create')
            ->notEmpty('updated_on');

        $validator
            ->date('accept_date')
            ->requirePresence('accept_date', 'create')
            ->notEmpty('accept_date');

        $validator
            ->integer('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->integer('submitted_from')
            ->requirePresence('submitted_from', 'create')
            ->notEmpty('submitted_from');

        $validator
            ->integer('notified')
            ->requirePresence('notified', 'create')
            ->notEmpty('notified');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['hotel_category_id'], 'HotelCategories'));
        $rules->add($rules->existsIn(['price_master_id'], 'PriceMasters'));

        return $rules;
    }
}
