<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesBlogComments Model
 *
 * @property \App\Model\Table\TripitoesBlogsTable|\Cake\ORM\Association\BelongsTo $TripitoesBlogs
 * @property \App\Model\Table\TripitoesBlogCommentRowsTable|\Cake\ORM\Association\HasMany $TripitoesBlogCommentRows
 *
 * @method \App\Model\Entity\TripitoesBlogComment get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogComment findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesBlogCommentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_blog_comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TripitoesBlogs', [
            'foreignKey' => 'tripitoes_blog_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('TripitoesBlogCommentRows', [
            'foreignKey' => 'tripitoes_blog_comment_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('comments')
            ->requirePresence('comments', 'create')
            ->notEmpty('comments');

       /* $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');
*/
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tripitoes_blog_id'], 'TripitoesBlogs'));

        return $rules;
    }
}
