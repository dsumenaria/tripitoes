<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesRatings Model
 *
 * @property \App\Model\Table\TripitoesUsersTable|\Cake\ORM\Association\BelongsTo $TripitoesUsers
 * @property |\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\TripitoesRating get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesRating newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesRating[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesRating|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesRating|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesRating patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesRating[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesRating findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesRatingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_ratings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TripitoesUsers', [
            'foreignKey' => 'tripitoes_user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('rating')
            ->requirePresence('rating', 'create')
            ->notEmpty('rating');

        $validator
            ->scalar('comments')
            ->requirePresence('comments', 'create')
            ->notEmpty('comments');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tripitoes_user_id'], 'TripitoesUsers'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
