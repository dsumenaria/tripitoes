<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesPackageReports Model
 *
 * @property \App\Model\Table\PostTravlePackagesTable|\Cake\ORM\Association\BelongsTo $PostTravlePackages
 * @property \App\Model\Table\TripitoesUsersTable|\Cake\ORM\Association\BelongsTo $TripitoesUsers
 *
 * @method \App\Model\Entity\TripitoesPackageReport get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesPackageReport findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesPackageReportsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_package_reports');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('PostTravlePackages', [
            'foreignKey' => 'post_travle_package_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TripitoesUsers', [
            'foreignKey' => 'tripitoes_user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['post_travle_package_id'], 'PostTravlePackages'));
        $rules->add($rules->existsIn(['tripitoes_user_id'], 'TripitoesUsers'));

        return $rules;
    }
}
