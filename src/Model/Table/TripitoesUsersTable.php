<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\EventManager;
use Cake\I18n\FrozenDate;
use Cake\I18n\FrozenTime;
/**
 * TripitoesUsers Model
 *
 * @property \App\Model\Table\FacebooksTable|\Cake\ORM\Association\BelongsTo $Facebooks
 * @property \App\Model\Table\InstagramsTable|\Cake\ORM\Association\BelongsTo $Instagrams
 * @property \App\Model\Table\GooglesTable|\Cake\ORM\Association\BelongsTo $Googles
 * @property \App\Model\Table\TripitoesHotelCartsTable|\Cake\ORM\Association\HasMany $TripitoesHotelCarts
 * @property \App\Model\Table\TripitoesHotelLikesTable|\Cake\ORM\Association\HasMany $TripitoesHotelLikes
 * @property \App\Model\Table\TripitoesHotelReportsTable|\Cake\ORM\Association\HasMany $TripitoesHotelReports
 * @property \App\Model\Table\TripitoesHotelViewsTable|\Cake\ORM\Association\HasMany $TripitoesHotelViews
 * @property \App\Model\Table\TripitoesPackageCartsTable|\Cake\ORM\Association\HasMany $TripitoesPackageCarts
 * @property \App\Model\Table\TripitoesPackageLikesTable|\Cake\ORM\Association\HasMany $TripitoesPackageLikes
 * @property \App\Model\Table\TripitoesPackageReportsTable|\Cake\ORM\Association\HasMany $TripitoesPackageReports
 * @property \App\Model\Table\TripitoesPackageViewsTable|\Cake\ORM\Association\HasMany $TripitoesPackageViews
 * @property \App\Model\Table\TripitoesTaxiCartsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiCarts
 * @property \App\Model\Table\TripitoesTaxiLikesTable|\Cake\ORM\Association\HasMany $TripitoesTaxiLikes
 * @property \App\Model\Table\TripitoesTaxiReportsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiReports
 * @property \App\Model\Table\TripitoesTaxiViewsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiViews
 *
 * @method \App\Model\Entity\TripitoesUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesUser|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesUser findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesUsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        
        $this->hasMany('TripitoesHotelCarts', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesHotelLikes', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesHotelReports', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesHotelViews', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesPackageCarts', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesPackageLikes', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesPackageReports', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesPackageViews', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesTaxiCarts', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesTaxiLikes', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesTaxiReports', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesTaxiViews', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        $this->hasMany('TripitoesRatings', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        
        $this->hasMany('SocialProfiles', [
            'foreignKey' => 'tripitoes_user_id'
        ]);
        
        $this->BelongsTo('Users');

        $this->hasMany('ADmad/HybridAuth.SocialProfiles');

        \Cake\Event\EventManager::instance()->on('HybridAuth.newUser', [$this, 'createUser']);
        \Cake\Event\EventManager::instance()->on('HybridAuth.login', [$this, 'updateUser']);
    }
    public function createUser(\Cake\Event\Event $event) {
        // Entity representing record in social_profiles table
        
        $profile = $event->data()['profile'];

        // Make sure here that all the required fields are actually present

        $user = $this->newEntity(['identifier' => $profile->identifier,'provider' => $profile->provider]);
        
        $user = $this->save($user);
        
        if (!$user) {
            throw new \RuntimeException('Unable to save new user');
        }
        return $user;
    }
    public function updateUser(\Cake\Event\Event $event, array $user) 
    {
        $this->updateAll(['logins = logins + 1', 'last_login' => new FrozenTime()], ['id' => $user['id']]);
        exit;
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        $validator
            ->requirePresence('identifier', 'create')
            ->notEmpty('identifier');

        /*$validator
            ->scalar('name')
            ->maxLength('name', 100)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        

        $validator
            ->scalar('mobile_no')
            ->maxLength('mobile_no', 15)
            ->requirePresence('mobile_no', 'create')
            ->notEmpty('mobile_no');

        $validator
            ->scalar('latitude')
            ->maxLength('latitude', 50)
            ->requirePresence('latitude', 'create')
            ->notEmpty('latitude');

        $validator
            ->scalar('longitude')
            ->maxLength('longitude', 50)
            ->requirePresence('longitude', 'create')
            ->notEmpty('longitude');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}
