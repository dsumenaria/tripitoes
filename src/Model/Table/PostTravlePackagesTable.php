<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PostTravlePackages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Currencies
 * @property \Cake\ORM\Association\BelongsTo $Countries
 * @property \Cake\ORM\Association\BelongsTo $PriceMasters
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $PostTravlePackageCities
 * @property \Cake\ORM\Association\HasMany $PostTravlePackageRows
 * @property \Cake\ORM\Association\HasMany $PostTravlePackageStates
 *
 * @method \App\Model\Entity\PostTravlePackage get($primaryKey, $options = [])
 * @method \App\Model\Entity\PostTravlePackage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostTravlePackage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage findOrCreate($search, callable $callback = null, $options = [])
 */
class PostTravlePackagesTable extends Table
{
	 
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('post_travle_packages');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PriceMasters', [
            'foreignKey' => 'price_master_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('PostTravlePackageCities', [
            'foreignKey' => 'post_travle_package_id',
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('PostTravlePackageRows', [
            'foreignKey' => 'post_travle_package_id',
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('PostTravlePackageLikes', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('PostTravlePackageViews', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('PostTravlePackageStates', [
            'foreignKey' => 'post_travle_package_id',
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('PostTravlePackageCountries', [
            'foreignKey' => 'post_travle_package_id',
            'saveStrategy'=>'replace'
        ]);
        $this->hasMany('PostTravlePackagePriceBeforeRenews', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('PostTravlePackageCarts', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('PostTravlePackageReports', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        
        $this->belongsToMany('PackageCities', [
            'className' => 'Cities',
            'foreignKey' => 'post_travle_package_id',
            'targetForeignKey' => 'city_id',
            'joinTable' => 'post_travle_package_cities',
            'propertyName' => 'package_cities'
        ]); 
        $this->belongsToMany('Countries', [
            'foreignKey' => 'post_travle_package_id',
            'targetForeignKey' => 'country_id',
            'joinTable' => 'post_travle_package_countries'
        ]);
        
        $this->hasMany('TripitoesPackageCarts', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('TripitoesPackageLikes', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('TripitoesPackageReports', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->hasMany('TripitoesPackageViews', [
            'foreignKey' => 'post_travle_package_id'
        ]);
        $this->belongsTo('TripitoesRatings');
        $this->belongsTo('TripitoesUsers');
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['price_master_id'], 'PriceMasters'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}

/**
 * PostTravlePackages Model
 *
 * @property \App\Model\Table\CurrenciesTable|\Cake\ORM\Association\BelongsTo $Currencies
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\PriceMastersTable|\Cake\ORM\Association\BelongsTo $PriceMasters
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PostTravlePackageCartsTable|\Cake\ORM\Association\HasMany $PostTravlePackageCarts
 * @property \App\Model\Table\PostTravlePackageCitiesTable|\Cake\ORM\Association\HasMany $PostTravlePackageCities
 * @property \App\Model\Table\PostTravlePackageCountriesTable|\Cake\ORM\Association\HasMany $PostTravlePackageCountries
 * @property \App\Model\Table\PostTravlePackageLikesTable|\Cake\ORM\Association\HasMany $PostTravlePackageLikes
 * @property \App\Model\Table\PostTravlePackagePriceBeforeRenewsTable|\Cake\ORM\Association\HasMany $PostTravlePackagePriceBeforeRenews
 * @property \App\Model\Table\PostTravlePackageReportsTable|\Cake\ORM\Association\HasMany $PostTravlePackageReports
 * @property \App\Model\Table\PostTravlePackageRowsTable|\Cake\ORM\Association\HasMany $PostTravlePackageRows
 * @property \App\Model\Table\PostTravlePackageStatesTable|\Cake\ORM\Association\HasMany $PostTravlePackageStates
 * @property \App\Model\Table\PostTravlePackageViewsTable|\Cake\ORM\Association\HasMany $PostTravlePackageViews
 * @property \App\Model\Table\TripitoesPackageCartsTable|\Cake\ORM\Association\HasMany $TripitoesPackageCarts
 * @property \App\Model\Table\TripitoesPackageLikesTable|\Cake\ORM\Association\HasMany $TripitoesPackageLikes
 * @property \App\Model\Table\TripitoesPackageReportsTable|\Cake\ORM\Association\HasMany $TripitoesPackageReports
 * @property \App\Model\Table\TripitoesPackageViewsTable|\Cake\ORM\Association\HasMany $TripitoesPackageViews
 *
 * @method \App\Model\Entity\PostTravlePackage get($primaryKey, $options = [])
 * @method \App\Model\Entity\PostTravlePackage newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostTravlePackage|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PostTravlePackage patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PostTravlePackage findOrCreate($search, callable $callback = null, $options = [])
 */
