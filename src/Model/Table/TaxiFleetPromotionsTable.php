<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TaxiFleetPromotions Model
 *
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\PriceMastersTable|\Cake\ORM\Association\BelongsTo $PriceMasters
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\TaxiFleetPromotionCartsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionCarts
 * @property \App\Model\Table\TaxiFleetPromotionCitiesTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionCities
 * @property \App\Model\Table\TaxiFleetPromotionLikesTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionLikes
 * @property \App\Model\Table\TaxiFleetPromotionPriceBeforeRenewsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionPriceBeforeRenews
 * @property \App\Model\Table\TaxiFleetPromotionReportsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionReports
 * @property \App\Model\Table\TaxiFleetPromotionRowsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionRows
 * @property \App\Model\Table\TaxiFleetPromotionStatesTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionStates
 * @property \App\Model\Table\TaxiFleetPromotionViewsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionViews
 * @property \App\Model\Table\TripitoesTaxiCartsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiCarts
 * @property \App\Model\Table\TripitoesTaxiLikesTable|\Cake\ORM\Association\HasMany $TripitoesTaxiLikes
 * @property \App\Model\Table\TripitoesTaxiReportsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiReports
 * @property \App\Model\Table\TripitoesTaxiViewsTable|\Cake\ORM\Association\HasMany $TripitoesTaxiViews
 *
 * @method \App\Model\Entity\TaxiFleetPromotion get($primaryKey, $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TaxiFleetPromotion findOrCreate($search, callable $callback = null, $options = [])
 */
class TaxiFleetPromotionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('taxi_fleet_promotions');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PriceMasters', [
            'foreignKey' => 'price_master_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('TaxiFleetPromotionCarts', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionCities', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionLikes', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionPriceBeforeRenews', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionReports', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionRows', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionStates', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TaxiFleetPromotionViews', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TripitoesTaxiCarts', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TripitoesTaxiLikes', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TripitoesTaxiReports', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->hasMany('TripitoesTaxiViews', [
            'foreignKey' => 'taxi_fleet_promotion_id'
        ]);
        $this->belongsTo('TripitoesUsers');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('fleet_detail')
            ->requirePresence('fleet_detail', 'create')
            ->notEmpty('fleet_detail');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->scalar('document')
            ->maxLength('document', 255)
            ->requirePresence('document', 'create')
            ->notEmpty('document');

        $validator
            ->decimal('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->integer('like_count')
            ->requirePresence('like_count', 'create')
            ->notEmpty('like_count');

        $validator
            ->date('visible_date')
            ->requirePresence('visible_date', 'create')
            ->notEmpty('visible_date');

        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->integer('edited_by')
            ->requirePresence('edited_by', 'create')
            ->notEmpty('edited_by');

        $validator
            ->dateTime('edited_on')
            ->requirePresence('edited_on', 'create')
            ->notEmpty('edited_on');

        $validator
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->integer('submitted_from')
            ->requirePresence('submitted_from', 'create')
            ->notEmpty('submitted_from');

        $validator
            ->integer('notified')
            ->requirePresence('notified', 'create')
            ->notEmpty('notified');

        $validator
            ->integer('position')
            ->requirePresence('position', 'create')
            ->notEmpty('position');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['price_master_id'], 'PriceMasters'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
