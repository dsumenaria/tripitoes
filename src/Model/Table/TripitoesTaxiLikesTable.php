<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesTaxiLikes Model
 *
 * @property \App\Model\Table\TaxiFleetPromotionsTable|\Cake\ORM\Association\BelongsTo $TaxiFleetPromotions
 * @property \App\Model\Table\TripitoesUsersTable|\Cake\ORM\Association\BelongsTo $TripitoesUsers
 *
 * @method \App\Model\Entity\TripitoesTaxiLike get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesTaxiLike findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesTaxiLikesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_taxi_likes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TaxiFleetPromotions', [
            'foreignKey' => 'taxi_fleet_promotion_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('TripitoesUsers', [
            'foreignKey' => 'tripitoes_user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('timestamp')
            ->requirePresence('timestamp', 'create')
            ->notEmpty('timestamp');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['taxi_fleet_promotion_id'], 'TaxiFleetPromotions'));
        $rules->add($rules->existsIn(['tripitoes_user_id'], 'TripitoesUsers'));

        return $rules;
    }
}
