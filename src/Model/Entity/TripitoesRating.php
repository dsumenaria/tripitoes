<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TripitoesRating Entity
 *
 * @property int $id
 * @property int $tripitoes_user_id
 * @property int $user_id
 * @property int $rating
 * @property string $comments
 * @property \Cake\I18n\FrozenTime $created_on
 *
 * @property \App\Model\Entity\TripitoesUser $tripitoes_user
 */
class TripitoesRating extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tripitoes_user_id' => true,
        'user_id' => true,
        'rating' => true,
        'comments' => true,
        'created_on' => true,
        'tripitoes_user' => true
    ];
}
