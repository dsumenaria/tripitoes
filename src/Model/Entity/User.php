<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $password
 * @property string $mobile_number
 * @property string $p_contact
 * @property string $fax
 * @property string $email
 * @property int $status
 * @property string $company_name
 * @property string $comp_image1
 * @property string $comp_image2
 * @property string $id_card
 * @property string $address
 * @property string $image
 * @property int $country_id
 * @property int $city_id
 * @property int $state_id
 * @property int $role_id
 * @property \Cake\I18n\FrozenTime $create_at
 * @property string $description
 * @property string $adress1
 * @property string $locality
 * @property string $pincode
 * @property string $preference
 * @property int $email_verified
 * @property string $profile_pic
 * @property string $pancard_pic
 * @property string $company_img_1_pic
 * @property string $company_img_2_pic
 * @property string $id_card_pic
 * @property string $company_shop_registration_pic
 * @property int $hotel_rating
 * @property string $hotel_name
 * @property string $hotel_categories
 * @property string $web_url
 * @property string $iata_pic
 * @property string $tafi_pic
 * @property string $taai_pic
 * @property string $iato_pic
 * @property string $adyoi_pic
 * @property string $iso9001_pic
 * @property string $uftaa_pic
 * @property string $adtoi_pic
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property \Cake\I18n\FrozenTime $last_login
 * @property string $activation
 * @property string $device_id
 * @property int $active
 * @property string $mobile_otp
 * @property int $blocked
 * @property int $is_deleted
 * @property \Cake\I18n\FrozenTime $deleted_on
 * @property float $percentage
 * @property bool $isVerified
 *
 * @property \App\Model\Entity\TravelCertificate[] $travel_certificates
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Role $role
 * @property \App\Model\Entity\Device $device
 * @property \App\Model\Entity\BusinessBuddy[] $business_buddies
 * @property \App\Model\Entity\Credit[] $credits
 * @property \App\Model\Entity\EventPlannerPromotionCart[] $event_planner_promotion_carts
 * @property \App\Model\Entity\EventPlannerPromotionLike[] $event_planner_promotion_likes
 * @property \App\Model\Entity\EventPlannerPromotionReport[] $event_planner_promotion_reports
 * @property \App\Model\Entity\EventPlannerPromotionView[] $event_planner_promotion_views
 * @property \App\Model\Entity\EventPlannerPromotion[] $event_planner_promotions
 * @property \App\Model\Entity\HotelPromotionCart[] $hotel_promotion_carts
 * @property \App\Model\Entity\HotelPromotionLike[] $hotel_promotion_likes
 * @property \App\Model\Entity\HotelPromotionReport[] $hotel_promotion_reports
 * @property \App\Model\Entity\HotelPromotionView[] $hotel_promotion_views
 * @property \App\Model\Entity\HotelPromotion[] $hotel_promotions
 * @property \App\Model\Entity\Hotel[] $hotels
 * @property \App\Model\Entity\PostTravlePackageCart[] $post_travle_package_carts
 * @property \App\Model\Entity\PostTravlePackageLike[] $post_travle_package_likes
 * @property \App\Model\Entity\PostTravlePackageReport[] $post_travle_package_reports
 * @property \App\Model\Entity\PostTravlePackageView[] $post_travle_package_views
 * @property \App\Model\Entity\PostTravlePackage[] $post_travle_packages
 * @property \App\Model\Entity\Promotion[] $promotion
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\Response[] $responses
 * @property \App\Model\Entity\TaxiFleetPromotionCart[] $taxi_fleet_promotion_carts
 * @property \App\Model\Entity\TaxiFleetPromotionLike[] $taxi_fleet_promotion_likes
 * @property \App\Model\Entity\TaxiFleetPromotionReport[] $taxi_fleet_promotion_reports
 * @property \App\Model\Entity\TaxiFleetPromotionView[] $taxi_fleet_promotion_views
 * @property \App\Model\Entity\TaxiFleetPromotion[] $taxi_fleet_promotions
 * @property \App\Model\Entity\TempRating[] $temp_ratings
 * @property \App\Model\Entity\Testimonial[] $testimonial
 * @property \App\Model\Entity\Transport[] $transports
 * @property \App\Model\Entity\UserChat[] $user_chats
 * @property \App\Model\Entity\UserFeedback[] $user_feedbacks
 * @property \App\Model\Entity\UserRating[] $user_ratings
 * @property \App\Model\Entity\UserRight[] $user_rights
 * @property \App\Model\Entity\Userdetail[] $userdetails
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
