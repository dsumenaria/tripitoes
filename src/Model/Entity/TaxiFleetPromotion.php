<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TaxiFleetPromotion Entity
 *
 * @property int $id
 * @property string $title
 * @property int $country_id
 * @property string $fleet_detail
 * @property string $image
 * @property string $document
 * @property int $price_master_id
 * @property float $price
 * @property int $like_count
 * @property \Cake\I18n\FrozenDate $visible_date
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created_on
 * @property int $edited_by
 * @property \Cake\I18n\FrozenTime $edited_on
 * @property int $is_deleted
 * @property int $submitted_from
 * @property int $notified
 * @property int $position
 *
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\PriceMaster $price_master
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\TaxiFleetPromotionCart[] $taxi_fleet_promotion_carts
 * @property \App\Model\Entity\TaxiFleetPromotionCity[] $taxi_fleet_promotion_cities
 * @property \App\Model\Entity\TaxiFleetPromotionLike[] $taxi_fleet_promotion_likes
 * @property \App\Model\Entity\TaxiFleetPromotionPriceBeforeRenews[] $taxi_fleet_promotion_price_before_renews
 * @property \App\Model\Entity\TaxiFleetPromotionReport[] $taxi_fleet_promotion_reports
 * @property \App\Model\Entity\TaxiFleetPromotionRow[] $taxi_fleet_promotion_rows
 * @property \App\Model\Entity\TaxiFleetPromotionState[] $taxi_fleet_promotion_states
 * @property \App\Model\Entity\TaxiFleetPromotionView[] $taxi_fleet_promotion_views
 * @property \App\Model\Entity\TripitoesTaxiCart[] $tripitoes_taxi_carts
 * @property \App\Model\Entity\TripitoesTaxiLike[] $tripitoes_taxi_likes
 * @property \App\Model\Entity\TripitoesTaxiReport[] $tripitoes_taxi_reports
 * @property \App\Model\Entity\TripitoesTaxiView[] $tripitoes_taxi_views
 */
class TaxiFleetPromotion extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];
}
