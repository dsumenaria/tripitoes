<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TripitoesUser Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $mobile_no
 * @property string $facebook_id
 * @property string $instagram_id
 * @property string $google_id
 * @property string $latitude
 * @property string $longitude
 * @property \Cake\I18n\FrozenTime $created_on
 *
 * @property \App\Model\Entity\Facebook $facebook
 * @property \App\Model\Entity\Instagram $instagram
 * @property \App\Model\Entity\Google $google
 * @property \App\Model\Entity\TripitoesHotelCart[] $tripitoes_hotel_carts
 * @property \App\Model\Entity\TripitoesHotelLike[] $tripitoes_hotel_likes
 * @property \App\Model\Entity\TripitoesHotelReport[] $tripitoes_hotel_reports
 * @property \App\Model\Entity\TripitoesHotelView[] $tripitoes_hotel_views
 * @property \App\Model\Entity\TripitoesPackageCart[] $tripitoes_package_carts
 * @property \App\Model\Entity\TripitoesPackageLike[] $tripitoes_package_likes
 * @property \App\Model\Entity\TripitoesPackageReport[] $tripitoes_package_reports
 * @property \App\Model\Entity\TripitoesPackageView[] $tripitoes_package_views
 * @property \App\Model\Entity\TripitoesTaxiCart[] $tripitoes_taxi_carts
 * @property \App\Model\Entity\TripitoesTaxiLike[] $tripitoes_taxi_likes
 * @property \App\Model\Entity\TripitoesTaxiReport[] $tripitoes_taxi_reports
 * @property \App\Model\Entity\TripitoesTaxiView[] $tripitoes_taxi_views
 */
class TripitoesUser extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'identifier' => true,
        'provider' => true,
        'logins' => true,
        'last_login' => true,
        'total_saved' => true,
        'created_on' => true,
        'tripitoes_hotel_carts' => true,
        'tripitoes_hotel_likes' => true,
        'tripitoes_hotel_reports' => true,
        'tripitoes_hotel_views' => true,
        'tripitoes_package_carts' => true,
        'tripitoes_package_likes' => true,
        'tripitoes_package_reports' => true,
        'tripitoes_package_views' => true,
        'tripitoes_taxi_carts' => true,
        'tripitoes_taxi_likes' => true,
        'tripitoes_taxi_reports' => true,
        'tripitoes_taxi_views' => true
    ];
}
