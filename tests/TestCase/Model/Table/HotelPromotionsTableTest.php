<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HotelPromotionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HotelPromotionsTable Test Case
 */
class HotelPromotionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HotelPromotionsTable
     */
    public $HotelPromotions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.hotel_promotions',
        'app.users',
        'app.hotel_categories',
        'app.price_masters',
        'app.hotel_promotion_carts',
        'app.hotel_promotion_cities',
        'app.hotel_promotion_likes',
        'app.hotel_promotion_price_before_renews',
        'app.hotel_promotion_reports',
        'app.hotel_promotion_views',
        'app.tripitoes_hotel_carts',
        'app.tripitoes_hotel_likes',
        'app.tripitoes_hotel_reports',
        'app.tripitoes_hotel_views'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('HotelPromotions') ? [] : ['className' => HotelPromotionsTable::class];
        $this->HotelPromotions = TableRegistry::getTableLocator()->get('HotelPromotions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HotelPromotions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
