<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TripitoesPackageReportsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TripitoesPackageReportsTable Test Case
 */
class TripitoesPackageReportsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TripitoesPackageReportsTable
     */
    public $TripitoesPackageReports;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_package_reports',
        'app.post_travle_packages',
        'app.tripitoes_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TripitoesPackageReports') ? [] : ['className' => TripitoesPackageReportsTable::class];
        $this->TripitoesPackageReports = TableRegistry::getTableLocator()->get('TripitoesPackageReports', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TripitoesPackageReports);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
