<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TripitoesBlogCommentRowsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TripitoesBlogCommentRowsTable Test Case
 */
class TripitoesBlogCommentRowsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TripitoesBlogCommentRowsTable
     */
    public $TripitoesBlogCommentRows;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_blog_comment_rows',
        'app.tripitoes_blog_comments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TripitoesBlogCommentRows') ? [] : ['className' => TripitoesBlogCommentRowsTable::class];
        $this->TripitoesBlogCommentRows = TableRegistry::getTableLocator()->get('TripitoesBlogCommentRows', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TripitoesBlogCommentRows);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
