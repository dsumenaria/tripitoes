<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersTable
     */
    public $Users;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users',
        'app.countries',
        'app.cities',
        'app.states',
        'app.roles',
        'app.devices',
        'app.business_buddies',
        'app.credits',
        'app.event_planner_promotion_carts',
        'app.event_planner_promotion_likes',
        'app.event_planner_promotion_reports',
        'app.event_planner_promotion_views',
        'app.event_planner_promotions',
        'app.hotel_promotion_carts',
        'app.hotel_promotion_likes',
        'app.hotel_promotion_reports',
        'app.hotel_promotion_views',
        'app.hotel_promotions',
        'app.hotels',
        'app.post_travle_package_carts',
        'app.post_travle_package_likes',
        'app.post_travle_package_reports',
        'app.post_travle_package_views',
        'app.post_travle_packages',
        'app.promotion',
        'app.requests',
        'app.responses',
        'app.taxi_fleet_promotion_carts',
        'app.taxi_fleet_promotion_likes',
        'app.taxi_fleet_promotion_reports',
        'app.taxi_fleet_promotion_views',
        'app.taxi_fleet_promotions',
        'app.temp_ratings',
        'app.testimonial',
        'app.transports',
        'app.travel_certificates',
        'app.user_chats',
        'app.user_feedbacks',
        'app.user_ratings',
        'app.user_rights',
        'app.userdetails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Users') ? [] : ['className' => UsersTable::class];
        $this->Users = TableRegistry::getTableLocator()->get('Users', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
