<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TripitoesHotelViewsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TripitoesHotelViewsTable Test Case
 */
class TripitoesHotelViewsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TripitoesHotelViewsTable
     */
    public $TripitoesHotelViews;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_hotel_views',
        'app.hotel_promotions',
        'app.tripitoes_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TripitoesHotelViews') ? [] : ['className' => TripitoesHotelViewsTable::class];
        $this->TripitoesHotelViews = TableRegistry::getTableLocator()->get('TripitoesHotelViews', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TripitoesHotelViews);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
