<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TaxiFleetPromotionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TaxiFleetPromotionsTable Test Case
 */
class TaxiFleetPromotionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TaxiFleetPromotionsTable
     */
    public $TaxiFleetPromotions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxi_fleet_promotions',
        'app.countries',
        'app.price_masters',
        'app.users',
        'app.taxi_fleet_promotion_carts',
        'app.taxi_fleet_promotion_cities',
        'app.taxi_fleet_promotion_likes',
        'app.taxi_fleet_promotion_price_before_renews',
        'app.taxi_fleet_promotion_reports',
        'app.taxi_fleet_promotion_rows',
        'app.taxi_fleet_promotion_states',
        'app.taxi_fleet_promotion_views',
        'app.tripitoes_taxi_carts',
        'app.tripitoes_taxi_likes',
        'app.tripitoes_taxi_reports',
        'app.tripitoes_taxi_views'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TaxiFleetPromotions') ? [] : ['className' => TaxiFleetPromotionsTable::class];
        $this->TaxiFleetPromotions = TableRegistry::getTableLocator()->get('TaxiFleetPromotions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaxiFleetPromotions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
