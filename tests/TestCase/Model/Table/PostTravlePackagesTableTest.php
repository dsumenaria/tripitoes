<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostTravlePackagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostTravlePackagesTable Test Case
 */
class PostTravlePackagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostTravlePackagesTable
     */
    public $PostTravlePackages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.post_travle_packages',
        'app.currencies',
        'app.countries',
        'app.price_masters',
        'app.users',
        'app.post_travle_package_carts',
        'app.post_travle_package_cities',
        'app.post_travle_package_countries',
        'app.post_travle_package_likes',
        'app.post_travle_package_price_before_renews',
        'app.post_travle_package_reports',
        'app.post_travle_package_rows',
        'app.post_travle_package_states',
        'app.post_travle_package_views',
        'app.tripitoes_package_carts',
        'app.tripitoes_package_likes',
        'app.tripitoes_package_reports',
        'app.tripitoes_package_views'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PostTravlePackages') ? [] : ['className' => PostTravlePackagesTable::class];
        $this->PostTravlePackages = TableRegistry::getTableLocator()->get('PostTravlePackages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PostTravlePackages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
