<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TripitoesPackageLikesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TripitoesPackageLikesTable Test Case
 */
class TripitoesPackageLikesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TripitoesPackageLikesTable
     */
    public $TripitoesPackageLikes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_package_likes',
        'app.post_travle_packages',
        'app.tripitoes_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TripitoesPackageLikes') ? [] : ['className' => TripitoesPackageLikesTable::class];
        $this->TripitoesPackageLikes = TableRegistry::getTableLocator()->get('TripitoesPackageLikes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TripitoesPackageLikes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
