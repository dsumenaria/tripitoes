<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TripitoesUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TripitoesUsersTable Test Case
 */
class TripitoesUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TripitoesUsersTable
     */
    public $TripitoesUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_users',
        'app.facebooks',
        'app.instagrams',
        'app.googles',
        'app.tripitoes_hotel_carts',
        'app.tripitoes_hotel_likes',
        'app.tripitoes_hotel_reports',
        'app.tripitoes_hotel_views',
        'app.tripitoes_package_carts',
        'app.tripitoes_package_likes',
        'app.tripitoes_package_reports',
        'app.tripitoes_package_views',
        'app.tripitoes_taxi_carts',
        'app.tripitoes_taxi_likes',
        'app.tripitoes_taxi_reports',
        'app.tripitoes_taxi_views'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TripitoesUsers') ? [] : ['className' => TripitoesUsersTable::class];
        $this->TripitoesUsers = TableRegistry::getTableLocator()->get('TripitoesUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TripitoesUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
