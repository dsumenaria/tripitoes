<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TestimonialTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TestimonialTable Test Case
 */
class TestimonialTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TestimonialTable
     */
    public $Testimonial;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.testimonial',
        'app.users',
        'app.authors',
        'app.requests',
        'app.promotions',
        'app.promotion_types',
        'app.responses'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Testimonial') ? [] : ['className' => TestimonialTable::class];
        $this->Testimonial = TableRegistry::getTableLocator()->get('Testimonial', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Testimonial);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
