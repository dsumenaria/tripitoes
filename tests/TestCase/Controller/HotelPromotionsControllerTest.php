<?php
namespace App\Test\TestCase\Controller;

use App\Controller\HotelPromotionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\HotelPromotionsController Test Case
 */
class HotelPromotionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.hotel_promotions',
        'app.users',
        'app.hotel_categories',
        'app.price_masters',
        'app.hotel_promotion_carts',
        'app.hotel_promotion_cities',
        'app.hotel_promotion_likes',
        'app.hotel_promotion_price_before_renews',
        'app.hotel_promotion_reports',
        'app.hotel_promotion_views',
        'app.tripitoes_hotel_carts',
        'app.tripitoes_hotel_likes',
        'app.tripitoes_hotel_reports',
        'app.tripitoes_hotel_views'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
