<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PostTravlePackagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PostTravlePackagesController Test Case
 */
class PostTravlePackagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.post_travle_packages',
        'app.currencies',
        'app.countries',
        'app.price_masters',
        'app.users',
        'app.post_travle_package_carts',
        'app.post_travle_package_cities',
        'app.post_travle_package_countries',
        'app.post_travle_package_likes',
        'app.post_travle_package_price_before_renews',
        'app.post_travle_package_reports',
        'app.post_travle_package_rows',
        'app.post_travle_package_states',
        'app.post_travle_package_views',
        'app.tripitoes_package_carts',
        'app.tripitoes_package_likes',
        'app.tripitoes_package_reports',
        'app.tripitoes_package_views'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
