<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TripitoesUsersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TripitoesUsersController Test Case
 */
class TripitoesUsersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tripitoes_users',
        'app.facebooks',
        'app.instagrams',
        'app.googles',
        'app.tripitoes_hotel_carts',
        'app.tripitoes_hotel_likes',
        'app.tripitoes_hotel_reports',
        'app.tripitoes_hotel_views',
        'app.tripitoes_package_carts',
        'app.tripitoes_package_likes',
        'app.tripitoes_package_reports',
        'app.tripitoes_package_views',
        'app.tripitoes_taxi_carts',
        'app.tripitoes_taxi_likes',
        'app.tripitoes_taxi_reports',
        'app.tripitoes_taxi_views'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
