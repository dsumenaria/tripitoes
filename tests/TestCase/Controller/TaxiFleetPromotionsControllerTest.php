<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TaxiFleetPromotionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TaxiFleetPromotionsController Test Case
 */
class TaxiFleetPromotionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.taxi_fleet_promotions',
        'app.countries',
        'app.price_masters',
        'app.users',
        'app.taxi_fleet_promotion_carts',
        'app.taxi_fleet_promotion_cities',
        'app.taxi_fleet_promotion_likes',
        'app.taxi_fleet_promotion_price_before_renews',
        'app.taxi_fleet_promotion_reports',
        'app.taxi_fleet_promotion_rows',
        'app.taxi_fleet_promotion_states',
        'app.taxi_fleet_promotion_views',
        'app.tripitoes_taxi_carts',
        'app.tripitoes_taxi_likes',
        'app.tripitoes_taxi_reports',
        'app.tripitoes_taxi_views'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
