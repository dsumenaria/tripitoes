<?php
namespace App\Controller\Component;
use App\Controller\AppController;
use Cake\Controller\Component;
class TripitoesRatingComponent extends Component
{
	function initialize(array $config) 
	{
		parent::initialize($config);
	}
	
	function rating($getRating){
		$one_star_user=0;
        $total_users=0; 
        $two_star_user=0; 
        $three_star_user=0; 
        $three_star_user=0; 
        $four_star_user=0; 
        $five_star_user=0;
        $overallrating=0;
        foreach ($getRating->user->tripitoes_ratings as $key => $value) {
           $rating=$value->rating;
           if($rating==5){
            $five_star_user++;
           }
           if($rating==4){
            $four_star_user++;
           }
           if($rating==3){
            $three_star_user++;
           }
           if($rating==2){
            $two_star_user++;
           }
           if($rating==1){
            $one_star_user++;
           }
           $total_users++;
        }
        if($total_users>0){
            $overallrating=(((1*$one_star_user)+(2*$two_star_user)+(3*$three_star_user)+(4*$four_star_user)+(5*$five_star_user))/$total_users); 
        }
         $ratingData['total_users']=$total_users;
         $ratingData['overallrating']=round($overallrating,1);
         return $ratingData;
	}
  function MakeRating($getRating){
        $one_star_user=0;
        $total_users=0; 
        $two_star_user=0; 
        $three_star_user=0; 
        $three_star_user=0; 
        $four_star_user=0; 
        $five_star_user=0;
        $overallrating=0;
        foreach ($getRating as $key => $value) {
           $rating=$value->rating;
           if($rating==5){
            $five_star_user++;
           }
           if($rating==4){
            $four_star_user++;
           }
           if($rating==3){
            $three_star_user++;
           }
           if($rating==2){
            $two_star_user++;
           }
           if($rating==1){
            $one_star_user++;
           }
           $total_users++;
        }
        if($total_users>0){
            $overallrating=(((1*$one_star_user)+(2*$two_star_user)+(3*$three_star_user)+(4*$four_star_user)+(5*$five_star_user))/$total_users); 
        }
        return round($overallrating,1);
  }
}
?>