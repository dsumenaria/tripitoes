<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. ``
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        //EventManager::instance()->on('HybridAuth.login', [$this->MyComponent, 'updateUser']);
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        //$this->loadComponent('Csrf');
        $this->loadComponent('Security');
        $this->loadComponent('Flash');
        //-- AWS DETAILS
        $this->loadModel('AwsFiles');
        $TripitoesRating=$this->loadComponent('TripitoesRating');
        $AWSFILE= $this->AwsFiles->find()->select(['cdn_path'])->first();
        $cdn_path=$AWSFILE['cdn_path'];
        $this->set('cdn_path',$cdn_path);
		$url = Router::url( $this->here, true );
		$full=str_replace("http://","https://",$url);
        $this->set('full_url',$full); 
        $this->set('site_url','https://tripitoes.com/'); 
        $this->set('TripitoesRating',$TripitoesRating);
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form',
                'ADmad/HybridAuth.HybridAuth' => [
                    // All keys shown below are defaults
                    'fields' => [
                        'provider' => 'provider',
                        'identifier' => 'identifier'
                    ],
					'loginAction' => [
                        'controller' => 'Pages',
                        'action' => 'index'
                    ],
                    'profileModel' => 'ADmad/HybridAuth.SocialProfiles',
                    'profileModelFkField' => 'tripitoes_user_id',

                    'userModel' => 'TripitoesUsers',

                    // The URL Hybridauth lib should redirect to after authentication.
                    // If no value is specified you are redirect to this plugin's
                    // HybridAuthController::authenticated() which handles persisting
                    // user info to AuthComponent and redirection.
                    'hauth_return_to' => null,
                    /*'hauth_return_to' => [
                        'controller'  => 'MyController',
                        'action'      => 'update_status',
                        'plugin' => false
                      ]*/
                ]
            ]
        ]);
    }
    public function json($data){
        $data = json_encode($data);
        $response = $this->response->withType('application/json')->withStringBody($data);
        return $response;
    }
    public function text($data){
        $response = $this->response->withType('application/text')->withStringBody($data);
        return $response;
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $auth_login="No";
        $total_saved=0;
        if($this->Auth->User())
        {
            $auth_login="Yes";
            $tripitoes_user_id = $this->Auth->User('id');
            $this->TripitoesUsers = TableRegistry::get('TripitoesUsers');
            $tripitoesUsers = $this->TripitoesUsers->find()->where(['id'=>$tripitoes_user_id])->select('total_saved');
            $tripitoesUserFirst = $tripitoesUsers->first();
            $total_saved = $tripitoesUserFirst->total_saved;
            $this->request->session()->delete('redirect');
        }
        $this->set(compact('auth_login','tripitoes_user_id','total_saved'));
    }
}
