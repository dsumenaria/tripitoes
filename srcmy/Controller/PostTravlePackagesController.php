<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * PostTravlePackages Controller
 *
 * @property \App\Model\Table\PostTravlePackagesTable $PostTravlePackages
 *
 * @method \App\Model\Entity\PostTravlePackage[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostTravlePackagesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['packageList','filterPackage','filterData','citylist','view','citylistpopup','saveCall','saveShare','edit']);
        //$this->Auth->allow(['packageSave','packageLike','packageReport']);
    }
    public function packageList($category=null)
    {
        $this->viewBuilder()->setLayout('detail_layout');
        $category = @$this->request->query('category'); 
        $startPrice = @$this->request->query('startPrice'); 
        $endPrice = @$this->request->query('endPrice');
        $duration = @$this->request->query('duration_range'); 
        $saved = @$this->request->query('saved'); 
        $package_destination = @$this->request->query('package_destination'); 

        $condition=array();
        if(!empty($duration)){ 
            if($duration==1){$duration_day_nights[]='1 N/2 D';}
            if($duration==2){$duration_day_nights[]='2 N/3 D';}
            if($duration==3){$duration_day_nights[]='3 N/4 D';}
            if($duration==4){$duration_day_nights[]='4 N/5 D';}
            if($duration==5){$duration_day_nights[]='5 N/6 D';}
            if($duration==6){$duration_day_nights[]='6 N/7 D';}
            if($duration==7){$duration_day_nights[]='7 N/8 D';}
            if($duration==8){$duration_day_nights[]='8 N/9 D';}
            if($duration==9){$duration_day_nights[]='9 N/10 D';}
            if($duration==10){$duration_day_nights[]='10 N/11 D';}
            if($duration==11){$duration_day_nights[]='11 N/12 D';}
            if($duration==12){$duration_day_nights[]='12 N/13 D';}
            if($duration==13){$duration_day_nights[]='13 N/14 D';}
            if($duration==14){$duration_day_nights[]='14 N/15 D';}
            if($duration==15){$duration_day_nights[]='15+ Days';} 
            $condition['PostTravlePackages.duration_day_night IN'] = $duration_day_nights;
        }
        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->PostTravlePackages->TripitoesPackageCarts->find()->where(['TripitoesPackageCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['post_travle_package_id'];
            }
        }  
        if(($startPrice>0) && !empty($endPrice)){
            $condition['PostTravlePackages.trip_price >='] = $startPrice;
            $condition['PostTravlePackages.trip_price <='] = $endPrice;
        }
        //pr($condition);
        $data_arr=array(); 
        $Filtertype='';
        $FilterState='';
        $Filtervalue='';
        $FilterCountry='';
        if(!empty($package_destination)){
            $cityStateCountryArray=explode(',',$package_destination);
            $Filtervalue=$cityStateCountryArray[0];
            $Filtertype=$cityStateCountryArray[1];
            if($Filtertype=='City'){
                $Pcity = $this->PostTravlePackages->PackageCities
                    ->find()->select(['country_id'])->where(['PackageCities.id' =>$Filtervalue])->first(); 
                $FilterCountry=$Pcity['country_id'];
                $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id' =>$Filtervalue])->toArray();
                
                if(!empty($search_bar_city_data))
                {
                    foreach($search_bar_city_data as $data)
                    {
                        $data_arr[] = $data->post_travle_package_id;
                    }
                }
            }
            if($Filtertype=='Country'){
                $FilterCountry=$Filtervalue;
                $search_bar_country_data = $this->PostTravlePackages->PostTravlePackageCountries->find()
                ->select(['post_travle_package_id'])->where(['PostTravlePackageCountries.country_id IN' =>$Filtervalue])->toArray();
                if(!empty($search_bar_country_data))
                {
                    foreach($search_bar_country_data as $dataa)
                    {
                        $data_arr[] = $dataa->post_travle_package_id;
                    }
                }
            }
            if($Filtertype=='State'){
                $Fcountry = $this->PostTravlePackages->PackageCities->States
                ->find()->select(['country_id','state_name'])->where(['States.id' => $Filtervalue])->first();
                $FilterCountry=$Fcountry['country_id']; 
                $FilterState=$Fcountry['state_name']; 
                $search_bar_cty = $this->PostTravlePackages->PackageCities
                ->find()->select(['id'])->where(['PackageCities.state_id' =>$Filtervalue]);
                if(!empty($search_bar_cty)) 
                {
                    $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                    ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id IN' =>$search_bar_cty])->toArray();
                    
                    if(!empty($search_bar_city_data))
                    {
                        foreach($search_bar_city_data as $data)
                        {
                            $data_arr[] = $data->post_travle_package_id;
                        }

                    }
                }
            }
            
            if(!empty($data_arr)){
                $condition['PostTravlePackages.id IN'] =$data_arr;
            }
            else
            {
                $condition['PostTravlePackages.id IN'] ='';
            }
        }         

         
        $limit=10;
        $page = $this->request->query('page'); 
        if(!$page){
            $page=1;
        }
        // QUERY
 
        $getTravelPackages=$this->PostTravlePackages->find();
        $getTravelPackages->contain([
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','web_url','address'])
                ->contain(['Cities','States','PCountries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Package']);
                }]);
            }
            ,'PostTravlePackageCities'=>['Cities'=>['States']]
            ,'PostTravlePackageRows'=>['PostTravlePackageCategories']
            ,'PostTravlePackageCountries'=>['Countries']
        ]);
        if($category){
            $getTravelPackages->matching('PostTravlePackageRows', function ($q) use($category) {
                return $q->where(['post_travle_package_category_id'=>$category]);
            });
        }else{
         //$getTravelPackages->where(['PostTravlePackages.position !='=>11]);  
        }
        if(!empty($CartsId)){
            $getTravelPackages->where(['PostTravlePackages.id IN' => $CartsId]);
        }
        $getTravelPackages->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d'),$condition]);
        $total_counts=$getTravelPackages->count();
        $getTravelPackages
            ->group(['PostTravlePackages.id'])
            ->order(['PostTravlePackages.trip_total_like' => 'DESC'])
            ->order(['Users.isVerified' =>'DESC','PostTravlePackages.position' =>'ASC','PostTravlePackages.id' =>'DESC'])
            ->limit(10)
            ->page(1) 
            ->autoFields(true);
        //pr($getTravelPackages->toArray()); exit;
        $packageCategories=$this->PostTravlePackages->PostTravlePackageRows->PostTravlePackageCategories->find()->where(['PostTravlePackageCategories.is_deleted'=>0])->order(['PostTravlePackageCategories.name'=>'ASC']);

        $socialController='PostTravlePackages';
        $socialAction='packageList';

        $countrylist=$this->PostTravlePackages->Countries->find()->where(['Countries.is_deleted'=>0]);
        //$FilterCountry='';
        $this->set(compact('getTravelPackages','packageCategories','total_counts','countrylist','category','socialController','socialAction','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved','FilterState'));
        $this->set('_serialize', ['getTravelPackages','packageCategories','total_counts','countrylist','category','socialController','socialAction','startPrice','endPrice','duration','Filtertype','Filtervalue','FilterCountry','saved','FilterState']);
    } 

    public function filterData($category_id=null)
    {
        $myJSON=$this->request->query('myJSON');
        $dataArray = json_decode($myJSON, true);
        $country = array_unique($dataArray[0]['country']);  
        $city = array_unique($dataArray[0]['city']);    
        $category = array_unique($dataArray[0]['category']);
        $sellerRate = array_unique($dataArray[0]['sellerRate']);

        $duration = array_unique($dataArray[0]['duration']);   
        $hotelclass = $dataArray[0]['hotelclass'];  
        $VarifiedSeller = $dataArray[0]['VarifiedSeller'];  
        $startPricem = $dataArray[0]['startPricem'];  
        $startPrice = $dataArray[0]['startPrice'];  
        $endPricem = $dataArray[0]['endPricem']; 
        $endPrice = $dataArray[0]['endPrice']; 
        $near = $dataArray[0]['near']; 
        $sort = $dataArray[0]['sort']; 
        $stateFilter = $dataArray[0]['stateFilter']; 
        $SortByMobile = @$dataArray[0]['SortByMobile'];  
        $page_no = @$dataArray[0]['page_no']; 
        $SellerCity = @$dataArray[0]['SellerCity'];
        $SellerCityMobile = @$dataArray[0]['SellerCityMobile'];
        $SellerCityMobile2 = @$dataArray[0]['SellerCityMobile2'];
        $saved = @$dataArray[0]['saved'];
        //pr($dataArray);
        $sellercitySearch='';
        if(!empty($SellerCity)){
            $sellercitySearch= $SellerCity;
        } 
        if(!empty($SellerCityMobile)){
            $sellercitySearch= $SellerCityMobile;
        } 
        if(!empty($SellerCityMobile2)){
            $sellercitySearch= $SellerCityMobile2;
        } 
        $CartsId=array();
        if($saved=='saved'){
            $tripitoes_user_id=$this->Auth->User('id');
            $carts=$this->PostTravlePackages->TripitoesPackageCarts->find()->where(['TripitoesPackageCarts.tripitoes_user_id'=>$tripitoes_user_id])->toArray();
            foreach ($carts as $value) {
               $CartsId[]=$value['post_travle_package_id'];
            }
             
        }

        $dasktopsearch = @ltrim($dataArray[0]['dasktopsearch']); 
        $mobileSearchBox = @ltrim($dataArray[0]['mobileSearchBox']);

        $search_bar='';
        if(!empty($dasktopsearch)){
            $search_bar= $dasktopsearch;
        } 
        if(!empty($mobileSearchBox)){
            $search_bar= $mobileSearchBox;
        }
        $condition=array();
        $FindInState=array();
        $city_get='';

        if($search_bar){
            $data_arr = [];
            $data_arr_title = [];
            //-- COuntry    
            $search_bar_state = $this->PostTravlePackages->PostTravlePackageCountries->Countries
            ->find()->select(['id'])->where(['Countries.country_name Like' =>'%'.$search_bar.'%']);
            
            if(!empty($search_bar_state)) 
            {
                $search_bar_cty = $this->PostTravlePackages->PostTravlePackageCountries
                ->find()->select(['id','post_travle_package_id'])->where(['PostTravlePackageCountries.country_id IN' =>$search_bar_state]);
                
                if(!empty($search_bar_cty)) 
                {
                    foreach($search_bar_cty as $data)
                    {
                        $data_arr[] = $data->post_travle_package_id;
                    }
                }
            }
            //-- state
            $search_bar_state = $this->PostTravlePackages->PackageCities->States
                ->find()->select(['id'])->where(['States.state_name Like' =>'%'.$search_bar.'%']);
            
            if(!empty($search_bar_state)) 
            {
                $search_bar_cty = $this->PostTravlePackages->PackageCities
                ->find()->select(['id'])->where(['PackageCities.state_id IN' =>$search_bar_state]);
                
                if(!empty($search_bar_cty)) 
                {
                    $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                    ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id IN' =>$search_bar_cty])->toArray();
                    
                    if(!empty($search_bar_city_data))
                    {
                        foreach($search_bar_city_data as $data)
                        {
                            $data_arr[] = $data->post_travle_package_id;
                        }
                    }
                }
            }
                
            //-- City
            $search_bar_city = $this->PostTravlePackages->PostTravlePackageCities->Cities
            ->find()->select(['id'])->where(['Cities.name Like' =>'%'.$search_bar.'%']);
            if(!empty($search_bar_city)) 
            { 

                $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id IN' =>$search_bar_city])->toArray();

                if(!empty($search_bar_city_data))
                {
                    foreach($search_bar_city_data as $data)
                    {
                        $data_arr[] = $data->post_travle_package_id;
                    }
                }
            }
            
            $search_bar_title = array_merge($data_arr_title,$data_arr);

            if(!empty($search_bar_title)){
            $search_bar_title = ['PostTravlePackages.id IN' =>$search_bar_title];
            }else
            {
                $search_bar_title = ['PostTravlePackages.id IN' =>''];
            }               
        }

        /*if(!empty($package_destination)){
            $cityStateCountryArray=explode(',',$package_destination);
            $Filtervalue=$cityStateCountryArray[0];
            $Filtertype=$cityStateCountryArray[1];
            if($Filtertype=='City'){
                $Pcity = $this->PostTravlePackages->PackageCities
                    ->find()->select(['country_id'])->where(['PackageCities.id' =>$Filtervalue])->first(); 
                $FilterCountry=$Pcity['country_id'];
                $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id' =>$Filtervalue])->toArray();
                
                if(!empty($search_bar_city_data))
                {
                    foreach($search_bar_city_data as $data)
                    {
                        $data_arr[] = $data->post_travle_package_id;
                    }
                }
            }
            if($Filtertype=='Country'){
                $FilterCountry=$Filtervalue;
                $search_bar_country_data = $this->PostTravlePackages->PostTravlePackageCountries->find()
                ->select(['post_travle_package_id'])->where(['PostTravlePackageCountries.country_id IN' =>$Filtervalue])->toArray();
                if(!empty($search_bar_country_data))
                {
                    foreach($search_bar_country_data as $dataa)
                    {
                        $data_arr[] = $dataa->post_travle_package_id;
                    }
                }
            }
            if($Filtertype=='State'){
                $Fcountry = $this->PostTravlePackages->PackageCities->States
                ->find()->select(['country_id'])->where(['States.id' => $Filtervalue])->first();
                $FilterCountry=$Fcountry['country_id']; 
                $search_bar_cty = $this->PostTravlePackages->PackageCities
                ->find()->select(['id'])->where(['PackageCities.state_id' =>$Filtervalue]);
                if(!empty($search_bar_cty)) 
                {
                    $search_bar_city_data = $this->PostTravlePackages->PostTravlePackageCities->find()
                    ->select(['post_travle_package_id'])->where(['PostTravlePackageCities.city_id IN' =>$search_bar_cty])->toArray();
                    
                    if(!empty($search_bar_city_data))
                    {
                        foreach($search_bar_city_data as $data)
                        {
                            $data_arr[] = $data->post_travle_package_id;
                        }
                    }
                }
            }
            if(!empty($data_arr)){
                $condition['PostTravlePackages.id IN'] =$data_arr;
            }else
            {
                $condition['PostTravlePackages.id IN'] ='';
            }
        }*/

        if(!empty($stateFilter)){
           $statefet=$this->PostTravlePackages->PackageCities->States->find()->select(['id','state_name'])->where(['States.country_id'=>$stateFilter]); 
           foreach ($statefet as $key => $value) {
               $FindInState[]=$value['id'];
           }
        }
        if(!empty($near)){
            /*$query = json_decode(file_get_contents('https://api.ipdata.co?api-key=c594b261dd9be8cbe94735fbfc395bb7032a622b8f9594b69b32296f'));
            $userCurrentLocation='';
               $userCurrentLocation=$query->city;
            pr($query->city);*/
            $userCurrentLocation=$near;
            if(!empty($userCurrentLocation)){
				$locationArray = explode(', ',$userCurrentLocation);
				$ftcArray=[];
				foreach($locationArray as $onebyOne){
					$cityFet=$this->PostTravlePackages->PackageCities->find()->select(['id','name'])
						->where(['PackageCities.name'=>$onebyOne])->first();
					$ftcArray[]=$cityFet;
					
				} 
				$mailArray=array_filter($ftcArray);
				foreach($mailArray as $key => $value)
				{
					$city_get=$value->id;
				} 
                /*$cityFet=$this->PostTravlePackages->PackageCities->find()->select(['id','name'])->where(['PackageCities.name LIKE '=> '%'.$userCurrentLocation.'%'])->first(); 
                if(!empty($cityFet)){
                    $city_get=$cityFet->id;
                }*/
            }
        }
        //print_r($city_get);
        $OrRating=array();
        if(!empty($sellerRate)){
            $fiveRange=array();
            $fourRange=array();
            $threeRange=array();
            $twoRange=array();
            $oneRange=array(); 
            $OrRating['OR']=array();;
            foreach ($sellerRate as $dataFetch) {
                if($dataFetch==5){
                    $OrRating['OR'][]['Users.trip_package_rating']='5';
                }
                if($dataFetch==4){
                    $OrRating['OR'][]=array('Users.trip_package_rating <=' => '4.9' , 'Users.trip_package_rating >=' => '4.0');
                }
                if($dataFetch==3){
                   $OrRating['OR'][]=array('Users.trip_package_rating <=' => '3.9' , 'Users.trip_package_rating >=' => '3.0');
                     
                }
                if($dataFetch==2){
                    $OrRating['OR'][]=array('Users.trip_package_rating <=' => '2.9' , 'Users.trip_package_rating >=' => '2.0');
                 }
                if($dataFetch==1){
                    $OrRating['OR'][]=array('Users.trip_package_rating <=' => '1.9' , 'Users.trip_package_rating >=' => '1.0');
                 }
            }
        }
        if(!empty($sellercitySearch)){
            $city_get=$sellercitySearch;
        } 
        //pr(['PackageCities.name LIKE'=>'%'.$SellerCity.'%']);
        if(!empty($duration)){ 
            foreach($duration as $daataa){
                if($daataa==1){$duration_day_nights[]='1 N/2 D';}
                if($daataa==2){$duration_day_nights[]='2 N/3 D';}
                if($daataa==3){$duration_day_nights[]='3 N/4 D';}
                if($daataa==4){$duration_day_nights[]='4 N/5 D';}
                if($daataa==5){$duration_day_nights[]='5 N/6 D';}
                if($daataa==6){$duration_day_nights[]='6 N/7 D';}
                if($daataa==7){$duration_day_nights[]='7 N/8 D';}
                if($daataa==8){$duration_day_nights[]='8 N/9 D';}
                if($daataa==9){$duration_day_nights[]='9 N/10 D';}
                if($daataa==10){$duration_day_nights[]='10 N/11 D';}
                if($daataa==11){$duration_day_nights[]='11 N/12 D';}
                if($daataa==12){$duration_day_nights[]='12 N/13 D';}
                if($daataa==13){$duration_day_nights[]='13 N/14 D';}
                if($daataa==14){$duration_day_nights[]='14 N/15 D';}
                if($daataa==15){$duration_day_nights[]='15+ Days';} 
            }  
            $condition['PostTravlePackages.duration_day_night IN'] = $duration_day_nights;
        }

        if(!empty($hotelclass)){ 
            $condition['PostTravlePackages.hotel_class IN'] = $hotelclass;
        } 

        if(!empty($VarifiedSeller)){
            $condition['Users.isVerified'] = $VarifiedSeller;
        }

        if(!empty($startPrice) && $endPrice<500000){
            $condition['PostTravlePackages.trip_price >='] = $startPrice;
            $condition['PostTravlePackages.trip_price <='] = $endPrice;
        }
        if(!empty($startPricem) && $endPricem<500000){
            $condition['PostTravlePackages.trip_price >='] = $startPricem;
            $condition['PostTravlePackages.trip_price <='] = $endPricem;
        }

        //-- OTher Table FIlter
        $countryFilter=null;
        if(!empty($country))
        { 
            $countryFilter = ['Countries.id IN' =>$country];
        }
        $cityFilter=null;
        if(!empty($city))
        { 
            $cityFilter = ['PackageCities.id IN'=>$city];
        }
        $categoryFilter=null;
        $postiion=null;
        if(sizeof($category)==1)
        {
            if (in_array("999", $category))
            {    
                $categoryFilter = '';
            }
            else if($category[0]=='0'){
                $postiion['PostTravlePackages.position !=']=11;  
            }
            else{
                 $categoryFilter = ['post_travle_package_category_id IN'=>$category];
            }
        }
        else{
            $categoryFilter = ['post_travle_package_category_id IN'=>$category];
        }
        //pr($categoryFilter);
         //-- Sorting
        $where_short=[];
        if(!empty($sort)){
            if($sort=='like_H2L'){
                $where_short = ['PostTravlePackages.trip_total_like' => 'DESC'];
            }
            if($sort=='view_H2L'){
                $where_short = ['PostTravlePackages.trip_views' => 'DESC']; 
            }
            if($sort=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC']; 
            }
            if($sort=='price_L2H'){
                $where_short = ['PostTravlePackages.trip_price' => 'ASC'];
            }
        }
        if(!empty($SortByMobile)){
            if($SortByMobile=='like_H2L'){
                $where_short = ['PostTravlePackages.trip_total_like' => 'DESC'];
            }
            if($SortByMobile=='view_H2L'){
                $where_short = ['PostTravlePackages.trip_views' => 'DESC']; 
            }
            if($SortByMobile=='rating_H2L'){
               $where_short = ['Users.trip_package_rating'=>'DESC'];
            }
            if($SortByMobile=='price_L2H'){
                $where_short = ['PostTravlePackages.trip_price' => 'ASC'];
            }
        }
        //-- Shoting
 
        // QUERY
        $limit=10;
        if(empty($page_no)){
           $page_no=1; 
        }
 
        $getTravelPackages=$this->PostTravlePackages->find();
        $getTravelPackages->contain([
            'Users'=>function($q){
                return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified','city_id','state_id','web_url','address'])
                ->contain(['Cities','States','PCountries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Package']);
                }]);
            }
            ,'PostTravlePackageCities'=>['Cities'=>['States']]
            ,'PostTravlePackageRows'=>['PostTravlePackageCategories']
            ,'PostTravlePackageCountries'=>['Countries']
        ]);
       
        if(!empty($categoryFilter)){    
            $getTravelPackages->matching('PostTravlePackageRows', function ($q) use($categoryFilter) {
                return $q->where($categoryFilter);
            });
        }

        if($cityFilter){  
            $getTravelPackages->matching('PackageCities', function ($q) use($cityFilter) {
                return $q->where($cityFilter);
            });
        }

        if($countryFilter){  
            $getTravelPackages->matching('Countries', function ($q)use($countryFilter){ 
                return $q->where($countryFilter); 
            });
        }  
        
        if(!empty($CartsId)){ 
            $getTravelPackages->where(['PostTravlePackages.id IN' => $CartsId]);
        }
        $getTravelPackages->where([            
            'PostTravlePackages.is_deleted' =>0,
            'PostTravlePackages.share'=>1,
            'PostTravlePackages.visible_date >=' =>date('Y-m-d')
        ]);

        if(!empty($condition)){ 
         $getTravelPackages->where($condition);  
        }
        if(!empty($postiion)){ 
         $getTravelPackages->where($postiion);  
        }
        //- Rating
        if(!empty($OrRating)){
         $getTravelPackages->where($OrRating);  
        }
        
 
        //-- Rate
        if(!empty($city_get)){ 
           $getTravelPackages->where(['Users.city_id' =>$city_get]);
        }
        if(!empty($FindInState)){ 
           $getTravelPackages->where(['Users.state_id IN' =>$FindInState]);
        } 
        if(!empty($search_bar_title)){  
           $getTravelPackages->where($search_bar_title);
        }
        
        $getTravelPackages->group(['PostTravlePackages.id']);
           
        if($where_short){
            $getTravelPackages->order($where_short);
        }
        $getTravelPackages->order(['Users.isVerified' =>'DESC','PostTravlePackages.position' =>'ASC','PostTravlePackages.id' =>'DESC']);
        //pr($where_short);
       // pr($getTravelPackages->toArray()); 
        $getTravelPackages->autoFields(true);
        $total_counts=$getTravelPackages->count();
        $getTravelPackages->limit($limit)->page($page_no);
 
         //pr($getTravelPackages->toArray()); exit;
        $this->set(compact('getTravelPackages','total_counts'));   
    }

    public function citylist()
    {
        if ($this->request->is(['ajax']))
        {
           $countries=$this->request->query('countries');  
           if(!empty($countries)){ 
               $countryList=explode(',',$countries);
               $cities=$this->PostTravlePackages->PackageCities->find()->where(['PackageCities.country_id IN' => $countryList]);
               echo '<ul id="list-2">';
               foreach ($cities as $value) {
                   echo "<li class='city_list FiterData' type='city' value='".$value->id."'>".$value->name."</li>";
               }
               echo"</ul>";
           }
        }
        exit;
    }
   
    public function citylistpopup()
    {
        if ($this->request->is(['ajax']))
        {
           $countries=$this->request->query('countries'); 
           if(!empty($countries)){
            $countryList=explode(',',$countries);
            $cities=$this->PostTravlePackages->PackageCities->find()->where(['PackageCities.country_id IN' => $countryList]);
            
               echo '<ul id="list-m-2">';
               foreach ($cities as $value) {
                   echo "<li class='city_list' type='city' value='".$value->id."'>".$value->name."</li>";
               }
               echo"</ul>";
           }
        }
        exit;
    }
   
 
    public function view($id = null)
    {
        $this->viewBuilder()->setLayout('detail_layout');
        //-- Increment View Count
        $Existcount=0;
        //-- Query
        $postTravlePackage=$this->PostTravlePackages->find()->contain(['Countries','Users'=>['Cities','States','PCountries','TripitoesRatings'=>function($q){
                    return $q->where(['TripitoesRatings.type'=>'Package'])->contain(['TripitoesUsers'=>['SocialProfiles']]);
                }],
                'PostTravlePackageCities'=>['Cities'],
                'PostTravlePackageCountries'=>['Countries'], 
                'TripitoesPackageLikes', 
                'TripitoesPackageCarts', 
                'TripitoesPackageLikes', 
                'TripitoesPackageReports',
                'PostTravlePackageRows'=>['PostTravlePackageCategories']
            ])
            ->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d'),'PostTravlePackages.id'=>$id]);
        $Existornot=$postTravlePackage->count();
        if($Existornot==0){
            return $this->redirect(['action' => 'packageList']);
        }
            $postTravlePackage= $postTravlePackage->first();
                 
            $Existcount=$postTravlePackage->trip_views;
            $postTravlePackage->trip_views=$Existcount+1;
            $this->PostTravlePackages->save($postTravlePackage);

            //-- Featured Packages

            $postTravlePackages=$this->PostTravlePackages->find()
                ->contain(['Users'=>function($q){
                      return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($s){
                        return $s->where(['TripitoesRatings.type'=>'Package']);
                      }]);
                  },'PostTravlePackageCities'=>['Cities'],'PostTravlePackageRows'=>['PostTravlePackageCategories'],'PostTravlePackageCountries'=>['Countries']])
                ->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d')
                ,'PostTravlePackages.position !='=>11,])
                ->group(['PostTravlePackages.id'])
                ->autoFields(true);
         
        $socialController='PostTravlePackages';
        $socialAction='view/'.$id; 
        $this->set('postTravlePackage', $postTravlePackage);
        $this->set('postTravlePackages', $postTravlePackages);
        $this->set('socialController', $socialController);
        $this->set('socialAction', $socialAction);
        $this->set('p_first', 'p_first');
    }

     
    public function add()
    {
        $postTravlePackage = $this->PostTravlePackages->newEntity();
        if ($this->request->is('post')) {
            $postTravlePackage = $this->PostTravlePackages->patchEntity($postTravlePackage, $this->request->getData());
            if ($this->PostTravlePackages->save($postTravlePackage)) {
                $this->Flash->success(__('The post travle package has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post travle package could not be saved. Please, try again.'));
        }
        $currencies = $this->PostTravlePackages->Currencies->find('list', ['limit' => 200]);
        $countries = $this->PostTravlePackages->Countries->find('list', ['limit' => 200]);
        $priceMasters = $this->PostTravlePackages->PriceMasters->find('list', ['limit' => 200]);
        $users = $this->PostTravlePackages->Users->find('list', ['limit' => 200]);
        $this->set(compact('postTravlePackage', 'currencies', 'countries', 'priceMasters', 'users'));
    }

     
    public function edit()
    {
      $this->viewBuilder()->setLayout('');
    }

     
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $postTravlePackage = $this->PostTravlePackages->get($id);
        if ($this->PostTravlePackages->delete($postTravlePackage)) {
            $this->Flash->success(__('The post travle package has been deleted.'));
        } else {
            $this->Flash->error(__('The post travle package could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function packageLike($post_travle_package_id,$tripitoes_user_id)
    {
        $tripitoesPackageLikes=$this->PostTravlePackages->TripitoesPackageLikes;
        $query = $tripitoesPackageLikes->query();
        $query->insert(['post_travle_package_id', 'tripitoes_user_id'])
            ->values([
                'post_travle_package_id' => $post_travle_package_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $postTravlePackages = $this->PostTravlePackages->get($post_travle_package_id);
        $postTravlePackages->trip_total_like+=1;
        $postTravlePackages = $this->PostTravlePackages->save($postTravlePackages);
        echo $postTravlePackages->trip_total_like;
        exit;
    }
    public function packageSave($post_travle_package_id,$tripitoes_user_id)
    {
        $tripitoesPackageCarts=$this->PostTravlePackages->TripitoesPackageCarts;
        $query = $tripitoesPackageCarts->query();
        $query->insert(['post_travle_package_id', 'tripitoes_user_id'])
            ->values([
                'post_travle_package_id' => $post_travle_package_id,
                'tripitoes_user_id' => $tripitoes_user_id
            ])
            ->execute();
        $tripitoesUsers = $this->PostTravlePackages->TripitoesUsers->get($tripitoes_user_id);
        $tripitoesUsers->total_saved+=1;
        $tripitoesUsers = $this->PostTravlePackages->TripitoesUsers->save($tripitoesUsers);
        echo $tripitoesUsers->total_saved;
        exit;
    }

    public function packageReport()
    {
        $packageid=$this->request->query('packageid');
        $report=$this->request->query('report');
        $tripitoes_user_id = $this->Auth->User('id');

        $TripitoesPackageReports=$this->PostTravlePackages->TripitoesPackageReports;
        $query = $TripitoesPackageReports->query();
        $query->insert(['post_travle_package_id', 'tripitoes_user_id','reason'])
            ->values([
                'post_travle_package_id' => $packageid,
                'tripitoes_user_id' => $tripitoes_user_id,
                'reason' => $report
            ])
            ->execute();
        echo "success";
        exit;
    }
    public function saveCall($id=null){
        $postTravlePackages = $this->PostTravlePackages->get($id);
        $postTravlePackages->trip_calls+=1;
        $postTravlePackages = $this->PostTravlePackages->save($postTravlePackages);
        exit;
    }
    public function saveShare($id=null){
        $hotelPromotions = $this->PostTravlePackages->get($id);
        $hotelPromotions->trip_shares+=1;
        $hotelPromotions = $this->PostTravlePackages->save($hotelPromotions);
        exit;
    }
}
