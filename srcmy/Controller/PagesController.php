<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Event\Event; 
use Cake\Network\Email\Email;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index','package','hotel','transport','autocompleteDestination','autocompleteCity','autocompleteHotel','blogList','blogDetails','loadMore','contactUs','privacyPolicy','aboutus','termConditions','add']);
        //index
        if($this->request->action==['index','blogDetails','contactUs']){
          $this->getEventManager()->off($this->Csrf);
        }
        $this->Security->setConfig('unlockedActions', ['contactUs']);
    }
    /**
     * Displays a vi
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    /*public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }*/
    public function package($id)
    {
        if ($this->request->is(['ajax']))
        { 
          if($id==0){
             $postTravlePackages = TableRegistry::get('PostTravlePackages');
             $postTravlePackages=$postTravlePackages->find()
              ->contain(['Users'=>function($q){
                      return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($s){
                        return $s->where(['TripitoesRatings.type'=>'Package']);
                      }]);
                  },'PostTravlePackageCities'=>['Cities'],'PostTravlePackageRows'=>['PostTravlePackageCategories'],'PostTravlePackageCountries'=>['Countries']])
              ->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d')
                ,'PostTravlePackages.position !='=>11,])
              ->group(['PostTravlePackages.id'])
              ->autoFields(true);
              $p_first='p_first';
          }
          else{           
            $package_page_no=@$this->request->query('package_page_no');
            $limit=1;
            if(empty($package_page_no)){
              $package_page_no=1;
              $limit=8;
            }
            $postTravlePackages = TableRegistry::get('PostTravlePackageCategories');
            $postTravlePackages=$postTravlePackages->get($id,[
                'contain'=>['PostTravlePackageRows'=>function($q)use($package_page_no,$limit){
                  return $q->contain(['PostTravlePackages'=>function($postTravlePackages) {
                    return $postTravlePackages ->select([
                            'trip_total_like'])
                    ->contain(['Users'=>function($q){
                            return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings']);;
                        },'PostTravlePackageCities'=>['Cities'],'PostTravlePackageRows'=>['PostTravlePackageCategories'],'PostTravlePackageCountries'=>['Countries']])
                        ->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d')])
                        ->group(['PostTravlePackages.id'])
                        ->autoFields(true);
                }]);
              }]
            ]);
          $p_first='dsasds';
          }
            
            $this->set(compact('postTravlePackages','p_first'));
        }
    }
    public function hotel($id)
    {
        //if ($this->request->is(['ajax']))
        { 
            //-- HOtel Category List
          if($id!=0){
            $hotelCategoryName = TableRegistry::get('HotelCategories');
            $hotelCategoryName = $hotelCategoryName->get($id);
          }
            //-- HOtel promotion List
            $where_short=array();
            $where_short=['Users.isVerified' =>'DESC','HotelPromotions.position' =>'ASC','HotelPromotions.id' =>'DESC'];
            $hotelPromotions = TableRegistry::get('HotelPromotions');
            $hotelPromotions=$hotelPromotions->find();
            $hotelPromotions->select(['id','trip_total_like'])
            ->contain(['HotelCategories','Cities'=>['States'],'Users'=>function($q){
                        return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($q){
                          return $q->where(['TripitoesRatings.type'=>'Hotel']);
                        }]);;
            }])
            ->where(['HotelPromotions.visible_date >=' =>date('Y-m-d')]);
            if($id==0){
              $hotelPromotions->where(['HotelPromotions.position !='=>11]);
            }
            else{
              $hotelPromotions->where(['HotelPromotions.hotel_category_id' =>$id]);
            }
            
            $hotelPromotions->where(['HotelPromotions.is_deleted' =>0,'HotelPromotions.share'=>1,'HotelPromotions.visible_date >=' =>date('Y-m-d')])
            ->group(['HotelPromotions.id']) 
            ->order($where_short)
            ->autoFields(true);
            $this->set(compact('hotelPromotions','hotelCategoryName'));
        }
    }
    public function transport($id)
    {
        if ($this->request->is(['ajax']))
        {
            //-- Taxi Promotion List 
            $where_short=array();
            $where_short=['Users.isVerified' =>'DESC','TaxiFleetPromotions.position' =>'ASC','TaxiFleetPromotions.id' =>'DESC'];
            if($id==0){
                $taxiFleetPromotions = TableRegistry::get('TaxiFleetPromotions');
                $taxiFleetPromotions=$taxiFleetPromotions->find()
                  ->contain(['Users'=>function($q){
                    return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($q){
                        return $q->where(['TripitoesRatings.type'=>'Transport']);
                    }]);
                },'TaxiFleetPromotionStates'=>['States'],'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']])
              ->where(['TaxiFleetPromotions.is_deleted' =>0,'TaxiFleetPromotions.share'=>1,'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d'),'TaxiFleetPromotions.position !='=>11])
              ->order($where_short)
              ->group(['TaxiFleetPromotions.id'])
              ->autoFields(true);
              $t_first='t_first';
            }
            else
            { 
              $taxiFleetPromotions = TableRegistry::get('TaxiFleetCarBuses');
              $taxiFleetPromotions=$taxiFleetPromotions->get($id,[
                'contain'=>['TaxiFleetPromotionRows'=>['TaxiFleetPromotions'=>function($taxiFleetPromotions)use($where_short){
                    return $taxiFleetPromotions ->select([
                            'trip_total_like'])
                    ->contain(['Users'=>function($q){
                        return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings']);;
                    },'TaxiFleetPromotionStates'=>['States'],'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']])
                    ->where(['TaxiFleetPromotions.is_deleted' =>0,'TaxiFleetPromotions.share'=>1,'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d')])
                    ->group(['TaxiFleetPromotions.id'])
                    ->order($where_short)
                    ->autoFields(true);
                }]]
              ]);
              $t_first="";
            }
            $this->set(compact('taxiFleetPromotions','t_first'));
        }
    }
    public function index()
    {
        $this->viewBuilder()->setLayout('front_layout');
        //-- SLider
        $tripitoesSliders = TableRegistry::get('TripitoesSliders');
        $tripitoesSliders = $tripitoesSliders->find()->where(['is_deleted'=>0]);

        //-- Package Category List
        $packageCategories = TableRegistry::get('PostTravlePackageCategories');
        $packageCategories = $packageCategories->find();
        $packageCategories->where(['PostTravlePackageCategories.is_deleted'=>0])
                          ->order(['PostTravlePackageCategories.name'=>'ASC']);
         
        $where_short=array();
        $where_short=['Users.isVerified' =>'DESC','PostTravlePackages.position' =>'ASC','PostTravlePackages.id' =>'DESC'];

        $postTravlePackages = TableRegistry::get('PostTravlePackages');
        $postTravlePackages=$postTravlePackages->find()
          ->contain(['Users'=>function($q){
                  return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($s){
                    return $s->where(['TripitoesRatings.type'=>'Package']);
                  }]);
              },'PostTravlePackageCities'=>['Cities'],'PostTravlePackageRows'=>['PostTravlePackageCategories'],'PostTravlePackageCountries'=>['Countries']])
          ->where(['PostTravlePackages.is_deleted' =>0,'PostTravlePackages.share'=>1,'PostTravlePackages.visible_date >=' =>date('Y-m-d')
            ,'PostTravlePackages.position !='=>11,])
          ->group(['PostTravlePackages.id'])
          ->order($where_short)
          ->autoFields(true);
         
        //-- HOtel Category List
        $hotelCategories = TableRegistry::get('HotelCategories');
        $hotelCategories = $hotelCategories->find()->where(['HotelCategories.is_deleted'=>0])
                                          ->order(['HotelCategories.name'=>'ASC']);

        $hotelCategoryName = TableRegistry::get('HotelCategories');
        $hotelCategoryName = $hotelCategoryName->get($hotelCategories->toArray()[0]->id);
        //-- HOtel promotion List
        $where_short=array();
        $where_short=['Users.isVerified' =>'DESC','HotelPromotions.position' =>'ASC','HotelPromotions.id' =>'DESC'];

        $hotelPromotions = TableRegistry::get('HotelPromotions');
        $hotelPromotions=$hotelPromotions->find();
        $hotelPromotions->select(['id','trip_total_like'])
        ->contain(['HotelCategories','Cities'=>['States'],'Users'=>function($q){
               return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($q){
                  return $q->where(['TripitoesRatings.type'=>'Hotel']);
               }]);
          }])
          ->where(['HotelPromotions.visible_date >=' =>date('Y-m-d')])
          ->where(['HotelPromotions.is_deleted' =>0,'HotelPromotions.share'=>1,'HotelPromotions.visible_date >=' =>date('Y-m-d'),'HotelPromotions.position !='=>11])
          ->group(['HotelPromotions.id']) 
          ->order($where_short)
          ->autoFields(true);
       
        //-- Taxi Category 
        $taxiCategories = TableRegistry::get('TaxiFleetCarBuses');
        $taxiCategories = $taxiCategories->find();
        $taxiCategories->where(['TaxiFleetCarBuses.is_deleted'=>0])
                      ->order(['TaxiFleetCarBuses.name'=>'ASC']);
        //-- Taxi Promotion List 
        $where_short=array();
        $where_short=['Users.isVerified' =>'DESC','TaxiFleetPromotions.position' =>'ASC','TaxiFleetPromotions.id' =>'DESC'];

        $taxiFleetPromotions = TableRegistry::get('TaxiFleetPromotions');
        $taxiFleetPromotions=$taxiFleetPromotions->find()
              ->contain(['Users'=>function($q){
                    return $q->select(['id','first_name','last_name','mobile_number','company_name','email','percentage','isVerified'])->contain(['TripitoesRatings'=>function($q){
                        return $q->where(['TripitoesRatings.type'=>'Transport']);
                    }]);
                },'TaxiFleetPromotionStates'=>['States'],'TaxiFleetPromotionRows'=>['TaxiFleetCarBuses']])
              ->where(['TaxiFleetPromotions.is_deleted' =>0,'TaxiFleetPromotions.share'=>1,'TaxiFleetPromotions.visible_date >=' =>date('Y-m-d'),'TaxiFleetPromotions.position !='=>11])
              ->group(['TaxiFleetPromotions.id'])
              ->order($where_short)
              ->autoFields(true);

        //-- Blogs List
        $tripitoesBlogs = TableRegistry::get('TripitoesBlogs');
        $tripitoesBlogs = $tripitoesBlogs->find()->where(['is_deleted'=>0])->limit(3)->order(['id'=>'DESC']);
        $socialController='Pages';
        $socialAction='index';
        $p_first='p_first';
        $t_first='t_first';

        // News Letters
        $TripitoesNewsletters = TableRegistry::get('TripitoesNewsletters');

        $tripitoesNewsletters = $TripitoesNewsletters->newEntity();
        if ($this->request->is('post')) {
            $tripitoesNewsletters = $TripitoesNewsletters->patchEntity($tripitoesNewsletters, $this->request->getData());
            if ($TripitoesNewsletters->save($tripitoesNewsletters)) {
                $this->Flash->success(__('Successfully Submitted'));
            }
            return $this->redirect(['action' => 'index']);
         }

        $this->set(compact('tripitoesSliders','postTravlePackages','packageCategories','hotelCategories','hotelPromotions','taxiCategories','taxiFleetPromotions','tripitoesBlogs','hotelCategoryName','socialController','socialAction','p_first','t_first'));

         
    }
    public function autocompleteDestination(){
       $this->viewBuilder()->layout('');
       $term = $this->request->query('term');     
       $this->Countries = TableRegistry::get('Countries');   
       $countries = $this->Countries->find()
           ->where([
               'Countries.country_name LIKE' => $term.'%',
               'Countries.is_deleted !=' => 1
           ]);
       $data=[];
       $data_duplicate=[];
       foreach ($countries as $country) {
           $data[]=['itemName' => $country->country_name, 'id' => $country->id, 'slug' => 'Country'];
           $data_duplicate[]=$country->country_name;
       }      

       $this->States = TableRegistry::get('States');   
       $states = $this->States->find()
                       ->where([
                           'States.state_name LIKE' => $term.'%',
                           'States.is_deleted !=' => 1
                       ]);
       
       foreach ($states as $state) {
            if(!in_array($state->state_name,$data_duplicate))
            {
                $data[]=['itemName' => $state->state_name, 'id' => $state->id, 'slug' => 'State'];
            }
       }      

       $this->Cities = TableRegistry::get('Cities');   
       $cities = $this->Cities->find()
                      ->contain(['States'])
                       ->where([
                           'Cities.name LIKE' => $term.'%',
                           'Cities.is_deleted !=' => 1
                       ]);
       
       foreach ($cities as $city) {
           $data[]=['itemName' => $city->name.'('.$city->state->state_name.')', 'id' => $city->id, 'slug' => 'City'];
       }  
       $object = (object) $data;
       echo json_encode($object);
       exit;
    }

    public function autocompleteHotel(){
       $this->viewBuilder()->layout('');
       $term = $this->request->query('term');

       $this->States = TableRegistry::get('States');   
       $states = $this->States->find()
                       ->where([
                           'state_name LIKE' => $term.'%',
                           'is_deleted' => 0,
                           'country_id' => '101'
                       ]);
       
       foreach ($states as $state) {
          $data[]=['itemName' => $state->state_name, 'id' => $state->id, 'slug' => 'State'];
       }      

       $this->Cities = TableRegistry::get('Cities');   
       $cities = $this->Cities->find()
                       ->contain(['States'])
                       ->where([
                           'Cities.name LIKE' => $term.'%',
                           'Cities.is_deleted !=' => 1
                       ]);
       
       foreach ($cities as $city) {
           $data[]=['itemName' => $city->name.'('.$city->state->state_name.')', 'id' => $city->id, 'slug' => 'City'];
       }
       $object = (object) $data;
       echo json_encode($object);
       exit;
    }
   
    public function autocompleteCity(){
       $this->viewBuilder()->layout('');
       $term = $this->request->query('term');     
       $this->Countries = TableRegistry::get('Countries');   
       
       $data=[];
       $this->Cities = TableRegistry::get('Cities');   
       $cities = $this->Cities->find()
                       ->contain(['States'])
                       ->where([
                           'Cities.name LIKE' => $term.'%',
                           'Cities.is_deleted !=' => 1
                       ]);
       
       foreach ($cities as $city) {
           $data[]=['itemName' => $city->name.'('.$city->state->state_name.')', 'id' => $city->id];
       }  
       $object = (object) $data;
       echo json_encode($object);
       exit;
    }

    public function blogList()
    {
      $this->viewBuilder()->setLayout('detail_layout');
      $this->TripitoesBlogs = TableRegistry::get('TripitoesBlogs'); 
      $tripitoesBlogs = $this->TripitoesBlogs->find()
         ->where([
             'TripitoesBlogs.is_deleted !=' => 1
         ])
         ->order(['TripitoesBlogs.id'=>'DESC'])
         ->limit(10)
         ->page(1);

      
      $letestBlog = $this->TripitoesBlogs->find()
       ->where([
           'TripitoesBlogs.is_deleted !=' => 1
       ])
       ->order(['TripitoesBlogs.id'=>'DESC'])
       ->limit(3);
      
      $this->set(compact('tripitoesBlogs','letestBlog'));         

    }
    public function blogDetails($id=null)
    {
    $this->viewBuilder()->setLayout('detail_layout');
    $this->TripitoesBlogs = TableRegistry::get('TripitoesBlogs'); 
    //$blog = $this->TripitoesBlogs->get($id,);
    $blog = $this->TripitoesBlogs->get($id, [
        'contain' => [
            'TripitoesBlogComments'=>['TripitoesBlogCommentRows'],
          ]
      ]);
    ///pr($blog);exit;
    $Existcount=$blog->trip_views;
    $blog->trip_views=$Existcount+1;
    $this->TripitoesBlogs->save($blog); 
    $tripitoesBlogComments = $this->TripitoesBlogs->TripitoesBlogComments->newEntity();

    if ($this->request->is(['post'])) {  
        $tripitoesBlogComments = $this->TripitoesBlogs->TripitoesBlogComments->patchEntity($tripitoesBlogComments, $this->request->getData());
        $tripitoesBlogComments->tripitoes_blog_id=$id;
        if ($this->TripitoesBlogs->TripitoesBlogComments->save($tripitoesBlogComments)) {
          $Existcounts=$blog->total_comment;
          $blog->total_comment=$Existcounts+1;
          $this->TripitoesBlogs->save($blog); 
            return $this->redirect(['action' => 'blogDetails/'.$id]);
        }
    }
    $this->set(compact('blog','tripitoesBlogComments'));
  }

  public function loadMore(){

    $page_no=$this->request->query('page_no');
    $this->TripitoesBlogs = TableRegistry::get('TripitoesBlogs'); 
    $tripitoesBlogs = $this->TripitoesBlogs->find()
      ->where([
           'TripitoesBlogs.is_deleted !=' => 1
       ])
      ->order(['TripitoesBlogs.id'=>'DESC'])
      ->limit(10)
      ->page($page_no);
      $this->set(compact('tripitoesBlogs'));
  }

  public function contactUs()
  {
    $this->viewBuilder()->setLayout('detail_layout');
    $this->Contacts = TableRegistry::get('Contacts'); 
    $contacts = $this->Contacts->newEntity();

    if ($this->request->is(['post'])) { 
      $contacts = $this->Contacts->patchEntity($contacts, $this->request->getData());
      if ($this->Contacts->save($contacts)) {

          $subject="Contact Us";
          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
          $headers .= 'From: Tripitoes <webmaster@tripitoes.com>' . "\r\n";
          $message='';
          $message.='<p><b>User Information</b></p>';
          $message.='<p><b>Name : </b> '.$this->request->getData('name').'</p>';
          $message.='<p><b>Mobile No. : </b> '.$this->request->getData('phone').'</p>';
          $message.='<p><b>E-Mail : </b> '.$this->request->getData('email').'</p>';
          $message.='<p><b>Subject : </b> '.$this->request->getData('subject').'</p>';
          $message.='<p><b>Message : </b> '.$this->request->getData('message').'</p>';

          $email = new Email();    
          $email->transport('gmail')
            ->from(['webmaster@tripitoes.com'=>'Tripitoes'])
            ->to('webmaster@tripitoes.com')
            ->subject($subject)
            ->emailFormat('html')
            ->viewVars(array('msg' => $message))
            ->send($message); 
 
          return $this->redirect(['action' => 'contactUs']);
      }
    }
    $this->set(compact('contacts'));
  }
  public function privacyPolicy()
  {
    $this->viewBuilder()->setLayout('detail_layout');
  }
  public function aboutus()
  {
    $this->viewBuilder()->setLayout('detail_layout');
  }
  public function termConditions()
  {
    $this->viewBuilder()->setLayout('detail_layout');
  }
  
  public function add()
    {
      $this->viewBuilder()->setLayout('detail_layout');
      $this->TripitoesBlogs = TableRegistry::get('TripitoesBlogs'); 
      $tripitoesBlogs = $this->TripitoesBlogs->find()
         ->where([
             'TripitoesBlogs.is_deleted !=' => 1
         ])
         ->order(['TripitoesBlogs.id'=>'DESC'])
         ->limit(10)
         ->page(1);

      
      $letestBlog = $this->TripitoesBlogs->find()
       ->where([
           'TripitoesBlogs.is_deleted !=' => 1
       ])
       ->order(['TripitoesBlogs.id'=>'DESC'])
       ->limit(3);
      
      $this->set(compact('tripitoesBlogs','letestBlog'));         

    }
  
}
