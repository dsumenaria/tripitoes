<!-- CONTENT START -->
  <div class="content"> 
    <!--======= SUB BANNER =========-->
    <section class="sub-banner animate fadeInUp heading-bg" data-wow-delay="0.4s">
      <div class="container">
        <h4>BLOGS</h4>
      </div>
    </section>
    
    <!-- Blog -->
    <section class="section-p-30px blog-page">
      <div class="container">
        <div class="row"> 
          
          <!-- Right Side Bar -->
          <div class="col-sm-3 animate fadeInLeft" data-wow-delay="0.4s">
            <div class="side-bar"> 
              
              <!--  SEARCH 
              <div class="search">
                <form>
                  <input type="text" placeholder="SEARCH FAQ">
                  <button type="submit"> <i class="fa fa-search"></i></button>
                </form>
              </div>-->
              
              <!-- HEADING -->
              <div class="heading">
                <h4>Latest post</h4>
              </div>
              <!-- CATEGORIES -->
              <ul class="cate latest-post">
                <?php 
                  foreach ($letestBlog as $key => $letest) {
                  ?>
                    <li>
                      <div class="media">
                        <div class="media-left"> 
                          <a href="#.">
                             
                             <?= $this->Html->image($cdn_path.'xyz.jpg',['alt'=>'','class'=>'redirect','style'=>'vertical-align: top !important;','redirectid'=>$this->Url->build(['controller'=>'Pages','action'=>'blogDetails/'.$letest->id])]);?>
                          </a>
                        </div>
                        <div class="media-body" style="vertical-align: bottom !important;">  
                          <?= $this->Html->link($this->Text->truncate($letest->title, 16, ['ellipsis' => '...','exact' => true]), ['controller'=>'Pages','action'=>'blogDetails',$letest->id], ['escape'=>false]) ?>
                            <ul class="info">
                              <li style="line-height: 10px !important;"><i class="fa fa-calendar-o"></i> <?= $letest->created_on->format('d-M');?>&nbsp; 
                              <i class="fa fa-comment"></i> <?= $letest->total_comment;?></li>
                            </ul> 
                        </div>
                      </div>
                    </li>
                <?php     
                  }
                ?>
              </ul>
              
              <!-- HEADING -->
             <!-- <div class="heading">
                <h4>Archive</h4>
              </div>
              <!-- CATEGORIES -->
              <!--<ul class="cate">
                <li><a href="#.">March 2015
                  Jan 2015</a></li>
                <li><a href="#."> December 2014</a></li>
                <li><a href="#."> November 2014</a></li>
                <li><a href="#."> July 2014</a></li>
              </ul>
              
              <!-- TAGS -->
              <!--<div class="heading">
                <h4>Tags</h4>
              </div>
			  
              <!--<ul class="tags">
                <li><a href="#.">FASHION</a></li>
                <li><a href="#.">BAGS</a></li>
                <li><a href="#.">TABLET</a></li>
                <li><a href="#.">ELECTRONIC</a></li>
                <li><a href="#.">BEAUTY</a></li>
                <li><a href="#.">TRtENDING</a></li>
                <li><a href="#.">SHOES</a></li>
              </ul>-->
            </div>
          </div> 
          <!-- Blog Bar -->
          <div class="col-sm-9 animate fadeInRight" data-wow-delay="0.4s"> 
            <!--  Blog Posts -->
            <div class="blog-posts Details medium-images">
              <ul class="more_data">
                 <?php
				if(sizeof($tripitoesBlogs->toArray())>0){  
					foreach ($tripitoesBlogs as $key => $blog) {
					?>
					  <li class="animate fadeInUp" data-wow-delay="0.4s">
						<div class="row"> 
						  <!--  Image -->
						  <div class="col-sm-5"> 
							<?= $this->Html->image($cdn_path.'xyz.jpg',['alt'=>'','class'=>'redirect img-responsive','width'=>'336px','height'=>'214px','redirectid'=>$this->Url->build(['controller'=>'Pages','action'=>'blogDetails/'.$blog->id])]);?>
						  </div>
						  <div class="col-sm-7"> 
							<?= $this->Html->link($blog->title, ['controller'=>'Pages','action'=>'blogDetails',$blog->id], ['escape'=>false,'class'=>'tittle-post']) ?>
							<ul class="info">
							  <li><i class="fa fa-user"></i> Written by - <span style="text-transform: capitalize;color: #5f9a02;">Preeti Singh</span></li>
							</ul>
							<p>
							  <?= $this->Text->truncate($blog->short_description, 100, ['ellipsis' => '...','exact' => true]); ?> 
							</p>
							<!--  Post Info -->
							<ul class="info"> 
							  <li><i class="fa fa-calendar-o"></i> <?= $blog->created_on->format('d-M');?></li>
							  <li><i class="fa fa-comment"></i> <?= $blog->total_comment;?></li>
							   <?php  $shareLink = $this->Url->build(['controller'=>'Pages','action'=>'blogList','_full'=>true,'_ssl'=>true]); ?> 
							  <li style="float: right;"><i class="fa fa-share"></i> Share on -  
								<a href="javascript:void(0);" class="shareBtn" shareLink="<?= $shareLink ?>"><i style=" padding: 8px 10px;  background: #000; color: #fff;   margin-right: 5px;" class="fa fa-facebook"></i></a>
								<a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i style="padding: 8px 5.7px; background: #000; color: #fff;" class="fa fa-google-plus"></i></a>


							  </li>
							</ul>
							<?= $this->Html->link('READ MORE', ['controller'=>'Pages','action'=>'blogDetails',$blog->id], ['escape'=>false,'class'=>'btn btn-small btn-dark']) ?>
						  </div>
						</div>
					  </li>
					  
					<?php
					}
				}
				?>
              </ul>
            </div>
            <input type="hidden" id ="page_no" value="2" class="page_no">
          </div>
        </div>
      </div>
    </section>
  </div> 
<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
function sharefb(shareLink){
   
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: shareLink,
  }, function(response){});
}
</script>
<?php
$js="
  $(document).on('click','.redirect',function(e){
      var url= $(this).attr('redirectid');
      url=url;
      window.location.href = url;
  });
  $(document).on('click','.shareBtn',function(e){
       sharefb($(this).attr('shareLink'));
  });
  $(window).on('load', function() {
      loadMore();

  });
  function loadMore(){ 
    var page_no = $('#page_no').val();
    var url='".$this->Url->build(['controller'=>'Pages','action'=>'loadMore'])."';
    url=url+'?page_no='+page_no;
    $.ajax({
        type: 'get',
        url: url,
        beforeSend: function(msg){
        },
        success: function(response) 
        { 
          if(response ==''){ 
          }
          else{
            $('.more_data').append(response);
            $('#page_no').val(parseInt(page_no)+1);
            loadMore();
          } 
        },
        error: function(e) 
        {
        }
      });
  }
";
$this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>