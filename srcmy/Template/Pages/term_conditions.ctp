<div class="content"> 
  <div class="container">
    <div class="row">
      <div class="col-md-12 animate fadeInRight" data-wow-delay="0.4s"> 
        <!--======= About Us =========-->
        <section class="section-p-30px about-us">
          <div class="sma-hed">
            <h4 style="margin-bottom: 20px;">Terms & Conditions</h4>
          </div>
          <div class="about-detail">
      <p>Please read these terms and conditions of use carefully before accessing, using or obtaining
      any materials, information, products or services. www.Tripitoes.com is a product of Durja
      Travel Network Pvt. Ltd. By accessing the Tripitoes website, or any other feature or other
      Tripitoes platform (collectively &quot;Our Website&quot;) you agree to be bound by these terms and
      conditions (&quot;Terms&quot;) and our Privacy Policy. If you do not accept all of these Terms, then
      you may not use Our Website. In these Terms, &quot;we&quot;, &quot;us&quot;, &quot;our&quot; or &quot;Tripitoes&quot; refers to
      Tripitoes.com and Durja Travel Network Pvt. Ltd., and &quot;you&quot; or &quot;your&quot; refers to you as the
      user of Our Website.</p>

      <p>We may modify these Terms, for any reason at any time, by posting a new version on Our
      Website; these changes do not affect rights and obligations that arose prior to such
      changes. Your continued use of Our Website following the posting of modified Terms will be
      subject to the Terms in effect at the time of your use. Please review these Terms
      periodically for changes. If you object to any provision of these Terms or any subsequent
      modifications to these Terms or become dissatisfied with Our Website in any way, your only
      recourse is to immediately terminate use of Our Website.</p>
      <p>We recommend you save and/or print out a copy of these Terms for your future reference.</p>
      
      <h6>1. We do not sell Travel Products</h6>
      <p>Our Website is a travel search engine. Tripitoes does not provide, own or control any of the
      travel services and products that you can access through Our Website, such as
      accommodations, rental cars, or packages. The Travel Products are owned, controlled or
      made available by third parties (the “Travel Providers”) either directly or as an agent (e.g.
      online travel agency). The Travel Providers are responsible for the Travel Products. The
      Travel Provider’s terms and privacy policies apply to your booking so you must agree to, and
      understand those terms. Further, the terms of the actual travel provider (hotel, tour
      operator, etc.) apply to your travel, so you must also agree to and understand those terms.
      Your interaction with any Travel Provider accessed through Our Website is at your own risk
      and Tripitoes does not have any responsibility should anything go wrong with your booking
      or during your travel.</p>

      <p>The display on Our Website of a Travel Product or Travel Provider does not in any way
      imply, suggest, or constitute a recommendation by Tripitoes of that Travel Product or Travel
      Provider, or any sponsorship or approval of Tripitoes by such Travel Provider, or any
      affiliation between such Travel Provider and Tripitoes.</p>

      <p>Tripitoes hosts content, including prices, made available by or obtained from Travel
      Providers. Tripitoes is in no way responsible for the accuracy, timeliness or completeness of
      such content. Since Tripitoes has no control over the Travel Products and does not verify the
      content uploaded by the Travel Providers, it is not possible for us to guarantee the prices
      displayed on Our Website. Prices change constantly and additional charges (e.g. payment
      fees, services charges, checked-in luggage fees, local taxes and fees) may apply, so you
      should always check whether the price asked for a booking is the one you expected.</p>
      
      
      <h6>2. Booking through Tripitoes</h6>
      <p>If you make a booking through Our Website for Travel Products, that booking is made with
      the Travel Provider named on the booking page and Our Website only acts as a user
      interface. Accordingly, Tripitoes has no responsibility for the booking or the Travel Product
      because Tripitoes has no involvement in creating the description of the Travel Product, in
      defining the price and any fees, and in providing the Travel Products that you book. If you
      have any issues or disputes around your booking and/or the Travel Product, you agree to
      address and resolve these with the Travel Provider and not with us.</p>
      
      <h6>3. Intellectual property</h6>
      <p>We, along with our corporate affiliates, the Travel Providers and other licensors own all of
      the text, images, software, trademarks, service marks or other material contained on Our
      Website. You will not copy or transmit any of the material except for your personal, non-
      commercial use. All copyright, trademark and other proprietary rights notices presented on
      Our Website must appear on all copies you print. Other non-Tripitoes product, service, or
      company designations on Our Website belong to those respective third parties and may be
      mentioned in Our Website for identification purposes only. You should contact the
      appropriate third party for more complete information regarding such designations and
      their registration status. Your use of and access to Our Website does not grant you any
      licence or right to use any of the marks included on Our Website.</p>
      
      <h6>4. Use of Our Website</h6>
      <p>You may only use and register to become a user of Our Website, if you are of sufficient legal
      age and can enter into binding contracts. If you become a registered user or make a booking
      resulting in the creation of an account, you are responsible for maintaining the secrecy of
      your passwords, login and account information. You will be responsible for all use of Our
      Website by you, anyone using your password and login information (with or without your
      permission) and anyone whom you allow to access your travel itineraries. All information
      that you provide to us must be accurate and up-to-date. If any of your information changes,
      you must immediately update it. If you have reason to believe that your account is no longer
      secure (e.g., loss, theft or unauthorised disclosure or use of your information or computer or
      mobile device used to access Our Website), you must promptly change your Personal
      information that is affected.</p>
      
      <p>If you decide to receive messages or other communications from Our Website directly to
      your mobile device, you are solely responsible for keeping us updated with your current
      phone number, respectively updating to the latest version of the mobile app, and for any
      charges incurred to receive such messages. We will not be liable for information sent to a
      device that is associated with your out-dated mobile phone number or using an out-dated
      mobile app. If you install any software or enable any service that stores information from
      Our Website on any mobile device or computer, it is your responsibility, prior to transfer or
      disposal of such device, to remove your information or otherwise disable access to such
      software or service, in order to prevent unauthorised access to your information or account.</p>
      
      <p>You may only use Our Website to search for legitimate travel deals and you may not use Our
      Website to make any false, fraudulent or speculative reservation or any reservation in
      anticipation of demand. By using Our Website you agree to comply with laws that apply to
      India and your own country, including laws that apply to exporting technical data.</p>

      <h6>In addition, you agree not to do any of the following without prior express written
permission of Tripitoes:</h6>
      <ul>
      <li>1. Access the site with any manual or automated process for any purpose other than
        your personal use or for inclusion of Tripitoes pages in a search index. Use of any
        automated system or software to extract data from Our Website (“screen scraping”),
        for commercial or non-commercial purposes, is prohibited;</li>
      <li>2. Violate the restrictions in any robot exclusion headers on Our Website or bypass or
circumvent other measures employed to prevent or limit access to Our Website;</li>
      <li>3. Use any device, software or routine that interferes or attempts to interfere with the
normal operation of Our Website or take any action that imposes an unreasonable
load on our computer or network equipment;</li>
      <li>4. Reproduce, duplicate, copy, sell, trade, resell or exploit Our Website;</li>
      <li>5. Use any feature of Our Website for any purpose that is unlawful, harmful, or
otherwise objectionable or inappropriate as determined by us;</li>
      <li>6. Post or distribute any material on Our Website that violates the rights of any third
party or applicable law;</li>
      <li>7. Use Our Website to collect or store personal data about others;</li>
      <li>8. Use Our Website for any commercial purpose unless we&#39;ve given you written permission;</li>
      <li>9. Transmit any ad or promo materials on Our Website.</li>
      </ul>
      
      <p>You are granted a limited, non-exclusive right to create a “hypertext” link to Our Website
      provided that such link does not portray Tripitoes or Our Website in a false, misleading,
      derogatory, or otherwise defamatory manner. This limited right may be revoked at any time
      for any reason whatever.</p>

      <p>We may, at our sole discretion, at any time and without advance notice or liability, suspend,
terminate or restrict your access to all or any component of Our Website. Further, you can
always delete your account here.</p>

      <p>When you provide us with Trips Information, or make a booking through Tripitoes you
      authorise us to make copies as we deem necessary in order to facilitate the storage and
      assimilation of the Trips Information. By providing us Trips Information you represent and
      warrant that you have the right to give us, an irrevocable, perpetual, non-exclusive,
      transferable, fully paid, worldwide licence (with the right to freely sublicense) to use, copy,
      modify, reformat, translate, syndicate, and distribute that Trips Information we receive from
      you for any purpose, including business, commercial, marketing, advertising, or otherwise,
      and to prepare derivative works of, or incorporate into other works, that Trips Information.
      You may remove your Trips Information from your account at any time, but the licence that
      you have granted will remain in effect. You understand that we do not control, and we are
      not responsible to review Trips Information. However, we reserve the right to review, edit,
      or delete any Trips Information or your account at any time.</p>
      
      <h6>5. Warranty disclaimer</h6>
      <p>All content and services provided on Our Website and all itineraries you obtain through
      Tripitoes are provided on an “as-is” and “as available” basis. Our content is largely
      generated in an automated fashion; errors can and do happen. We usually have many
      search results, but we are not comprehensive and do not display all available providers and
      offers. Accordingly, we do not always display the lowest available price. Tripitoes expressly
      disclaims to the fullest extent permissible all warranties of any kind, whether express or
      implied, including, but not limited to, any implied warranties of merchantability, fitness for a
      particular purpose, title, non-infringement, and security and accuracy, as well as all
      warranties arising by usage of trade, course of dealing, or course of performance.</p>

      <h6>6. Our liability is limited</h6>
      <p>We (together with our officers, directors, employees, representatives, shareholders,
      affiliates, and providers) to the extent permitted by law hereby expressly exclude any
      responsibility and liability for (a) any loss or damages to, or viruses that may infect, your
      computer equipment or other property as the result of your access to Our Website, your
      downloading of any content from Our Website or your use of your account or (b) any injury,
      death, loss, claim, act of god, accident, delay, or any direct, special, exemplary, punitive,
      indirect, incidental or consequential damages of any kind (including without limitation lost
      profits or lost savings), whether based in contract, tort, strict liability or otherwise, that arise
      out of or is in any way connected with: (i) any use of Our Website, account or our content;
      (ii) any failure or delay (including without limitation the use of or inability to use any
      component of this Website for reservations or booking); or (iii) the performance or non
      performance by us or any Travel Provider, even if we have been advised of the possibility of
      damages to such parties or any other party. Some states or countries do not allow this
      limitation of liability, so the limitations above may not apply or apply only partially to you.</p>
      
      <h6>7. You agree to protect us</h6>
      <p>Subject to these Terms, you will defend, indemnify and hold us and each of our officers,
      directors, employees and agents, harmless from and against any claim, cause of action,
      liability, expense, loss or demand, including without limitation reasonable legal and
      accounting fees, arising out of, or in any way connected with your breach of these Terms or
      the agreements made part of these Terms by reference, your breach of any applicable law,
      and your use of or access to Our Website, your account or the Intellectual Property.</p>
      
      <h6>8. Links</h6>
      <p>Our Website may contain links to other websites that we do not operate or control and for
      which we are not responsible (“Other Websites”). We provide these links for your reference
      and convenience and do not endorse the contents of Other Websites and accept no
      responsibility for them or for any loss or damages that may arise from your use of them. You
      should refer to the separate terms of use, privacy policies, and other rules posted on Other
      Websites before you use them. You agree not to create a link from any website, including
      any website controlled by you, to Our Website.</p>
      
      <h6>9. General requirements</h6>
      <p>We may change the site and these Terms at any time, in our sole discretion and without
      notice to You. You are responsible for remaining knowledgeable about these Terms. Your
      continued use of the site constitutes your acceptance of any changes to these Terms and
      any changes will supersede all previous versions of the Terms. Unless otherwise specified
      herein, all changes to these Terms apply to all users, including those enrolled before the
      date the changes take effect. Further, we may terminate this agreement with you under
      these Terms at any time by notifying you in writing (including by email) and/or, if you are a
      registered user, by cancelling your account and your access to your account. You may
      terminate this agreement with us under these Terms at any time by deleting your account
      and ceasing use of Our Website.</p>
      
      <p>Nothing contained in these Terms will be deemed to constitute either party as the agent or
      representative of the other party, or both parties as joint venturers or partners for any
      purpose. You may not assign, delegate or transfer your rights or obligations under these
      Terms. We may assign our rights and duties under these Terms without such assignment
      being considered a change to the Terms and without notice to you, provided your rights
      under these Terms are not prejudiced.</p>
      
      <p>If we fail to act with respect to your breach or anyone else&#39;s breach on any occasion, we are
      not waiving our right to act with respect to future or similar breaches. If a court finds any of
      these Terms to be unenforceable or invalid, that Term will be enforced to the fullest extent
      permitted by applicable law and the other Terms will remain valid and enforceable. These
      Terms, together with those agreements made a part of these Terms by reference, make up
      the entire agreement between us relating to your use of Our Website, and replace any prior
      understandings or agreements (whether oral or written) regarding your use of Our Website.</p>
      
      <p>If you take any legal action relating to your use of Our Website, including Trips, or these
      Terms, or our services, you agree to file such action only in the courts located in Udaipur,
      India; if you are a consumer, the law may allow you to bring proceedings also in the courts
      for the place where you are domiciled. In any such action or any action we may initiate, the
      prevailing party will be entitled to recover all legal expenses incurred in connection with the
      action, including but not limited to costs, both taxable and non-taxable, and reasonable
      attorney fees. To the extent permitted by law, you agree that any disputes, claims and
      causes of action arising out of or connected with Our Website and/or these Terms, will be
      resolved individually, without resort to any form of class action.</p>
            
      <h6>10. Your feedback</h6>
      <p>We encourage you to share your comments and questions with us here, but we may not be
      able to respond to all of them. Please note that we assume no responsibility for reviewing
      unsolicited ideas for our business (like product or advertising ideas) and will not incur any
      liability as a result of any similarities between those ideas and materials that may appear in
      future Tripitoes products or services. Also, please remember that you are responsible for
      whatever material you submit, including its reliability, originality, and copyright. Please do
      not reveal trade secrets or other confidential information in your messages. Any and all
      rights to materials submitted to us become the exclusive property of Tripitoes. Further, by
      submitting Feedback you are granting us an irrevocable, perpetual, non-exclusive,
      transferable, fully paid, worldwide licence (with the right to freely sublicense) to use, copy,
      modify, publicly perform, publicly display, reformat, translate, syndicate, republish, excerpt
      (in whole or in part) and distribute Feedback we receive from you for any purpose, including
      business, commercial, marketing, advertising, or otherwise.</p>
      
          </div>
        </section>
      </div>
    </div>
  </div>
    
  </div>