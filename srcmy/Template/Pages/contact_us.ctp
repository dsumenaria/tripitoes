<!-- CONTENT START -->
  <div class="content"> 
  
  	<section class="sub-banner animate fadeInUp heading-bg" data-wow-delay="0.4s">
      <div class="container">
        <h4>Contact Us</h4>
      </div>
    </section>
    
    <!--======= Contact Us =========-->
    <section class="section-p-30px conact-us conact-us-2 no-padding-b"> 
      <!--======= CONTACT FORM =========-->
      <div class="container">
        <div class="row"> 
          <div class="col-sm-4 animate fadeInRight" data-wow-delay="0.4s">
            <div class="boxes-in">
              <div class="tittle">
                <h5>Contact Details</h5>
                </div>
              <ul class="location">
                <li> <i class="fa fa-phone"></i>
                  <p>Phone: +91-9549220077</p>
                </li>
                <li> <i class="fa fa-envelope"></i>
                  <p>contactus@tripitoes.com</p>
                </li>
                <li> <i class="fa fa-building"></i>
                  <p>Office A, Shivam Apartments,<br>
                    Durga Nursery Road, Udaipur,<br>
                    Rajasthan, India-313001<br>
                  </p>
                </li>
              </ul>
            </div>
          </div>
          <!-- Contact Form -->
          <div class="col-sm-8 animate fadeInleft" data-wow-delay="0.4s"> 
            <!-- TITTLE -->
            <div class="boxes-in">
              <div class="tittle">
                <h5>Contact Form</h5>
                 
            </div>
            <div class="contact">
              <div class="contact-form"> 
                <!--======= FORM  =========-->
                 <?= $this->Form->create($contacts,['id'=>'contact_form']) ?>
                  <div class="row">
                    <div class="col-md-12">
                      <ul class="row">
                        <li class="col-sm-12">
                          <label>
                            <?= $this->Form->input('name',['class'=>'form-control','placeholder'=>'NAME*','label'=>false,'required']);?>
                          </label>
                        </li>
                        
                        <li class="col-sm-12">
                          <label>
                            <?= $this->Form->input('phone',['class'=>'form-control','placeholder'=>'MOBILE NO*','label'=>false,'maxlength'=>10,'minlength'=>10,'required','oninput'=>"this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"]);?> 
                          </label>
                        </li>
                        
                        <li class="col-sm-12">
                          <label>
                            <?= $this->Form->input('email',['class'=>'form-control','placeholder'=>'EMAIL*','label'=>false,'type'=>'email','required']);?>
                          </label>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-12">
                      <ul class="row">
                        <li class="col-sm-12">
                          <label>
                            <?= $this->Form->input('subject',['class'=>'form-control','placeholder'=>'SUBJECT*','label'=>false,'required']);?> 
                          </label>
                        </li>
                        <li class="col-sm-12">
                          <label>
                            <?= $this->Form->textarea('message',['class'=>'form-control','style'=>'border: 1px solid #eee','rows'=>'1','placeholder'=>'MESSAGE*','required']);?>
                          </label>
                        </li>
                        <li class="col-sm-12 no-margin">
                          <button type="submit" value="submit" class="btn" id="btn_submit" >SEND MESSAGE</button>
                        </li>
                      </ul>
                    </div>
                  </div>
                <?= $this->Form->end() ?>
              </div>
            </div>
          </div>           
        </div>
      </div>
    </section>
        
  </div>
  
  <!--======= Footer =========-->