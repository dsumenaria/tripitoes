
  <!-- Header End --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  
  
  <!--======= SEARCH BAR =========-->
  
  
    <!--======= WHY TRIPITOES =========-->
    

  <!--======= FEATURED PACKAGES =========-->

    <section class="section-p-20px new-arrival new-arri-w-slide">
      <div class="container"> 
        
        <!--  Tittle -->
        <div class="tittle-p animate fadeInUp" data-wow-delay="0.4s">
          <h4><span id="packagename">Featured Packages</span>
          <div style="float:right" class="mobile">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'PostTravlePackages','action'=>'packageList'],['class'=>'btn btn-list','escape'=>false]) ?>
          </div>
          </h4>
        </div>
        
        <!-- Nav tabs -->
        <div class="row">
          <div class="col-sm-10 col-md-10">
            <!-- <input id="package_page_no" value="2"> -->
            <ul class="nav nav-tabs"  data-wow-delay="0.4s">
              <li  class="active first">
                <?= $this->Html->link('Featured',['controller'=>'Pages','action'=>'package',0],['escape'=>false,'class'=>'packageFilter','val'=>0 ]) ?>
              </li>
              <?php
              $i=0;
              foreach ($packageCategories as $packageCategory) {
                if($i==0)
                {
                  $class_active='';
                  $active_id=$packageCategory->id;
                }
                else
                {
                  $class_active='';
                }
                $i++;
                ?>
                <li class="<?= $class_active ?>">
                  <?= $this->Html->link($packageCategory->name,['controller'=>'Pages','action'=>'package',$packageCategory->id],['escape'=>false,'class'=>'packageFilter','val'=>$packageCategory->id ]) ?>
                </li>
                <?php
              }
              ?>
            </ul>
          </div>
          <div class="col-sm-2 col-md-2 desktop">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'PostTravlePackages','action'=>'packageList'],['class'=>'btn btn-list','escape'=>false]) ?>
          </div>
        </div>
        
        <!-- Tab panes -->
        <div class="tab-content animate fadeInUp"> 
          <!-- Beach  -->
          <div class="tab-pane fade in active"  style="float:left; width:100%; margin-top:20px;"> 
             <!--  Tabs Packages  -->
              <div id="beach_1" class="popurlar_product client-slide div-content"> 
                <?= $this->element('package') ?>
              </div>
            </div>
        </div>
      </div>
    </section>

    <section class="section-p-20px new-arrival new-arri-w-slide">
      <div class="container"> 

        <!--  Tittle -->
        <div class="tittle-h animate fadeInUp" data-wow-delay="0.4s">
          <h4 ><span id="hotelname">
          Featured Hotels</span>
            <div style="float:right" class="mobile">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'HotelPromotions','action'=>'hotelList'],['class'=>'btn btn-list','escape'=>false]) ?>
            </div>
          </h4>
        </div>

        <!-- Nav tabs -->
        <div class="row">
          <div class="col-sm-10 col-md-10">
            <ul class="nav nav-tabs"   data-wow-delay="0.4s">
                <li class="active first">
                  <?= $this->Html->link('Featured Hotels',['controller'=>'Pages','action'=>'hotel',0],['escape'=>false,'class'=>'hotelFilter']) ?>
                </li>
               <?php
                $i=0;
                foreach ($hotelCategories as $hotelCategory) {
                  if($i==0)
                  {
                    $class_active='';
                    $active_id=$hotelCategory->id;
                  }
                  else
                  {
                    $class_active='';
                  }
                  $i++;
                  ?>
                  <li   class="<?= $class_active ?>">
                    <?= $this->Html->link($hotelCategory->name,['controller'=>'Pages','action'=>'hotel',$hotelCategory->id],['escape'=>false,'class'=>'hotelFilter']) ?>
                  </li>
                  <?php
                }
              ?>
            </ul>
          </div>
          <div class="col-sm-2 col-md-2 desktop">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'HotelPromotions','action'=>'hotelList'],['class'=>'btn btn-list','escape'=>false]) ?>
          </div>
        </div>
      <!-- Tab panes -->
        <div class="tab-content animate fadeInUp"> 
        <!-- Boutique  -->
          <div class="tab-pane fade in active" id="boutique-hotel" style="float:left; width:100%; margin-top:20px;"> 
          <!--  Tabs Hotels  -->
            <div id="boutique_hotel" class="popurlar_hotel fur-slide"> 
                <?= $this->element('hotel') ?>
            </div>
          </div>
        </div>
      </div>
    </section>


  
      <!--======= FEATURED TRANSPORT =========-->
    <section class="section-p-20px new-arrival new-arri-w-slide">
      <div class="container"> 
        
        <!--  Tittle -->
        <div class="tittle-t animate fadeInUp" data-wow-delay="0.4s">
          <h4 ><span id="transportname">
          Featured Transport</span>
          <div style="float:right" class="mobile">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportList'],['class'=>'btn btn-list','escape'=>false]) ?>
          </div>
          </h4>
        </div>
        
        <!-- Nav tabs -->
        <div class="row">
          <div class="col-sm-10 col-md-10">
            <ul class="nav nav-tabs"  data-wow-delay="0.4s">
              <li class="active first">
                <?= $this->Html->link('Featured Transport',['controller'=>'Pages','action'=>'transport',0],['escape'=>false,'class'=>'transportFilter']) ?>
              </li>
              <?php
              $i=0;
              foreach ($taxiCategories as $taxiCategory) {
                if($i==0)
                {
                  $class_active='';
                  $active_id=$taxiCategory->id;
                }
                else
                {
                  $class_active='';
                }
                $i++;
                ?>
                <li class="<?= $class_active ?>">
                  <?= $this->Html->link($taxiCategory->name,['controller'=>'Pages','action'=>'transport',$taxiCategory->id],['escape'=>false,'class'=>'transportFilter']) ?>
                </li>
                <?php
              }
              ?>
            </ul>
          </div>
          <div class="col-sm-2 col-md-2 desktop">
            <?= $this->Html->link('<i class="fa fa-list"></i> <span>List View</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportList'],['class'=>'btn btn-list','escape'=>false]) ?>
          </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content animate fadeInUp"> 
          <div class="tab-pane fade in active" id="nano" style="float:left; width:100%; margin-top:20px;"> 
             <!--  Tabs Transport  -->
            <div id="transport_id" class="popurlar_product transport-slide transport"> 
              <!--  featured Transport  -->
              <?= $this->element('transport') ?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="container">
      <div class="section-p-30px why_tripitoes">
      <h4>Benefits of Tripitoes</h4>
      <p>You pick the destination, we connect you with the best service providers!</p>
        <div class="row">
          <div class="col-xs-12 col-md-3">
           <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Buy from Destination Experts','width'=>'62px','data-src'=>$cdn_path.'tripitoes_images/experts.png']);?>
            <!--<img class="img-responsive" src="images/500+ destinations.png">-->
            <h6>Buy from Destination Experts</h6>
          </div>
          <div class="col-xs-12 col-md-3">
           <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Reliable Service','width'=>'47px','data-src'=>$cdn_path.'tripitoes_images/reliable.png']);?>
            <!--<img class="img-responsive" src="images/fast booking.png">-->
            <h6>Reliable Service</h6>
          </div>
          <div class="col-xs-12 col-md-3">
           <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Human Touch','width'=>'53px','data-src'=>$cdn_path.'tripitoes_images/human.png']);?>
            <!--<img class="img-responsive" src="images/price guarantee.png">-->
            <h6>Human Touch</h6>
          </div>
          <div class="col-xs-12 col-md-3">
           <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Get the Best Deals','width'=>'52px','data-src'=>$cdn_path.'tripitoes_images/best.png']);?>
            <!--<img class="img-responsive" src="images/price guarantee.png">-->
            <h6>Get the Best Deals</h6>
          </div>
        </div>
      </div>
    </div>
    

  <section class="shades section-p-30px">
      <div class="container">
          <h4>Explore Specific Category</h4>            
                <div class="service_category animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        <div class="col-sm-4 col-md-4">
                            <div style="text-align:center;">
                <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Tripitoes','style'=>'width:90px;','data-src'=>$cdn_path.'tripitoes_images/package-icon.png']);?>
                                <!--<img src="images/package-icon.png" class="img-responsive" alt="">-->
                                <h5>Package</h5>
                            </div>
                            <div class="service_pht">
                                <div class="media-body" style="text-align:center">
                                    <ul class="main-link">
                                    <?php
                                    foreach ($packageCategories as $packageCategory) {
                                      ?>
                                      <li>
                                        <?= $this->Html->link($packageCategory->name,['controller'=>'PostTravlePackages','action'=>'packageList?category='.$packageCategory->id],['escape'=>false]) ?>
                                      </li>
                                      <?php
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-4 col-md-4">
                            <div style="text-align:center;">
                <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Tripitoes','style'=>'width:90px;','data-src'=>$cdn_path.'tripitoes_images/hotel-icon.png']);?>
                                <!--<img src="images/hotel-icon.png" class="img-responsive" alt="">-->
                                <h5>Hotel</h5>
                            </div>
                            <div class="service_pht">
                                <div class="media-body" style="text-align:center">
                                    <ul class="main-link">
                                    <?php
                                    foreach ($hotelCategories as $hotelCategory) {
                                      ?>
                                      <li>
                                        <?= $this->Html->link($hotelCategory->name,['controller'=>'HotelPromotions','action'=>'hotelList?category='.$hotelCategory->id],['escape'=>false]) ?>
                                      </li>
                                      <?php
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-4 col-md-4">
                            <div style="text-align:center;">
                <?= $this->Html->image('hello.png',['class'=>'img-responsive lazy','alt'=>'Tripitoes','style'=>'width:90px;','data-src'=>$cdn_path.'tripitoes_images/transport-icon.png']);?>
                              <!--  <img src="images/transport-icon.png" class="img-responsive" alt="">-->
                                <h5>Transport</h5>
                            </div>
                            <div class="service_pht">
                                <div class="media-body" style="text-align:center">
                                    <ul class="main-link">
                                    <?php
                                    foreach ($taxiCategories as $taxiCategory) {
                                      ?>
                                      <li>
                                        <?= $this->Html->link($taxiCategory->name,['controller'=>'TaxiFleetPromotions','action'=>'transportList?category='.$taxiCategory->id],['escape'=>false]) ?>
                                      </li>
                                      <?php
                                    }
                                    ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                  <div class="all-category animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                <div class="col-sm-4 col-md-4">        
                    <div class="media-body" style="text-align:center">
                        <ul class="main-link" style="text-align:center">
                          <?php
                          foreach ($packageCategories as $packageCategory) {
                            ?>
                            <li>
                              <?= $this->Html->link($packageCategory->name,['controller'=>'PostTravlePackages','action'=>'packageList?category='.$packageCategory->id],['escape'=>false]) ?>
                            </li>
                            <?php
                          }
                          ?>
                        </ul>
                  </div>
                    </div>
                    
                    
                    <div class="col-sm-4 col-md-4">        
                      <div class="media-body" style="text-align:center">
                          <ul class="main-link">
                            <?php
                            foreach ($hotelCategories as $hotelCategory) {
                              ?>
                              <li>
                                <?= $this->Html->link($hotelCategory->name,['controller'=>'HotelPromotions','action'=>'hotelList?category='.$hotelCategory->id],['escape'=>false]) ?>
                              </li>
                              <?php
                            }
                            ?>
                          </ul>
                      </div>
                    </div>
                    
                    <div class="col-sm-4 col-md-4"> 
                      <div class="media-body" style="text-align:center">
                          <ul class="main-link">
                            <?php
                            foreach ($taxiCategories as $taxiCategory) {
                              ?>
                              <li>
                                <?= $this->Html->link($taxiCategory->name,['controller'=>'TaxiFleetPromotions','action'=>'transportList?category='.$taxiCategory->id],['escape'=>false]) ?>
                              </li>
                              <?php
                            }
                            ?>
                          </ul>
                      </div>
                    </div>
                 </div>
                       
      </div>
    </section>


	<!--<section class="section-p-30px blog no-padding-b">
      <div class="container"> 
        <div class="tittle-2 animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
          <h5><span>Latest News & Articles For Our Blog</span></h5>
        </div>
        
        <div class="blog-posts">
          <ul class="row">
            <?php
            foreach ($tripitoesBlogs as $tripitoesBlog) 
            {
            ?>
              <li class="col-sm-4 animate fadeInRight blogHeight" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"> 
                <?= $this->Html->image('hello.png',['class'=>'img-responsive blogImage lazy','alt'=>'Tripitoes','style'=>'height:230px;width: 100%;','data-src'=>$cdn_path.$tripitoesBlog->image_url]);?> 
                <ul class="info">
                  <li><?= date('d F Y',strtotime($tripitoesBlog->created_on)) ?></li>
                </ul> 
                <?= $this->Html->link($tripitoesBlog->title, ['controller'=>'Pages','action'=>'blogDetails',$tripitoesBlog->id], ['escape'=>false,'class'=>'tittle-post']) ?>
                 <p class="font-montserrat h_category" >  
                  <?= $this->Text->truncate($tripitoesBlog->short_description, 150, ['ellipsis' => '...','exact' => true]); ?>
              </p>        
              </li>
            <?php
            }
            ?>
          </ul>
        </div>
      </div>
    </section>-->
<div class="fixed-bar-m-home">
	<ul>
		<li>
			<a href="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageList']) ?>" class="fix-package">
				<div class="package-icon"></div>
				<span>Packages</span>
			</a>
		</li>
		<li>
			<a href="<?= $this->Url->build(['controller'=>'HotelPromotions','action'=>'hotelList']) ?>" class="fix-hotel">
				<div class="hotel-icon"></div>
				<span>Hotels</span>
			</a>
		</li>
		<li>
			<a href="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'transportList']) ?>" class="fix-transport">
				<div class="transport-icon"></div>
				<span>Transport</span>
			</a>
		</li>
	</ul>
</div>
  
  <section class="subcribe news-letter animate fadeInUp" data-wow-delay="0.4s" data-stellar-background-ratio="0.8" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
     
      <div class="overlay">
        <div class="container"> 
          <!-- Tittle -->
          <div class="col-md-6 white animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
            <h5>NEWSLETTER</h5>
            <p>Subscribe for updates & promotions</p>
          </div>
          
          <!--  Subsribe Form -->
          <div class="col-md-6 animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
            <div class="sub-mail">
              <?= $this->Form->create('sdfd'); ?>
                <input type="email" required name="email" placeholder="YOUR EMAIL ADDRESS..">
                <!--  Button -->
                <button type="submit">SUBSCRIBE</button>
                <?= $this->Form->unlockField('email') ?>
              <?= $this->Form->end();?>
            </div>
          </div>
        </div>
      </div>
    
    </section>
    
  <section class="animate fadeInUp" style="text-align:center; padding: 20px 0;">
      <h2 class="call">Call Us On: <span style="color:#5f9a02;">+91-9549220077</span></h2>
  </section>
  <?= $this->element('social_popup') ?>
  <?= $this->Html->css($cdn_path.'trip_front/css/select2.css',['block'=>'select2css']) ?>
  <?= $this->Html->script($cdn_path.'trip_front/js/select2.js',['block'=>'select2js']) ?>
   <?php
   $js="
    $(function() {
      $('.lazy').lazy();
    })
    $(document).ready(function(){
      $(document).on('click','a.packageLike,a.transportLike,a.hotelLike',function(e){
        e.preventDefault();
        var like=$(this).find('span');
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              like.html(response);
            },
            error: function(e) 
            {

            }
        });
      });
      $(document).on('click','a.packageSave,a.transportSave',function(e){
        e.preventDefault();
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              $('span.total_saved').html(response);
            },
            error: function(e) 
            {

            }
        });
      });
      $(document).on('click','a.packageFilter',function(e){
        var url = $(this).attr('href');
        var this_obj = $(this);
        e.preventDefault();
          $.ajax({
            type: 'get',
            url: url,
             beforeSend: function(msg){
              $('#beach_1').prepend($('#beforeload').html());
            },
            success: function(response) 
            { 
              if(response == '')
              {
                $('#beach_1').find('.overlay-divloader').remove();
                //$('a.packageFilter').closest('li').removeClass('active');
                //this_obj.closest('ul.nav').find('li.first').addClass('active');
              }
              else
              {
                $('#package_page_no').val('2');
                $('a.packageFilter').closest('li').removeClass('active');
                this_obj.closest('li').addClass('active');
                var packagename = this_obj.text();
                $('#packagename').html(packagename+' Packages');
                $('#beach_1').html(response);
                $('.client-slide').trigger('destroy.owl.carousel');
                $('.client-slide').owlCarousel({
                  autoplay:false,
                  autoplayHoverPause:true,
                  singleItem  : true,
                  navText: ['<i class=\"fa fa-angle-left\"></i>','<i class=\"fa fa-angle-right\"></i>'],
                  lazyLoad:true,
                  nav: false,
                  margin:30,
                  dots: false,
                  pagination: false,
                  animateOut: 'fadeOut',  
                  responsive:{
                    0:{
                            items:1
                        },
                        630:{
                            items:2
                        },
                        768:{
                            items:2
                        },
                        991:{
                            items:3
                        },
                        1200:{
                            items:4
                        }}
                });
                $('[data-toggle=tooltip]').tooltip();

                $('.popup-vedio').magnificPopup({
                  type: 'inline',
                  fixedContentPos: false,
                  fixedBgPos: true,
                  overflowY: 'auto',
                  closeBtnInside: true,
                  preloader: true,
                  midClick: true,
                  removalDelay: 300,
                  mainClass: 'my-mfp-slide-bottom'
                });
                $('.lazy').lazy();
              } 
               
            },
            error: function(e) 
            {
            }
          });
      });
      $(document).on('click','a.hotelFilter',function(e){
        var url = $(this).attr('href');
        var this_obj = $(this);
        e.preventDefault();
          $.ajax({
            type: 'get',
            url: url,
             beforeSend: function(msg){
              $('#boutique_hotel').prepend($('#beforeload').html());
            },
            success: function(response) 
            {
              if(response == '')
              {  
                $('#boutique_hotel').find('.overlay-divloader').remove();
               // $('a.hotelFilter').closest('li').removeClass('active');
                //this_obj.closest('ul.nav').find('li.first').addClass('active');
              }
              else
              {
                $('a.hotelFilter').closest('li').removeClass('active'); 
                this_obj.closest('li').addClass('active');
                var hotelname = this_obj.text();
                $('#hotelname').html(hotelname);
                $('#boutique_hotel').html(response);
                $('.fur-slide').trigger('destroy.owl.carousel');
                $('.fur-slide').owlCarousel({ 
                  autoplay:false,
                  autoplayHoverPause:true,
                  singleItem  : true,
                  navText: ['<i class=\"fa fa-angle-left\"></i>','<i class=\"fa fa-angle-right\"></i>'],
                  lazyLoad:true,
                  nav: false,
                  margin:30,
                  dots: false,
                  pagination: false,
                  responsive:{
                        0:{
                            items:1
                        },
                    682:{
                            items:2
                        },
                    767:{
                            items:1
                        },
                    991:{
                            items:2
                        },
                        1000:{
                            items:2,
                        }}
                });
                $('[data-toggle=tooltip]').tooltip();
                $('.lazy').lazy(); 
              }
               
            },
            error: function(e) 
            {
                //alert('An error occurred: ' + e.responseText.message);
            }
          });
      });
      $(document).on('click','a.transportFilter',function(e){
        var url = $(this).attr('href');
        var this_obj = $(this);
        e.preventDefault();
          $.ajax({
            type: 'get',
            url: url,
             beforeSend: function(msg){
              $('#transport_id').prepend($('#beforeload').html());
            },
            success: function(response) 
            {
              if(response == '')
              {
                $('#transport_id').find('.overlay-divloader').remove();
                //$('a.transportFilter').closest('li').removeClass('active');
                //this_obj.closest('ul.nav').find('li.first').addClass('active');
              }
              else
              {
                $('a.transportFilter').closest('li').removeClass('active');
                this_obj.closest('li').addClass('active');
                var transportname = this_obj.text();
                $('#transportname').html(transportname);
                $('#transport_id').html(response);
                $('.transport-slide').trigger('destroy.owl.carousel');
                $('.transport-slide').owlCarousel({
                  autoplay:false,
                  autoplayHoverPause:true,
                  singleItem  : true,
                  navText: ['<i class=\"fa fa-angle-left\"></i>','<i class=\"fa fa-angle-right\"></i>'],
                  lazyLoad:true,
                  nav: false,
                  margin:30,
                  dots: false,
                  pagination: false,
                  animateOut: 'fadeOut',  
                  responsive:{
                    0:{
                            items:1
                        },
                        630:{
                            items:2
                        },
                        768:{
                            items:2
                        },
                        991:{
                            items:3
                        },
                        1200:{
                            items:4
                        }}
                });
                $('[data-toggle=tooltip]').tooltip();
                $('.lazy').lazy();
              } 
               
            },
            error: function(e) 
            {
               // alert('An error occurred: ' + e.responseText.message);
            }
          });
      });
    });
   ";

//--- NAV BAR JAVASCRIPT
$js.="
   $(document).on('click','.PackageSearch',function(e){  
      e.preventDefault();
      var startPrice = $('.price-min').html().replace('Rs.', '');
      var endPrice = $('.price-max').html().replace('Rs.', ''); 
      var duration_range= $('#duration_range option:selected').val();
      var package_destination = $('input[name=package_destination]').val();

      var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageList'])."';
      url = url +'?startPrice='+startPrice+'&endPrice='+endPrice+'&duration_range='+duration_range+'&package_destination='+package_destination;
      window.location.href = url;
  });
  $(document).on('click','.HotelSearch',function(e){  
      e.preventDefault();
      var startPrice = $(this).closest('div.row').find('.price-min').html().replace('Rs.', '');
      var endPrice = $(this).closest('div.row').find('.price-max').html().replace('Rs.', ''); 
      var category= $('#category option:selected').val();
      var hotel_destination = $('input[name=hotel_destination]').val();
 
      var url='".$this->Url->build(['controller'=>'HotelPromotions','action'=>'hotelList'])."';
      url = url +'?startPrice='+startPrice+'&endPrice='+endPrice+'&category='+category+'&hotel_destination='+hotel_destination;
      window.location.href = url;
  });

  $(document).on('click','.TransportSearch',function(e){  
      e.preventDefault();
     
      var category= $('#t_category option:selected').val();
      var pickup_city = $('input[name=pickup_city]').val();
      var destination_city = $('input[name=destination_city]').val();
 
      var url='".$this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'transportList'])."';
      url = url +'?destination_city='+destination_city+'&category='+category+'&pickup_city='+pickup_city;
      window.location.href = url;
  });

  $(document).on('click','.redirect',function(){  
      var url=$(this).attr('redirectid');
      window.location.href = url;
  });

";

//- LOAD MORE
  


$js.="

  $(window).on('load', function() {
    //getPackageHeight();
    //getTransportHeight();
    //getNavSliderHeight();
    //getBlogHeight();
    //loadMore();
  });
  function getBlogHeight()
  {
    $('.blogHeight').each(function(){
      var divheight = $(this).height();
      var divheight1 = (divheight/2);
      $(this).find('img.blogImage').css('height',divheight1+'px');
    });
  }
  /*function getPackageHeight()
  {
    $('.packageheight').each(function(){
      var divheight = $(this).height();
      var divheight1 = (divheight/2)+35;
      $(this).find('img.imgheight').css('height',divheight1+'px');
      $(this).find('img.imgheight').css('width',divheight1+30+'px');
    });
  }
  function getTransportHeight()
  {
    $('.transportheight').each(function(){
      var divheight = $(this).height();
      var divheight1 = (divheight/2)+35;
      $(this).find('img.imgheight').css('width',divheight1+30+'px');
    });
  }*/
  function getNavSliderHeight()
  {
    $('.navSlider').each(function(){
      var divheight = $(this).height();
      $(this).find('img.ieeightfallbackimage').css('height',divheight+30+'px');
    });
  }
  function loadMore(){
      
      var package_page_no = $('#package_page_no').val();
      var url = $('a.packageFilter').closest('li.active').find('a').attr('href');  
       url=url+'?package_page_no='+package_page_no;
           $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              if(response == '')
              {
              }
              else
              {
                $('#beach_1').owlCarousel().trigger('add.owl.carousel',[$(''+response+'')]).trigger('refresh.owl.carousel');

                $('[data-toggle=tooltip]').tooltip();
                $('.popup-vedio').magnificPopup({
                  type: 'inline',
                  fixedContentPos: false,
                  fixedBgPos: true,
                  overflowY: 'auto',
                  closeBtnInside: true,
                  preloader: true,
                  midClick: true,
                  removalDelay: 300,
                  mainClass: 'my-mfp-slide-bottom'
                });
                $('#package_page_no').val(parseInt(package_page_no)+1);
                //loadMore();
              } 
            },
            error: function(e) 
            {
                //alert('An error occurred: ' + e.responseText.message);
            }
          });
      
    }

";
$js.='jQuery(document).ready(function($) {    
    //  Price Filter ( noUiSlider Plugin)
      $(".price-range-package ").noUiSlider({
      range: {
        min: [ 0 ],
        max: [ 100000 ]
      },
      start: [0, 100000],
          connect:true,
          serialization:{
              lower: [
          $.Link({
            target: $("#price-min-m")
          })
        ],
        upper: [
          $.Link({
            target: $("#price-max-m")
          })
        ],
        format: {
        // Set formatting
          decimals: 0,
          prefix: "Rs. "
        }
          }
    });
  });;'
;
$this->Html->scriptBlock($js,['block'=>'scriptPageBottom']);
?>