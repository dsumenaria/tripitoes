<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TripitoesUser $tripitoesUser
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Tripitoes User'), ['action' => 'edit', $tripitoesUser->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Tripitoes User'), ['action' => 'delete', $tripitoesUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesUser->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Carts'), ['controller' => 'TripitoesHotelCarts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Cart'), ['controller' => 'TripitoesHotelCarts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Likes'), ['controller' => 'TripitoesHotelLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Like'), ['controller' => 'TripitoesHotelLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Reports'), ['controller' => 'TripitoesHotelReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Report'), ['controller' => 'TripitoesHotelReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Views'), ['controller' => 'TripitoesHotelViews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel View'), ['controller' => 'TripitoesHotelViews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Package Carts'), ['controller' => 'TripitoesPackageCarts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Package Cart'), ['controller' => 'TripitoesPackageCarts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Package Likes'), ['controller' => 'TripitoesPackageLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Package Like'), ['controller' => 'TripitoesPackageLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Package Reports'), ['controller' => 'TripitoesPackageReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Package Report'), ['controller' => 'TripitoesPackageReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Package Views'), ['controller' => 'TripitoesPackageViews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Package View'), ['controller' => 'TripitoesPackageViews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Carts'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Cart'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Likes'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Like'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Reports'), ['controller' => 'TripitoesTaxiReports', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Report'), ['controller' => 'TripitoesTaxiReports', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Views'), ['controller' => 'TripitoesTaxiViews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tripitoesUsers view large-9 medium-8 columns content">
    <h3><?= h($tripitoesUser->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($tripitoesUser->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($tripitoesUser->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mobile No') ?></th>
            <td><?= h($tripitoesUser->mobile_no) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facebook Id') ?></th>
            <td><?= h($tripitoesUser->facebook_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Instagram Id') ?></th>
            <td><?= h($tripitoesUser->instagram_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Google Id') ?></th>
            <td><?= h($tripitoesUser->google_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitude') ?></th>
            <td><?= h($tripitoesUser->latitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longitude') ?></th>
            <td><?= h($tripitoesUser->longitude) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tripitoesUser->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created On') ?></th>
            <td><?= h($tripitoesUser->created_on) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Tripitoes Hotel Carts') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_hotel_carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Hotel Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_hotel_carts as $tripitoesHotelCarts): ?>
            <tr>
                <td><?= h($tripitoesHotelCarts->id) ?></td>
                <td><?= h($tripitoesHotelCarts->hotel_promotion_id) ?></td>
                <td><?= h($tripitoesHotelCarts->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesHotelCarts->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesHotelCarts', 'action' => 'view', $tripitoesHotelCarts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesHotelCarts', 'action' => 'edit', $tripitoesHotelCarts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesHotelCarts', 'action' => 'delete', $tripitoesHotelCarts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesHotelCarts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Hotel Likes') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_hotel_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Hotel Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_hotel_likes as $tripitoesHotelLikes): ?>
            <tr>
                <td><?= h($tripitoesHotelLikes->id) ?></td>
                <td><?= h($tripitoesHotelLikes->hotel_promotion_id) ?></td>
                <td><?= h($tripitoesHotelLikes->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesHotelLikes->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesHotelLikes', 'action' => 'view', $tripitoesHotelLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesHotelLikes', 'action' => 'edit', $tripitoesHotelLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesHotelLikes', 'action' => 'delete', $tripitoesHotelLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesHotelLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Hotel Reports') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_hotel_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Hotel Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_hotel_reports as $tripitoesHotelReports): ?>
            <tr>
                <td><?= h($tripitoesHotelReports->id) ?></td>
                <td><?= h($tripitoesHotelReports->hotel_promotion_id) ?></td>
                <td><?= h($tripitoesHotelReports->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesHotelReports->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesHotelReports', 'action' => 'view', $tripitoesHotelReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesHotelReports', 'action' => 'edit', $tripitoesHotelReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesHotelReports', 'action' => 'delete', $tripitoesHotelReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesHotelReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Hotel Views') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_hotel_views)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Hotel Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_hotel_views as $tripitoesHotelViews): ?>
            <tr>
                <td><?= h($tripitoesHotelViews->id) ?></td>
                <td><?= h($tripitoesHotelViews->hotel_promotion_id) ?></td>
                <td><?= h($tripitoesHotelViews->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesHotelViews->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesHotelViews', 'action' => 'view', $tripitoesHotelViews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesHotelViews', 'action' => 'edit', $tripitoesHotelViews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesHotelViews', 'action' => 'delete', $tripitoesHotelViews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesHotelViews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Package Carts') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_package_carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Post Travle Package Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_package_carts as $tripitoesPackageCarts): ?>
            <tr>
                <td><?= h($tripitoesPackageCarts->id) ?></td>
                <td><?= h($tripitoesPackageCarts->post_travle_package_id) ?></td>
                <td><?= h($tripitoesPackageCarts->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesPackageCarts->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesPackageCarts', 'action' => 'view', $tripitoesPackageCarts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesPackageCarts', 'action' => 'edit', $tripitoesPackageCarts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesPackageCarts', 'action' => 'delete', $tripitoesPackageCarts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesPackageCarts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Package Likes') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_package_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Post Travle Package Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_package_likes as $tripitoesPackageLikes): ?>
            <tr>
                <td><?= h($tripitoesPackageLikes->id) ?></td>
                <td><?= h($tripitoesPackageLikes->post_travle_package_id) ?></td>
                <td><?= h($tripitoesPackageLikes->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesPackageLikes->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesPackageLikes', 'action' => 'view', $tripitoesPackageLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesPackageLikes', 'action' => 'edit', $tripitoesPackageLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesPackageLikes', 'action' => 'delete', $tripitoesPackageLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesPackageLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Package Reports') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_package_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Post Travle Package Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_package_reports as $tripitoesPackageReports): ?>
            <tr>
                <td><?= h($tripitoesPackageReports->id) ?></td>
                <td><?= h($tripitoesPackageReports->post_travle_package_id) ?></td>
                <td><?= h($tripitoesPackageReports->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesPackageReports->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesPackageReports', 'action' => 'view', $tripitoesPackageReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesPackageReports', 'action' => 'edit', $tripitoesPackageReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesPackageReports', 'action' => 'delete', $tripitoesPackageReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesPackageReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Package Views') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_package_views)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Post Travle Package Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Created On') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_package_views as $tripitoesPackageViews): ?>
            <tr>
                <td><?= h($tripitoesPackageViews->id) ?></td>
                <td><?= h($tripitoesPackageViews->post_travle_package_id) ?></td>
                <td><?= h($tripitoesPackageViews->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesPackageViews->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesPackageViews', 'action' => 'view', $tripitoesPackageViews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesPackageViews', 'action' => 'edit', $tripitoesPackageViews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesPackageViews', 'action' => 'delete', $tripitoesPackageViews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesPackageViews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Carts') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_taxi_carts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_taxi_carts as $tripitoesTaxiCarts): ?>
            <tr>
                <td><?= h($tripitoesTaxiCarts->id) ?></td>
                <td><?= h($tripitoesTaxiCarts->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiCarts->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiCarts->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'view', $tripitoesTaxiCarts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'edit', $tripitoesTaxiCarts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'delete', $tripitoesTaxiCarts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiCarts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Likes') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_taxi_likes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_taxi_likes as $tripitoesTaxiLikes): ?>
            <tr>
                <td><?= h($tripitoesTaxiLikes->id) ?></td>
                <td><?= h($tripitoesTaxiLikes->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiLikes->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiLikes->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'view', $tripitoesTaxiLikes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'edit', $tripitoesTaxiLikes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'delete', $tripitoesTaxiLikes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiLikes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Reports') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_taxi_reports)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_taxi_reports as $tripitoesTaxiReports): ?>
            <tr>
                <td><?= h($tripitoesTaxiReports->id) ?></td>
                <td><?= h($tripitoesTaxiReports->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiReports->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiReports->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiReports', 'action' => 'view', $tripitoesTaxiReports->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiReports', 'action' => 'edit', $tripitoesTaxiReports->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiReports', 'action' => 'delete', $tripitoesTaxiReports->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiReports->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tripitoes Taxi Views') ?></h4>
        <?php if (!empty($tripitoesUser->tripitoes_taxi_views)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Taxi Fleet Promotion Id') ?></th>
                <th scope="col"><?= __('Tripitoes User Id') ?></th>
                <th scope="col"><?= __('Timestamp') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($tripitoesUser->tripitoes_taxi_views as $tripitoesTaxiViews): ?>
            <tr>
                <td><?= h($tripitoesTaxiViews->id) ?></td>
                <td><?= h($tripitoesTaxiViews->taxi_fleet_promotion_id) ?></td>
                <td><?= h($tripitoesTaxiViews->tripitoes_user_id) ?></td>
                <td><?= h($tripitoesTaxiViews->timestamp) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'view', $tripitoesTaxiViews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TripitoesTaxiViews', 'action' => 'edit', $tripitoesTaxiViews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TripitoesTaxiViews', 'action' => 'delete', $tripitoesTaxiViews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesTaxiViews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
