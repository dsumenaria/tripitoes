<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TripitoesUser[]|\Cake\Collection\CollectionInterface $tripitoesUsers
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Tripitoes User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Carts'), ['controller' => 'TripitoesHotelCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Cart'), ['controller' => 'TripitoesHotelCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Likes'), ['controller' => 'TripitoesHotelLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Like'), ['controller' => 'TripitoesHotelLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Reports'), ['controller' => 'TripitoesHotelReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Report'), ['controller' => 'TripitoesHotelReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Views'), ['controller' => 'TripitoesHotelViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel View'), ['controller' => 'TripitoesHotelViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Carts'), ['controller' => 'TripitoesPackageCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Cart'), ['controller' => 'TripitoesPackageCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Likes'), ['controller' => 'TripitoesPackageLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Like'), ['controller' => 'TripitoesPackageLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Reports'), ['controller' => 'TripitoesPackageReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Report'), ['controller' => 'TripitoesPackageReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Views'), ['controller' => 'TripitoesPackageViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package View'), ['controller' => 'TripitoesPackageViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Carts'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Cart'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Likes'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Like'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Reports'), ['controller' => 'TripitoesTaxiReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Report'), ['controller' => 'TripitoesTaxiReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Views'), ['controller' => 'TripitoesTaxiViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tripitoesUsers index large-9 medium-8 columns content">
    <h3><?= __('Tripitoes Users') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('mobile_no') ?></th>
                <th scope="col"><?= $this->Paginator->sort('facebook_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('instagram_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('google_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('latitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('longitude') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_on') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($tripitoesUsers as $tripitoesUser): ?>
            <tr>
                <td><?= $this->Number->format($tripitoesUser->id) ?></td>
                <td><?= h($tripitoesUser->name) ?></td>
                <td><?= h($tripitoesUser->email) ?></td>
                <td><?= h($tripitoesUser->mobile_no) ?></td>
                <td><?= h($tripitoesUser->facebook_id) ?></td>
                <td><?= h($tripitoesUser->instagram_id) ?></td>
                <td><?= h($tripitoesUser->google_id) ?></td>
                <td><?= h($tripitoesUser->latitude) ?></td>
                <td><?= h($tripitoesUser->longitude) ?></td>
                <td><?= h($tripitoesUser->created_on) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $tripitoesUser->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tripitoesUser->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tripitoesUser->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesUser->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
