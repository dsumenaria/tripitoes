<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TripitoesUser $tripitoesUser
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $tripitoesUser->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $tripitoesUser->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Users'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Carts'), ['controller' => 'TripitoesHotelCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Cart'), ['controller' => 'TripitoesHotelCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Likes'), ['controller' => 'TripitoesHotelLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Like'), ['controller' => 'TripitoesHotelLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Reports'), ['controller' => 'TripitoesHotelReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel Report'), ['controller' => 'TripitoesHotelReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Hotel Views'), ['controller' => 'TripitoesHotelViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Hotel View'), ['controller' => 'TripitoesHotelViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Carts'), ['controller' => 'TripitoesPackageCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Cart'), ['controller' => 'TripitoesPackageCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Likes'), ['controller' => 'TripitoesPackageLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Like'), ['controller' => 'TripitoesPackageLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Reports'), ['controller' => 'TripitoesPackageReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package Report'), ['controller' => 'TripitoesPackageReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Package Views'), ['controller' => 'TripitoesPackageViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Package View'), ['controller' => 'TripitoesPackageViews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Carts'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Cart'), ['controller' => 'TripitoesTaxiCarts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Likes'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Like'), ['controller' => 'TripitoesTaxiLikes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Reports'), ['controller' => 'TripitoesTaxiReports', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi Report'), ['controller' => 'TripitoesTaxiReports', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tripitoes Taxi Views'), ['controller' => 'TripitoesTaxiViews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tripitoes Taxi View'), ['controller' => 'TripitoesTaxiViews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="tripitoesUsers form large-9 medium-8 columns content">
    <?= $this->Form->create($tripitoesUser) ?>
    <fieldset>
        <legend><?= __('Edit Tripitoes User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('email');
            echo $this->Form->control('mobile_no');
            echo $this->Form->control('facebook_id');
            echo $this->Form->control('instagram_id');
            echo $this->Form->control('google_id');
            echo $this->Form->control('latitude');
            echo $this->Form->control('longitude');
            echo $this->Form->control('created_on');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
