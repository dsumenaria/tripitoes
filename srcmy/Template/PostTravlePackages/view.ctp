<title><?= ucwords(strtolower($postTravlePackage->title));?> Tour Package</title>
<?php
//pr($postTravlePackage);
$one_star_user=0;
$total_users=0;
$one_star_percentage=0;
$two_star_user=0;
$two_star_percentage=0;
$three_star_user=0;
$three_star_percentage=0;
$three_star_user=0;
$four_star_percentage=0;
$four_star_user=0;
$five_star_percentage=0;
$five_star_user=0;
$overallrating=0;
//pr($postTravlePackage); exit;
if(!empty($postTravlePackage->user->tripitoes_ratings)){
  foreach ($postTravlePackage->user->tripitoes_ratings as $key => $value) {
     $rating=$value->rating;
     if($rating==5){
      $five_star_user++;
     }
     if($rating==4){
      $four_star_user++;
     }
     if($rating==3){
      $three_star_user++;
     }
     if($rating==2){
      $two_star_user++;
     }
     if($rating==1){
      $one_star_user++;
     }
     $total_users++;
  }
  if($total_users>0){
    $one_star_percentage=($one_star_user/$total_users)*100;
    $two_star_percentage=($two_star_user/$total_users)*100;
    $three_star_percentage=($three_star_user/$total_users)*100;
    $four_star_percentage=($four_star_user/$total_users)*100;
    $five_star_percentage=($five_star_user/$total_users)*100;


  $overallrating=(((1*$one_star_user)+(2*$two_star_user)+(3*$three_star_user)+(4*$four_star_user)+(5*$five_star_user))/$total_users); 
  }
}
// DUMMY Graph
else{
  
  $total_users=1;
  $one_star_user=5;
  $one_star_percentage=5;

  $two_star_user=15;
  $two_star_percentage=15;

  $three_star_percentage=5;
  $three_star_user=5;

  $four_star_percentage=20;
  $four_star_user=20;

  $five_star_percentage=55;
  $five_star_user=55; 

  $overallrating=5; 
}
$url = (strtolower($postTravlePackage->title.' Tour Package'));
$urlTitle=str_replace(" ","-",$url);
$urlTitle=str_replace("/","-",$urlTitle);
$ShareMessage=urlencode($site_url.'PostTravelPackages/view/'.$postTravlePackage->id.'/'.$urlTitle);

?>
<?php  $shareLink = $this->Paginator->generateUrl(['_full'=>true,'_ssl'=>true],null,true); ?> 
<div class="content"> 
<input type="hidden" value="<?=$postTravlePackage->id;?>" class="package_id">
<input type="hidden" value="<?=$postTravlePackage->user_id;?>" class="user_id">
  <section class="animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
      <div class="container">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
          <li>
            <?= $this->Html->link('Home','https://www.tripitoes.com',['alt'=>'','_full'=>true]);?>
          </li>
          <li> <?= $this->Html->link('Tour Package',['controller'=>'PostTravlePackages','action'=>'packageList'],['alt'=>'']);?></li>
          <li class="active"><?= $postTravlePackage->title;?></li>
        </ol>
      </div>
    </section>

    <!--======= PAGES INNER =========-->
    <section class="section-p-30px pages-in item-detail-page">
      <div class="container">
        <div class="row desktop"> 
          <!--======= IMAGES SLIDER =========-->
          <div class="col-sm-4 text-center large-detail animate fadeInLeft" data-wow-delay="0.4s">
            <div class="product-slides port-folio-row items">
              <div>
                 <?= $this->Html->image('hello.png',['class'=>'lazy','alt'=>'Tripitoes','style'=>'height:350px;width:360px','data-src'=>$cdn_path.$postTravlePackage->trip_image]);?>

                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', $cdn_path.$postTravlePackage->trip_image, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
            </div>
            
            <ul class="share-with">
              <li><a href="javascript:void(0);"><i class="ion ion-ios-eye-outline"></i></a><span><?= $postTravlePackage->trip_views; ?></span></li>
              <li>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$postTravlePackage->trip_total_like.'</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$postTravlePackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$postTravlePackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>  
                <!--<a href="#"><i class="ion ion-ios-heart-outline"></i></a><span><?= $postTravlePackage->trip_likes; ?></span>-->
              </li>
              <li class="mobile"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
              <li class="mobile"><a href="#contact-info"><i class="ion-information"></i></a><span>Seller</span></li>
              <li><a href="#reportPopup" class="link popup-vedio video-btn"><i class="ion-ios-flag"></i></a><span>Report</span></li>
              
              <li class="share desktop">
                <i class="ion-android-share-alt"></i>
                <ul>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    </li>-->
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                    <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                      <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                    </li>
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage ?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                    </li>
                </ul>
              </li>
              <li class="desktop" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip"><a href="https://api.whatsapp.com/send?phone=+91<?= $postTravlePackage->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a></li>
              <li class="desktop" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip"><a href="javascript:void(0);"><i class="ion ion-ios-telephone-outline"></i></a><span>Call</span></li>

               
            </ul>
          </div>
          
          <!--======= ITEM DETAILS =========-->
          <div class="col-sm-8 animate fadeInRight" data-wow-delay="0.4s">
            <div class="row">
              <div class="col-sm-12">
                <h3>
                <span class="title"><?= $postTravlePackage->title;?> </span>
                <?php
                if($postTravlePackage->user->isVerified==1){ ?>
                <span class="verified">
                  <img src="<?= $cdn_path?>tripitoes_images/verified.png" class="img-responsive">
                </span> 
                <?php } ?>
                </h3>
                <p>Duration: <span class="blue"><?= $postTravlePackage->duration_day_night;?></p>
              </div>
              <div class="col-sm-12">
                <div class="seller_rating">
                    <div style="width: 20px;float: left;font-size: 30px;margin-right: 15px; line-height: 36px;">
                        <i class="fa fa-star"></i>
                    </div>
                    <div style="width: 50px;float: left; line-height: 16px; font-weight:600;">
                        <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span style="color:#000; font-size:10px;">/5</span>
                        <span style="color:#000; font-size:10px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span>
                    </div>
                    <div style="width: 80px;float: left;border-left: 1px solid #000;padding-left: 10px; line-height: 16px;">
                        <span>Seller Rating</span>
                    </div>
                </div>
              </div>
            </div>
            <p style="margin-bottom: 0; margin-top:10px;">Hotel Class: <span><?= $postTravlePackage->hotel_class;?> Star</span></p>
            <?php
              $package_name=[];
              foreach ($postTravlePackage->post_travle_package_rows as $post_travle_package_row) {
                  $package_name[]=$post_travle_package_row->post_travle_package_category->name;
              }
              $packages=implode(', ', $package_name);

              $city_name=[];
              foreach ($postTravlePackage->post_travle_package_cities as $post_travle_package_row_city) {
                  $city_name[]=$post_travle_package_row_city->city->name;
              }
              $city_name=array_unique($city_name);
              $cityname=implode(', ', $city_name);

              $Country=[];
              foreach ($postTravlePackage->post_travle_package_countries as $post_travle_package_row_countries) {
                  $Country[]=$post_travle_package_row_countries->country->country_name;
              }
              $Country=array_unique($Country);
              $countryname=implode(', ', $Country);

              $seller_loaction=$postTravlePackage->user->address.', '.$postTravlePackage->user->city->name.', '.$postTravlePackage->user->state->state_name.', '.$postTravlePackage->user->p_country->country_name;

              $map_loaction=$postTravlePackage->user->address.', '.$postTravlePackage->user->city->name.', '.$postTravlePackage->user->state->state_name.', '.$postTravlePackage->user->p_country->country_name;
              $map_loaction=str_replace(", ","+",$map_loaction);
              $map_loaction='http://maps.google.com/?q='.$map_loaction;

            ?>
            <p>Category: <span><?= $packages ?></span></p>
            <p style="margin-bottom: 0;">Cities: <span><?= $cityname;?></span></p>
            <p>Country: <span><?= $countryname;?></span></p>
            
            <p style="margin-bottom:0;">Valid Till: 
                <span class="red"><?= date('d-M-Y',strtotime($postTravlePackage->valid_date))?></span></p>
            
            <p class="data font-montserrat" style="margin-top:0px; margin-bottom:20px;">
                <i class="ion-ios-location-outline"></i>
                Seller Location: <span><a target="_blank" href="<?= $map_loaction; ?>"><?= $seller_loaction?></a></span>
            </p>
            <div class="row"> 
              
              <div class="col-sm-6 col-md-6">
                <p class="price font-montserrat" style="margin-top:10px;">
                Price: 
                <span class="blue">
                Rs. <?= $postTravlePackage->trip_price; ?>
                </span>
                <span style="font-size:12px;">(Per Person)</span>
                </p> 
              </div>
              <div class="col-sm-6 col-md-6">
                  <ul class="share-with">
                      <li class="mobile" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="https://api.whatsapp.com/send?phone=+91<?= $postTravlePackage->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a>
                        </li>
                      <li class="share mobile">
                          <i class="ion-android-share-alt"></i>
                        <ul>
                             <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                              <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>-->
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                              <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                              </li>
                              <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                                <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                              </li>
                              <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                                 <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage ?>" class="hello" ><i class="fa fa-whatsapp "></i></a>
                              </li>
                        </ul>
                        </li>
                      <li class="mobile" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="#."><i class="ion ion-ios-telephone-outline"></i></a>
                      </li>
                  </ul>
                  <div class="con-info desktop">
                  <a href="#contact-info" class="btn-small contact-info">Contact Info</a> 
                  </div>
                  
                  <div class="desktop">
                   <!--<a href="#" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>-->
                  </div>
                  
              </div>
              
            </div>
          </div>
        </div>
        
        <div class="row mobile">
          <!--======= ITEM DETAILS =========-->
          <div class="col-sm-8 animate fadeInRight" data-wow-delay="0.4s">
            <div class="row">
              <div class="col-sm-12">
                <h3>
                <span class="title"><?= $postTravlePackage->title;?></span>
                <?php
                if($postTravlePackage->user->isVerified==1){ ?>
                <span class="verified">
                  <img src="<?= $cdn_path?>tripitoes_images/verified.png" class="img-responsive">
                </span> 
                <?php } ?>
                </h3>
                <p>Duration: <span class="blue"><?= $postTravlePackage->duration_day_night;?></span></p>
              </div>
              <div class="col-sm-12">
                <div class="seller_rating">
                    <div style="width: 20px;float: left;font-size: 30px;margin-right: 15px; line-height: 36px;">
                        <i class="fa fa-star"></i>
                    </div>
                    <div style="width: 50px;float: left; line-height: 16px; font-weight:600;">
                        <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span style="color:#000; font-size:10px;">/5</span>
                        <span style="color:#000; font-size:10px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span>
                    </div>
                    <div style="width: 80px;float: left;border-left: 1px solid #000;padding-left: 10px; line-height: 16px;">
                        <span>Seller Rating</span>
                    </div>
                </div>
              </div>
            </div>
            <p style="margin-bottom: 0; margin-top:10px;">Hotel Class: <span><?= $postTravlePackage->hotel_class;?> Star</span></p>
            <p>Category: <span><?= $packages ?></span></p>
            <p style="margin-bottom: 0;">Cities: <span><?= $cityname ?></span></p>
            <p>Country: <span><?= $countryname ?></span></p>
            
            <p style="margin-bottom:0;">Valid Till: <span class="red"><?= date('d-M-Y',strtotime($postTravlePackage->valid_date))?></span></p>
            
            <p class="data font-montserrat" style="margin-top:0px; margin-bottom:0px;">
                <i class="ion-ios-location-outline"></i>
                Seller Location: <span><a  target="_blank" href="<?= $map_loaction; ?>">
                <?= $seller_loaction?>
              </a></span>
            </p>
            <div class="row" style="margin-bottom:20px;"> 
              
              <div class="col-sm-6 col-md-6">
                <p class="price font-montserrat" style="margin-top:10px;">
                Price: 
                <span class="blue">
                Rs. <?= $postTravlePackage->trip_price; ?> 
                </span>
                <span style="font-size:12px;">(Per Person)</span>
                </p> 
              </div>
              <div class="col-sm-6 col-md-6">
                  <ul class="share-with">
                      <li class="mobile" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="https://api.whatsapp.com/send?phone=+91<?= $postTravlePackage->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a>
                        </li>
                      <li class="share mobile">
                          <i class="ion-android-share-alt"></i>
                        <ul>
                            <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                              <a href="#"><i class="fa fa-instagram"></i></a>
                            </li>-->
                            <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                              <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                              </li>
                              <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                                <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                              </li>
                              <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                                <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage ?>" class="hello"><i class="fa fa-whatsapp "></i></a>
                              </li>
                        </ul>
                        </li>
                      <li class="mobile" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip">
                        <a href="tel:<?= $postTravlePackage->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveCall/'.$postTravlePackage->id]) ?>"><i class="ion ion-ios-telephone-outline"></i></a>
                      </li>
                  </ul>
                  <div class="con-info desktop">
                  <a href="#contact-info" class="btn-small contact-info">Contact Info</a> 
                  </div>
                  
                  <!--<div class="desktop">
                   <a href="#" class="youtube-btn"><i class="fa fa-youtube-play"></i><span>Watch Video</span></a>
                  </div>-->
              </div>
            </div>
          </div>
          
           <!--======= IMAGES SLIDER =========-->
          <div class="col-sm-4 text-center large-detail animate fadeInLeft" data-wow-delay="0.4s">
            <div class="product-slides port-folio-row items">
              <div>
                  <?= $this->Html->image('hello.png',['alt'=>'Tripitoes','class'=>'lazy','data-src'=>$cdn_path.$postTravlePackage->trip_image]);?>

                  <div class="hover-port">
                      <div class="position-center-center"> 
                       <?= $this->Html->link('<i class="fa fa-search"></i>', $cdn_path.$postTravlePackage->trip_image, ['escape'=>false,'data-lighter']) ?>
                      </div>
                  </div>
              </div>
            </div>
            
            <ul class="share-with">
              <li><a href="javascript:void(0);"><i class="ion ion-ios-eye-outline"></i></a><span><?= $postTravlePackage->trip_views?></span></li>
              <li>
                <?php
                if($auth_login=='Yes')
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$postTravlePackage->trip_total_like.'</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$postTravlePackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
                }
                else
                {
                  echo $this->Html->link('<i class="ion ion-ios-heart-outline"></i><span>'.$postTravlePackage->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
                }
                ?>  
                <!--<a href="#"><i class="ion ion-ios-heart-outline"></i></a><span><?= $postTravlePackage->trip_likes?></span>-->

              </li>
              <!--<li class="mobile"><a href="#"><i class="fa fa-youtube-play"></i></a><span>Watch</span></li>-->
              <li class="mobile"><a href="#contact-info"><i class="ion-information"></i></a><span>Seller</span></li>
              <li><a href="#reportPopup" class="link popup-vedio video-btn"><i class="ion-ios-flag"></i></a><span>Report</span></li>
              
              <li class="share desktop">
                <i class="ion-android-share-alt"></i>
                <ul>
                    <!--<li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                      <a href="#"><i class="fa fa-instagram"></i></a>
                    </li>-->
                    <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                      <a href="javascript:void(0);" class="shareBtn"><i class="fa fa-facebook"></i></a>
                    </li>
                      <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                          <a href="https://plus.google.com/share?url=<?= $shareLink; ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" ><i class="fa fa-google-plus"></i></a>
                        </li>
                      <li class="ShareCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveShare/'.$postTravlePackage->id]) ?>">
                      <a href="https://api.whatsapp.com/send?phone=&text=<?= $ShareMessage;?>" class="hello" ><i class="fa fa-whatsapp "></i></a>
                      </li>
                </ul>
                
              </li>
              <li class="desktop" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip"><a href="https://api.whatsapp.com/send?phone=+91<?= $postTravlePackage->user->mobile_number; ?>&text="><i class="fa fa-whatsapp"></i></a></li> 
              <li class="desktop" data-original-title="<?= $postTravlePackage->user->mobile_number; ?>" data-toggle="tooltip"><a href="javascript:void(0);"><i class="ion ion-ios-telephone-outline"></i></a><span>Call</span></li>
            </ul>
          </div>
          
        </div>
        </div>
        
        <div class="container">
          <div class="tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; margin-top: 30px; float:left; width:100%"> 
            <div style="border:1px solid #ebebeb; padding:20px; border-radius: 10px;">
                <h4>Included in Package</h4>
                  <p style="font-size: 12px; font-weight: 100;">
                    <?= $this->Text->autoParagraph($postTravlePackage->package_detail); ?>
                  </p>
           </div>
         </div>
       </div>

       <div class="container">
        <div class="tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; margin-top: 30px; float:left;  width:100%"> 
          <div style="border:1px solid #ebebeb; padding:20px; border-radius: 10px; ">
              <h4>Excluded from Package</h4>
                <p style="font-size: 12px; font-weight: 100;">
                  <?= $this->Text->autoParagraph($postTravlePackage->excluded_detail); ?>
                </p>
          </div>
        </div>
       </div>
       <?php
      if(sizeof($postTravlePackage->user->tripitoes_ratings)>0){  ?>
       <div class="container">
       <!--Review Graph-->
       <div class="section-p-30px tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; margin-top: 30px;">
         <h4>Reviews</h4>
         <div class="row">
            <div class="col-lg-6">
              <div style="float:left; width:100%;">
                  <div class="stars"> 
                      <span><?= $overallrating; ?></span> 
                        <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <span><?= $total_users; ?> Reviews</span>
                  </div>
                <div class="graph">
                  <div class="rate-5">
                      <div class="side">
                          <div>5 star</div>
                      </div>
                      <div class="r-5" style="width:<?= @$five_star_percentage/2?>% !important;">
                          <div class="bar-5"></div>
                      </div>
                      <div class="side right">
                          <div><?= $five_star_percentage;?><span class="size">%</span> (<?= $five_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-4">
                      <div class="side">
                          <div>4 star</div>
                      </div>
                      <div class="r-4"  style="width:<?= @$four_star_percentage/2?>% !important;">
                              <div class="bar-4"></div>
                      </div>
                      <div class="side right">
                          <div><?= $four_star_percentage;?><span class="size">%</span> (<?= $four_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-3">
                      <div class="side">
                          <div>3 star</div>
                      </div>
                      <div class="r-3"  style="width:<?= @$three_star_percentage/2?>% !important;">
                              <div class="bar-3"></div>
                      </div>
                      <div class="side right">
                          <div><?= $three_star_percentage;?><span class="size">%</span> (<?= $three_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-2">
                      <div class="side">
                          <div>2 star</div>
                      </div>
                      <div class="r-2"  style="width:<?= @$two_star_percentage/2?>% !important;">
                              <div class="bar-2"></div>
                      </div>
                      <div class="side right">
                          <div><?= $two_star_percentage;?><span class="size">%</span> (<?= $two_star_user;?>)</div>
                      </div>
                  </div>
                  <div class="rate-1">
                      <div class="side">
                          <div>1 star</div>
                      </div>
                      <div class="r-1"  style="width:<?= @$one_star_percentage/2?>% !important;">
                              <div class="bar-1"></div>
                      </div>
                      <div class="side right" >
                          <div><?= $one_star_percentage;?><span class="size">%</span> (<?= $one_star_user;?>)</div>
                      </div>
                  </div>
               </div>
               </div>
               </div>
               
               <div class="col-lg-6">
                <!-- Slider Section -->
                    <div class="testi-slides testimonial animate fadeInUp"  data-wow-delay="0.4s">
                    <?php 
                    foreach ($postTravlePackage->user->tripitoes_ratings as $key => $value) {
                      $rating=$value->rating;  
                       ?>
                      <!-- Slider 1 -->
                      <div class="testi">
                        <div class="avatar"> <img src="<?= $value->tripitoes_user->social_profiles[0]->photo_url?>" alt=""> </div>
                        <div class="comment">
                            <p>“<?= $value->comments?>”</p>
                            <hr>
                            <h6 ><?= $value->tripitoes_user->social_profiles[0]->display_name?></h6>
                        </div>
                      </div>
                      <?php } ?>

                     
                       
                    </div> 
               </div>
               
            </div>
          </div>
       </div>
       <?php } ?>
       
       <div class="container" id="contact-info"> 
        <div class="section-p-30px tab-content animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
        
          <div class="row">
              
                <div class="col-lg-5 col-md-5 col-xs-12 item-decribe" >
                  <div class="seller-info">
                      <h4>Seller Contact Info</h4>
                      <p>Name: <span><?= $postTravlePackage->user->company_name; ?></span></p>
                        <p>Contact Person: <span><?= $postTravlePackage->user->first_name.' '.$postTravlePackage->user->last_name; ?></span></p>
                        <p>Mobile: <span><?php if($postTravlePackage->user->mobile_number){ echo $postTravlePackage->user->mobile_number;} else {echo"N/A";} ?></span></p>
                        <p>Email ID: <span><?php if($postTravlePackage->user->email){ echo $postTravlePackage->user->email;} else {echo"N/A";} ?></span></p>
                        <p>Website: <span><?php if($postTravlePackage->user->web_url){ echo $postTravlePackage->user->web_url;} else {echo"N/A";} ?></span></p>
                        <div class="add" style="height: 80px;">Address:&nbsp;</div>
                        <div class="add-data">
                             <?= $postTravlePackage->user->address; ?>, <?= $postTravlePackage->user->city->name?>, <?=$postTravlePackage->user->state->state_name?>, <?=$postTravlePackage->user->p_country->country_name?>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-7 col-md-7 col-xs-12 item-decribe">
                  <div style="border:1px solid #ebebeb; padding:20px; padding-bottom: 40px; border-radius: 10px;">
                    <h4>Leave a Rating</h4>
                  <form id="review-form">
                        <ul class="row">
                          <li class="col-sm-6" style="margin-bottom: 20px;"> 
                        <div class="reviewRatings"> 
                              <label>Seller Rating</label> 
                                <input style="display:none;" type="radio" checked value="0" name="review"/>
                                <input class="RRs RRs-5" id="star-5-21" type="radio" value="5" name="review"/>
                                <span class="RRs RRs-5" for="star-5-21"></span>
                                <input class="RRs RRs-4" id="star-4-21" type="radio" value="4" name="review"/>
                                <span class="RRs RRs-4" for="star-4-21"></span>
                                <input class="RRs RRs-3" id="star-3-21" type="radio" value="3" name="review"/>
                                <span class="RRs RRs-3" for="star-3-21"></span>
                                <input class="RRs RRs-2" id="star-2-21" type="radio" value="2" name="review"/>
                                <span class="RRs RRs-2" for="star-2-21"></span>
                                <input class="RRs RRs-1" id="star-1-21" type="radio" value="1" name="review"/>
                                <span class="RRs RRs-1" for="star-1-21"></span>
                             </div>
                        </li>
                        <li class="col-sm-12" style="margin-bottom: 20px;">
                          <label> Comment
                            <textarea rows="3" id="reviewConmments" cols="50"></textarea>
                          </label>
                          <div class="ErrorMessage" style="color:red"></div>
                        </li>
                      <li align="center">
                      
                      <div class="col-sm-12" align="center">
                        <ul>
                          <?php 
                        if($auth_login=='Yes')
                        {
                            echo "<li class='SubmitRating' style='cursor:pointer'><a href='javascript:void(0);' class='social-login SubmitRating  reportButtonS'>Submit</a></li>";
                        }
                        else{
                          $redirect= $this->Url->build(['controller'  => $socialController,'action'=> $socialAction,'plugin' => false,'_full'=>true,'_ssl'=>true]);
                         
                          echo $this->Form->postLink(
                              '<li><span> LOG IN WITH </span> <i class="fa fa-facebook"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Facebook']],['escape'=>false,'class'=>'social-login ratingButton','block'=>true]
                          ); ?>
		  
                          <?php echo $this->Form->postLink(
                              '<li><span> LOG IN WITH </span> <i class="fa fa-google-plus"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'Google']],['escape'=>false,'class'=>'social-login ratingButton','block'=>true]
                          ); ?> 
                          <?php echo $this->Form->postLink(
                              '<li><span> LOG IN WITH </span> <i class="fa fa-linkedin"></i></li>',
                              ['controller' => 'TripitoesUsers', 'action' => 'login', '?' => ['provider' => 'LinkedIn']],['escape'=>false,'class'=>'social-login ratingButton','style'=>'margin-right: 0;','block'=>true]
                          );  
                        }
                        ?> 
                          
                           
                       </div>
                       </ul>
                     </li>
                       </ul>    
                  </form>
            <?= $this->fetch('postLink');?>
                    </div>
                </div>
                
            </div>
            </div>
        </div>
       
      </div>
    </section>
    
    
     <section class="section-p-30px new-arrival new-arri-w-slide">
        <div class="container"> 
          
          <!--  Tittle -->
          <div class="tittle-2 animate fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
          <h5><span>Featured Packages</span></h5>
        </div>          
          <!--  New Arrival Tabs Products  -->
          <div class="popurlar_product client-slide animate fadeInUp" data-wow-delay="0.4s"> 
            <?= $this->element('package') ?>
          </div>
        </div>
    </section>

<?= $this->element('social_popup') ?>
<?= $this->element('report_popup') ?>

<!--   <a onClick="onLinkedInLoad();" href="javascript: void(0)">
      Share our IN page!
  </a> -->

</div>

<!--  Facbook share page -->



<!-- Linked In Share Page -->

<!--<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
 <script type="IN/Share" data-url="<?php echo $shareLink; ?>"></script> -->

<!--  <script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key: '81hcd9jf57sord'
  lang: en_US
</script> -->
<!-- 
<script type="text/javascript">
    
  // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {  alert();
      IN.Event.on(IN, "auth", shareContent);
    }

  // Handle the successful return from the API call
  function onSuccess(data) {
    console.log(data);
  }

  // Handle an error response from the API call
  function onError(error) {
    console.log(error);
  }

  // Use the API call wrapper to share content on LinkedIn
  function shareContent() {
        
    // Build the JSON payload containing the content to be shared
    var payload = { 
      "comment": "Check out developer.linkedin.com! http://linkd.in/1FC2PyG", 
      "visibility": { 
        "code": "anyone"
      } 
    };
    IN.User.authorize(callbackFunction, callbackScope);
    IN.API.Raw("/people/~/shares?format=json")
      .method("POST")
      .body(JSON.stringify(payload))
      .result(onSuccess)
      .error(onError);

  }

</script>  -->
<?= $this->Html->css('/trip_front/css/select2.css',['block'=>'select2css']) ?>
<?= $this->Html->script('/trip_front/js/select2.js',['block'=>'select2js']) ?>
<?php

$js=" 
   $(window).on('load', function() { 
    $('.product-slides').closest('div').find('.owl-nav').hide();
    $('.owl-dots').css('visibility','hidden'); 

    $(document).on('click','.RRs', function(){
      var valu=$(this).attr('for');
      $(this).closest('div.reviewRatings').find('#'+valu).prop('checked','checked')
    });  
  }); 
";
$js.="
      $(document).on('click','.shareBtn',function(e){
        sharefb();
      });
      $(document).on('click','.hello',function(e){
        var url= $(this).attr('href');
         window.location.href = url;
      });
      $(document).on('click','a.packageLike',function(e){
        e.preventDefault();
        var like=$(this).find('span');
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              like.html(response);
            },
            error: function(e) 
            {
            }
        });
      });
      $(document).on('click','a.packageSave',function(e){
        e.preventDefault();
        var url = $(this).attr('href')
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              $('span.total_saved').html(response);
            },
            error: function(e) 
            {

            }
        });
      });
      $(document).on('click','.packageReport',function(e){  
        e.preventDefault();
        var packageid= $('.package_id').val();
        var report = $('input[name=report]:checked').val();
        var url='".$this->Url->build(['controller'=>'PostTravlePackages','action'=>'packageReport'])."';
        url = url +'?packageid='+packageid+'&report='+report;
        $.ajax({
            type: 'get',
            url: url,
            success: function(response) 
            {
              location.reload();
            },
            error: function(e) 
            {
              alert('Something went wrong. Please try again.');
            }
        });
      });

      $(document).on('click','.SubmitRating',function(e){
        e.preventDefault();
        var userid= $('.user_id').val();
        var reviewConmments= $('#reviewConmments').val();
        var type= 'Package';
        var review = $('input[name=review]:checked').val();
        if(reviewConmments !=0){
          if(review != 0)
          {
            $('.ErrorMessage').html('');
            var url='".$this->Url->build(['controller'=>'TripitoesUsers','action'=>'submitRating'])."';
            url = url +'?userid='+userid+'&review='+review+'&reviewConmments='+reviewConmments+'&type='+type;
             $.ajax({
                type: 'get',
                url: url,
                success: function(response) 
                {
                  location.reload();
                },
                error: function(e) 
                {
                 //alert('Something went wrong. Please try again.');
                }
            }); 
          }
          else {
            $('.ErrorMessage').html('Please Select Seller Rating.');
          }
        }
        else {
          $('.ErrorMessage').html('Please enter some text to describe your experience.');
        }
      });
  $(document).on('click','.redirect',function(){  
      var url=$(this).attr('redirectid');
      window.location.href = url;
  });
   ";
echo $this->Html->scriptBlock($js, array('block' => 'scriptBottom'));  ?>
<script src='https://connect.facebook.net/en_US/all.js'></script>
<script>
window.fbAsyncInit = function() {
    FB.init({
      appId            : '1820634581382722',
      autoLogAppEvents : true,
      xfbml            : true,
      cookie           : true
    });
  };
function sharefb(){
  FB.ui({
    method: 'share',
    display: 'popup',
    mobile_iframe: true,
    href: '<?php echo $shareLink; ?>',
  }, function(response){});
}
</script>