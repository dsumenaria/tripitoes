<?php
$controller = strtolower($this->request->getParam('controller'));
$action = strtolower($this->request->getParam('action'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="canonical" href="<?php echo $full_url; ?>" />
<?php if($controller=='pages' && $action=='index'){ ?>
	<title>Book Tour Packages Online | Holiday Packages | Best Hotel Deals Online | Taxi Services | Tripitoes</title>
	<meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
   <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Taxi Services in India, Tripitoes">
<?php }

else if($controller=='pages' && $action=='termconditions'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Cab Booking in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php }

else if($controller=='pages' && $action=='aboutus'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Taxi Services | Cab Booking | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } 

else if($controller=='pages' && $action=='privacypolicy'){ ?>
  <title>Book Tour Packages Online | International Holiday Packages | Taxi Services in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Taxi Services in India, Tripitoes">
<?php } 

else if($controller=='pages' && $action=='contactus'){ ?>
  <title>Book Tour Packages Online | Holiday Packages | Car Rental in India | Tripitoes</title>
  <meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } ?>


<?php if($controller=='posttravlepackages'){ ?>
	<title>Book Tour Packages Online | Holiday Packages | Taxi Services | Cab Booking | Tripitoes</title>
	<meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } ?>
<?php if($controller=='hotelpromotions'){ ?>
	<title>Hotel Booking Deals | Book Hotel at affordable price | Best Hotel Deals | Tripitoes</title>
	<meta name="description" content="Find best deals at Tripitoes for Holiday Packages to explore fantastic holiday destinations of your choice, Hotels, Bus and Taxi/Cab Booking for India and International travel. Get Best Tour packages and special deals on Hotel Bookings and taxi/cab Booking at affordable price." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } ?>
<?php if($controller=='taxifleetpromotions'){ ?>
	<title>Taxi Services |  Car Rental Services | Book Taxi India | Cab Booking | Transportation | Tripitoes</title>
	<meta name="description" content="Book Taxi from the best car rental services across India. Wide range of vehicle rentals - Luxury Cars, Sedans, Tempo Travellers, Buses, Chartered Planes and Boats. Book Taxi now and get amazing discount." />
  <meta name="keywords" content="Book Tour Packages Online, Holiday Packages, Tour Packages India, International Tour Packages, taxi Booking, Cab Booking, Taxi Servies, Hotel Booking Deals, Book Hotel at affordable price, International Holiday Packages, Car Rentals, Tripitoes">
<?php } ?>

    
<!-- FONTS -->
<?php echo $this->Html->css(
	[
		$cdn_path.'trip_front/css/bootstrap.min.css',
		/*$cdn_path.'trip_front/css/main-style.css',*/

    '/trip_front/main-style.css',
		$cdn_path.'trip_front/css/responsives.css',
		$cdn_path.'trip_front/css/font-awesom.min.css',
	], ['rel'=>"stylesheet", 'type'=>"text/css", 'media'=>'screen', 'async'=>'async']);
	echo $this->Html->css($cdn_path.'trip_front/rs-plugin/css/settings.css',['media'=>'screen']);		
	?>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&subset=cyrillic-ext" type="text/css" media="screen" async="async" media="not all" onload="if (media != 'all')media='all'"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Ultra" type="text/css" media="screen" async="async" media="not all" onload="if (media != 'all')media='all'"/>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playball" type="text/css" media="screen" async="async" media="not all" onload="if (media != 'all')media='all'"/>
			
<!-- JavaScripts -->
  <?= $this->fetch('autocompletecss'); ?> 
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon32.png" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon16.png" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon.ion" type="image/x-icon"/>
<link rel="SHORTCUT ICON" href="<?= $cdn_path;?>tripitoes_images/favicon76.png" type="image/x-icon"/>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131654937-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131654937-1');
</script>


</head>
<body>
<!-- LOADER ===========================================-->
<?= $this->element('div_loader'); ?>
<div id="loader">
  <div class="loader">
    <div class="position-center-center">
	<?= $this->Html->image($cdn_path.'tripitoes_images/logo-beta.svg',['alt'=>'Tripitoes','height'=>'40']);?>
	 <p class="text-center">Please Wait...</p>
      <div class="loading">
      	<div class="ball"></div>
        <div class="ball"></div>
        <div class="ball"></div>
      </div>
    </div>
  </div>
</div>

<!-- Page Wrap -->
<div id="wrap">
<!-- Header starts -->
	<?= $this->element('trip_header'); ?>
<!-- Header End --> 

<!--======= HOME MAIN SLIDER =========-->
	<?= $this->element('trip_slider'); ?>
<!--======= SEARCH BAR =========-->
	<?= $this->element('trip_navbar'); ?>
  <?= $this->element('sell_popup'); ?>
<!--======= WHY TRIPITOES =========-->
<!--======= FEATURED PACKAGES =========-->
  <?= $this->fetch('content'); ?> 
<!--======= Footer =========-->
<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'fdcb6d2a-dc51-4bcb-a1c6-7ceaa071de3b', f: true }); done = true; } }; })();</script>

  <?= $this->element('trip_footer'); ?>
<a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP End -->
</div>

<!-- Wrap End --> 
<?php  
	 $this->Html->script([
							$cdn_path.'trip_front/js/jquery-1.11.3.js',
							$cdn_path.'trip_front/js/wow.min.js',
							$cdn_path.'trip_front/js/bootstrap.min.js',
							$cdn_path.'trip_front/js/own-menu.js',
							$cdn_path.'trip_front/js/owl.carousel.min.js',
							$cdn_path.'trip_front/js/jquery.magnific-popup.min.js',
							$cdn_path.'trip_front/js/jquery.flexslider-min.js',
							$cdn_path.'trip_front/js/jquery.isotope.min.js',
							$cdn_path.'trip_front/js/jquery.nouislider.min.js',
							$cdn_path.'trip_front/js/bootstrap-select.min.js',
							$cdn_path.'trip_front/js/jquery.scrolling-tabs.js',
							$cdn_path.'trip_front/rs-plugin/js/jquery.themepunch.tools.min.js',
							$cdn_path.'trip_front/rs-plugin/js/jquery.themepunch.revolution.min.js',
							$cdn_path.'trip_front/js/main.js',

						]); 
						
						?>
						
						<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery-1.11.3.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/wow.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/bootstrap.min.js"></script>
						<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/own-menu.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/owl.carousel.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.magnific-popup.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.flexslider-min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.isotope.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.nouislider.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/bootstrap-select.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/jquery.scrolling-tabs.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
						<script defer src="https://d2fv6tmynngobp.cloudfront.net/trip_front/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
						<script src="https://d2fv6tmynngobp.cloudfront.net/trip_front/js/main.js"></script>
						
						<script type="text/javascript" defer >
						;(function() {
						  'use strict';
						  $(activate);
						  function activate() {
							$('.nav-tabs')
							  .scrollingTabs()
							  .on('ready.scrtabs', function() {
								$('.tab-content').show();
							  });
						  }
						}());
						</script>
						
						<?php  $this->Html->script($cdn_path.'trip_front/js/modernizr.js'); ?>
						<?= $this->Html->script($cdn_path.'trip_front/jquery.lazy.min.js'); ?>
						
						
						
						
	<?php echo 
	$this->Html->css([$cdn_path.'trip_front/css/bootstrap-select.min.css']);?>					
<?= $this->fetch('select2css') ?>
<?= $this->fetch('select2js') ?>

<script type="text/javascript">
if (window.location.hash == '#_=_')window.location.hash = '';
</script>
<script>
$(function() {
  $('.lazy').lazy();
});
$(".package_destination").select2({
    minimumInputLength: 2,
    placeholder: "Select Destination",
    allowClear: true,
    ajax: {
        url: "<?php echo $this->Url->build(['controller'=>'Pages','action'=>'autocompleteDestination']); ?>",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) { 
                    return {
                        id: item.id,
                        text: item.itemName,
                        slug: item.slug
                    }
                })
            };
        }
    }
});
$(".hotel_destination").select2({
    minimumInputLength: 2,
    placeholder: "Select Destination",
    allowClear: true,
    ajax: {
        url: "<?php echo $this->Url->build(['controller'=>'Pages','action'=>'autocompleteHotel']); ?>",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) { 
                    return {
                        id: item.id,
                        text: item.itemName,
                        slug: item.slug
                    }
                })
            };
        }
    }
});
$(".pickup_city,.destination_city").select2({
    minimumInputLength: 2,
    placeholder: "Select City",
    allowClear: true,
    ajax: {
        url: "<?php echo $this->Url->build(['controller'=>'Pages','action'=>'autocompleteCity']); ?>",
        dataType: 'json',
        type: "GET",
        quietMillis: 50,
        data: function (term) {
            return {
                term: term
            };
        },
        results: function (data) {
            return {
                results: $.map(data, function (item) { 
                    return {
                        id: item.id,
                        text: item.itemName
                    }
                })
            };
        }
    }
});
$(".package_destination").on("change", function(e) {
   $(this).attr('value',e['added'].id+','+e['added'].slug);
});
$(".hotel_destination").on("change", function(e) {
   $(this).attr('value',e['added'].id+','+e['added'].slug);
});
$(".package_destination").css('letter-spacing','3px');
$(document).on('click','.CallCount,.ShareCount',function(e){  
    //e.preventDefault();
    var url=$(this).attr('redirecturl'); 
    $.ajax({
      type: 'get',
      url: url,
      beforeSend: function(msg){
         
      },
      success: function(response) 
      {
      },
      error: function(e) 
      {
      }
    });
});
</script>
<script>

$(document).ready(function(){ 
    $('[data-toggle="tooltip"]').tooltip();   
});

$(document).ready(function($) {
  //  Price Filter ( noUiSlider Plugin)
    $("#price-range").noUiSlider({
    range: {
      'min': [ 1 ],
      'max': [ 500000 ]
    },
    start: [1, 500000],
        connect:true,
        serialization:{
            lower: [
        $.Link({
          target: $("#price-min")
        })
      ],
      upper: [
        $.Link({
          target: $("#price-max")
        })
      ],
      format: {
      // Set formatting
        decimals: 0,
        prefix: 'Rs.'
      }
        }
  })
  
  $("#price-range-hotel").noUiSlider({
    range: {
      'min': [ 1 ],
      'max': [ 100000 ]
    },
    start: [0, 100000],
        connect:true,
        serialization:{
            lower: [
        $.Link({
          target: $("#price-min-h")
        })
      ],
      upper: [
        $.Link({
          target: $("#price-max-h")
        })
      ],
      format: {
      // Set formatting
        decimals: 0,
        prefix: 'Rs.'
      }
        }
  })
  
})

</script>
</body>
<?= $this->fetch('autocompletejs'); ?>
<?= $this->fetch('scriptPageBottom'); ?>
</html>
