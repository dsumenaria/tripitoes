 <style type="text/css">
  .div-content{left: 0; top: 0; right: 0; bottom: 0;}
  .overlay-divloader {
      position: absolute;
      transform: translateY(-50%);
       -webkit-transform: translateY(-50%);
       -ms-transform: translateY(-50%);
      top: 50%;
      left: 0;
      right: 0;
      text-align: center;
      color: #555;
      background-color: rgba(255,255,255,0.8);
      height: 100%;
      z-index: 2;
  }
</style>
<div id="beforeload" style="display: none">
	<div class="overlay-divloader">
		<div class="loading" style="top: 35%;width: 150px;text-indent: 0;font-size: 12px;">
			<p class="text-left" style="margin-left: 11px;"><i>Please Wait...</i></p>
			<div class="ball"></div>
			<div class="ball"></div>
			<div class="ball"></div>
		</div>
	</div>
</div>