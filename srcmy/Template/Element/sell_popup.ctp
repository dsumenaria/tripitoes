<div id="sell-Popup" class="zoom-anim-dialog mfp-hide pop-open-style">
  <div class="pop_up">
    <div class="video"> 
        <h6>Sell your Travel Packages, Hotels, or Taxi Services here by joining TravelB2BHub.com!  </h6>
        <div align="left" style="width:100%;margin-top:0px">
        <p>It is also an all India B2B Tourism Network / Marketplace!</p>

        <?php echo $this->Html->link('Take me There <i class="fa fa-arrow-circle-right"></i>','https://travelb2bhub.com/',
             ['escape'=>false,'class'=>'apply-btn','style'=>'color:#FFF;font-size:12px !important','target'=>'_blank','rel'=>"noreferrer"]
        ); ?>

        
        <?php echo $this->Html->link('Download App <i class="fa fa-arrow-circle-right"></i>','https://play.google.com/store/apps/details?id=com.app.travel.TravelB2B',
             ['escape'=>false,'class'=>'apply-btn','style'=>'color:#FFF;font-size:12px !important','target'=>'_blank','rel'=>"noreferrer"]
        ); ?>
        </div>                     
    </div>
  </div>
</div>