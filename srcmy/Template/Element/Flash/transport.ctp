<?php
if($t_first=='t_first'){
  foreach ($taxiFleetPromotions as $taxiFleetPromotion) 
  {
    $ratingData=$TripitoesRating->rating($taxiFleetPromotion);
    $total_users=$ratingData['total_users'];
    $overallrating=$ratingData['overallrating'];
    if($overallrating==0){
        $total_users=1;
        $overallrating=5;
    }
	
	$url = (strtolower($taxiFleetPromotion->title));
	$urlTitle=str_replace(" ","-",$url);
	$urlTitle=str_replace("/","-",$urlTitle);
  ?>
     <div class="items-in transportheight"> 
      <!-- Image --> 
        <?= $this->Html->image($cdn_path.$taxiFleetPromotion->image,['alt'=>'','class'=>'redirect spaical','redirectid'=>$this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'details/'.$taxiFleetPromotion->id.'/'.$urlTitle])]);?>

      <!-- Hover Details -->
        <div class="over-item">
          <ul class="animated fadeIn">
            <li> <a href="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'details/'.$taxiFleetPromotion->id.'/'.$urlTitle]) ?>"><i class="ion-ios-eye-outline"></i><span><?= $taxiFleetPromotion->trip_views ?></span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>'.$taxiFleetPromotion->trip_total_like.'</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportLike',$taxiFleetPromotion->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportLike']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>'.$taxiFleetPromotion->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
            <li> <a href="tel:<?= $taxiFleetPromotion->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveCall/'.$taxiFleetPromotion->id]) ?>"><i class="ion-ios-telephone-outline"></i><span>Call</span></a></li>
             <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportSave',$taxiFleetPromotion->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportSave']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
          </ul>
        </div>
      <!-- Item Name -->
        <div class="details-sec"> 
           <?= $this->Html->link(ucwords(strtolower($taxiFleetPromotion->title)),['controller'=>'TaxiFleetPromotions','action'=>'details',$taxiFleetPromotion->id,$urlTitle],['class'=>'title','data-toggle'=>'tooltip','title'=>ucwords(strtolower($taxiFleetPromotion->title))]) ?>
          <div style="margin-bottom:5px;">
              <div class="stars font-montserrat">
                  <i class="fa fa-star"></i> <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span>/5 <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span> Seller Rating</span>
              </div>
              <?php
              if($taxiFleetPromotion->user->isVerified==1)
              {?>
                  <div class="verified dasktopverify">
                    <?= $this->Html->image($cdn_path.'tripitoes_images/verified.png',['class'=>'img-responsive ','alt'=>'']);?>
                  </div>
                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image($cdn_path.'tripitoes_images/verifiedmobile.png',['style'=>'height: 25px !important; width: 26px !important; ','alt'=>'']);?>
                  </span>
              <?php
              } 
              ?>
          </div>
          <?php
          $package_name=[];
          foreach ($taxiFleetPromotion->taxi_fleet_promotion_rows as $taxi_fleet_promotion_row) {
              $package_name[]=$taxi_fleet_promotion_row->taxi_fleet_car_bus->name;
          }
          $packages=implode(', ', $package_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $packages ?>">Category: 
            <span>
              <?= $packages; ?>
            </span>
          </p>
          <?php
          $state_name=[];
          foreach ($taxiFleetPromotion->taxi_fleet_promotion_states as $taxi_fleet_promotion_state) {
              $state_name[]=$taxi_fleet_promotion_state->state->state_name;
          }
          $states=implode(', ', $state_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $states ?>">Operates In: 
             <span>
             <?= 'All Cities ('.$states.')'; ?>
            </span>
          </p>
        </div>
    </div>
      <?php
  }
}
else
{
  foreach ($taxiFleetPromotions->taxi_fleet_promotion_rows as $taxiFleetPromotion) 
  {
    $ratingData=$TripitoesRating->rating($taxiFleetPromotion->taxi_fleet_promotion);
    $overallrating=$ratingData['overallrating'];
    $total_users=$ratingData['total_users'];
    if($overallrating==0){
        $total_users=1;
        $overallrating=5;
    }
	$url = (strtolower($taxiFleetPromotion->taxi_fleet_promotion->title));
	$urlTitle=str_replace(" ","-",$url);
	$urlTitle=str_replace("/","-",$urlTitle);
  ?>
     <div class="items-in transportheight"> 
      <!-- Image --> 
        <?= $this->Html->image($cdn_path.$taxiFleetPromotion->taxi_fleet_promotion->image,['alt'=>'','class'=>'redirect','redirectid'=>$this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'details/'.$taxiFleetPromotion->taxi_fleet_promotion->id])]);?>

      <!-- Hover Details -->
        <div class="over-item">
          <ul class="animated fadeIn">
            <li> <a href="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'details/'.$taxiFleetPromotion->taxi_fleet_promotion->id]) ?>"><i class="ion-ios-eye-outline"></i><span><?= $taxiFleetPromotion->taxi_fleet_promotion->trip_views ?></span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>'.$taxiFleetPromotion->taxi_fleet_promotion->trip_total_like.'</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportLike',$taxiFleetPromotion->taxi_fleet_promotion->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportLike']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>'.$taxiFleetPromotion->taxi_fleet_promotion->trip_total_like.'</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
            <li> <a href="tel:<?= $taxiFleetPromotion->taxi_fleet_promotion->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'TaxiFleetPromotions','action'=>'saveCall/'.$taxiFleetPromotion->id]) ?>"><i class="ion-ios-telephone-outline"></i><span>Call</span></a></li>
             <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>',['controller'=>'TaxiFleetPromotions','action'=>'transportSave',$taxiFleetPromotion->taxi_fleet_promotion->id,$tripitoes_user_id],['escape'=>false,'class'=>'transportSave']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
          </ul>
        </div>
      <!-- Item Name -->
        <div class="details-sec"> 
           <?= $this->Html->link(ucwords(strtolower($taxiFleetPromotion->taxi_fleet_promotion->title)),['controller'=>'TaxiFleetPromotions','action'=>'details',$taxiFleetPromotion->taxi_fleet_promotion->id],['class'=>'title','data-toggle'=>'tooltip','title'=>ucwords(strtolower($taxiFleetPromotion->taxi_fleet_promotion->title))]) ?>
          <div style="margin-bottom:5px;">
              <div class="stars font-montserrat">
                  <i class="fa fa-star"></i> <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span>/5 <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span> Seller Rating</span>
              </div> 
              <?php
              if($taxiFleetPromotion->taxi_fleet_promotion->user->isVerified==1)
              {?>
                  <div class="verified dasktopverify">
                    <?= $this->Html->image($cdn_path.'tripitoes_images/verified.png',['class'=>'img-responsive ','alt'=>'']);?>
                  </div>
                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image($cdn_path.'tripitoes_images/verifiedmobile.png',['style'=>'height: 25px !important; width: 26px !important; ','alt'=>'']);?>
                  </span>
              <?php
              } 
              ?>
               
          </div>
          <?php
          $package_name=[];
          foreach ($taxiFleetPromotion->taxi_fleet_promotion->taxi_fleet_promotion_rows as $taxi_fleet_promotion_row) {
              $package_name[]=$taxi_fleet_promotion_row->taxi_fleet_car_bus->name;
          }
          $packages=implode(', ', $package_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $packages ?>">Category: 
            <span>
              <?= $packages; ?>
            </span>
          </p>
          <?php
          $state_name=[];
          foreach ($taxiFleetPromotion->taxi_fleet_promotion->taxi_fleet_promotion_states as $taxi_fleet_promotion_state) {
              $state_name[]=$taxi_fleet_promotion_state->state->state_name;
          }
          $states=implode(', ', $state_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $states ?>">Operates In: 
             <span>
              <?= 'All Cities ('.$states.')'; ?>
            </span>
          </p>
        </div>
    </div>
      <?php
  }
}
?>