<?php
if($p_first=='p_first'){
  foreach ($postTravlePackages as $postTravlePackage) 
  {
    $ratingData=$TripitoesRating->rating($postTravlePackage);
    $total_users=$ratingData['total_users'];
    $overallrating=$ratingData['overallrating'];
    if($overallrating==0){
        $total_users=1;
        $overallrating=5;
    }
	$url = (strtolower($postTravlePackage->title.' Tour Package'));
	$urlTitle=str_replace(" ","-",$url);
	
	$urlTitle=str_replace("/","-",$urlTitle);
   ?>
     <div class="items-in"> 
      <div class="hot-tag"> 
        <p><?= $postTravlePackage->duration_day_night ?></p> 
        <p>Rs. <?= $postTravlePackage->trip_price ?></p> 
      </div>
       
        <?= $this->Html->image($cdn_path.$postTravlePackage->trip_image,['alt'=>'','class'=>'redirect spaical','redirectid'=>$this->Url->build(['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->id,$urlTitle])]);?> 

      <!-- Hover Details -->
        <div class="over-item" >
          <ul class="animated fadeIn">
            <li> <a href="<?= $this->Url->build(['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->id,$urlTitle]) ?>"><i class="ion-ios-eye-outline"></i><span>&nbsp;<?= $postTravlePackage->trip_views ?>&nbsp;</span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>&nbsp;'.$postTravlePackage->trip_total_like.'&nbsp;</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$postTravlePackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>&nbsp;'.$postTravlePackage->trip_total_like.'&nbsp;</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
            <li> <a href="tel:<?= $postTravlePackage->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveCall/'.$postTravlePackage->id]) ?>"><i class="ion-ios-telephone-outline"></i><span>Call</span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>',['controller'=>'PostTravlePackages','action'=>'packageSave',$postTravlePackage->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageSave']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
          </ul>
        </div>
      <!-- Item Name -->
        <div class="details-sec">
          <?= $this->Html->link(ucwords(strtolower($postTravlePackage->title)),['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->id,$urlTitle],['class'=>'title','data-toggle'=>'tooltip','title'=>ucwords(strtolower($postTravlePackage->title))]) ?>
          <div style="margin-bottom:5px;">
              <div class="stars font-montserrat">
                  <i class="fa fa-star"></i> <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span>/5 <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span> Seller Rating</span>
              </div>
              <?php
              if($postTravlePackage->user->isVerified==1)
              {?>
                  <div class="verified dasktopverify">
                    <?= $this->Html->image($cdn_path.'tripitoes_images/verified.png',['class'=>'img-responsive ','alt'=>'']);?>
                  </div>
                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image($cdn_path.'tripitoes_images/verifiedmobile.png',['style'=>'height: 25px !important; width: 26px !important; ','alt'=>'']);?>
                  </span>
              <?php
              } 
              ?>
          </div>
          <?php
          $package_name=[];
          foreach ($postTravlePackage->post_travle_package_rows as $post_travle_package_row) {
              $package_name[]=$post_travle_package_row->post_travle_package_category->name;
          }
          $packages=implode(', ', $package_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $packages ?>">Category: 
            <span>
              <?= $packages; ?>
            </span>
          </p>
          <?php
          $city_name=[];
          foreach ($postTravlePackage->post_travle_package_cities as $post_travle_package_city) {
              $city_name[]=$post_travle_package_city->city->name;
          }
          $cities=implode(', ', $city_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $cities ?>">Cities: 
             <span>
              <?= $cities; ?>
            </span>
          </p>
          <?php
          $country_name=[];
          foreach ($postTravlePackage->post_travle_package_countries as $post_travle_package_country) {
              $country_name[]=$post_travle_package_country->country->country_name;
          }
          $countries=implode(', ', $country_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $countries ?>">Country: 
            <span>
              <?= $countries; ?>
            </span>
          </p> 
        </div>
    </div> 
     <?php
  }
}
else{
  foreach ($postTravlePackages->post_travle_package_rows as $postTravlePackage) 
  {
    $ratingData=$TripitoesRating->rating($postTravlePackage->post_travle_package);
    $overallrating=$ratingData['overallrating'];
    $total_users=$ratingData['total_users'];
    if($overallrating==0){
        $total_users=1;
        $overallrating=5;
    }
	$url = ucwords(strtolower($postTravlePackage->post_travle_package->title.' Tour Package'));
	$urlTitle=str_replace(" ","-",$url);
	$urlTitle=str_replace("/","-",$urlTitle);
  ?>
     <div class="items-in packageheight"> 
      <div class="hot-tag"> 
        <p><?= $postTravlePackage->post_travle_package->duration_day_night ?></p> 
        <p>Rs. <?= $postTravlePackage->post_travle_package->trip_price ?></p> 
      </div>
      <!-- Image --> 
        <?= $this->Html->image($cdn_path.$postTravlePackage->post_travle_package->trip_image,['alt'=>'','class'=>'redirect','redirectid'=>$this->Url->build(['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->post_travle_package->id,$urlTitle])]);?>

      <!-- Hover Details -->
        <div class="over-item">
          <ul class="animated fadeIn">
            <li> <a href="<?= $this->Url->build(['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->post_travle_package->id,$urlTitle]) ?>"><i class="ion-ios-eye-outline"></i><span>&nbsp;<?= $postTravlePackage->post_travle_package->trip_views ?> &nbsp;</span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>&nbsp;'.$postTravlePackage->post_travle_package->trip_total_like.'&nbsp;</span>',['controller'=>'PostTravlePackages','action'=>'packageLike',$postTravlePackage->post_travle_package->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageLike']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-heart-outline"></i><span>&nbsp;'.$postTravlePackage->post_travle_package->trip_total_like.'&nbsp;</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
            <li> <a href="tel:<?= $postTravlePackage->post_travle_package->user->mobile_number; ?>" class="CallCount" redirecturl="<?= $this->Url->build(['controller'=>'PostTravlePackages','action'=>'saveCall/'.$postTravlePackage->post_travle_package->id]) ?>"><i class="ion-ios-telephone-outline"></i><span>Call</span></a></li>
            <li> 
              <?php
              if($auth_login=='Yes')
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>',['controller'=>'PostTravlePackages','action'=>'packageSave',$postTravlePackage->post_travle_package->id,$tripitoes_user_id],['escape'=>false,'class'=>'packageSave']);
              }
              else
              {
                echo $this->Html->link('<i class="ion-ios-bookmarks-outline"></i><span>Save</span>','#pop-open',['escape'=>false,'class'=>'link popup-vedio video-btn']);
              }
              ?>
            </li>
          </ul>
        </div>
      <!-- Item Name -->
        <div class="details-sec">
          <?= $this->Html->link(ucwords(strtolower($postTravlePackage->post_travle_package->title)),['controller'=>'PostTravelPackages','action'=>'view',$postTravlePackage->post_travle_package->id,$urlTitle],['class'=>'title','data-toggle'=>'tooltip','title'=>ucwords(strtolower($postTravlePackage->post_travle_package->title))]) ?>
          <div style="margin-bottom:5px;">
              <div class="stars font-montserrat">
                  <i class="fa fa-star"></i> <span style="color:#39aeff; font-size:14px;"><?= $overallrating ?></span><span>/5 <span style="color:#000; font-size:8px; letter-spacing: 1px; line-height: 10px;">(<?= $total_users ?>)</span> Seller Rating</span>
              </div>
              
              <?php
              if($postTravlePackage->post_travle_package->user->isVerified==1)
              {?>
                  <div class="verified dasktopverify">
                    <?= $this->Html->image($cdn_path.'tripitoes_images/verified.png',['class'=>'img-responsive ','alt'=>'']);?>
                  </div>
                  <span class="verified mobileverify" style="display:none">
                      <?= $this->Html->image($cdn_path.'tripitoes_images/verifiedmobile.png',['style'=>'height: 25px !important; width: 26px !important; ','alt'=>'']);?>
                  </span>
              <?php
              } 
              ?>
             
          </div>
          <?php
          $package_name=[];
          foreach ($postTravlePackage->post_travle_package->post_travle_package_rows as $post_travle_package_row) {
              $package_name[]=$post_travle_package_row->post_travle_package_category->name;
          }
          $packages=implode(', ', $package_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $packages ?>">Category: 
            <span>
              <?= $packages ?>
            </span>
          </p>
          <?php
          $city_name=[];
          foreach ($postTravlePackage->post_travle_package->post_travle_package_cities as $post_travle_package_city) {
              $city_name[]=$post_travle_package_city->city->name;
          }
          $cities=implode(', ', $city_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $cities ?>">Cities: 
             <span>
              <?= $cities ?>
            </span>
          </p>
          <?php
          $country_name=[];
          foreach ($postTravlePackage->post_travle_package->post_travle_package_countries as $post_travle_package_country) {
              $country_name[]=$post_travle_package_country->country->country_name;
          }
          $countries=implode(', ', $country_name)
          ?>
          <p class="font-montserrat data" data-toggle="tooltip" title="<?= $countries ?>">Country: 
            <span>
              <?= $countries; ?>
            </span>
          </p> 
        </div>
    </div>
    <?php
  }
}
?>