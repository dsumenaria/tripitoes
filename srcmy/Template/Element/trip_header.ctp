<?php
$controller = strtolower($this->request->getParam('controller'));
$action = strtolower($this->request->getParam('action'));

?>

 <!-- Header -->
  <header class="header-style-2 header-style-3"> 
    <!-- Top Bar -->
    <div class="top-bar">
      <div class="container"> 
        <!-- Language --> 
        <div class="language"> <a>+91-9549220077</a> | <a href="#">contactus@tripitoes.com</a></div>
        <div class="top-links">
          <ul>
            <li class="sell home">
              <?= $this->Html->link('<i class="fa fa-home"></i> HOME', 'https://www.tripitoes.com', ['escape'=>false]) ?>
            </li>
            
            <li class="sell"><a href="#sell-Popup" class="popup-vedio"><i class="fa fa-tag"></i> SELL</a></li>
            <li class="login">
              <?php
              if($auth_login!='Yes')
              { ?>
                <a href="#pop-open" class="popup-vedio"><i class="fa fa-user"></i> LOGIN</a>
              <?php }
              else{ 
                echo $this->Html->link('<i class="fa fa-power-off"></i> EXIT', ['controller'=>'TripitoesUsers','action'=>'logout'], ['escape'=>false]);
              } ?>
            </li>
          </ul>
          
          <!-- Social Icons -->
        </div>
      </div>
    </div>
    
    <!-- Logo -->
    <div class="sticky">
    <div class="container">
      <div class="logo"> 

        <?= $this->Html->link($this->Html->image('hello.png',['alt'=>'Tripitoes','height'=>'40','data-src'=>$cdn_path.'tripitoes_images/logo-beta.svg','class'=>'lazy']), 'https://www.tripitoes.com', ['escape'=>false,'_full'=>true]) ?>
         </div>
      
      <!-- Nav -->
      <nav>
        <ul id="ownmenu" class="ownmenu">
          <li <?php if($controller=='pages' && $action=='index'){ ?> class="active" <?php } ?>>
            <?php $this->Html->link('Home', ['controller'=>'Pages','action'=>'index'], ['escape'=>false]) ?>
			<a href="https://www.tripitoes.com">Home</a>
          </li>
          <li <?php if($controller=='posttravlepackages'){ ?> class="active" <?php } ?>>
            <?= $this->Html->link('Tour Packages', ['controller'=>'PostTravlePackages','action'=>'packageList'], ['escape'=>false]) ?>
          </li>
          <li <?php if($controller=='hotelpromotions'){ ?> class="active" <?php } ?>>
            <?= $this->Html->link('Hotels', ['controller'=>'HotelPromotions','action'=>'hotelList'], ['escape'=>false]) ?>
          </li>
          <li <?php if($controller=='taxifleetpromotions'){ ?> class="active" <?php } ?>>
            <?= $this->Html->link('Transport', ['controller'=>'TaxiFleetPromotions','action'=>'transportList'], ['escape'=>false]) ?>
          </li>
          <li>
              <?= $this->Html->link('Blog', 'https://www.tripitoes.com/blog/', ['escape'=>false]) ?>
          </li>
           
           
          <!--======= Save Services =========-->
          <li class="shop-cart"><a href="javascript:void(0);"><i class="ion-ios-bookmarks-outline"></i></a> <span class="numb total_saved"><?= $total_saved ?></span>
            <ul class="dropdown">
              <?php
              if($auth_login=='Yes')
              {
                ?>
                <li>
                  <?= $this->Html->link('Holiday Package', ['controller'=>'PostTravlePackages','action'=>'packageList?saved=saved'], ['escape'=>false]) ?>
                </li>
                <li>
                  <?= $this->Html->link('Hotel', ['controller'=>'HotelPromotions','action'=>'hotelList?saved=saved'], ['escape'=>false]) ?>
                </li>
                <li>
                    <?= $this->Html->link('Transport', ['controller'=>'TaxiFleetPromotions','action'=>'transportList?saved=saved'], ['escape'=>false]) ?>
                </li>
                <?php
              }
              else
              {
                ?>
                <li>
                  <a href="javascript:void(0);">Holiday Package</a>
                </li>
                <li>
                  <a href="javascript:void(0);">Hotel</a>
                </li>
                <li>
                  <a href="javascript:void(0);">Transport</a>
                </li> 
                <?php
              }
              ?>
              
            </ul>
          </li>
          <li class="search-nav"> 
			 <?= $this->Html->link($this->Html->image('hello.png',['alt'=>'Tripitoes','height'=>'40','data-src'=>$cdn_path.'tripitoes_images/logo-beta.svg','class'=>'lazy']), 'https://www.tripitoes.com', ['escape'=>false,'_full'=>true]) ?>
          </li>
        </ul>
      </nav>
    </div>
    </div>
  </header>