 <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner-fix" >
        <ul>
		<div style="position: absolute;
                z-index: 99;
                padding: 11% 0%;
                text-align: center;
                background: #00000045;
                width:100%
            ">
		<h1 style="color: #fff; font-size: 28px; font-weight: 600; letter-spacing:2px;">
		 HOLIDAY PACKAGES, HOTELS, AND TRANSPORT AT YOUR TOE TIPS!
		 </h1>
		 <p style="color: #fff; font-weight: 100; font-size: 14px; ">
		 EXPERIENCE THE MAGIC OF EXPLORING A NEW PLACE
		 </p>
		</div>
          <?php foreach($tripitoesSliders as $sliderImage){ ?>
          <li> 
          <?= $this->Html->image($cdn_path.$sliderImage->image_url,['data-bgposition'=>'center center','class'=>'','alt'=>'Tripitoes']);?>
            <!-- Layer -->
            <div class="tp-caption sfb font-montserrat tp-resizeme" 
                  data-x="right" 
                  data-y="center" data-voffset="80" 
                  data-speed="700" 
                  data-start="1500" 
                  style="color: #fff; font-weight: 100; font-size: 16px; z-index:9;">
                <a target="_blank" rel="noreferrer" href="<?= $sliderImage->link;?>"><?php echo $sliderImage->name;?></a>
            </div>
                   
                  
            <!-- Layer -->
            <div class="tp-caption font-montserrat tp-resizeme" 
                  data-x="right" 
                  data-y="center" data-voffset="100" 
                  data-speed="700" 
                  data-start="1500" 
                  
                  style="color: #fff; font-weight: 100; font-size: 12px;"><a target="_blank" rel="noreferrer" href="<?= $sliderImage->link;?>"><?php echo $sliderImage->location;?> <i class="fa fa-hand-o-right"></i></a></div>
          </li>
          
          <?php } ?>
        </ul>
      </div>
    </div>
  </section>
  
		<div class="animate fadeInRight" data-wow-delay="0.4s">
            <div class="boxes-in mobile">
    				<h1 style="font-family: 'ultra';">HOLIDAY PACKAGES, HOTELS, AND TRANSPORT AT YOUR TOE TIPS!</h1>
    				<p>EXPERIENCE THE MAGIC OF EXPLORING A NEW PLACE</p>
            <div class="testi-slides"> 
              <?php foreach($tripitoesSliders as $sliderImage){ ?>   
                <!-- SLIDER -->
                <div class="slides">
                  <div class="avatar"> 
          					<?= $this->Html->image($cdn_path.$sliderImage->image_url,['data-bgposition'=>'center top','class'=>'','alt'=>'Tripitoes']);?>
                    <span class="slidername"><a href="<?= $sliderImage->link;?>" rel="noreferrer" target="_blank"><?php echo $sliderImage->name;?> </a></span>
          					<span><a href="<?= $sliderImage->link;?>" rel="noreferrer" target="_blank"><?php echo $sliderImage->location;?> <i class="fa fa-hand-o-right"></i></a></span>
          				</div>
                </div>
              <?php } ?>	  
        </div>
      </div>
    </div>
</div>
