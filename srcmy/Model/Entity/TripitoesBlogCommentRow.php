<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TripitoesBlogCommentRow Entity
 *
 * @property int $id
 * @property int $tripitoes_blog_comment_id
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $created_on
 *
 * @property \App\Model\Entity\TripitoesBlogComment $tripitoes_blog_comment
 */
class TripitoesBlogCommentRow extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tripitoes_blog_comment_id' => true,
        'comment' => true,
        'created_on' => true,
        'tripitoes_blog_comment' => true
    ];
}
