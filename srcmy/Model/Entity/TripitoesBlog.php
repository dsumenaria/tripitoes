<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TripitoesBlog Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property \Cake\I18n\FrozenTime $created_on
 * @property string $image_url
 * @property int $is_deleted
 * @property int $trip_views
 *
 * @property \App\Model\Entity\TripitoesBlogComment[] $tripitoes_blog_comments
 */
class TripitoesBlog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'created_on' => true,
        'image_url' => true,
        'is_deleted' => true,
        'trip_views' => true,
        'tripitoes_blog_comments' => true
    ];
}
