<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PostTravlePackage Entity
 *
 * @property int $id
 * @property string $title
 * @property int $duration_night
 * @property int $duration_day
 * @property \Cake\I18n\FrozenDate $valid_date
 * @property int $currency_id
 * @property float $starting_price
 * @property int $country_id
 * @property string $package_detail
 * @property string $excluded_detail
 * @property string $image
 * @property string $document
 * @property int $price_master_id
 * @property float $price
 * @property int $like_count
 * @property \Cake\I18n\FrozenDate $visible_date
 * @property string $duration_day_night
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created_on
 * @property int $edited_by
 * @property \Cake\I18n\FrozenTime $edited_on
 * @property int $is_deleted
 * @property int $submitted_from
 * @property int $notified
 * @property int $position
 *
 * @property \App\Model\Entity\Currency $currency
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\PriceMaster $price_master
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\PostTravlePackageCart[] $post_travle_package_carts
 * @property \App\Model\Entity\PostTravlePackageCity[] $post_travle_package_cities
 * @property \App\Model\Entity\PostTravlePackageCountry[] $post_travle_package_countries
 * @property \App\Model\Entity\PostTravlePackageLike[] $post_travle_package_likes
 * @property \App\Model\Entity\PostTravlePackagePriceBeforeRenews[] $post_travle_package_price_before_renews
 * @property \App\Model\Entity\PostTravlePackageReport[] $post_travle_package_reports
 * @property \App\Model\Entity\PostTravlePackageRow[] $post_travle_package_rows
 * @property \App\Model\Entity\PostTravlePackageState[] $post_travle_package_states
 * @property \App\Model\Entity\PostTravlePackageView[] $post_travle_package_views
 * @property \App\Model\Entity\TripitoesPackageCart[] $tripitoes_package_carts
 * @property \App\Model\Entity\TripitoesPackageLike[] $tripitoes_package_likes
 * @property \App\Model\Entity\TripitoesPackageReport[] $tripitoes_package_reports
 * @property \App\Model\Entity\TripitoesPackageView[] $tripitoes_package_views
 */
class PostTravlePackage extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];
}
