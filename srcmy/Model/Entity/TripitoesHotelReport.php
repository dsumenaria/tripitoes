<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TripitoesHotelReport Entity
 *
 * @property int $id
 * @property int $hotel_promotion_id
 * @property int $tripitoes_user_id
 * @property \Cake\I18n\FrozenTime $timestamp
 *
 * @property \App\Model\Entity\HotelPromotion $hotel_promotion
 * @property \App\Model\Entity\TripitoesUser $tripitoes_user
 */
class TripitoesHotelReport extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];
}
