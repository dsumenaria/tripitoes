<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * City Entity
 *
 * @property int $id
 * @property string $name
 * @property string $state_id
 * @property int $country_id
 * @property int $price
 * @property string $category
 * @property \Cake\I18n\FrozenTime $updated_at
 * @property \Cake\I18n\FrozenTime $created_at
 * @property int $is_deleted
 *
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\EventPlannerPromotionCity[] $event_planner_promotion_cities
 * @property \App\Model\Entity\HotelPromotionCity[] $hotel_promotion_cities
 * @property \App\Model\Entity\Hotel[] $hotels
 * @property \App\Model\Entity\PostTravlePackageCity[] $post_travle_package_cities
 * @property \App\Model\Entity\RequestStop[] $request_stops
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\TaxiFleetPromotionCity[] $taxi_fleet_promotion_cities
 * @property \App\Model\Entity\Transport[] $transports
 * @property \App\Model\Entity\Userdetail[] $userdetails
 * @property \App\Model\Entity\User[] $users
 */
class City extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];
}
