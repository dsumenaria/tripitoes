<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * State Entity
 *
 * @property int $id
 * @property string $state_name
 * @property int $country_id
 * @property \Cake\I18n\FrozenTime $created_at
 * @property int $is_deleted
 *
 * @property \App\Model\Entity\Country $country
 * @property \App\Model\Entity\City[] $cities
 * @property \App\Model\Entity\EventPlannerPromotionState[] $event_planner_promotion_states
 * @property \App\Model\Entity\Hotel[] $hotels
 * @property \App\Model\Entity\PostTravlePackageState[] $post_travle_package_states
 * @property \App\Model\Entity\RequestStop[] $request_stops
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\TaxiFleetPromotionState[] $taxi_fleet_promotion_states
 * @property \App\Model\Entity\Userdetail[] $userdetails
 * @property \App\Model\Entity\User[] $users
 */
class State extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true 
    ];
}
