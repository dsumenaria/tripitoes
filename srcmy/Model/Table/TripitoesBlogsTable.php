<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesBlogs Model
 *
 * @property \App\Model\Table\TripitoesBlogCommentsTable|\Cake\ORM\Association\HasMany $TripitoesBlogComments
 *
 * @method \App\Model\Entity\TripitoesBlog get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesBlog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlog|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlog findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesBlogsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_blogs');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('TripitoesBlogComments', [
            'foreignKey' => 'tripitoes_blog_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
       $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

      /*  $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');

        $validator
            ->scalar('image_url')
            ->requirePresence('image_url', 'create')
            ->notEmpty('image_url');

        $validator
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->integer('trip_views')
            ->requirePresence('trip_views', 'create')
            ->notEmpty('trip_views');
*/
        return $validator;
    }
}
