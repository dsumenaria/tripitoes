<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TripitoesBlogCommentRows Model
 *
 * @property \App\Model\Table\TripitoesBlogCommentsTable|\Cake\ORM\Association\BelongsTo $TripitoesBlogComments
 *
 * @method \App\Model\Entity\TripitoesBlogCommentRow get($primaryKey, $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TripitoesBlogCommentRow findOrCreate($search, callable $callback = null, $options = [])
 */
class TripitoesBlogCommentRowsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('tripitoes_blog_comment_rows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('TripitoesBlogComments', [
            'foreignKey' => 'tripitoes_blog_comment_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('comment')
            ->requirePresence('comment', 'create')
            ->notEmpty('comment');
/*
        $validator
            ->dateTime('created_on')
            ->requirePresence('created_on', 'create')
            ->notEmpty('created_on');
*/
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['tripitoes_blog_comment_id'], 'TripitoesBlogComments'));

        return $rules;
    }
}
