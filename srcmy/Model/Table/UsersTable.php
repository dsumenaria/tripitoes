<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\CountriesTable|\Cake\ORM\Association\BelongsTo $Countries
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\StatesTable|\Cake\ORM\Association\BelongsTo $States
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 * @property \App\Model\Table\DevicesTable|\Cake\ORM\Association\BelongsTo $Devices
 * @property \App\Model\Table\BusinessBuddiesTable|\Cake\ORM\Association\HasMany $BusinessBuddies
 * @property \App\Model\Table\CreditsTable|\Cake\ORM\Association\HasMany $Credits
 * @property \App\Model\Table\EventPlannerPromotionCartsTable|\Cake\ORM\Association\HasMany $EventPlannerPromotionCarts
 * @property \App\Model\Table\EventPlannerPromotionLikesTable|\Cake\ORM\Association\HasMany $EventPlannerPromotionLikes
 * @property \App\Model\Table\EventPlannerPromotionReportsTable|\Cake\ORM\Association\HasMany $EventPlannerPromotionReports
 * @property \App\Model\Table\EventPlannerPromotionViewsTable|\Cake\ORM\Association\HasMany $EventPlannerPromotionViews
 * @property \App\Model\Table\EventPlannerPromotionsTable|\Cake\ORM\Association\HasMany $EventPlannerPromotions
 * @property \App\Model\Table\HotelPromotionCartsTable|\Cake\ORM\Association\HasMany $HotelPromotionCarts
 * @property \App\Model\Table\HotelPromotionLikesTable|\Cake\ORM\Association\HasMany $HotelPromotionLikes
 * @property \App\Model\Table\HotelPromotionReportsTable|\Cake\ORM\Association\HasMany $HotelPromotionReports
 * @property \App\Model\Table\HotelPromotionViewsTable|\Cake\ORM\Association\HasMany $HotelPromotionViews
 * @property \App\Model\Table\HotelPromotionsTable|\Cake\ORM\Association\HasMany $HotelPromotions
 * @property \App\Model\Table\HotelsTable|\Cake\ORM\Association\HasMany $Hotels
 * @property \App\Model\Table\PostTravlePackageCartsTable|\Cake\ORM\Association\HasMany $PostTravlePackageCarts
 * @property \App\Model\Table\PostTravlePackageLikesTable|\Cake\ORM\Association\HasMany $PostTravlePackageLikes
 * @property \App\Model\Table\PostTravlePackageReportsTable|\Cake\ORM\Association\HasMany $PostTravlePackageReports
 * @property \App\Model\Table\PostTravlePackageViewsTable|\Cake\ORM\Association\HasMany $PostTravlePackageViews
 * @property \App\Model\Table\PostTravlePackagesTable|\Cake\ORM\Association\HasMany $PostTravlePackages
 * @property \App\Model\Table\PromotionTable|\Cake\ORM\Association\HasMany $Promotion
 * @property \App\Model\Table\RequestsTable|\Cake\ORM\Association\HasMany $Requests
 * @property \App\Model\Table\ResponsesTable|\Cake\ORM\Association\HasMany $Responses
 * @property \App\Model\Table\TaxiFleetPromotionCartsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionCarts
 * @property \App\Model\Table\TaxiFleetPromotionLikesTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionLikes
 * @property \App\Model\Table\TaxiFleetPromotionReportsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionReports
 * @property \App\Model\Table\TaxiFleetPromotionViewsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotionViews
 * @property \App\Model\Table\TaxiFleetPromotionsTable|\Cake\ORM\Association\HasMany $TaxiFleetPromotions
 * @property \App\Model\Table\TempRatingsTable|\Cake\ORM\Association\HasMany $TempRatings
 * @property \App\Model\Table\TestimonialTable|\Cake\ORM\Association\HasMany $Testimonial
 * @property \App\Model\Table\TransportsTable|\Cake\ORM\Association\HasMany $Transports
 * @property \App\Model\Table\TravelCertificatesTable|\Cake\ORM\Association\HasMany $TravelCertificates
 * @property \App\Model\Table\UserChatsTable|\Cake\ORM\Association\HasMany $UserChats
 * @property \App\Model\Table\UserFeedbacksTable|\Cake\ORM\Association\HasMany $UserFeedbacks
 * @property \App\Model\Table\UserRatingsTable|\Cake\ORM\Association\HasMany $UserRatings
 * @property \App\Model\Table\UserRightsTable|\Cake\ORM\Association\HasMany $UserRights
 * @property \App\Model\Table\UserdetailsTable|\Cake\ORM\Association\HasMany $Userdetails
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'role_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Devices', [
            'foreignKey' => 'device_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('BusinessBuddies', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Credits', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EventPlannerPromotionCarts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EventPlannerPromotionLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EventPlannerPromotionReports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EventPlannerPromotionViews', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('EventPlannerPromotions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HotelPromotionCarts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HotelPromotionLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HotelPromotionReports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HotelPromotionViews', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('HotelPromotions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Hotels', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostTravlePackageCarts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostTravlePackageLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostTravlePackageReports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostTravlePackageViews', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PostTravlePackages', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Promotion', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Requests', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Responses', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TaxiFleetPromotionCarts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TaxiFleetPromotionLikes', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TaxiFleetPromotionReports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TaxiFleetPromotionViews', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TaxiFleetPromotions', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TempRatings', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Testimonial', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Transports', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TravelCertificates', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserChats', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserFeedbacks', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserRatings', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('UserRights', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Userdetails', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('TripitoesRatings', [
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('HotelState', [
            'className' => 'States',
            'foreignKey' => 'state_id',
            'propertyName' => 'hotel_state'
        ]);
        $this->belongsTo('PCountries', [
            'className' => 'Countries',
            'foreignKey' => 'country_id',
            'propertyName' => 'p_country'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 250)
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 250)
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->scalar('password')
            ->maxLength('password', 250)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('mobile_number')
            ->maxLength('mobile_number', 100)
            ->requirePresence('mobile_number', 'create')
            ->notEmpty('mobile_number');

        $validator
            ->scalar('p_contact')
            ->maxLength('p_contact', 100)
            ->requirePresence('p_contact', 'create')
            ->notEmpty('p_contact');

        $validator
            ->scalar('fax')
            ->maxLength('fax', 100)
            ->requirePresence('fax', 'create')
            ->notEmpty('fax');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 200)
            ->requirePresence('company_name', 'create')
            ->notEmpty('company_name');

        $validator
            ->scalar('comp_image1')
            ->maxLength('comp_image1', 255)
            ->requirePresence('comp_image1', 'create')
            ->notEmpty('comp_image1');

        $validator
            ->scalar('comp_image2')
            ->maxLength('comp_image2', 255)
            ->requirePresence('comp_image2', 'create')
            ->notEmpty('comp_image2');

        $validator
            ->scalar('id_card')
            ->maxLength('id_card', 255)
            ->requirePresence('id_card', 'create')
            ->notEmpty('id_card');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('image')
            ->maxLength('image', 200)
            ->requirePresence('image', 'create')
            ->notEmpty('image');

        $validator
            ->dateTime('create_at')
            ->requirePresence('create_at', 'create')
            ->notEmpty('create_at');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->scalar('adress1')
            ->allowEmpty('adress1');

        $validator
            ->scalar('locality')
            ->maxLength('locality', 200)
            ->allowEmpty('locality');

        $validator
            ->scalar('pincode')
            ->maxLength('pincode', 10)
            ->requirePresence('pincode', 'create')
            ->notEmpty('pincode');

        $validator
            ->scalar('preference')
            ->allowEmpty('preference');

        $validator
            ->integer('email_verified')
            ->requirePresence('email_verified', 'create')
            ->notEmpty('email_verified');

        $validator
            ->scalar('profile_pic')
            ->maxLength('profile_pic', 255)
            ->allowEmpty('profile_pic');

        $validator
            ->scalar('pancard_pic')
            ->maxLength('pancard_pic', 255)
            ->allowEmpty('pancard_pic');

        $validator
            ->scalar('company_img_1_pic')
            ->maxLength('company_img_1_pic', 255)
            ->allowEmpty('company_img_1_pic');

        $validator
            ->scalar('company_img_2_pic')
            ->maxLength('company_img_2_pic', 255)
            ->allowEmpty('company_img_2_pic');

        $validator
            ->scalar('id_card_pic')
            ->maxLength('id_card_pic', 255)
            ->allowEmpty('id_card_pic');

        $validator
            ->scalar('company_shop_registration_pic')
            ->maxLength('company_shop_registration_pic', 255)
            ->allowEmpty('company_shop_registration_pic');

        $validator
            ->scalar('travel_certificates')
            ->maxLength('travel_certificates', 100)
            ->allowEmpty('travel_certificates');

        $validator
            ->integer('hotel_rating')
            ->allowEmpty('hotel_rating');

        $validator
            ->scalar('hotel_name')
            ->requirePresence('hotel_name', 'create')
            ->notEmpty('hotel_name');

        $validator
            ->scalar('hotel_categories')
            ->maxLength('hotel_categories', 100)
            ->allowEmpty('hotel_categories');

        $validator
            ->scalar('web_url')
            ->maxLength('web_url', 255)
            ->requirePresence('web_url', 'create')
            ->notEmpty('web_url');

        $validator
            ->scalar('iata_pic')
            ->maxLength('iata_pic', 255)
            ->allowEmpty('iata_pic');

        $validator
            ->scalar('tafi_pic')
            ->requirePresence('tafi_pic', 'create')
            ->notEmpty('tafi_pic');

        $validator
            ->scalar('taai_pic')
            ->requirePresence('taai_pic', 'create')
            ->notEmpty('taai_pic');

        $validator
            ->scalar('iato_pic')
            ->requirePresence('iato_pic', 'create')
            ->notEmpty('iato_pic');

        $validator
            ->scalar('adyoi_pic')
            ->requirePresence('adyoi_pic', 'create')
            ->notEmpty('adyoi_pic');

        $validator
            ->scalar('iso9001_pic')
            ->requirePresence('iso9001_pic', 'create')
            ->notEmpty('iso9001_pic');

        $validator
            ->scalar('uftaa_pic')
            ->requirePresence('uftaa_pic', 'create')
            ->notEmpty('uftaa_pic');

        $validator
            ->scalar('adtoi_pic')
            ->requirePresence('adtoi_pic', 'create')
            ->notEmpty('adtoi_pic');

        $validator
            ->dateTime('updated_at')
            ->requirePresence('updated_at', 'create')
            ->notEmpty('updated_at');

        $validator
            ->dateTime('last_login')
            ->requirePresence('last_login', 'create')
            ->notEmpty('last_login');

        $validator
            ->scalar('activation')
            ->maxLength('activation', 255)
            ->requirePresence('activation', 'create')
            ->notEmpty('activation');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        $validator
            ->scalar('mobile_otp')
            ->maxLength('mobile_otp', 20)
            ->requirePresence('mobile_otp', 'create')
            ->notEmpty('mobile_otp');

        $validator
            ->integer('blocked')
            ->requirePresence('blocked', 'create')
            ->notEmpty('blocked');

        $validator
            ->integer('is_deleted')
            ->requirePresence('is_deleted', 'create')
            ->notEmpty('is_deleted');

        $validator
            ->dateTime('deleted_on')
            ->allowEmpty('deleted_on');

        $validator
            ->decimal('percentage')
            ->requirePresence('percentage', 'create')
            ->notEmpty('percentage');

        $validator
            ->boolean('isVerified')
            ->requirePresence('isVerified', 'create')
            ->notEmpty('isVerified');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['country_id'], 'Countries'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['state_id'], 'States'));
        $rules->add($rules->existsIn(['role_id'], 'Roles'));
        $rules->add($rules->existsIn(['device_id'], 'Devices'));

        return $rules;
    }
}
